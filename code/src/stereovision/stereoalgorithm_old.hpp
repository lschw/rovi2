#pragma once
#include <vector>
#include <future>
#include <exception>

#include <opencv2/core/core.hpp>

#include <rw/common/Log.hpp>


#include <Eigen/Core>
#include <Eigen/SVD>

#include "../vision/vision_algo.h"
#include "../utils/defines_matrices.hpp"

namespace rovi {

/**
 * @brief The stereoalgorithm class Applies a supplied image algorithm to a set of images and returns the result.
 */
class Stereoalgorithm_old
{
public:
    Stereoalgorithm_old() {}

    /**
     * @brief stereopsis Calculates the 3D point of and ideal disparity point.
     * @param disparity_point The disparity point to use (x, y, d).
     * @param basline Length of baseline.
     * @param focal In pixel.
     * @param three_d_point The returned 3D point (x, y, z).
     */
    void stereopsis(cv::Point3d &disparity_point, double baseline, double focal, cv::Point3d &three_d_point);

    /**
     * @brief stereopsis Calculates the 3D point of and ideal disparity point.
     * @param disparity_points The disparity point to use (x, y, d).
     * @param basline Length of baseline.
     * @param focal In pixel.
     * @param three_d_points The returned 3D point (x, y, z).
     */
    void stereopsis(std::vector< cv::Point3d > &disparity_points, double baseline, double focal, std::vector< cv::Point3d > &three_d_points);

    /**
     * @brief simpleFeaturePairing Finds the 2D points that belong together base on their x and y position in the image
     * @param left Points in the left image.
     * @param right Points in the right image.
     * @param disparity_points Resulting disparity points (x, y, d), 'x' as in left camera view and 'y' avg of the two.
     */
    void simpleFeaturePairing(std::vector< cv::Point2d > &left, std::vector< cv::Point2d > &right, std::vector< cv::Point3d > &disparity_points);

    /**
     * @brief stereopsis
     * @param disparity_points
     * @param perspective
     * @param three_d_point
     * @return
     */
    void stereopsis(const std::vector< cv::Point3d > &disparity_points, const Eigen::MatrixXd &perspective, std::vector< cv::Point3d > &three_d_point);

protected:

};

}
