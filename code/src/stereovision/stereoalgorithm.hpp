#pragma once
#include <vector>
#include <future>
#include <exception>

#include <opencv2/core/core.hpp>

#include <rw/common/Log.hpp>

#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/SVD>


#include <utility>




#include "../vision/vision_algo.h"
#include "../utils/defines_matrices.hpp"
#include "../utils/log.hpp"

namespace rovi {

/**
 * @brief The stereoalgorithm class Applies a supplied image algorithm to a set of images and returns the result.
 */
class Stereoalgorithm
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    virtual ~Stereoalgorithm() { }

   Stereoalgorithm(){}

    Stereoalgorithm(const rovi::Matrix3by4d &perspective_left, const rovi::Matrix3by4d &perspective_right)
        :_p_left(perspective_left), _p_right(perspective_right) {}

    Stereoalgorithm(const rovi::Matrix3by4d &perspective_left, const rovi::Matrix3by4d &perspective_right,
                    const Eigen::Matrix2d &covariance_left, const Eigen::Matrix2d &covariance_right)
        :_p_left(perspective_left), _p_right(perspective_right)
    {
        //set2DUncertainity(covariance_left, covariance_right);
        _cov_2D.setZero();
        _cov_2D.block<2,2>(0,0) = covariance_left;
        _cov_2D.block<2,2>(2,2) = covariance_right;
    }


    void set2DUncertainity(const Eigen::Matrix2d &covariance_left, const Eigen::Matrix2d &covariance_right);

    void setPerspectiveTransform( const rovi::Matrix3by4d &perspective_left, const rovi::Matrix3by4d &perspective_right);


    /**
     * @brief applyAlgorithm Applies the applied algorithm to the given input.
     * @param images The images to pply the algorithm to.
     * @param algorithm The image algorithm to apply.
     * @param ret_values The vector of return templates.
     */
    void applyAlgorithm(std::vector< cv::Mat > &images, rovi::vision::vision_algo &algorithm, std::vector< std::vector< cv::Point2d > > &ret_values);

    /**
     * @brief simpleFeaturePairing Finds the 2D points that belong together base on their x and y position in the image
     * @param left Points in the left image.
     * @param right Points in the right image.
     * @param paired_points Resulting paired points.
     */
    void simpleFeaturePairing(const std::vector< cv::Point2d > &left, const std::vector< cv::Point2d > &right, std::vector< std::vector< cv::Point2d > > &paired_points) const;


    /**
     * @brief stereopsis Calcuates the 3D pose of a point given the projection matrices.
     * @param two_d_points The 2D points used for reconstruction, == 2
     * @param three_d_point The resulting 3D point.
     */
    void stereopsis(const std::vector< cv::Point2d > &two_d_points, Eigen::Vector3d &three_d_point);

    /**
     * @brief uncertainityPorpagation Propagates the 2D error into 3D.
     * @param two_d_points The 2D points used for reconstruction, == 2
     * @param covariance_3D The resulting propagated error
     */
    void uncertainityPropagation(const std::vector< cv::Point2d > &two_d_points, Eigen::Matrix3d &covariance_3D);

    /**
     * @brief completeStereopsis Computes the 3D point and its uncertainty given the two 2D points.
     * @param two_d_points The 2D points used for reconstruction, == 2
     * @param three_d_point The resulting 3D point.
     * @param covariance_3D The resulting propagated error
     */
    void completeStereopsis(const std::vector< cv::Point2d > &two_d_points, Eigen::Vector3d &three_d_point, Eigen::Matrix3d &covariance_3D);

    /**
     * @brief rectifiedStereopsis Numerically somewhat stable stereo matching with 3D uncertainties.
     * @param left_and_right Pixel coordinates in left (first) and right (second) camera of 3D point to reconstruct.
     * @return 3D point (first) and 3D Covariance matrix (second)
     *
     * Usage:
     * #include <tuple> //std::tie
     * #include <utility> //std::make_pair
     * ... //construct my_stereo
     *
     * cv::Point2d uv_left, uv_right;
     *
     * Eigen::Vector3d my_3d_point; //M
     * Eigen::Matrix3d my_3d_covariance_matrix; //R
     *
     * //Measurement for Kalman filter:
     * std::tie( my_3d_point , my_3d_covariance_matrix ) = my_stereo.rectifiedStereopsis(std::make_pair(uv_left,uv_right));
     *
     * //Measurement for the lulz and not caring a f about uncertainties!
     * std::tie( my_3d_point , std::ignore ) = my_stereo.rectifiedStereopsis(std::make_pair(uv_left,uv_right));
     *
     *
     *
     * Uses most stable and failsafe SVD to calculate 3D point.
     * Uses relatively stable (calculates normal equations AT*A) method
     * to propagate 2D uncertainties.
     *
     * Assumes This Stereoalgorithm is setup with projection matrices
     * of a rectified stereo pair of cameras.
     * That means the left projection matrix is the same
     * as the right projection matrix except for the 4th column.
     * Also assumes the left projection matrix 4th column is all zeros!!!!!!!!!!!!!!!!
     *
     * Declared inline to save about 15 microseconds per call.
     * That's why it's in the .hpp file, not the .cpp file.
     */
    inline std::pair<Eigen::Vector3d, Eigen::Matrix3d > rectifiedStereopsis(const std::pair<cv::Point2d,cv::Point2d> &left_and_right);

protected:

    /**
     * @brief calcAMatrix Calculates the A matrix used in A M = b.
     * @param two_d_points
     */
    void calcAMatrix(const std::vector< cv::Point2d > &two_d_points);

    /**
     * @brief calcBMatrix Calculates the b matrix in A M = b.
     * @param two_d_points
     */
    void calcBMatrix(const std::vector< cv::Point2d > &two_d_points);


    /// General: 'Pure' functions do not calculate A and b, this is such that a single function call can be made to calc 3D pose and unc. without calc A and b twice :)

    /**
     * @brief stereopsis Calcuates the 3D pose of a point given the projection matrices.
     * @param two_d_points The 2D points used for reconstruction, >= 2
     * @param projection The 3x4 projection matrix.
     * @param three_d_point The resulting 3D point.
     */
    void pureStereopsis(Eigen::Vector3d &three_d_point);

    /**
     * @brief pureUncertaintyPropagation Propagates the 2D error to the 3D error
     * @param covariance_3D the resulting 3D covariance
     */
    /// see the master paper or Lukas' short resume for details on equations
    void pureUncertaintyPropagation(Eigen::Matrix3d &covariance_3D);


    void computeJacobian();

    void computeInverse(Eigen::Matrix3d &matrixToInvert) const;

    rovi::Matrix3by4d _p_left, _p_right;
    rovi::Matrix4by3d _a_matrix;
    Eigen::Vector4d _b_matrix;
    Eigen::Matrix4d _cov_2D;
    rovi::Matrix3by4d _jacobian;

};

}
































inline std::pair<Eigen::Vector3d, Eigen::Matrix3d> rovi::Stereoalgorithm::rectifiedStereopsis(const std::pair<cv::Point2d, cv::Point2d> &left_and_right) {
    //Quickly make the std::pair abstraction so nobody gets confused
    const cv::Point2d & uv_left = left_and_right.first;
    const cv::Point2d & uv_right = left_and_right.second;

    //A
    _a_matrix << _p_right.block<1,3>(0, 0) - uv_left.x * _p_right.block<1,3>(2, 0),
            _p_right.block<1,3>(1, 0) - uv_left.y * _p_right.block<1,3>(2, 0),
            _p_right.block<1,3>(0, 0) - uv_right.x * _p_right.block<1,3>(2, 0),
            _p_right.block<1,3>(1, 0) - uv_right.y * _p_right.block<1,3>(2, 0);

    //b
    //b_matrix top two elements are 0 if p_left has 0s in 4th column (rectified standard case)
    _b_matrix << 0,
            0,
            uv_right.x * _p_right(2, 3) - _p_right(0, 3),
            uv_right.y * _p_right(2, 3) - _p_right(1, 3);


    //The 3D-point M can now be solved for
    //with A*M=b and SVD.
    //But, as it turns out, we are going to solve some
    //stuff with the normal equations AT*A later on.
    //So multiply above equation with AT to get:
    // AT*A*M=AT*b
    //Solving this STILL gives the 3D point.
    //BUT!!! This can be solved with Cholesky decomposition.
    //That is faster.
    //BUT!!!! Forming the product AT*A is not as numerically stable
    //as solving with SVD!
    const Eigen::JacobiSVD< Matrix4by3d, Eigen::FullPivHouseholderQRPreconditioner> svd_A(_a_matrix, Eigen::ComputeFullU | Eigen::ComputeFullV);
    //const Eigen::JacobiSVD< Matrix4by3d, Eigen::ColPivHouseholderQRPreconditioner> svd_A(_a_matrix, Eigen::ComputeFullU | Eigen::ComputeFullV);
    const Eigen::Vector3d M = svd_A.solve(_b_matrix);


    //AT*A is of course positive semidefinite!
    //but unfortunately also ugly.
    const Eigen::LDLT<Eigen::Matrix3d> ch_AT_A(_a_matrix.transpose() * _a_matrix);
    //        const Eigen::Vector3d &AT_A_AT_b = M;
    //        const Eigen::Vector3d AT_A_AT_b = ch_AT_A.solve(_a_matrix.transpose()*_b_matrix);
    //        const Eigen::Vector3d &M = AT_A_AT_b; //This is actually the 3D point! :-)
    rovi::Matrix4by3d Q_i3;
    Q_i3 << - _p_left.block<1,3>(2, 0),
            0,0,0,
            0,0,0,
            0,0,0;
    Eigen::Vector4d q_i34;
    _jacobian.block<3,1>(0,0)=ch_AT_A.solve(-(Q_i3.transpose()* _a_matrix+_a_matrix.transpose()*Q_i3)*M);
    //_jacobian.block<3,1>(0,0)=ch_AT_A.solve(-(Q_i3.transpose()* _a_matrix+_a_matrix.transpose()*Q_i3)*AT_A_AT_b);// + Q_i3.transpose()*_b_matrix);
    Q_i3.block<1,3>(0,0).swap(Q_i3.block<1,3>(1,0));
    _jacobian.block<3,1>(0,1)=ch_AT_A.solve(-(Q_i3.transpose()* _a_matrix+_a_matrix.transpose()*Q_i3)*M);
    //_jacobian.block<3,1>(0,1)=ch_AT_A.solve(-(Q_i3.transpose()* _a_matrix+_a_matrix.transpose()*Q_i3)*AT_A_AT_b);// + Q_i3.transpose()*_b_matrix);
    Q_i3.block<1,3>(1,0).setZero();
    Q_i3.block<1,3>(2,0) = - _p_right.block<1,3>(2, 0);
    q_i34 << 0,
            0,
            _p_right(2,3),
            0;
    _jacobian.block<3,1>(0,2)=ch_AT_A.solve(-(Q_i3.transpose()* _a_matrix+_a_matrix.transpose()*Q_i3)*M + Q_i3.transpose()*_b_matrix+_a_matrix.transpose()*q_i34);
    //_jacobian.block<3,1>(0,2)=ch_AT_A.solve(-(Q_i3.transpose()* _a_matrix+_a_matrix.transpose()*Q_i3)*AT_A_AT_b + Q_i3.transpose()*_b_matrix+_a_matrix.transpose()*q_i34);
    Q_i3.block<1,3>(2,0).swap(Q_i3.block<1,3>(3,0));
    q_i34.segment<1>(3).swap(q_i34.segment<1>(4));
    _jacobian.block<3,1>(0,3)=ch_AT_A.solve(-(Q_i3.transpose()* _a_matrix+_a_matrix.transpose()*Q_i3)*M + Q_i3.transpose()*_b_matrix+_a_matrix.transpose()*q_i34);
    //_jacobian.block<3,1>(0,3)=ch_AT_A.solve(-(Q_i3.transpose()* _a_matrix+_a_matrix.transpose()*Q_i3)*AT_A_AT_b + Q_i3.transpose()*_b_matrix+_a_matrix.transpose()*q_i34);

    //Uncertainty propagation:
    const Eigen::Matrix3d covariance_3D = _jacobian * _cov_2D * (_jacobian.transpose());

    return std::make_pair(std::move(M),std::move(covariance_3D));
}
