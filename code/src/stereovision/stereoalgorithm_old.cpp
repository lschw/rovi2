#include "stereoalgorithm_old.hpp"

void rovi::Stereoalgorithm_old::stereopsis(cv::Point3d &disparity_point, double baseline, double focal, cv::Point3d &three_d_point){
    // calculate the points
    three_d_point.x = disparity_point.x * baseline / disparity_point.z;
    three_d_point.y = disparity_point.y * baseline / disparity_point.z;
    three_d_point.z = -focal * baseline / disparity_point.z;
}


void rovi::Stereoalgorithm_old::stereopsis(std::vector< cv::Point3d > &disparity_points, double baseline, double focal, std::vector< cv::Point3d > &three_d_points){
    three_d_points.resize(disparity_points.size());
    for(unsigned int p = 0; p < disparity_points.size(); p++){
        stereopsis(disparity_points[p], baseline, focal, three_d_points[p]);
    }
}

void rovi::Stereoalgorithm_old::simpleFeaturePairing(std::vector< cv::Point2d > &left, std::vector< cv::Point2d > &right, std::vector< cv::Point3d > &disparity_points){
    // pairs the features on the following criterias:
    // - y coordinates have to agree approximetly
    // - x_right < x_left
    // This is done by matching two points together one at a time, does not ensure 1 to 1 mapping.
    // better would be to make a tree of matching possibilities and optimize matches

    // must be within 15px offset 1/(offset + 1)
    double min_y_offset =  1 / 16;
    disparity_points.clear();
    // match in order so the image with fewest balls is used to match (reduces likelihood of 1 to many mapping)
    // score is based on height difference, set to zero if x-property not kept
    if(left.size() > right.size()){
        for(unsigned int r = 0; r < right.size(); r++){
            unsigned int best_match = left.size();
            double best_score = 0;
            for(unsigned int l = 0; l < left.size(); l++){
                if(right[r].x < left[l].x){
                    double score = 1 / (std::abs(right[r].y - left[l].y) + 1);
                    if(score > best_score){
                        best_score = score;
                        best_match = l;
                    }
                }
            }
            if(best_score > min_y_offset && best_match < left.size()){
                disparity_points.emplace_back(left[best_match].x, (left[best_match].y + right[r].y)/2, left[best_match].x - right[r].x);
            }
        }
    } else {
        for(unsigned int l = 0; l < left.size(); l++){
            unsigned int best_match = right.size();
            double best_score = 0;
            for(unsigned int r = 0; r < right.size(); r++){
                if(right[r].x < left[l].x){
                    double score = 1 / (std::abs(right[r].y - left[l].y) + 1);
                    if(score > best_score){
                        best_score = score;
                        best_match = r;
                    }
                }
            }
            if(best_score > min_y_offset && best_match < right.size()){
                disparity_points.emplace_back(left[l].x, (left[l].y + right[best_match].y)/2, left[l].x - right[best_match].x);
            }
        }
    }
}


void rovi::Stereoalgorithm_old::stereopsis(const std::vector< cv::Point3d > &disparity_points, const Eigen::MatrixXd &perspective, std::vector< cv::Point3d > &three_d_point){
    // [X,Y,Z,W]^T = Q * [x, y, d, 1]^T

    if(perspective.rows() != 4 && perspective.cols() != 4){
        std::cerr << "Perspective transform given not the right dimensions.\n";
        return;
    }

    three_d_point.resize(disparity_points.size());
    for(unsigned int i = 0; i < disparity_points.size(); i++){
        Eigen::VectorXd dispPoint(4);
        dispPoint(0) = disparity_points[i].x;
        dispPoint(0) = disparity_points[i].y;
        dispPoint(0) = disparity_points[i].z;
        dispPoint(0) = 1;

        Eigen::VectorXd threeDp = perspective * dispPoint.transpose();

        three_d_point[i].x = threeDp[0] / threeDp[3];
        three_d_point[i].y = threeDp[1] / threeDp[3];
        three_d_point[i].z = threeDp[2] / threeDp[3];
    }
}

