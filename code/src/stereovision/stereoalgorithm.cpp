#include "stereoalgorithm.hpp"


void rovi::Stereoalgorithm::applyAlgorithm(std::vector< cv::Mat > &images, rovi::vision::vision_algo& algorithm, std::vector< std::vector< cv::Point2d > > &ret_values){
    // use future to apply algorithm
    ret_values.resize(images.size());
    std::vector< std::future<void> > threads(images.size());
    for(unsigned int t = 0; t < images.size(); t++){
        threads[t] = std::async(algorithm, std::ref(images[t]), std::ref(ret_values[t]));
    }
    // wait for threads to complete
    for(unsigned int w = 0; w < threads.size(); w++){
        threads[w].get();
    }
}


void rovi::Stereoalgorithm::simpleFeaturePairing(const std::vector< cv::Point2d > &left, const std::vector< cv::Point2d > &right, std::vector< std::vector< cv::Point2d > > &paired_points) const{
    /// TODO: Sort the matches so the best is the first.

    // pairs the features on the following criterias:
    // - y coordinates have to agree approximetly
    // - x_right < x_left
    // This is done by matching two points together one at a time, does not ensure 1 to 1 mapping.
    // better would be to make a tree of matching possibilities and optimize matches

    // must be within 15px offset 1/(offset + 1)
    double min_y_offset =  1 / 16;
    paired_points.clear();
    // match in order so the image with fewest balls is used to match (reduces likelihood of 1 to many mapping)
    // score is based on height difference, set to zero if x-property not kept
    if(left.size() > right.size()){
        for(unsigned int r = 0; r < right.size(); r++){
            unsigned int best_match = left.size();
            double best_score = 0;
            for(unsigned int l = 0; l < left.size(); l++){
                if(right[r].x < left[l].x){
                    double score = 1 / (std::abs(right[r].y - left[l].y) + 1);
                    if(score > best_score){
                        best_score = score;
                        best_match = l;
                    }
                }
            }
            if(best_score > min_y_offset && best_match < left.size()){
                // add match
                paired_points.emplace_back(std::vector< cv::Point2d >{left[best_match], right[r]});
            }
        }
    } else {
        for(unsigned int l = 0; l < left.size(); l++){
            unsigned int best_match = right.size();
            double best_score = 0;
            for(unsigned int r = 0; r < right.size(); r++){
                if(right[r].x < left[l].x){
                    double score = 1 / (std::abs(right[r].y - left[l].y) + 1);
                    if(score > best_score){
                        best_score = score;
                        best_match = r;
                    }
                }
            }
            if(best_score > min_y_offset && best_match < right.size()){
                // add match
                paired_points.emplace_back(std::vector< cv::Point2d >{left[l], right[best_match]});
            }
        }
    }
}


void rovi::Stereoalgorithm::stereopsis(const std::vector< cv::Point2d > &two_d_points, Eigen::Vector3d &three_d_point){
    if(two_d_points.size() != 2){
        ROVI_WARN("Stereopsis was given wrong number of points.");
    }

    // first calc a and b
    calcAMatrix(two_d_points);
    calcBMatrix(two_d_points);

    // calculate M
    pureStereopsis(three_d_point);
}


void rovi::Stereoalgorithm::pureStereopsis( Eigen::Vector3d &three_d_point){
    // Solves the equations A_i M = b_i with each given point
    // => M = (A^T A)^(-1) A^T b, where A and b are the stacked A_i's and b_i's
    // this is, however, solved using SVD instead to tackle possible numerical instability
    // M is then the resulting 3D point
    // see 'Multiple-view vision-based robot calibration' pdf for details

    // calculate M
    Eigen::Vector3d M;
    Eigen::JacobiSVD< Matrix4by3d > svd(_a_matrix, Eigen::ComputeThinU | Eigen::ComputeThinV); // compute svd of A, where A x = b
    M = svd.solve(_b_matrix);
    three_d_point = M;
}


void rovi::Stereoalgorithm::calcAMatrix(const std::vector< cv::Point2d > &two_d_points){
    // upper part: Q_(i,1)^T - u_i * Q_(i,3)^T
    _a_matrix.block<1,3>(0, 0) = _p_left.block<1,3>(0, 0) - two_d_points[0].x * _p_left.block<1,3>(2, 0);
    _a_matrix.block<1,3>(2, 0) = _p_right.block<1,3>(0, 0) - two_d_points[1].x * _p_right.block<1,3>(2, 0);
    // upper part: Q_(i,2)^T - v_i * Q_(i,3)^T
    _a_matrix.block<1,3>(1, 0) = _p_left.block<1,3>(1, 0) - two_d_points[0].y * _p_left.block<1,3>(2, 0);
    _a_matrix.block<1,3>(3, 0) = _p_right.block<1,3>(1, 0) - two_d_points[1].y * _p_right.block<1,3>(2, 0);
}

void rovi::Stereoalgorithm::calcBMatrix(const std::vector< cv::Point2d > &two_d_points){
    // u_i * q_(i,34) - q_(i,14)
    _b_matrix(0) = two_d_points[0].x * _p_left(2, 3) - _p_left(0, 3);
    _b_matrix(2) = two_d_points[1].x * _p_right(2, 3) - _p_right(0, 3);
    // v_i '-' q_(i,34) - q_(i,24)
    _b_matrix(1) = two_d_points[0].y * _p_left(2, 3) - _p_left(1, 3);
    _b_matrix(3) = two_d_points[1].y * _p_right(2, 3) - _p_right(1, 3);
}


void rovi::Stereoalgorithm::set2DUncertainity(const Eigen::Matrix2d &covariance_left, const Eigen::Matrix2d &covariance_right){
    _cov_2D.setZero();
    _cov_2D.block<2,2>(0,0) = covariance_left;
    _cov_2D.block<2,2>(2,2) = covariance_right;
}


void rovi::Stereoalgorithm::setPerspectiveTransform(const rovi::Matrix3by4d &perspective_left, const rovi::Matrix3by4d &perspective_right){
    _p_left = perspective_left;
    _p_right = perspective_right;
}


void rovi::Stereoalgorithm::uncertainityPropagation(const std::vector< cv::Point2d > &two_d_points, Eigen::Matrix3d &covariance_3D){
    if(two_d_points.size() != 2){
        ROVI_WARN("Stereopsis was given wrong number of points.");
    }

    // calculate A and b
    calcAMatrix(two_d_points);
    calcBMatrix(two_d_points);

    // propagate the error
    pureUncertaintyPropagation(covariance_3D);
}


void rovi::Stereoalgorithm::pureUncertaintyPropagation(Eigen::Matrix3d &covariance_3D){
    // compute the jacobian
    computeJacobian();

    covariance_3D = _jacobian * _cov_2D * (_jacobian.transpose());
}


void rovi::Stereoalgorithm::computeJacobian(){
    // compute the four rows
    Eigen::Matrix3d AT_A = _a_matrix.transpose() * _a_matrix, A_DS;
    rovi::Matrix4by3d Q_i3;
    Eigen::Vector4d q_i34;
    Eigen::JacobiSVD< Eigen::Matrix3d > svd_AT_A(AT_A, Eigen::ComputeThinU | Eigen::ComputeThinV);
    Eigen::Vector3d AT_A_AT_b = svd_AT_A.solve(_a_matrix.transpose() * _b_matrix);
    for(size_t i = 0; i < 4; i++){
        // make Q_i3, where it is alternating top and bottom row = (left for first 2 and right P for last 2)'s last column, likewise make q_i34
        Q_i3.setZero();
        q_i34.setZero();
        if(i < 2){
            Q_i3.row(i) = - _p_left.block<1,3>(2, 0);
            q_i34(i) = _p_left(2,3);
        } else {
            Q_i3.row(i) = - _p_right.block<1,3>(2, 0);
            q_i34(i) = _p_right(2,3);
        }
        // calc A_DS, changes on every iteration dependent on Q_i3. 'A' only part only
        A_DS = Q_i3.transpose() * _a_matrix + _a_matrix.transpose() * Q_i3;
        // compute the jacobian entry
        _jacobian.col(i) = svd_AT_A.solve( - A_DS * AT_A_AT_b + Q_i3.transpose() * _b_matrix + _a_matrix.transpose() * q_i34);
    }

}


void rovi::Stereoalgorithm::computeInverse(Eigen::Matrix3d &matrixToInvert) const{
    double precision = 1e-6;
    // this part is stolen from RobWorks :D
    Eigen::JacobiSVD < Eigen::Matrix3d > svd( matrixToInvert ,Eigen::ComputeThinU | Eigen::ComputeThinV);
    double tolerance = precision * std::max(matrixToInvert.cols(), matrixToInvert.rows()) * svd.singularValues().array().abs().maxCoeff();

    matrixToInvert = svd.matrixV() *
            Eigen::MatrixXd((svd.singularValues().array().abs() > tolerance).select(svd.singularValues().array().inverse(), 0)).asDiagonal() * svd.matrixU().adjoint();
}


void rovi::Stereoalgorithm::completeStereopsis(const std::vector<cv::Point2d> &two_d_points, Eigen::Vector3d &three_d_point, Eigen::Matrix3d &covariance_3D){
    if(two_d_points.size() != 2){
        ROVI_WARN("Complete Stereopsis was given wrong number of points.");
    }

    // calculate A and b
    calcAMatrix(two_d_points);
    calcBMatrix(two_d_points);

    // compute Jacobian
    computeJacobian();

    // calc the 3D point
    pureStereopsis(three_d_point);

    // calc the propagated uncertainty
    pureUncertaintyPropagation(covariance_3D);
}

