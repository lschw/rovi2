#include "graspplanner.hpp"

#include <iostream>

#include <rw/models/Joint.hpp>
#include <rw/models/RevoluteJoint.hpp>

rovi::GraspPlanner::GraspPlanner(const rw::models::Device::Ptr robot, const rw::models::WorkCell::Ptr wc):GraspPlanner(robot, robot->getEnd(), wc)
{}

rovi::GraspPlanner::GraspPlanner(const rw::models::Device::Ptr robot, rw::kinematics::Frame::Ptr endeffector, const rw::models::WorkCell::Ptr wc)
    :_robot(new rw::models::SerialDevice(robot->getBase(), &*endeffector, robot->getName(), wc->getDefaultState())),
      _target(nullptr), _endeffector(endeffector), _invkin(_robot, wc->getDefaultState()),
      _wc(wc), _collDetector(nullptr),
      _angleDifference(20.0 * rw::math::Deg2Rad), _angleTries(200),
      _triesDone(0), _colChecks(0),
      _lower(1, 0.0), _upper(1, 0.0), _indices(0)
{
    _invkin.setCheckJointLimits(true);

    _collDetector = new rw::proximity::CollisionDetector(_wc, rwlibs::proximitystrategies::ProximityStrategyFactory::makeDefaultCollisionStrategy());

    // kidnaped from RW Ambiguity
    const std::vector<rw::models::Joint*>& joints = _robot->getJoints();
    size_t index = 0;
    BOOST_FOREACH(const rw::models::Joint* joint, joints) {
        if (dynamic_cast<rw::models::RevoluteJoint*>(joints[index])) {
            for (int i = 0; i<joint->getDOF(); i++) {
                _indices.push_back(index);
                ++index;
            }
        } else {
            index += joint->getDOF();
        }
    }

    _lower = _robot->getBounds().first;
    _upper = _robot->getBounds().second;
}



void rovi::GraspPlanner::setTarget(rw::kinematics::Frame::Ptr target){
    if(target == NULL){
        ROVI_THROW("No valid target supplied.");
    }
    _target = target;
}


rw::math::Q rovi::GraspPlanner::planGrasp(const rw::math::Vector3D<> targetMoveDir, const rw::kinematics::State &state){
    // take the direct path, if not moving
    if(targetMoveDir.norm2() <= 0.01){
        return planGrasp(state);
    }

    // find initial stuff needed
    rw::math::Transform3D<double> baseTtarget = _robot->baseTframe(&*_target, state);
    rw::math::Rotation3D<> worldTend =  rw::kinematics::Kinematics::worldTframe(_robot->getEnd(), _wc->getDefaultState()).R();

    // find orientation from targetMoveDir
    double roll = std::atan2(targetMoveDir[1], targetMoveDir[0]);
    double yaw = std::atan2(targetMoveDir[2] * targetMoveDir[2], targetMoveDir[1] * targetMoveDir[1] + targetMoveDir[0] * targetMoveDir[0]);// + rw::math::Pi/2;
    if(targetMoveDir[2] >= 0){
        yaw = -yaw;
    }

    // modify to fit
    roll = -roll - rw::math::Pi / 2;

    rw::math::Rotation3D<> towardstarget = rw::math::RPY<>(0, roll, yaw).toRotation3D();

    baseTtarget.R() = worldTend * towardstarget;
    std::vector< rw::math::Q > goodConfigs = findCollFreeQ(baseTtarget, state);

    if(goodConfigs.size() == 0){
        ROVI_THROW("Could not find a valid configuration after " + std::to_string(_triesDone) + " tries.");
    }

    // find ambigious q's
    goodConfigs = expandQ(goodConfigs);

    // output closest config
    rw::math::Q currentConfig = _robot->getQ(state);
    int bestQ = -1;
    double leastdistance = 0;
    for(size_t i = 0; i < goodConfigs.size(); i++){
        double dist = (currentConfig - goodConfigs[i]).norm2();
        if( dist < leastdistance || bestQ < 0 ){
            leastdistance = dist;
            bestQ = i;
        }
    }
    return (goodConfigs[bestQ]);

}


std::vector< rw::math::Q > rovi::GraspPlanner::findQ(const rw::math::Transform3D<> &baseTpose, const rw::kinematics::State &startstate) const{
    // interpolate the path to utilise IK on small steps.
    // use ambiguity solver to find all solutions
    rw::kinematics::State state = startstate;
    state.upgrade();

    // find the final Q
    std::vector< rw::math::Q > configs;
    configs = _invkin.solve(baseTpose, state);
    if(configs.size() == 0){
        ROVI_THROW("Did not find a Q.");
    }
    return configs;
}


rw::math::Q rovi::GraspPlanner::planGrasp(const rw::kinematics::State &state){
    rw::math::Transform3D<double> baseTtarget = _robot->baseTframe(&*_target, state);

    // _baseTtarget.R() must point in direction of the pos vector
    rw::math::Vector3D<> direction = -baseTtarget.P();
    double roll = std::atan2(direction[1], direction[0]);

    rw::math::Rotation3D<> baseThand = rw::kinematics::Kinematics::frameTframe(_robot->getBase(), _robot->getEnd(), _wc->getDefaultState()).R();
    rw::math::Rotation3D<> towardsbase = rw::math::RPY<>(0, - (roll - rw::math::Pi / 2), 0).toRotation3D();

    baseTtarget.R() = baseThand * towardsbase;
    std::vector< rw::math::Q > goodConfigs = findCollFreeQ(baseTtarget, state);

    if(goodConfigs.size() == 0){
        ROVI_THROW("Could not find a valid configuration.");
    }

    // find ambigious q's
    goodConfigs = expandQ(goodConfigs);

    // output closest config
    rw::math::Q currentConfig = _robot->getQ(state);

    // possibly add collision checking?
    int bestQ = -1;
    double leastdistance = 0;
    for(size_t i = 0; i < goodConfigs.size(); i++){
        double dist = (currentConfig - goodConfigs[i]).norm2();
        if( dist < leastdistance || bestQ < 0 ){
            leastdistance = dist;
            bestQ = i;
        }
    }
    return (goodConfigs[bestQ]);
}


std::vector< rw::math::Q > rovi::GraspPlanner::findCollFreeQ(const rw::math::Transform3D<> &baseTpose, const rw::kinematics::State &startstate){
    int reps = -1;
    std::vector< rw::math::Q > goodConfigs;
    int colChecks = 0;

    rw::models::Device::Ptr robot = _robot;
    rw::proximity::CollisionDetector::Ptr collDetector = _collDetector;
    ROVI_WARN("Entering parallel for loop!");
#pragma omp parallel for shared(reps, goodConfigs, robot, colChecks, collDetector)
    for(int i = -1; i < _angleTries; i++){
        if(goodConfigs.size() > 0){
            i = _angleTries;
            continue;
        }
        rw::math::Transform3D<double> transform = baseTpose;
        reps++;
        if(i >= 0){
            transform.R() = transform.R() * randRotation();
        }

        std::vector< rw::math::Q > possibleConfigs;

        try {
            rw::kinematics::State teststate = startstate;
            possibleConfigs = findQ(transform, teststate);

            // add q's that are not in collision
            for(size_t j = 0; j < possibleConfigs.size();j++){
                robot->setQ(possibleConfigs.at(j), teststate);

                if(!collDetector->inCollision(teststate)){
                    goodConfigs.push_back(possibleConfigs.at(j));
//                    std::cout << "Added a config" << std::endl;
                }
                colChecks++;
            }
        } catch( std::runtime_error &err){
            ROVI_WARN(err.what());
            //throw err;
        }
    }
//    } while (++reps < _angleTries && goodConfigs.size() == 0);
    _colChecks = colChecks;
    _triesDone = reps + 1;

    return goodConfigs;
}

rw::math::Rotation3D<> rovi::GraspPlanner::randRotation() const{
    // construct a random rotation
    // 1) generate a random vector to rotate about
    rw::math::Vector3D<> rotVector;
    double norm;
    int rand = 0;
    do {
        rotVector = rw::math::Vector3D<>(Eigen::Vector3d::Random());
        norm = rotVector.norm2();
        rand++;
    } while(norm < 10e-6);
    rotVector /= norm;
    if(rand > 1){
        std::cout << "Took " << rand << " times to generate valid vector.\n";
    }
    // 2) generate a random angle within the specified range (+/-)
    const int res = 100000000;
    double angle = static_cast<double>(std::rand() % (2 * res + 1)) / static_cast<double>(res) - 1;
    angle *= _angleDifference;

    // generate rotaion 3D
    rw::math::EAA<> randomRotation(rotVector, angle);

    // test stuff
//    rw::math::RPY<> rpy(randomRotation.toRotation3D());
//    rw::common::Log::log().info() << "Random RPY: " << rpy[0] * rw::math::Rad2Deg << ", " << rpy[1] * rw::math::Rad2Deg << ", " << rpy[2] * rw::math::Rad2Deg << "\n";

    return randomRotation.toRotation3D();
}

std::vector< rw::math::Q > rovi::GraspPlanner::expandQ(const std::vector< rw::math::Q > config) const {
    std::vector< rw::math::Q > res1 = config;
    std::vector< rw::math::Q > res2;
    const double pi2 = 2*rw::math::Pi;


    for(auto index : _indices){
        res2.clear();
//        std::cout<<"Index = "<<index<<std::endl;
        for(auto q : res1) {
            //std::cout<<"Input= "<<q<<std::endl;

            double d = q(index);
            while (d>_lower(index))
                d -= pi2;
            while (d<_lower(index))
                d += pi2;
            while (d <_upper(index)) {
                rw::math::Q tmp(q);
                tmp(index) = d;
                res2.push_back(tmp);
              //  std::cout<<"Output = "<<tmp<<std::endl;
                d += pi2;
            }
        }
        res1 = res2;
        //std::cout<<"Output Count = "<<res1.size()<<std::endl;
    }
    return res1;
}
