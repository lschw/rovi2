#pragma once

#include <rw/math/Transform3D.hpp>
#include <rw/math/Vector3D.hpp>
#include <rw/math/Rotation3D.hpp>
#include <rw/math/RPY.hpp>
#include <rw/math/Q.hpp>
#include <rw/math/EAA.hpp>
#include <rw/models/Device.hpp>
#include <rw/models/SerialDevice.hpp>
#include <rw/models/WorkCell.hpp>
#include <rw/kinematics/Frame.hpp>
#include <rw/kinematics/State.hpp>
#include <rw/kinematics/Kinematics.hpp>
#include <rw/invkin/ClosedFormIKSolverUR.hpp>
//#include <rw/invkin/AmbiguityResolver.hpp>
#include <rw/proximity/CollisionDetector.hpp>

#include <rwlibs/proximitystrategies/ProximityStrategyFactory.hpp>

#include "../utils/log.hpp"

#include <iostream>
#include <vector>
#include <random>

#include <eigen3/Eigen/Dense>

namespace rovi {

/**
 * @brief The GraspPlanner class Calculates a grasp direction for the robot
 */
class GraspPlanner
{
public:
    GraspPlanner(const rw::models::Device::Ptr robot, const rw::models::WorkCell::Ptr wc);

    GraspPlanner(const rw::models::Device::Ptr robot, rw::kinematics::Frame::Ptr endeffector, const rw::models::WorkCell::Ptr wc);


    /// setters
    void setTarget(rw::kinematics::Frame::Ptr target);

    /// Supporting Functionality

    /**
     * @brief planGrasp Plans the grasp to catch the ball in the moving direction.
     * @param targetMoveDir The moving direction.
     * @param state
     * @return
     */
    rw::math::Q planGrasp(const rw::math::Vector3D<> targetMoveDir, const rw::kinematics::State &state);

    /**
     * @brief planGrasp Plans a grasp of the obj without considering it moving (gripper directly aligned with the base of the robot).
     * @param state
     * @return
     */
    rw::math::Q planGrasp(const rw::kinematics::State &state);


    void setRandomness(const double angle, const int tries){
        _angleDifference = angle;
        _angleTries = tries;
    }

    int getTries() const {
        return _triesDone;
    }

    int getColChecks() const{
        return _colChecks;
    }

protected:
    // general functionality
    // taken from RW ambiguity solver..
    std::vector< rw::math::Q > expandQ(const std::vector< rw::math::Q > config) const;

    /**
     * @brief randRotation Generates a random rotation using the predefined angle limit. Uses EAA to do so.
     * @return
     */
    rw::math::Rotation3D<> randRotation() const;

    /**
     * @brief findQ Finds the Q using of T using IK
     * @param pose
     * @return
     */
    std::vector< rw::math::Q > findQ(const rw::math::Transform3D<> &baseTpose, const rw::kinematics::State &startstate) const;

    /**
     * @brief findCollFreeQ Finds a valid set of Q's around T
     * @param baseTpose
     * @param startstate
     * @return
     */
    std::vector< rw::math::Q > findCollFreeQ(const rw::math::Transform3D<> &baseTpose, const rw::kinematics::State &startstate);


    rw::models::SerialDevice::Ptr _robot;

    // vals used
    rw::kinematics::Frame::Ptr _target, _endeffector;


    rw::invkin::ClosedFormIKSolverUR _invkin;
//    rw::invkin::AmbiguityResolver _ambRes;

    rw::models::WorkCell::Ptr _wc;

    rw::proximity::CollisionDetector::Ptr _collDetector;

    // settings used to try find closeby grasps that are the same
    double _angleDifference; // the maximum angle to try
    int _angleTries; // how many different angles to try when the ideal fails
    int _triesDone, _colChecks;

    // used for ambiguity
    rw::math::Q _lower;
    rw::math::Q _upper;
    std::vector<size_t> _indices;


};

}
