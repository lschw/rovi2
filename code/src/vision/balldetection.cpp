#include "balldetection.hpp"
#include <iostream>

std::vector<cv::Point2d> rovi::vision::BallDetector::findCircles(cv::Mat &image, double circlyness, int min_area, int max_area){
    std::vector<cv::Vec4i> hierarchy;

    //Find contours
    std::vector<std::vector<cv::Point> > contours;
    std::vector<std::vector<cv::Point> > good_contours;
    cv::findContours( image, contours, hierarchy, CV_RETR_LIST, cv::CHAIN_APPROX_NONE);

    //select circles
    std::vector<cv::Point2d> center;
    cv::Moments mu; //use moments to get center of mass
    for(unsigned int i = 0; i< contours.size(); i++ ){
        double area = cv::contourArea(contours[i]);
        double circumference = cv::arcLength(contours[i],true);
        double circle_likeness = 4.0 * M_PI * area / (circumference * circumference);
        if((area > static_cast<double>(min_area)) && (area < static_cast<double>(max_area))) {
            //std::cout << "CIRCLEEE" << circle_likeness << std::endl;
            if ((circle_likeness > circlyness)) {
                mu = cv::moments(contours[i],false);
                cv::Point2d com = cv::Point2d( mu.m10/mu.m00 , mu.m01/mu.m00 );
                center.push_back(com);
                good_contours.push_back(contours[i]);
            }
        }
    }

//    cv::Mat img_contours_red;
//    img_contours_red = cv::Mat::zeros(image.rows, image.cols, CV_8UC3);// image.clone();
    //Mat dst = Mat::zeros(src.rows, src.cols, CV_8UC3);
//    cv::drawContours(img_contours_red,good_contours,-1,cv::Scalar(0,0,255),CV_FILLED);
//    cv::imshow("RED!!",img_contours_red);
//    cv::waitKey(10);

    return center;
}

void rovi::vision::BallDetector::findBlob(cv::Mat &image, std::vector< cv::Point2d > &ball_centers){
    // convert to hsv and take the red channel
    cv::Mat img_hsv, lower_red_hue_range, upper_red_hue_range;
    cv::cvtColor(image, img_hsv, CV_BGR2HSV);

    int hue_min, hue_max; //range 0 - 180 degrees
    int sat_min = 0.4 * 255, sat_max = 1.0 * 255; // range 0 - 255   radius   // 0.4, 1.0
    int val_min = 0.10 * 255, val_max = 1 * 255; // range 0 - 255 intensity   //0.10, 1.0
    hue_min = 0; hue_max = 10;  //0, 10
    cv::inRange(img_hsv, cv::Scalar(hue_min, sat_min, val_min), cv::Scalar(hue_max,sat_max,val_max), lower_red_hue_range);
    hue_min = 170; hue_max = 180;  //170, 180
    cv::inRange(img_hsv, cv::Scalar(hue_min, sat_min, val_min), cv::Scalar(hue_max,sat_max,val_max), upper_red_hue_range);


    cv::Mat img_red;
    cv::addWeighted(lower_red_hue_range, 1.0, upper_red_hue_range, 1.0, 0.0, img_red);

    size_t dilation_size = 3;
    size_t erosion_size = 2;
    cv::Mat element_dil = getStructuringElement( cv::MORPH_ELLIPSE,
                                           cv::Size( 2*dilation_size + 1, 2*dilation_size+1 ),
                                           cv::Point( dilation_size, dilation_size ) );
    cv::Mat element_ero = getStructuringElement( cv::MORPH_ELLIPSE,
                                           cv::Size( 2*erosion_size + 1, 2*erosion_size+1 ),
                                           cv::Point( erosion_size, erosion_size ) );

    cv::erode(img_red,img_red,element_ero);
    cv::dilate(img_red,img_red,element_dil);


//    cv::imshow("red", img_red);
//    cv::waitKey(10);
    // find circles
    ball_centers = findCircles(img_red, 0.6, 80, 10000000);

}
