#pragma once

#include <vector>

#include <opencv2/imgproc/imgproc.hpp>

#include "balldetection.hpp"

namespace rovi {
namespace vision {

class vision_algo : public rovi::vision::BallDetector
{
public:

    enum algorithm{ findRedBlob};


    vision_algo(algorithm algo):_algo(algo){
    }

    void operator ()(cv::Mat &image, std::vector< cv::Point2d > &ball_centers){
        if(_algo == findRedBlob){

            findBlob(image, ball_centers);
        } else {

        }
    }

protected:
    algorithm _algo;

};

}

}
