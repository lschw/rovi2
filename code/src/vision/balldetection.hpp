#pragma once
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>


namespace rovi {
namespace vision {


class BallDetector
{
public:
    BallDetector() {}

    virtual ~BallDetector(){}

    /**
     * @brief Finds a red blob in the image.
     * @param image The image to search.
     * @param ball_centers the ball centers found.
     */
    void findBlob(cv::Mat &image, std::vector< cv::Point2d > &ball_centers);

protected:
    std::vector<cv::Point2d> findCircles(cv::Mat &image, double circlyness = 0.80, int min_area = 150, int max_area = 60000);
};



}


}
