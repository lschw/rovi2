#include "linearpath.hpp"



rw::math::Vector3D< double > rovi::LinearPath::pos(double t){
    // find the pose
    rw::math::Vector3D< double > pose = _position;

    // velocity contribution
    pose += _velocity * t;

    // acceleration contribution
    pose += 0.5 * _acceleration * t * t;

    return pose;
}

rw::math::Vector3D< double > rovi::LinearPath::vel(double t){

}

rw::math::Vector3D< double > rovi::LinearPath::acc(double t){

}




