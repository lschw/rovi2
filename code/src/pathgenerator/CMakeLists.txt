SET(SRC_FILES
	${SRC_FILES}
	${CMAKE_CURRENT_SOURCE_DIR}/linearpath.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/cubicsplinepolynomial.cpp
	CACHE INTERNAL ""
)
