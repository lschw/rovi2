/**
 * @file cubicsplinepolynomial.hpp
 * @author Mikael Westermann
 */
#pragma once
#include <cassert>
#include <eigen3/Eigen/Dense>

namespace rovi {

/**
 * @brief The CubicSplinePolynomial class
 * Cubic spline polynomial based on eq. 5.22 of robotics notes.
 * Eq. 5.22 has been rewritten to polynomial form with coefficients in t.
 * The polynomial C(t) is calculated for a given time t as follows:
 * C(t)=a*t^3+b*t^2+c*t+d
 *
 * The idea behind rewriting to polynomial form is
 * that the cubic spline may be found entirely in advance
 * to time tesselation.
 */
template<typename Vector=Eigen::Vector3d>
class CubicSplinePolynomial {
protected:
    Vector _a, _b, _c, _d;
    const double _t_start, _t_end;
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    /**
     * @brief CubicSplinePolynomial Constructor
     * @param pos_start Start position vector
     * @param pos_end End position vector
     * @param vel_start Start velocity vector
     * @param vel_end End velocity vector
     * @param t_start Start time
     * @param t_end End time
     *
     * Note that "position" does not necessarily refer to
     * 3D translational position. It may as well refer to
     * a tool space 7-tuple consisting of a position vector
     * and an orientation quaternion,
     * or it might simply refer to a joint configuration.
     * Likewise for velocity. It is simply the time derivative
     * in whichever representation is used.
     *
     * In case
     * rotational velocity is zero throughout entire motion,
     * only interpolation of positions is necessary,
     * and the pos and vel vectors input to this class
     * are simply 3D vectors in tool space (no quaternions!).
     */
    CubicSplinePolynomial(const Vector &pos_start,
       const Vector &pos_end,
       const Vector &vel_start,
       const Vector &vel_end,
       const double t_start,
       const double t_end);

    /**
     * @brief evaluate Calculates cubic spline at time t.
     * @param t time between t_start and t_end.
     * @return
     */
    virtual const Vector evaluate(const double t) const;
};

template<typename Vector>
CubicSplinePolynomial<Vector>::CubicSplinePolynomial(const Vector &pos_start, const Vector &pos_end, const Vector &vel_start, const Vector &vel_end, const double t_start, const double t_end)
    :
      _a(),_b(),_c(),_d(),
      _t_start(t_start),_t_end(t_end)
{
    const double dt = t_end-t_start;
    const double dt2=dt*dt;
    const double dt3=dt2*dt;
    const double ts2 = t_start*t_start;
    const double ts3 = ts2*t_start;
    const Vector dp = pos_end-pos_start;
    const Vector tfull_vsum_2dp = (dt)*(vel_end+vel_start)-2*(dp);
    const Vector three_dp_dt_vf_2vs = (3*(dp)-(dt)*(vel_end+2*vel_start));
    assert(dt>0);
    _d = -ts3*tfull_vsum_2dp/dt3
            + ts2*three_dp_dt_vf_2vs/dt2
            + pos_start
            - t_start*vel_start;
    _c = 3*ts2*tfull_vsum_2dp/dt3
            -2*t_start*three_dp_dt_vf_2vs/dt2
            + vel_start;
    _b = three_dp_dt_vf_2vs/dt2
            -3*t_start*tfull_vsum_2dp/dt3;
    _a = tfull_vsum_2dp/dt3;
}

template<typename Vector>
const Vector CubicSplinePolynomial<Vector>::evaluate(const double t) const {
    assert(t>=_t_start);
    assert(t<=_t_end);
    const double t2=t*t;
    return (((t2*t)*_a)+(t2*_b)+(t*_c)+_d);
}

}
