#pragma once

#include <rw/math/Vector3D.hpp>
#include <rw/trajectory/Path.hpp>


namespace rovi {


/**
 * @brief The LinearPath class Generates a path of Vector3D that is linear with respect position, velocity or acceleration.
 */
class LinearPath{

public:

    LinearPath(){}

    LinearPath(rw::math::Vector3D< double > position,
               rw::math::Vector3D< double > velocity,
               rw::math::Vector3D< double > acceleration):
        _position(position),
        _velocity(velocity),
        _acceleration(acceleration){}

    rw::math::Vector3D< double > pos(double t);

    rw::math::Vector3D< double > vel(double t);

    rw::math::Vector3D< double > acc(double t);

    virtual ~LinearPath(){}


protected:
    // starting settings
    rw::math::Vector3D< double > _position, _velocity, _acceleration;
};

}
