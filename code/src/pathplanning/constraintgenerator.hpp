#pragma once

#include <rw/kinematics/State.hpp>
#include <rw/kinematics/Frame.hpp>
#include <rw/kinematics/MovableFrame.hpp>
#include <rw/kinematics/Kinematics.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/math/RPY.hpp>
#include <rw/math/Rotation3D.hpp>

#include <rw/common/Log.hpp>

#include <cmath>


namespace rovi {
namespace pathplanners {

class ConstraintGenerator
{
public:
    ConstraintGenerator():
        _origo(nullptr), _pointer(nullptr),
        _pointer_length(0), _pointer_distance(0) {}

    void setOrigo(rw::kinematics::Frame::Ptr origo){
        _origo = origo;
    }

    void setPointer(rw::kinematics::MovableFrame::Ptr pointer){
        _pointer = pointer;
    }

    void setPointerDistance(const double pointerlength, const double pointerdistance){
        _pointer_distance = pointerdistance;
        _pointer_length = pointerlength;
    }

    void pointTowards(const rw::math::Transform3D<> &origo_to_point, rw::kinematics::State &state);

protected:
    rw::kinematics::Frame::Ptr _origo;
    rw::kinematics::MovableFrame::Ptr _pointer;

    double _pointer_length, _pointer_distance;
};


}
}
