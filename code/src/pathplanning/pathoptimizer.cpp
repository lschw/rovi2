#include "pathoptimizer.hpp"

rovi::pathOptimizer::pathOptimizer(rovi::CollisionChecker &collChecker){
    _collChecker = collChecker;
}

void rovi::pathOptimizer::prune(rw::trajectory::QPath &path, double epsilon){
    int i = 0;
    while(path.size() - 2 > i){
        // if point after next is reachable from current the remove next
        if(!_collChecker.checkCollision(path[i], path[i+2], epsilon)){
            // if path valid, remove next node
            path.erase(path.begin() + i + 1);
        } else{
            i++;
        }
    }

}
