#include "pathplanner.hpp"
#include <rw/rw.hpp>
#include <rw/math/Vector3D.hpp>
#include <rw/math/Math.hpp>
#include <rw/math/Q.hpp>
#include <rw/trajectory/Path.hpp>

//#include <rwlibs/pathplanners/prm/PRMPlanner.hpp>
/*
 * Instead of using the robwork PRM planner, I copied the code into our project.
 * This allows us to make changes to the planner.
 * To make the "transistion" as easy as possible is
 * the rwlibs namespace renamed to rovi.
 */
#include "PRMPlanner.hpp"
#include <iostream>

#include <rw/common.hpp>

#include <rwlibs/proximitystrategies/ProximityStrategyFactory.hpp>
#include <rw/proximity/CollisionDetector.hpp>
#include <rw/common/Ptr.hpp>
#include <rw/invkin/JacobianIKSolver.hpp>
#include <rw/kinematics/MovableFrame.hpp>

#include "pathoptimizer.hpp"



rovi::PathPlanner::PathPlanner (rw::models::WorkCell::Ptr wc, std::string device_name, std::string constraint_name, size_t nodes) :
    _work_cell(wc),
    _state(_work_cell->getDefaultState()),
    _device(_work_cell->findDevice(device_name)),
    _detector(_work_cell,rwlibs::proximitystrategies::ProximityStrategyFactory::makeDefaultCollisionStrategy()),
    _constraint(rw::pathplanning::PlannerConstraint::make(& _detector,_device,_state)),
    _sampler(rw::pathplanning::QSampler::makeConstrained(rw::pathplanning::QSampler::makeUniform(_device),_constraint.getQConstraintPtr())),
    _resolution(0.1),
//    _constraint_object( static_cast<rw::kinematics::MovableFrame*>(_work_cell->findFrame(constraint_name)) ),
    _coll_checker(_state, _device,&_detector),
    _path_optimizer(_coll_checker),
    _planner(& *_device, _state,& _detector,_resolution){
    rw::math::Math::seed();
    std::cout << "device: " << device_name << " constraint: " << constraint_name << std::endl;
    _planner.setCollisionCheckingStrategy(rovi::pathplanners::PRMPlanner::FULL); //Check node and edges on construction.
    _planner.setNeighSearchStrategy(rovi::pathplanners::PRMPlanner::KDTREE);
    _planner.setShortestPathSearchStrategy(rovi::pathplanners::PRMPlanner::A_STAR);
    _planner.buildRoadmap(nodes);
}

rovi::PathPlanner::PathPlanner (std::string work_cell_file, std::string device_name, std::string constraint_name, size_t nodes):
    PathPlanner(rw::loaders::WorkCellLoader::Factory::load(work_cell_file), device_name, constraint_name, nodes)
{
}

rw::trajectory::QPath rovi::PathPlanner::plan(const rw::math::Q &from, const rw::math::Q &to, const bool optimize){
    rw::trajectory::QPath path;
    _planner.query(from,to,path/*,5.0 seconds*/);
    if(optimize){
        _path_optimizer.prune(path,0.1);
    }
    return std::move(path);
}

// DEPRECATED
//void rovi::PathPlanner::move_constraint(rw::math::Transform3D<double> new_position){
//    _constraint_object->setTransform(new_position, _state);
//    _planner.update_constraints(_state);
//}

void rovi::PathPlanner::updateState(rw::kinematics::State &state){
    _planner.update_constraints(state);
    _state = state;
}

