#include "constraintgenerator.hpp"


void rovi::pathplanners::ConstraintGenerator::pointTowards(const rw::math::Transform3D<> &origo_to_point, rw::kinematics::State &state){
    // realign the pointer to point from origo towards the point
    rw::math::Transform3D<> pointer;
    // the position is given by the position of origo
    rw::math::Transform3D<> origo = rw::kinematics::Kinematics::worldTframe(&*_origo, state);
    pointer.P() = origo.P();

    // find the rotation from the translation part of the  transformation from origo to the point and adding the already present rotation

    double yaw = -std::atan2(origo_to_point.P()[1], origo_to_point.P()[2]);
    double pitch;
    if(origo_to_point.P()[2] >= 0){
        pitch = - std::atan2(origo_to_point.P()[0] * std::cos(yaw), origo_to_point.P()[2]);
    } else {
        pitch = std::atan2(origo_to_point.P()[0] * std::cos(yaw), - origo_to_point.P()[2]);
    }
    double roll = std::atan2(std::cos(yaw), std::sin(yaw) * std::sin(pitch)) + rw::math::Pi/2;

//    rw::common::Log::log().info() << "Angel (r,p,y) = " << roll << ", " << pitch << ", " << yaw << "\n";

    rw::math::RPY<> rot(roll, pitch, yaw);

    // origo.R() *
    pointer.R() = origo.R() * rot.toRotation3D();

    // modify length
    if(_pointer_distance != 0 || _pointer_length != 0){
        double distToPoint = origo_to_point.P().norm2();
        rw::math::Transform3D<> lift(rw::math::Vector3D<>(0,0,-_pointer_length + distToPoint - _pointer_distance), rw::math::Rotation3D<>::identity());

        pointer = pointer * lift;
    }
    // modify pointer pos
    _pointer->setTransform(pointer, state);
}
