#include "collisionchecker.hpp"


rovi::CollisionChecker::CollisionChecker(rw::kinematics::State &state, rw::models::Device::Ptr &device, rw::proximity::CollisionDetector *detector){
    _state = state;
    _device = device;
    _detector = detector;
}

bool rovi::CollisionChecker::checkConfiguration(rw::math::Q &configuration){
    // set device
    _device->setQ(configuration, _state);
    // detect collision
    rw::proximity::CollisionDetector::QueryResult data;
    bool collision = _detector->inCollision(_state,&data);

    return collision;
}

bool rovi::CollisionChecker::checkCollision(rw::math::Q &from, rw::math::Q &to, double &epsilon){
    return true;
}


bool rovi::SFTChecker::checkCollision(rw::math::Q &from, rw::math::Q &to, double &epsilon){
    // dq
    rw::math::Q dq = to - from;
    // number of configurations
    unsigned int n = (dq.norm2() / epsilon) - 1;
    // step size
    rw::math::Q step = epsilon * (dq / dq.norm2());
    // add configurations to list
    for(unsigned int i = 0; i < n; i++){
        rw::math::Q nextQ = (i + 1) * step + from;
        if(checkConfiguration(nextQ)){
            return true;
        }
    }
    return false;
}


bool rovi::USSChecker::checkCollision(rw::math::Q &from, rw::math::Q &to, double &epsilon){
    // dq
    rw::math::Q dq = to - from;
    // number of configurations
    unsigned int n = (dq.norm2() / epsilon) - 1;
    // step sizes
    rw::math::Q step = dq / (n + 1);
    // add configurations to list
    for(unsigned int i = 0; i < n; i++){
        rw::math::Q nextQ = (i + 1) * step + from;
        if(checkConfiguration(nextQ)){
            return true;
        }
    }
    return false;
}


bool rovi::BinChecker::checkCollision(rw::math::Q &from, rw::math::Q &to, double &epsilon){
    // dq
    rw::math::Q dq = to - from;
    // number of configurations
    unsigned int n = (dq.norm2() / epsilon);
    // levels
    unsigned int levels = std::ceil(std::log2(n));
    // add configurations to list
    for(unsigned int i = 0; i < levels; i++){
        unsigned int steps = std::pow(2, i);
        rw::math::Q step = dq / steps;
        for(unsigned int j = 0; j < steps; j++){
            rw::math::Q nextQ = from + (j + 0.5) * step;
            if(checkConfiguration(nextQ)){
                return true;
            }
        }
    }
    return false;
}


bool rovi::EBinChecker::checkCollision(rw::math::Q &from, rw::math::Q &to, double &epsilon){
    // dq
    rw::math::Q dq = to - from;
    // number of configurations
    unsigned int n = (dq.norm2() / epsilon);
    // levels
    unsigned int levels = std::ceil(std::log2(n));
    // number of divisions made
    unsigned int divisions = std::pow(2,levels) - 1;
    // expansion of dq
    double expansion = (double)divisions / (double)n;
    // expanded dq
    rw::math::Q edq = dq * expansion;
    // add configurations to list
    for(unsigned int i = 0; i < levels; i++){
        unsigned int steps = std::pow(2, i);
        rw::math::Q step = edq / steps;
        for(unsigned int j = 0; j < steps; j++){
            rw::math::Q nextQ = (j + 0.5) * step;
            // check if step exceeds the wanted point
            if(nextQ.norm2() >= dq.norm2()){
                break;
            }
            // increment counter of number of colision checks
            nextQ += from;
            if(checkConfiguration(nextQ)){
                return true;
            }
        }
    }
    return false;
}


