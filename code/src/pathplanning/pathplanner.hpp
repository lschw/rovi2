#pragma once

#include "rw/models/WorkCell.hpp"
#include "rw/kinematics/State.hpp"
#include "rw/models/Device.hpp"
#include "rw/proximity/CollisionDetector.hpp"
#include "rw/pathplanning/PlannerConstraint.hpp"
#include "rw/pathplanning/QSampler.hpp"
#include "rw/math/Q.hpp"
#include "rw/invkin/JacobianIKSolver.hpp"
#include "rw/kinematics/MovableFrame.hpp"
#include "collisionchecker.hpp"
#include "pathoptimizer.hpp"
#include "PRMPlanner.hpp"
#include "rw/trajectory/Path.hpp"

namespace rovi {

/**
 * @brief The LinearPath class Generates a path of Vector3D that is linear with respect position, velocity or acceleration.
 */
class PathPlanner {
public:
    rw::models::WorkCell::Ptr _work_cell;
private:
    rw::kinematics::State _state;
    rw::models::Device::Ptr _device;
    rw::proximity::CollisionDetector _detector;
public:
    rw::pathplanning::PlannerConstraint _constraint;
private:
    rw::pathplanning::QSampler::Ptr _sampler;
private:
    double _resolution;
//    rw::kinematics::MovableFrame *_constraint_object;
    rovi::EBinChecker _coll_checker;
    rovi::pathOptimizer _path_optimizer;
    rovi::pathplanners::PRMPlanner _planner;
public:
    void move_constraint(rw::math::Transform3D<double> new_position);
    void updateState(rw::kinematics::State &state);
    PathPlanner(std::string work_cell_file, std::string device_name, std::string constraint_name, size_t nodes=10000);
    PathPlanner(rw::models::WorkCell::Ptr wc, std::string device_name, std::string constraint_name, size_t nodes=10000);

    rw::trajectory::QPath plan(const rw::math::Q &from, const rw::math::Q &to, const bool optimize=false);
    ~PathPlanner(){ }
    void write(){_planner.write_graph_to_file();}
    size_t size(){return _planner.size();}
};
} //end namespace
