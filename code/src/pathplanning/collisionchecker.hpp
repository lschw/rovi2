#pragma once

#include <cmath>

#include <rw/common.hpp>
#include <rw/math/Q.hpp>
#include <rw/models.hpp>
#include <rw/kinematics/State.hpp>
#include <rw/proximity/CollisionDetector.hpp>
#include <rwlibs/proximitystrategies/ProximityStrategyFactory.hpp>


//    Implement the four test strategies described in the document, create a small
//    program generating random collision free configurations and tests the edges
//    between them.

namespace rovi {

class CollisionChecker
{
public:
    CollisionChecker(rw::kinematics::State &state, rw::models::Device::Ptr &device, rw::proximity::CollisionDetector *detector);

    // virtual checker, dependent on method
    virtual bool checkCollision(rw::math::Q &from, rw::math::Q &to, double &epsilon);
    CollisionChecker(){}
    ~CollisionChecker(){}

protected:
    // common function
    bool checkConfiguration(rw::math::Q &configuration);


    // data
    rw::models::Device::Ptr _device;
    rw::kinematics::State _state;
    rw::proximity::CollisionDetector *_detector;


};


class SFTChecker : public CollisionChecker
{
public:

    SFTChecker(rw::kinematics::State &state, rw::models::Device::Ptr &device, rw::proximity::CollisionDetector *detector)
        : CollisionChecker(state, device, detector){}
    ~SFTChecker(){}

/**
 * @brief configurationDivision_SFT Generates a path to test using Straight Forward Approach
 * @param from
 * @param to
 * @param epsilon
 * @param checklist
 */
    bool checkCollision(rw::math::Q &from, rw::math::Q &to, double &epsilon);
};

class USSChecker : public CollisionChecker
{
public:

    USSChecker(rw::kinematics::State &state, rw::models::Device::Ptr &device, rw::proximity::CollisionDetector *detector)
        : CollisionChecker(state, device, detector){}
    ~USSChecker(){}
/**
 * @brief configurationDivision_USS Generates a path to test using Uniform Step Size
 * @param from
 * @param to
 * @param epsilon
 * @param checklist
 */
    bool checkCollision(rw::math::Q &from, rw::math::Q &to, double &epsilon);
};

class BinChecker : public CollisionChecker
{
public:

    BinChecker(rw::kinematics::State &state, rw::models::Device::Ptr &device, rw::proximity::CollisionDetector *detector)
        : CollisionChecker(state, device, detector){}
    ~BinChecker(){}

/**
 * @brief configurationDivision_Bin Generates a path to test using Binary
 * @param from
 * @param to
 * @param epsilon
 * @param checklist
 */
    bool checkCollision(rw::math::Q &from, rw::math::Q &to, double &epsilon);
};

class EBinChecker : public CollisionChecker
{
public:

    EBinChecker(rw::kinematics::State &state, rw::models::Device::Ptr &device, rw::proximity::CollisionDetector *detector)
        : CollisionChecker(state, device, detector){}
    ~EBinChecker(){}

/**
 * @brief configurationDivision_EBin Generates a path to test using Extended Binary
 * @param from
 * @param to
 * @param epsilon
 * @param checklist
 */
    bool checkCollision(rw::math::Q &from, rw::math::Q &to, double &epsilon);
};

}
