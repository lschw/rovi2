SET(SRC_FILES
	${SRC_FILES}
        ${CMAKE_CURRENT_SOURCE_DIR}/pathplanner.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/collisionchecker.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/pathoptimizer.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/KDTreeQ.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/PRMPlanner.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/constraintgenerator.cpp
	CACHE INTERNAL ""
)
