#include "kalman.hpp"
#include <Eigen/Cholesky>


//void rovi::Kalman::addSample(rw::math::Transform3D<double> &pose, rw::math::Transform3D<double> &uncertainity){

//}

//rw::math::Transform3D<double> rovi::Kalman::predictPose(const double &time){

//}

//rw::math::Transform3D<double> rovi::Kalman::predictSpeed(const double &time){

//}

//rw::math::Transform3D<double> rovi::Kalman::predictVelocity(const double &time){

//}

rovi::Kalman::Kalman()
    :Kalman(0.05, 0.05)
{

}

rovi::Kalman::Kalman(const double acceleration_delta, double dt)
    :
      _state_vector_predicted(Vector9d::Zero()),
      _state_vector_updated(Vector9d::Zero()),
      _state_transition_matrix(),
      _delta_t(dt),
      _process_noise_covariance(Matrix9d::Identity()),
      _state_estimate_covariance_predicted(Matrix9d::Zero()),
      _state_estimate_covariance_updated(Matrix9d::Zero()),
      _measurement_matrix(Matrix3by9d::Identity()),
      _measurement_noise_covariance(Matrix3d::Identity()),
      _innovation_covariance(Matrix3d::Identity()),
      _kalman_gain(Matrix9by3d::Zero()),
      _kalman_gain_is_optimal(true),
      _temp_H_times_Ppre(Matrix3by9d::Zero())
{
    Kalman::setSamplingPeriod(_delta_t);

    const double acceleration_noise_std_deviation = 0.5*acceleration_delta; //Choose between [ delta_a/2 , delta_a ].
    double sigsqx = std::pow(acceleration_noise_std_deviation,2);
    double sigsqy = sigsqx;
    double sigsqz = sigsqx;
    double dt2 = std::pow(_delta_t,2);
    double dt3 = std::pow(_delta_t,3);
    double dt4 = std::pow(_delta_t,4);
    _process_noise_covariance << sigsqx*dt4/4.0, 0, 0, sigsqx*dt3/2.0, 0, 0, sigsqx*dt2/2.0, 0, 0,
            0, sigsqy*dt4/4.0, 0, 0, sigsqy*dt3/2.0, 0, 0, sigsqy*dt2/2.0, 0,
            0, 0, sigsqz*dt4/4.0, 0, 0, sigsqz*dt3/2.0, 0, 0, sigsqz*dt2/2.0,
            sigsqx*dt3/2.0, 0, 0, sigsqx*dt2, 0, 0, sigsqx*_delta_t, 0, 0,
            0, sigsqy*dt3/2.0, 0, 0, sigsqy*dt2, 0, 0, sigsqy*_delta_t, 0,
            0, 0, sigsqz*dt3/2.0, 0, 0, sigsqz*dt2, 0, 0, sigsqz*_delta_t,
            sigsqx*dt2/2.0, 0, 0, sigsqx*_delta_t, 0, 0, sigsqx, 0, 0,
            0, sigsqy*dt2/2.0, 0, 0, sigsqy*_delta_t, 0, 0, sigsqy, 0,
            0, 0, sigsqz*dt2/2.0, 0, 0, sigsqz*_delta_t, 0, 0, sigsqz;

    //    setProcessNoiseCovarianceMatrix(Q);
}

void rovi::Kalman::setInitialState(const rovi::Vector9d &state, const Matrix3d &R, const size_t stabilization_iterations) {
    _state_vector_updated=state;
    setMeasurementNoiseCovarianceMatrix(Matrix3d::Zero());
    this->predict();
    for(size_t i=0; i<stabilization_iterations; ++i) {
        this->update(state.head(3));
        this->predict();
    }
    this->update(state.head(3),R);
    for(size_t i=0; i<stabilization_iterations-1; ++i) {
        this->predict();
        this->update(state.head(3));
    }
}

void rovi::Kalman::setSamplingPeriod(const double delta_t) {
    _delta_t = delta_t;
    const double dtdthalf=0.5*_delta_t*_delta_t;
    _state_transition_matrix
            << 1,0,0,_delta_t,0,0,dtdthalf,0,0,
            0,1,0,0,_delta_t,0,0,dtdthalf,0,
            0,0,1,0,0,_delta_t,0,0,dtdthalf,
            0,0,0,1,0,0,_delta_t,0,0,
            0,0,0,0,1,0,0,_delta_t,0,
            0,0,0,0,0,1,0,0,_delta_t,
            0,0,0,0,0,0,1,0,0,
            0,0,0,0,0,0,0,1,0,
            0,0,0,0,0,0,0,0,1;
}

void rovi::Kalman::setProcessNoiseCovarianceMatrix(const rovi::Matrix9d &Q) {
    _process_noise_covariance = Q;
}

void rovi::Kalman::setMeasurementNoiseCovarianceMatrix(const Eigen::Matrix3d &R) {
    _measurement_noise_covariance = R;
}

const rovi::Vector9d &rovi::Kalman::predict() {
    //x_(k|k-1)=F*x_(k-1|k-1)
    _state_vector_predicted = _state_transition_matrix*_state_vector_updated;

    //P_(k|k-1)=F*P_(k-1|k-1)*F^T + Q
    //evaluated in two steps:
    //http://eigen.tuxfamily.org/dox/TopicWritingEfficientProductExpression.html
    //P_(k|k-1)=temp=Q
    _state_estimate_covariance_predicted = _process_noise_covariance;
    //temp += F*P_(k-1|k-1)*F^T
    _state_estimate_covariance_predicted.noalias()
            += _state_transition_matrix*
            _state_estimate_covariance_updated*
            _state_transition_matrix.transpose();

    //We no longer need the old estimates, so we might as well update
    //the a posteriori estimates to be equal to the a priori estimates.
    //This allows us to skip the update step if we miss
    //a measurement (if the ball is not detected)
    _state_vector_updated = _state_vector_predicted;
    _state_estimate_covariance_updated = _state_estimate_covariance_predicted;

    return _state_vector_predicted;
}

const rovi::Vector9d &rovi::Kalman::predict(const double delta_t) {
    setSamplingPeriod(delta_t);
    return predict();
}

const rovi::Vector9d &rovi::Kalman::update(const Eigen::Vector3d &measurement_xyz) {
    // Get temporaty matrix H * P_(k|k-1)
    // to be used in three other calculations below.
    _temp_H_times_Ppre = _measurement_matrix*_state_estimate_covariance_predicted;

    // Innovation covariance S_(k) = H*P_(k|k-1)*H^T + R
    // in two steps for optimal Eigen evaluation
    _innovation_covariance = _measurement_noise_covariance; //=R
    // @todo: Just take upper left 3 by 3 elements and add them:
    // Actually, Eigen already optimizes this...
    _innovation_covariance.noalias() +=
            _temp_H_times_Ppre*                     //H * P_(k|k-1)
            _measurement_matrix.transpose();        //H^T

    // Kalman gain calculation
    // --------------------------
    // Dropping suffixes for readability.
    // S is S_(k), P is P_(k|k-1) and K is K_(k)
    // S = H * P * H^T + R
    // where P and R are positive semidefinite, so S i as well.
    // That means S = S^T.
    // K = P * H^T * S^-1
    // Now define
    // X^T := H^T * S^-1
    // then we have:
    // K = P * X^T
    // Multiply X^T := H^T * S^-1 by S on both sides:
    // X^T * S = H^T
    // Take transpose on both sides
    // S^T * X = H
    // Remember S^T is S
    // S * X = H
    // This should be really easy to solve for X
    // with Cholesky decomposition of S (LDLT in Eigen).
    // So the design matrix (LHS) is S, and the RHS is H.
    //_kalman_gain
    //        = _state_estimate_covariance_predicted *
    //        _innovation_covariance.ldlt().solve(_measurement_matrix).transpose();
    //-----------------------ALTERNATIVELY-----------------------------------------
    // the whole thing at once,
    // where  K = P * H^T * S^-1 as before, so that
    // S * K^T = H * P
    // simply solve for K^T with Cholesky decomposition (LDLT) and
    // take the transpose
    // We use this one because we already have H*P_(k|k-1) in a temp. matrix.
    _kalman_gain
            = _innovation_covariance.ldlt().solve(
                _temp_H_times_Ppre).transpose();

    // Update state estimate with Kalman gain * measurement residual
    // x_(k|k) = x_(k|k-1) + K_(k) * ( z - H * x_(k|k-1) )
    // @todo: instead of H*x_(k|k-1), just take upper 3 elements of x_(k|k-1):
    // Actually, Eigen already optimizes this...
    _state_vector_updated = _state_vector_predicted +
            _kalman_gain * (measurement_xyz - _measurement_matrix*_state_vector_predicted);

    // Update state estimate covariance with Kalman gain
    // P_(k|k) = P_(k|k-1) - K_(k) * H * P_(k|k-1)
    //         = (I - K_(k) * H) * P_(k|k-1)
    _state_estimate_covariance_updated = _state_estimate_covariance_predicted;
    _state_estimate_covariance_updated.noalias() -=
            _kalman_gain * _temp_H_times_Ppre;
    if(!_kalman_gain_is_optimal) {
        // Joseph form
        // P_(k|k) = ( I - K_(k) * H ) * P_(k|k-1) * ( I - K_(k) * H )^T + K_(k) * R_(k) * K_(k)^T
        // we have   ( I - K_(k) * H ) * P_(k|k-1) from above.
        // Now we multiply with ( I - K_(k) * H )^T
        _state_estimate_covariance_updated = _state_estimate_covariance_updated *
                (Matrix9d::Identity()-_kalman_gain*_measurement_matrix).transpose();
        // and add K_(k) * R_(k) * K_(k)^T
        _state_estimate_covariance_updated.noalias() +=
                _kalman_gain*_measurement_noise_covariance*_kalman_gain.transpose();
    }


    return _state_vector_updated;
}

const rovi::Vector9d &rovi::Kalman::update(const Eigen::Vector3d &measurement_xyz, const Eigen::Matrix3d &R) {
    setMeasurementNoiseCovarianceMatrix(R);
    return update(measurement_xyz);
}

const rovi::Vector9d &rovi::Kalman::getStatePredicted() const {
    return _state_vector_predicted;
}

const rovi::Vector9d &rovi::Kalman::getStateUpdated() const {
    return _state_vector_updated;
}

const rovi::Vector9d rovi::Kalman::guessFuture(const double delta_t) const {
    Matrix9d state_transition_matrix;
    double dtdthalf=0.5*delta_t*delta_t;
    state_transition_matrix
            << 1,0,0,delta_t,0,0,dtdthalf,0,0,
            0,1,0,0,delta_t,0,0,dtdthalf,0,
            0,0,1,0,0,delta_t,0,0,dtdthalf,
            0,0,0,1,0,0,delta_t,0,0,
            0,0,0,0,1,0,0,delta_t,0,
            0,0,0,0,0,1,0,0,delta_t,
            0,0,0,0,0,0,1,0,0,
            0,0,0,0,0,0,0,1,0,
            0,0,0,0,0,0,0,0,1;
    Vector9d state_vector_future = state_transition_matrix*_state_vector_updated;
    return std::move(state_vector_future);
}
