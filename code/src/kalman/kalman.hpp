#pragma once

//#include <vector>
//#include <rw/math/Transform3D.hpp>


#include <Eigen/Core>

#include <utility>

#include "../utils/defines_matrices.hpp"

namespace rovi {

class Kalman {
protected:
    Vector9d _state_vector_predicted;               //x_(k|k-1)
    Vector9d _state_vector_updated;                 //x_(k|k) (The important output of the filter)
    Matrix9d _state_transition_matrix;              //F
    double  _delta_t;
    Matrix9d _process_noise_covariance;             //Q
    Matrix9d _state_estimate_covariance_predicted;  //P_(k|k-1)
    Matrix9d _state_estimate_covariance_updated;    //P_(k|k)

    const Matrix3by9d _measurement_matrix;          //H
    Matrix3d _measurement_noise_covariance;         //R_(k) //@todo Is R constant?

    Matrix3d _innovation_covariance;                //S_(k)

    Matrix9by3d _kalman_gain;                       //K_(k)
    bool _kalman_gain_is_optimal;

    Matrix3by9d _temp_H_times_Ppre;
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    /**
     * @brief Kalman Default constructor
     */
    Kalman();

    Kalman(const double acceleration_delta, double dt = 0.05);

    virtual void setInitialState(const Vector9d &state, const Matrix3d &R, const size_t stabilization_iterations=1);


    /**
     * @brief ~Kalman Destructor
     */
    virtual ~Kalman() { }

    /**
     * @brief setSamplingPeriod Sets sampling period and updates state transition matrix F.
     * @param delta_t New sampling period.
     */
    virtual void setSamplingPeriod(const double delta_t);

    /**
     * @brief setProcessNoiseCovarianceMatrix Sets Q.
     * @param Q The process noise covariance matrix.
     */
    virtual void setProcessNoiseCovarianceMatrix(const Matrix9d &Q);

    /**
     * @brief setMeasurementNoiseCovarianceMatrix Sets R.
     * @param R The measurement noise covariance matrix.
     */
    virtual void setMeasurementNoiseCovarianceMatrix(const Matrix3d &R);

    /**
     * @brief predict Predicts state vector x_(k|k-1) and the covariance matrix P_(k|k-1).
     * @return Reference to predicted state vector x_(k|k-1)
     * Also sets updated state vector x_(k|k) = x_(k|k-1)
     * and updated covariance P_(k|k) = P_(k|k-1)
     * so that you don't have to call update() if you don't have a new measurement.
     */
    virtual const Vector9d& predict();

    /**
     * @brief predict Modifies the sampling period and predicts state as usual.
     * @param delta_t New sampling period.
     * @return Reference to predicted state vector x_(K|k-1)
     */
    virtual const Vector9d& predict(const double delta_t);

    /**
     * @brief update Updates state and covariance estimate with a new measurement.
     * @param measurement_xyz New measurement.
     * @return Reference to updated state vector x_(k|k).
     */
    virtual const Vector9d& update(const Vector3d &measurement_xyz);

    /**
     * @brief update Updates state and covariance estimate with a new measurement and a new measurement covariance matrix.
     * @param measurement_xyz New measurement
     * @param R Covariance matrix of new measurement.
     * @return Reference to updated state vector x_(k|k).
     */
    virtual const Vector9d& update(const Vector3d &measurement_xyz, const Matrix3d &R);

    /**
     * @brief getStatePredicted Gets latest prediction output without modifying state.
     * @return Prediction
     */
    virtual const Vector9d &getStatePredicted() const;

    /**
     * @brief getStateUpdated Gets latest update output without modifying state.
     * @return Update
     */
    virtual const Vector9d &getStateUpdated() const;




    /**
     * @brief guessFuture Guesses a state based on a possible large delta_t. Does not modify anything.
     * @param delta_t Time from now in which we want to know the state.
     * @return State vector corresponding to one naïve Euler integration step.
     * @todo Check if this is even usable.
     */
    virtual const Vector9d guessFuture(const double delta_t) const;

};


//class Kalman
//{
//public:
//    Kalman() {}

//    // setters
//    void setSamplingPeriod(const double &samplerate){
//        _sampling_period = samplerate;
//    }

//    // sample adder
//    void addSample(rw::math::Transform3D<double> &pose, rw::math::Transform3D<double> &uncertainity);

//    // predictors, predicts relative to last added sample
//    rw::math::Transform3D<double> predictPose(const double &time);

//    rw::math::Transform3D<double> predictSpeed(const double &time);

//    rw::math::Transform3D<double> predictVelocity(const double &time);


//protected:
//    // sample rate
//    // maybe we should use a vector of timestamps?
//    double _sampling_period;

//    // list of observed positions of the object
//    std::vector< rw::math::Transform3D<double> > _pose;

//    // uncertainities of the points in _pose
//    // .P() gives the uncertainity, .R() gives the directions of the uncertainity relative to the pose.
//    std::vector< rw::math::Transform3D<double> > _uncertainity;

//};

}
