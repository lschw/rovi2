#pragma once
#include <rw/math/Transform3D.hpp>
#include <rw/math/EAA.hpp>
#include <deque>
#include <exception>
#include <utility>
#include <cmath>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/QR>
#include <eigen3/Eigen/SVD>
#include <eigen3/Eigen/Eigenvalues>
#include <eigen3/unsupported/Eigen/KroneckerProduct>

#include <pcl/point_types.h>
#include <pcl/registration/transformation_estimation_svd.h>


#include "utils/defines_matrices.hpp"

////How to load an Eigen matrix
////from a text file, where numbers
////are separated by whitespace.
//namespace Eigen{
//template<typename Derived>
//std::istream & operator >> (std::istream & s, MatrixBase<Derived> & m) {
//    for (int i = 0; i < m.rows(); ++i)
//        for (int j = 0; j < m.cols(); j++)
//            s >> m(i,j);
//    return s;
//}
//}
//also works, but probably slower.
//namespace Eigen{
//template<typename Derived>
//std::istream & operator >> (std::istream & s, MatrixBase<Derived> & m) {
//    for (int i = 0; i < m.rows(); ++i) {
//        string line;
//        getline(s,line);
//        stringstream ssline(line);
//        for (int j = 0; j < m.cols(); j++)
//            ssline >> m(i,j);
//    }
//    return s;
//}
//}


//QR24 registration based on:
// Ernst et al.: Non-orthogonal tool/flange and robot/world calibration
namespace rovi {
class Registration {
protected:
    rw::math::Transform3D<double> _endTmarker; //M: End effector to marker transform
    rw::math::Transform3D<double> _worldTcam; //N: Base to camera transform
    bool _prioritizeTranslationalAccuracy;
public:
    /**
     * @brief Registration Constructor.
     * @param prioritizeTranslationalAccuracy If true, translational errors will be minimized in [mm] instead of [m] at the cost of increased rotational error.
     */
    Registration(bool prioritizeTranslationalAccuracy)
        :_endTmarker(),_worldTcam(),_prioritizeTranslationalAccuracy(prioritizeTranslationalAccuracy)
    { }

    Registration()
        :Registration(true)
    { }

    virtual ~Registration() { }

    /**
     * @brief transformFromCameraToBase Transforms a 6D pose from the camera frame to the robot world frame
     * @param pose_in_camera_frame
     * @return  Pose in robot world frame
     * @todo Is this supposed to only orthonormalize rotation? PROBABLY! I don't know what I have done!
     */
    virtual rw::math::Transform3D<double> transformFromCameraToBase(const rw::math::Transform3D<double> &pose_in_camera_frame) const {
        //Call the camera frame pose T
        const rw::math::Transform3D<double> &T=pose_in_camera_frame;

        //Calculate NT  = baseTcam*T and convert to Eigen matrix
        //MT is the non-orthonormal transform from camera frame to robot world frame.
        const rw::math::Transform3D<double> rwNT=_worldTcam*T;
        Eigen::Matrix4d MT;
        MT << rwNT.R().e(), rwNT.P().e(),
                0.0, 0.0, 0.0, 1.0;

        //Calculate SVD of non-orthonormal transform NT
        const Eigen::JacobiSVD<Eigen::Matrix4d,Eigen::NoQRPreconditioner> svdNT( MT, Eigen::ComputeFullU | Eigen::ComputeFullV );

        //Calculate orthonormal transform orthNT as U*V^T
        const Eigen::Matrix4d orthMT = (svdNT.matrixU()*svdNT.matrixV().transpose());
        rw::math::Transform3D<double> rwOrthNT(rw::math::Vector3D<double>(orthMT.block<3,1>(0,3)));
        for(size_t i=0; i<3; ++i)
            for(size_t j=0; j<3; ++j)
                rwOrthNT(i,j)=orthMT(i,j);
        return rwOrthNT;
    }

    static Eigen::Matrix4d rwTransform3dToEigenMatrix(const rw::math::Transform3D<double> &rwT) {
        Eigen::Matrix4d eigTransform;
        eigTransform << rwT(0,0), rwT(0,1), rwT(0,2), rwT(0,3),
                rwT(1,0), rwT(1,1), rwT(1,2), rwT(1,3),
                rwT(2,0), rwT(2,1), rwT(2,2), rwT(2,3),
                rwT(3,0), rwT(3,1), rwT(3,2), rwT(3,3);
        return std::move(eigTransform);
    }

    static rw::math::Transform3D<double> eigenMatrixToRwTransform3d(const Eigen::Matrix4d &eigM) {
        return rw::math::Transform3D<double>(
                    rw::math::Vector3D<double>(eigM(0,3),eigM(1,3),eigM(2,3)),
                    rw::math::Rotation3D<double>(
                        eigM(0,0),eigM(0,1),eigM(0,2),
                        eigM(1,0),eigM(1,1),eigM(1,2),
                        eigM(2,0),eigM(2,1),eigM(2,2))
                    );
    }

    /**
     * @brief transformOrthogonalize Calculates closest orthogonal approximation to transform*pose.
     * @param transform
     * @param pose
     * @return
     */
    static rw::math::Transform3D<double> transformOrthogonalize(const rw::math::Transform3D<double> &transform, const rw::math::Transform3D<double> &pose) {
        const rw::math::Transform3D<double> TP=transform*pose;
        Eigen::Matrix4d TPe;
        TPe << TP.R().e(), TP.P().e(),
                0.0,0.0,0.0,1.0;
        std::cout << "!!!\n" << TPe << "\n";
        const Eigen::JacobiSVD<Eigen::Matrix4d,Eigen::NoQRPreconditioner> svdTP(TPe,Eigen::ComputeFullU|Eigen::ComputeFullV);
        const Eigen::Matrix4d orthTP = svdTP.matrixU()*(svdTP.matrixV().transpose());
        std::cout << "...\n" << orthTP << "\n";
        rw::math::Transform3D<double> rwOrthTP(rw::math::Vector3D<double>(orthTP.block<3,1>(0,3)));
        for(size_t i=0; i<3; ++i)
            for(size_t j=0; j<3; ++j)
                rwOrthTP(i,j)=orthTP(i,j);
        return rwOrthTP;
    }

    static Eigen::Matrix3d nearestOrthonormalRotation(const Eigen::Matrix3d rot) {
        const Eigen::JacobiSVD<Eigen::Matrix3d,Eigen::NoQRPreconditioner> svd(rot,Eigen::ComputeFullU|Eigen::ComputeFullV);
        const double d = (svd.matrixU()*(svd.matrixV().transpose())).determinant();
        Eigen::Matrix3d sigOpt = Eigen::Matrix3d::Identity();
        sigOpt(2,2)=d;
        const Eigen::Matrix3d orthRot = svd.matrixU()*sigOpt*(svd.matrixV().transpose());
        return std::move(orthRot);
    }

    /**
     * @brief transformFromCameraToBase Translate camera frame position into robot world frame position
     * @param position_in_camera_frame 3D coordinate in camera coordinates
     * @return 3D coordinate in robot base coordinates
     * @todo Subtract or add?
     */
    virtual Eigen::Vector3d transformFromCameraToBase(const Eigen::Vector3d &position_in_camera_frame) const {
        return position_in_camera_frame-_worldTcam.P().e();
    }

    /**
     * @brief calibrate_QR24 Uses QR24 method to calculate endTmarker and baseTcam transforms from measurements.
     * @param worldTend End effector pose in world frame (obtained from forward kinematics)
     * @param camTmarker Camera to marker transform (find chessboard coordinate system in rectified camera frame)
     */
    void calibrate_QR24(const std::deque<rw::math::Transform3D<double> > &worldTend, const std::deque<rw::math::Transform3D<double> > &camTmarker) {
        auto &R = worldTend;
        auto &T = camTmarker;
        const size_t n=R.size();
        if(n!=T.size())
            throw std::runtime_error("Registration: worldTend and camTmarker are not the same size.");

        //Now solve:
        // R_i * M = N * T_i
        //
        // Start by rewriting to
        // Ax=b
        //
        // A is 12n by 24
        // b is 12n by 1
        // x is 24 by 1
        Eigen::Matrix<double,Eigen::Dynamic,24> A(12*n,24);
        Eigen::VectorXd b(12*n);
        Eigen::Matrix<double,24,1> x;

        for(size_t i=0; i<n; ++i) {
            const Eigen::Matrix3d RRi = R[i].R().e();
            rw::math::Transform3D<double> Ti = T[i];
            Ti.P() *= (_prioritizeTranslationalAccuracy?1000.0:1.0);
            const Eigen::Vector3d TRi = (_prioritizeTranslationalAccuracy?1000.0:1.0)*R[i].P().e();
            std::cout << "TRi: " << TRi.transpose() << "\n";
            Eigen::Matrix<double,12,24> Ai;
            Eigen::Matrix<double,12,1> bi;
            Ai.block<3,3>(0,0)=Ti(0,0)*RRi;
            Ai.block<3,3>(0,3)=Ti(1,0)*RRi;
            Ai.block<3,3>(0,6)=Ti(2,0)*RRi;
            Ai.block<3,3>(3,0)=Ti(0,1)*RRi;
            Ai.block<3,3>(3,3)=Ti(1,1)*RRi;
            Ai.block<3,3>(3,6)=Ti(2,1)*RRi;
            Ai.block<3,3>(6,0)=Ti(0,2)*RRi;
            Ai.block<3,3>(6,3)=Ti(1,2)*RRi;
            Ai.block<3,3>(6,6)=Ti(2,2)*RRi;
            Ai.block<3,3>(9,0)=Ti(0,3)*RRi;
            Ai.block<3,3>(9,3)=Ti(1,3)*RRi;
            Ai.block<3,3>(9,6)=Ti(2,3)*RRi;
            Ai.block<9,3>(0,9).setZero();//=Eigen::Matrix<double,9,3>::Zero();
            Ai.block<3,3>(9,9)=RRi;
            Ai.block<12,12>(12,12)=-Eigen::Matrix<double,12,12>::Identity().eval();
            bi.head<9>() = Eigen::Matrix<double,9,1>::Zero();
            bi.tail<3>() = -TRi;

            A.block<12,24>(i*12,0)=Ai;
            b.segment<12>(i*12)=bi;
        }
        //Use QR decomposition to minimize quadratic error ||R_i * M - N * T_i||Fb  (Frobenius norm)
        //and obtain non-orthonormal solution (intended)
        //x = A.fullPivHouseholderQr().solve(b);
        Eigen::FullPivHouseholderQR<Eigen::Matrix<double,Eigen::Dynamic,24> > svdA(A);
        x=svdA.solve(b);

        std::cout << "A:\n" << A << "\n\nb:\n" << b << "\n\nx:\n" << x << std::endl;

        //x = M11, M21, M31, ... , M34, N11, N21, N31, ... , N34
        for(size_t i=0; i<12; ++i) {
            _endTmarker(i/4,i%4)=x(i)*(((i%4==3)&&_prioritizeTranslationalAccuracy)?(0.001):(1.0));
            _worldTcam(i/4,i%4)=x(i+12)*(((i%4==3)&&_prioritizeTranslationalAccuracy)?(0.001):(1.0));
        }
        std::cout << "endTmarker:" << std::endl;
        std::cout << _endTmarker << std::endl;
        std::cout << "worldTcam:" << std::endl;
        std::cout << _worldTcam << std::endl;
    }

    virtual void calibrate_QR24Eig(const Eigen::Matrix4Xd &worldTend, const Eigen::Matrix4Xd &camTmarker) {
        std::deque<rw::math::Transform3D<double> > worldTendRW(worldTend.cols()/4);
        std::deque<rw::math::Transform3D<double> > camTmarkerRW(worldTend.cols()/4);
        for(size_t i=0; i<worldTendRW.size(); ++i) {
            worldTendRW[i]=eigenMatrixToRwTransform3d(worldTend.block<4,4>(0,4*i));
            camTmarkerRW[i]=eigenMatrixToRwTransform3d(camTmarker.block<4,4>(0,4*i));
        }
        calibrate_QR24(worldTendRW,camTmarkerRW);
    }



    /**
     * @brief getAB_worldTcam Gets A and B for AX=XB with X = worldTcam
     * @param worldTend Horizontally concatenated worldTend poses
     * @param camTmarker Horizontally concatenated camTmarker poses
     * @return A (first), B (second)
     */
    static std::pair<Eigen::Matrix4Xd,Eigen::Matrix4Xd> getAB_worldTcam(const Eigen::Matrix4Xd &worldTend, const Eigen::Matrix4Xd &camTmarker) {
        //        endTworld1 * worldTcam * camTmarker1 = endTworld2 * worldTcam * camTmarker2
        //        =>
        //        inverse(endTworld2) * endTworld1 * worldTcam = worldTcam * camTmarker2 * inverse(camTmarker1)
        //        worldTend2 * endTworld1 * worldTcam = worldTcam * camTmarker2 * markerTcam1
        //        AX=XB
        //        A = inverse(endTworld2) * endTworld1
        //        B = camTmarker2 * inverse(camTmarker1)
        const size_t n=worldTend.cols()/4;
        Eigen::Matrix4Xd A(4,4*(n-1));
        Eigen::Matrix4Xd B(4,4*(n-1));

        for(size_t i=0; i<(n-1); ++i) {
            Eigen::Matrix4d endTworld_i;
            endTworld_i << worldTend.block<3,3>(0,4*i).transpose(), -worldTend.block<3,3>(0,4*i).transpose()*worldTend.block<3,1>(0,4*i+3),
                    0,0,0,1;
            Eigen::Matrix4d markerTcam_i;
            markerTcam_i << camTmarker.block<3,3>(0,4*i).transpose(), -camTmarker.block<3,3>(0,4*i).transpose()*camTmarker.block<3,1>(0,4*i+3),
                    0,0,0,1;
            A.block<4,4>(0,4*i)=worldTend.block<4,4>(0,4*(i+1))*endTworld_i; //Aij
            B.block<4,4>(0,4*i)=camTmarker.block<4,4>(0,4*(i+1))*markerTcam_i; //Bij
        }

        return std::make_pair(A,B);
    }

    /**
     * @brief getAB_endTmarker Gets A and B for AX=XB with X = endTmarker
     * @param worldTend Horizontally concatenated worldTend poses
     * @param camTmarker Horizontally concatenated camTmarker poses
     * @return A (first), B (second)
     */
    static std::pair<Eigen::Matrix4Xd,Eigen::Matrix4Xd> getAB_endTmarker(const Eigen::Matrix4Xd &worldTend, const Eigen::Matrix4Xd &camTmarker) {
        //        worldTend1 * endTmarker * markerTcam1 = worldTend2 * endTmarker * markerTcam2
        //        inverse(worldTend2) * worldTend1 * endTmarker = endTmarker * markerTcam2 * inverse(markerTcam1)
        //        endTworld2 * worldTend1 * endTmarker = endTmarker * markerTcam2 * camTmarker1
        //        AX=XB
        //        A = inverse(worldTend2) * worldTend1
        //        B = markerTcam * inverse(markerTcam1)
        const size_t n=worldTend.cols()/4;
        Eigen::Matrix4Xd A(4,4*(n-1));
        Eigen::Matrix4Xd B(4,4*(n-1));

        for(size_t i=0; i<(n-1); ++i) {
            Eigen::Matrix4d endTworld_j;
            endTworld_j << worldTend.block<3,3>(0,4*(i+1)).transpose(), -worldTend.block<3,3>(0,4*(i+1)).transpose()*worldTend.block<3,1>(0,4*(i+1)+3),
                    0,0,0,1;
            Eigen::Matrix4d markerTcam_j;
            markerTcam_j << camTmarker.block<3,3>(0,4*(i+1)).transpose(), -camTmarker.block<3,3>(0,4*(i+1)).transpose()*camTmarker.block<3,1>(0,4*(i+1)+3),
                    0,0,0,1;
            A.block<4,4>(0,4*i) = endTworld_j*worldTend.block<4,4>(0,4*i); //Aij
            B.block<4,4>(0,4*i) = markerTcam_j*camTmarker.block<4,4>(0,4*i); //Bij
        }

        return std::make_pair(A,B);
    }




    /**
     * @brief calibrate_Shah Finds worldTcam and endTmarker using the Shah method of Kronecker products.
     * @param worldTend 4 by 4n matrix of n horizontally concatenated homogeneous transformation matrices.
     * @param camTmarker 4 by 4n matrix of n horizontally concatenated homogeneous transformation matrices.
     * @see M. Shah: Solving the Robot-World/Hand-Eye Calibration Problem Using the Kronecker Product, ASME Journal of Mechanisms and Robotics, Vol. 5, Issue 3 (2013).
     * @note Due to noise, rotation matrices may not be orthonormal. Use nearestOrthonormalRotation()
     *
     * Solves AX=YB, where
     * A = worldTend
     * B = camTmarker
     * X = endTmarker
     * Y = worldTcam
     *
     * Internally, uses most stable SVD and Kronecker products.
     */
    virtual void calibrate_Shah(const Eigen::Matrix4Xd &worldTend, const Eigen::Matrix4Xd &camTmarker) {
        auto &A = worldTend;
        auto &B = camTmarker;
        const size_t n=worldTend.cols()/4;

        //Find Rx and Ry
        Eigen::Matrix<double,9,9> K=Eigen::Matrix<double,9,9>::Zero(9,9);
        for(size_t i=0; i<n; ++i) {
            const Eigen::Matrix3d Ra = A.block<3,3>(0,4*i);
            const Eigen::Matrix3d Rb = B.block<3,3>(0,4*i);
            K += Eigen::kroneckerProduct(Rb,Ra);
        }
        const Eigen::JacobiSVD<Eigen::Matrix<double,9,9>,Eigen::FullPivHouseholderQRPreconditioner>
                svdK(K,Eigen::ComputeFullU|Eigen::ComputeFullV);
        // vx is right singular vector v_n
        Eigen::Matrix<double,9,1> vx = svdK.matrixV().block<9,1>(0,0);
        // uy is left singular vector u_n
        Eigen::Matrix<double,9,1> uy = svdK.matrixU().block<9,1>(0,0);

        //Vx
        Eigen::Matrix3d Rx = Eigen::Map<Eigen::Matrix3d>(vx.data(),3,3);
        double det=Rx.determinant();
        //alpha
        double s = ((det>0)?(1.0):(det<0?-1.0:0.0))/std::pow(std::abs(det),(1.0/3.0));
        Rx *= s;
        Eigen::JacobiSVD<Eigen::Matrix3d,Eigen::FullPivHouseholderQRPreconditioner>
                svd3x3(Rx,Eigen::ComputeFullU|Eigen::ComputeFullV);
        Rx = svd3x3.matrixU()*(svd3x3.matrixV().transpose());
        //Vy
        Eigen::Matrix3d Ry = Eigen::Map<Eigen::Matrix3d>(uy.data(),3,3);
        det=Ry.determinant();
        //beta
        s=((det>0)?(1.0):(det<0?-1.0:0.0))/std::pow(std::abs(det),(1.0/3.0));
        Ry *= s;
        svd3x3.compute(Ry);
        Ry = svd3x3.matrixU()*(svd3x3.matrixV().transpose());


        //Find translation t
        Eigen::Matrix<double,Eigen::Dynamic,6> At=Eigen::Matrix<double,Eigen::Dynamic,6>::Zero(3*n,6);
        Eigen::VectorXd bt = Eigen::VectorXd(3*n);
        const Eigen::Matrix<double,9,1> vecRy = Eigen::Map<Eigen::Matrix<double,9,1>>(Ry.data(),9,1);
        for(size_t i=0; i<n; ++i) {
            At.block<3,3>(3*i,0)=-A.block<3,3>(0,4*i);
            At.block<3,3>(3*i,3)=Eigen::Matrix3d::Identity(3,3);
            const Eigen::Matrix<double,3,9> kronBi = Eigen::kroneckerProduct((B.block<3,1>(0,4*i+3)).transpose(),Eigen::Matrix3d::Identity(3,3));
            bt.segment<3>(3*i)=A.block<3,1>(0,4*i+3)-kronBi*vecRy;
        }
        const Eigen::JacobiSVD<Eigen::Matrix<double,Eigen::Dynamic,6>,Eigen::FullPivHouseholderQRPreconditioner>
                svdAt(At,Eigen::ComputeFullU|Eigen::ComputeFullV);
        const Eigen::Matrix<double,6,1> t = svdAt.solve(bt);

        //X
        _endTmarker.P() = rw::math::Vector3D<double>(t.head<3>());
        _endTmarker.R() = rw::math::Rotation3D<double>(Rx(0,0),Rx(0,1),Rx(0,2),
                                                       Rx(1,0),Rx(1,1),Rx(1,2),
                                                       Rx(2,0),Rx(2,1),Rx(2,2));
        //Y
        _worldTcam.P() = rw::math::Vector3D<double>(t.tail<3>());
        _worldTcam.R() = rw::math::Rotation3D<double>(Ry(0,0),Ry(0,1),Ry(0,2),
                                                     Ry(1,0),Ry(1,1),Ry(1,2),
                                                     Ry(2,0),Ry(2,1),Ry(2,2));
    }

    const rw::math::Transform3D<double> &getEndTmarker() const {
        return _endTmarker;
    }
    const rw::math::Transform3D<double> &getWorldTcam() const {
        return _worldTcam;
    }

    Eigen::Matrix4d getEndTmarkerEig() const {
        Eigen::Matrix4d X;
        X << _endTmarker.R().e(), _endTmarker.P().e(),
                0,0,0,1;
        return X;
    }

    Eigen::Matrix4d getWorldTcamEig() const {
        Eigen::Matrix4d Y;
        Y << _worldTcam.R().e(), _worldTcam.P().e(),
                0,0,0,1;
        return Y;
    }

    /**
     * @brief computeErrors Computes translational and rotational errors of registration on pose dataset.
     * @param worldTend End effector pose in world frame (obtained from forward kinematics)
     * @param camTmarker Camera to marker transform (find chessboard coordinate system in rectified camera frame)
     * @return Translational Root Mean Squared Error (first) and Rotational Mean Average Error (second)
     */
    virtual std::pair<double,double> computeErrors(const std::deque<rw::math::Transform3D<double> > &worldTend, const std::deque<rw::math::Transform3D<double> > &camTmarker) const {
        auto &R = worldTend;
        auto &T = camTmarker;
        const size_t n=R.size();
        if(n!=T.size())
            throw std::runtime_error("Registration: worldTend and camTmarker are not the same size.");

        double translational_SSE = 0.0;
        double rotational_SAE = 0.0;
        for(size_t i=0; i<n; ++i) {
            rw::math::Transform3D<double> A = rw::math::inverse(_endTmarker)*rw::math::inverse(R[i])*_worldTcam*T[i];
            translational_SSE += A(0,3)*A(0,3) + A(1,3)*A(1,3) + A(2,3)*A(2,3);
            rotational_SAE += rw::math::EAA<double>(A.R()).angle();
        }
        double translational_MSE = translational_SSE/static_cast<double>(n);
        double translational_RMSE = std::sqrt(translational_MSE);
        double rotational_MAE = rotational_SAE/static_cast<double>(n);

        return std::make_pair(translational_RMSE,rotational_MAE);
    }

    virtual std::pair<double,double> computeErrorsEig(const Eigen::Matrix4Xd &worldTend, const Eigen::Matrix4Xd &camTmarker) const {
        std::deque<rw::math::Transform3D<double> > worldTendRW(worldTend.cols()/4);
        std::deque<rw::math::Transform3D<double> > camTmarkerRW(worldTend.cols()/4);
        for(size_t i=0; i<worldTendRW.size(); ++i) {
            worldTendRW[i]=eigenMatrixToRwTransform3d(worldTend.block<4,4>(0,4*i));
            camTmarkerRW[i]=eigenMatrixToRwTransform3d(camTmarker.block<4,4>(0,4*i));
        }
        return std::move(computeErrors(worldTendRW,camTmarkerRW));
    }


    /**
     * @brief estimateCamToMarkerEig Estimates transform from camera to marker
     * @param marker_markerframe Checkerboard points in marker frame
     * @param marker_cameraframe Checkerboard points in camera frame
     * @return camTmarker so that cam_points = camTmarker * marker_points and marker_points = inverse(camTmarker) * cam_points
     */
    static Eigen::Matrix4d estimateCamToMarkerEig(const Eigen::Matrix3Xd &marker_markerframe, const Eigen::Matrix3Xd &marker_cameraframe) {
        //Convert input point sets into point clouds
        pcl::PointCloud<pcl::PointXYZ> pc_markerframe, pc_cameraframe;
        pc_markerframe.points.resize(marker_markerframe.cols());
        pc_cameraframe.points.resize(marker_cameraframe.cols());
        for(int i=0; i<marker_markerframe.cols(); ++i)
            pc_markerframe.points[i].getVector3fMap() = marker_markerframe.block<3,1>(0,i).transpose().cast<float>();
        for(int i=0; i<marker_cameraframe.cols(); ++i)
            pc_cameraframe.points[i].getVector3fMap() = marker_cameraframe.block<3,1>(0,i).transpose().cast<float>();

        //Estimate the rigid transformation between point clouds using sum of squared point-to-point distances
        pcl::registration::TransformationEstimationSVD<pcl::PointXYZ,pcl::PointXYZ,float>::Matrix4 myT;
        pcl::registration::TransformationEstimationSVD<pcl::PointXYZ,pcl::PointXYZ,float> myTransformer;
        myTransformer.estimateRigidTransformation(pc_markerframe,pc_cameraframe,myT);

        return myT.cast<double>();
    }

    /**
     * @brief estimateCamToMarkerRW Estimates transform from camera to marker
     * @param marker_markerframe Checkerboard points in marker frame
     * @param marker_cameraframe Checkerboard points in camera frame
     * @return camTmarker so that cam_points = camTmarker * marker_points and marker_points = inverse(camTmarker) * cam_points
     */
    static rw::math::Transform3D<double> estimateCamToMarkerRW(const Eigen::Matrix3Xd &marker_markerframe, const Eigen::Matrix3Xd &marker_cameraframe) {
        const Eigen::Matrix4d myT = estimateCamToMarkerEig(marker_markerframe,marker_cameraframe);
        //Convert transformation matrix to RobWork format.
        rw::math::Transform3D<double> camTmarker(rw::math::Vector3D<double>(myT(0,3),myT(1,3),myT(2,3)),
                                                 rw::math::Rotation3D<double>(
                                                     myT(0,0),myT(0,1),myT(0,2),
                                                     myT(1,0),myT(1,1),myT(1,2),
                                                     myT(2,0),myT(2,1),myT(2,2)));
        return std::move(camTmarker);
    }





    /**
     * @brief loadMarkerData Loads marker points in marker frame and marker points in camera frame
     * @param num_chessboard_corners Total number of chessboard corners per image
     * @param num_image_pairs Total number of stereo image pairs represented in "stereoPoints.txt"
     * @param num_chessboard_corners marker points in marker frame
     * @return world points (first), stereo points(second)
     */
    static std::pair<Eigen::Matrix3Xd,Eigen::Matrix3Xd> loadMarkerData(const size_t num_chessboard_corners = 20, const size_t num_image_pairs=12) {
        const std::string file_world_points("worldPoints.txt");
        const std::string file_stereo_points("stereoPoints.txt");
        //const std::string file_left_points("leftPoints.txt");
        //const std::string file_right_points("rightPoints.txt");
        //const std::string file_ok_configurations("ok_configurations.txt");

        Eigen::MatrixX2d world_points(num_chessboard_corners,2);
        std::ifstream ifs(file_world_points);
        if(!ifs.is_open())
            throw std::runtime_error("Registration: Could not open worldpoints.txt");
        ifs >> world_points;
        ifs.close();

        Eigen::MatrixX3d stereo_points(num_chessboard_corners*num_image_pairs,3);
        ifs.open(file_stereo_points);
        ifs >> stereo_points;
        ifs.close();


        Eigen::MatrixX3d world_points_z0(num_chessboard_corners,3);
        world_points_z0 << world_points, Eigen::VectorXd::Zero(num_chessboard_corners);

        return std::make_pair(world_points_z0.transpose(),stereo_points.transpose());

        //        Eigen::MatrixX2d
        //                left_points(num_chessboard_corners*num_image_pairs,2),
        //                right_points(num_chessboard_corners*num_image_pairs,2);
        //        ifs.open(file_left_points);
        //        ifs >> left_points;
        //        ifs.close();
        //        ifs.open(file_right_points);
        //        ifs >> right_points;
        //        ifs.close();

        //Eigen::Matrix<double,Eigen::Dynamic,6> ok_configurations(num_image_pairs,6);
        //ifs.open(file_ok_configurations);
        //ifs >> ok_configurations;
        //ifs.close();


    }


};
} //namespace rovi
