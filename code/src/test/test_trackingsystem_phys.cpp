#include <iostream>
#include "tracker/tracker.hpp"
#include <ros/init.h>
#include <ros/macros.h>


using namespace std;


int main(int argc, char *argv[]){
//    const size_t num_input_arguments = static_cast<size_t>(argc-1);

    cout << "Tracking system" << endl;
    int dummyInt=0;
    char **dummyPtr=nullptr;
    ros::init(dummyInt, dummyPtr,"stereocamera_imgreader");
    ROS_INFO("Connected to roscore");

    rovi::Tracker ballTracker;
    cout << "Starting ball tracker in separate thread..." << endl;
    ballTracker.startTracker();
    cout << "Ball tracker is running." << endl;

    for(size_t i=0; i<20*3600; ++i) {
        //Blocks until new ball state (up to 50 ms on live system, same rate as new images coming in).
        //There is also a non-blocking version which just gets the latest state.
        const rovi::Vector9d ball_state = ballTracker.getBallBlocking();
        cout << "Ball pos: " << ball_state.segment<3>(0).transpose()
             << ", vel: " << ball_state.segment<3>(3).transpose() << endl;
    }

    cout << "Stopping ball tracker." << endl;
    ballTracker.stopTracker();
    cout << "Ball tracker stopped." << endl;

    return 0;
}
