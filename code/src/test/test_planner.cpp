#include <iostream>
#include "../pathplanning/pathplanner.hpp"
#include <chrono>
#include <rw/pathplanning/PathAnalyzer.hpp>

#include <rw/pathplanning/PlannerConstraint.hpp>
#include <rwlibs/pathoptimization/pathlength/PathLengthOptimizer.hpp>
#include <rw/math/Math.hpp>
#include <rw/math/MetricFactory.hpp>
#include <rw/loaders/WorkCellFactory.hpp>

#define TIME_AFTER(TIME) std::chrono::duration_cast<std::chrono::duration<double> >(std::chrono::high_resolution_clock::now() - TIME).count()

void print_path(rw::trajectory::QPath path, std::string robot_name="UR1"){
    std::cout << "wc = rws.getRobWorkStudio():getWorkCell()\n"
                 "state = wc:getDefaultState()\n"
                 "device = wc:findDevice(\""
              << robot_name << "\")\n"
                 "function setQ(q)\n"
                 "qq = rw.Q(#q,q[1],q[2],q[3],q[4],q[5],q[6])\n"
                 "device:setQ(qq,state)\n"
                 "rws.getRobWorkStudio():setState(state)\n"
                 "rw.sleep(0.02)\n"
                 "end\n\n";
    for (auto & it : path) {
        std::cout << "setQ({"
                  << it[0];

        for(uint i = 1; i < it.size(); i++){
            std::cout << "," << (it)[i];
        }
        std::cout << "})" << std::endl;
    }
}

double avg_distance_between_points(rw::trajectory::QPath &path){
    double dist = 0;
    for(size_t i =1; i < path.size(); ++i){
        dist += rw::math::MetricUtil::dist2Sqr(path[i-1],path[i]);
    }
    return dist / static_cast<double>(path.size()-1);
}

void analyze_planner(rovi::PathPlanner &planner, size_t nodes, rw::math::Q from, rw::math::Q to, bool enable_path_optimization=false){
    std::chrono::high_resolution_clock::time_point t1;
    std::string wcFile = "../../workcell/WorkStation3/WS3_Scene_prm.wc.xml";
    std::string robotName = "UR1";
    std::string toolName = "PG70";

    rw::trajectory::QPath path;
    rw::models::WorkCell::Ptr work_cell = rw::loaders::WorkCellLoader::Factory::load(wcFile);
    rw::kinematics::State state = work_cell->getDefaultState();
    rw::models::Device::Ptr device = work_cell->findDevice(robotName);
    rw::kinematics::Frame *tool( work_cell->findFrame(toolName) ); //RW made me
    rw::pathplanning::PathAnalyzer path_analyzer(device,state);
    rw::pathplanning::PathAnalyzer::CartesianAnalysis cart_analysis;
    rw::pathplanning::PathAnalyzer::JointSpaceAnalysis joint_analysis;

    std::cout << nodes << ",\t" << planner.size() << ",\t"; //number of nodes in the PRM
    t1 = std::chrono::high_resolution_clock::now();
    path = planner.plan(from,to,enable_path_optimization);
    std::cout << TIME_AFTER(t1) << ",\t"; //Time in seconds it take to calculate path

    cart_analysis = path_analyzer.analyzeCartesian(path, tool);
    joint_analysis = path_analyzer.analyzeJointSpace(path);

    std::cout << cart_analysis.length << ",\t"
              << joint_analysis.length << ",\t" //Path length in joint space (radians)?
              << path.size() << ",\t" //Path length in joint space (radians)?
              << avg_distance_between_points(path);

    std::cout << std::endl;
}

int main(){
    std::chrono::high_resolution_clock::time_point t1;
    std::string wcFile = "../../workcell/WorkStation3/WS3_Scene_prm.wc.xml";
    std::string robotName = "UR1";
    std::string toolName = "PG70";
    std::string constraint_name = "ball_rod";
    rw::math::Q from(6,0,-1.571,0,-1.571,0,0);
    rw::math::Q to_easy(6,-0.708,-2.333,-1.305,4.588,0.292,0.695);
    rw::math::Q to_hardA(6,1.905, -0.273, 0.182, -1.241, -0.360, 1.030);
    rw::math::Q to_hardB(6,-1.353,-2.333,-0.414,-1.573,0.292,0.695);
    rw::math::Q collision(6, 2.237, -0.360, 0.081, -1.931, -2.566, 0.965);
    rw::math::Transform3D<double> cone_pos_f(rw::math::Vector3D<double>(-0.660, -1, 1), rw::math::RPY<double>(0.364, 0.087,0.401).toRotation3D() );
    rw::math::Transform3D<double> cone_pos(rw::math::Vector3D<double>(-0.660, -0.370, 1), rw::math::RPY<double>(0.364, 0.087,0.401).toRotation3D() );
    std::cout << "started" << std::endl;

    rw::trajectory::QPath path;


    rw::models::WorkCell::Ptr work_cell = rw::loaders::WorkCellLoader::Factory::load(wcFile);
    rw::kinematics::State state = work_cell->getDefaultState();
    rw::models::Device::Ptr device( work_cell->findDevice(robotName) );
    rw::proximity::CollisionDetector::Ptr detector = rw::common::ownedPtr( new rw::proximity::CollisionDetector(work_cell,rwlibs::proximitystrategies::ProximityStrategyFactory::makeDefaultCollisionStrategy()) );

//    rw::pathplanning::QConstraint::Ptr constraint = rw::pathplanning::QConstraint::make(detector, device, state);
    rw::pathplanning::PlannerConstraint constraint = rw::pathplanning::PlannerConstraint::make(&*detector, device,state);
    rw::math::Q metric_weights = rw::pathplanning::PlannerUtil::estimateMotionWeights(
                *device,
                device->getEnd(),
                state,
                rw::pathplanning::PlannerUtil::WORSTCASE,
                500);
    rw::math::QMetric::Ptr metric = rw::math::MetricFactory::makeWeightedEuclidean<rw::math::Q>(metric_weights);
    rwlibs::pathoptimization::PathLengthOptimizer optimizer(constraint, metric);

    // make csv for timing for offline building of graph. (10 iterations may take bit more than a day, so maybe it should be run in parallel)
    /* <-- add / to switch functionality
    for(size_t nodes = 1000; nodes <= 15000; nodes+=500){ //uncomment write to file and set if sentence to false
        std::cout << nodes ;
        for(int i = 0; i < 5; ++i){
            t1 = std::chrono::high_resolution_clock::now();
            rovi::PathPlanner my_planner(wcFile, robotName, constraint_name,nodes);
            std::cout << ", " << TIME_AFTER(t1);
        }
        std::cout << std::endl;
    }
    /*/ //Test the planner on 10 different paths
    std::cout << "desired n nodes, actual n nodes, plan time, cart length, joint length, path size, dist within path" << std::endl;
    for (size_t nodes = 1000; nodes <= 15000; nodes+=100) {
        rovi::PathPlanner my_planner(wcFile, robotName, constraint_name,nodes);
        analyze_planner(my_planner, nodes, from,to_easy);
        analyze_planner(my_planner, nodes, from,to_hardA);
        analyze_planner(my_planner, nodes, from,to_hardB);
        analyze_planner(my_planner, nodes, from,collision);
        analyze_planner(my_planner, nodes, to_easy,to_hardA);
        analyze_planner(my_planner, nodes, to_easy,to_hardB);
        analyze_planner(my_planner, nodes, to_easy,collision);
        analyze_planner(my_planner, nodes, to_hardA,to_hardB);
        analyze_planner(my_planner, nodes, to_hardA,collision);
        analyze_planner(my_planner, nodes, to_hardB,collision);
        my_planner.write();
    }
    //*/


    return 0;
}
