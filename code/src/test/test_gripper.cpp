#include <iostream>

#include "../hwcomponents/gripper.hpp"

/**
 * @brief main Tests the gripper connection by closing and openning it.
 * @return
 */
int main(){
    std::cout << "-- Testing HW-Component: Gripper --" << std::endl;

    std::string acting_namespace = "caros_schunkpg70";
    rw::math::Q force(1, 30);

    // setup ros
    char** argv = NULL;
    int argc = 0;
    ros::init(argc, argv,"test_gripper");
    ROS_INFO("Connected to roscore");

    rovi::hwcomponents::Gripper schunk(acting_namespace);

    if(!schunk.isOK()){
        std::cerr << "Could not connect to gripper!\n";
    }

    schunk.grip(force);
    while(schunk.isMoving());
    schunk.open();
    while(schunk.isMoving());

    std::cout << "Test grasp complete!\n";

    return 0;
}
