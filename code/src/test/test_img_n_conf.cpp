#include <iostream>
#include <iomanip>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <ctime> // used for tmp file name generation
#include <fstream>

#include <boost/filesystem.hpp>

#include <rw/math/Q.hpp>

#include "../hwcomponents/stereocamera.hpp"
#include "../hwcomponents/robot.h"


void show(cv::Mat &left, cv::Mat &right){
    cv::namedWindow("left", cv::WINDOW_NORMAL);
    cv::namedWindow("right", cv::WINDOW_NORMAL);
    cv::imshow("left", left);
    cv::imshow("right", right);
    cv::waitKey(1);
}

void saveData(cv::Mat &left, cv::Mat &right, rw::math::Q &config){
    // get time to add to file name
    std::time_t timenow;
    std::time(&timenow);
    struct tm zero = {0};
    double t = std::difftime(timenow, std::mktime(&zero));

    // left camera name
    std::string temporaryFilename_left = "left/img_" + std::to_string(t);
    int l = temporaryFilename_left.size();
    temporaryFilename_left.erase(l-7, std::string::npos);
    temporaryFilename_left += ".png";

    // right camera name
    std::string temporaryFilename_right = "right/img_" + std::to_string(t);
    l = temporaryFilename_right.size();
    temporaryFilename_right.erase(l-7, std::string::npos);
    temporaryFilename_right += ".png";

    // nemae to refrence the images
    std::string temp_save_name = std::to_string(t);;
    l = temp_save_name.size();
    temp_save_name.erase(l-7, std::string::npos);

    while(boost::filesystem::exists(temporaryFilename_left) || boost::filesystem::exists(temporaryFilename_right)){
        // left
        l = temporaryFilename_left.size();
        temporaryFilename_left.erase(l-4, std::string::npos);
        temporaryFilename_left += "f.png";

        // right
        l = temporaryFilename_right.size();
        temporaryFilename_right.erase(l-4, std::string::npos);
        temporaryFilename_right += "f.png";

        // q name
        temp_save_name += "f";
    }



    // save left and right image
    cv::imwrite(temporaryFilename_left, left);
    cv::imwrite(temporaryFilename_right, right);

    // open file and save the data
    std::string filename = "configurations.csv";
    std::fstream file(filename, std::fstream::out | std::fstream::app);

    file << temp_save_name << std::scientific << std::setprecision(16);

    for(size_t i = 0; i < config.size(); i++){
        file << ", " << config[i]; //nice blaming RobWork for low precision!....
    }
    file << "\n";

    file.close();

}

int main(){
    std::cout << "-- Testing HW-Component: StereoCamera --" << std::endl;

    // setup ros
    char** argv = NULL;
    int argc = 0;
    ros::init(argc, argv,"stereocamera_imgreader");
    ROS_INFO("Connected to roscore");

    std::string acting_namespace = "caros_universalrobot";

    rovi::hwcomponents::Stereocamera cam("/stereo_camera/left/image_rect_color", "/stereo_camera/right/image_rect_color");
    rovi::hwcomponents::Robot ur5(acting_namespace);

    if(!ur5.isOK()){
        std::cerr << "Could not connect to robot!\n";
        return false;
    }

    cam.start(); // start the QThread to recieve data from ROS

    cv::Mat l, r;
    char c;

    std::time_t time_now;
    std::time_t time_start;
    std::time(&time_start);


    do {
        c = cv::waitKey(1);
        if(cam.getStereoImage(l,r)){
            std::cout << "IMG RECEIVED" << std::flush;
            show(l,r);
        }
        if(c == 'r'){
            rw::math::Q current_config = ur5.getConfiguration();
            saveData(l, r, current_config);
        }
        // output time to see how far its been running
        std::time(&time_now);
        int t = std::difftime(time_now, time_start);
        std::cout << "\r" << std::floor(t / (3600)) << "h " << std::floor((t % 3600) / 60) << "m ";
        if(t % 60 < 10){
            std::cout << (int)0;
        }
        std::cout << t % 60 << "s";

    } while( c != 'q');

    cam.quitNow();


    return 0;
}
