#include <iostream>

#include "../cameracalibration/stereocalibration.hpp"

/**
 * @brief main Tests the gripper connection by closing and openning it.
 * @return
 */
int main(){
    std::cout << "-- Testing Stereo Camera Calibration --" << std::endl;

    // don't change the left and right filename, just remember to run cmake once after you've pulled :)
    std::string left("left_images.csv"), right("right_images.csv"), save("calibration_file.xml");

    rovi::cameracalibration::stereocalibration calibrate;

    calibrate.setBoardSize(6,4);
    calibrate.setSquareSize(0.0518);
    calibrate.setImageFile(left, right);

    calibrate.calibrate();
    calibrate.saveCalibrationFile(save);

    calibrate.visualizeChessBoards();



    std::cout << "Test Stereo Camera Calibration complete!\n";

    return 0;
}
