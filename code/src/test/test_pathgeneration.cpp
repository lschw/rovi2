#include <iostream>
#include <vector>
#include <cassert>
#include <numeric>
#include <fstream>
#include <iomanip>
#include <chrono>
#include "utils/defines_matrices.hpp"
#include "pathgenerator/cubicsplinepolynomial.hpp"


class BallPath {
public:
    using Vector = rovi::Vector3d;
    using Spline = rovi::CubicSplinePolynomial<Vector>;
protected:
    std::vector<Spline > _splines;
    std::vector<double> _times;
public:
    virtual ~BallPath() { }
    BallPath(const double start_time)
        :_splines(),_times(1,start_time)
    { }
    BallPath()
        :BallPath(0.0)
    { }

    virtual rovi::Vector3d evaluate(const double t) const {
        assert(!_splines.empty());
        if(t<_times.front())
            throw std::runtime_error("Ball Path: Time t is too small!");
        if(t>_times.back())
            throw std::runtime_error("Ball Path: Time t is too large!");
        size_t i;
        for(i=0; i<_times.size(); ++i)
            if(t<_times.at(i+1))
                break;
        return _splines.at(i).evaluate(t);
    }

    virtual void addSegment(const Vector &pos_start, const Vector &pos_end,
                            const Vector &vel_start, const Vector &vel_end,
                            const double delta_t) {
        assert(delta_t>0.0);
        const double t_end = _times.back()+delta_t;
        _splines.emplace_back(pos_start,pos_end,vel_start,vel_end,_times.back(),t_end);
        _times.push_back(t_end);
    }
};


using namespace std;

int main(int argc, char *argv[]){
    const size_t num_input_arguments = static_cast<size_t>(argc-1);



    if(num_input_arguments>0) {
        unsigned int temp;
        stringstream(argv[1]) >> temp;
        srand(temp);
    }
    else {
        srand(std::chrono::high_resolution_clock::now().time_since_epoch().count());
    }
    constexpr double vmax = 1.0;

    std::vector<rovi::Vector3d,Eigen::aligned_allocator<rovi::Vector3d> > positions, velocities;
    std::vector<double> time_deltas;

    auto addPosition = [&positions](double x, double y, double z) {
        rovi::Vector3d position;
        position << x,y,z;
        positions.push_back(position);
    };

//    0.0172645
//    0.00890128
//    0.0154868
//    0.0416411
//    0.015909

    //0.161,-0.117,-1.007
    //-0.062,0.310,-1.251
    //-0.188,0.197,-1.297
    //-0.131,0.028,-1.040
    //0.119,0.028,-0.465
    //0.120,-0.102,-0.832






    addPosition(0.161,-0.117,-1.007);
    velocities.push_back(vmax*rovi::Vector3d::Random());
    time_deltas.push_back(0.5);

    addPosition(-0.062,0.310,-1.251);
    velocities.push_back(vmax*rovi::Vector3d::Random());
    time_deltas.push_back(0.5);

    addPosition(-0.188,0.197,-1.297);
    velocities.push_back(vmax*rovi::Vector3d::Random());
    time_deltas.push_back(0.5);

    addPosition(-0.131,0.028,-1.140);
    velocities.push_back(vmax*rovi::Vector3d::Random());
    time_deltas.push_back(0.5);

    addPosition(0.119,0.028,-0.665);
    velocities.push_back(vmax*rovi::Vector3d::Random());
    time_deltas.push_back(0.5);

    addPosition(0.120,-0.102,-0.832);
    velocities.push_back(vmax*rovi::Vector3d::Random());

    const double time_total = accumulate(time_deltas.begin(), time_deltas.end(), 0.0);

    BallPath testPath;
    for(size_t i=0; i<positions.size()-1; ++i) {
        testPath.addSegment(positions[i],positions[i+1],velocities[i],velocities[i+1],time_deltas[i]);
    }

//    for(double t=0.0; t<time_total; t=t+1.0/20.0)
//        cout << "t = " << t << flush << ", P = " << testPath.evaluate(t).transpose() << endl;

    ofstream output("test_pathgeneration_path.csv");
    output << scientific << setprecision(16);
    rovi::Vector3d p0=testPath.evaluate((0.0));
//    for(size_t i=0; i<20; ++i)
//        output << p0(0) << ", " << p0(1) << ", " << p0(2) << ", 0.0, 0.0, 0.0\n";
    for(double t=0.0; t<time_total; t=t+1.0/20.0) {
        rovi::Vector3d p=testPath.evaluate(t);
        output << p(0) << ", "  << p(1) << ", " << p(2) << ", 0.0, 0.0, 0.0\n";
    }
    output.close();

    return 0;
}
