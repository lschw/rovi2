#include <iostream>
#include <rw/common.hpp>
#include <rw/math.hpp>

int main(){
    std::cout << "Hello World!" << std::endl;

    rw::math::Q my_q((int)4,3.0,2.0,1.0,0.0);

    std::cout << "Q = " << my_q << std::endl;

    return 0;
}
