#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <exception>
#include <cmath>
#include "kalman/kalman.hpp"
#include "utils/defines_matrices.hpp"
#include <eigen3/Eigen/Core>

using namespace rovi;
using namespace std;



int main(){
    //input parameters
    const double dt = 1.0/20.0; //20Hz
    const size_t num_points = 71;
    const double acceleration_delta = 0.05; //Maximum change in acceleration in one time step delta_t, in m/s^2
    const double acceleration_noise_std_deviation = 0.5*acceleration_delta; //Choose between [ delta_a/2 , delta_a ].
    //input (CMake)
    const string filename_R("/home/mikael/skole/rovi2/rovi2/data/kalman_test/measurement_noise_covariance_matrix.csv");
    const string filename_tN("/home/mikael/skole/rovi2/rovi2/data/kalman_test/trajectory_noisy.csv");
    const string filename_tR("/home/mikael/skole/rovi2/rovi2/data/kalman_test/trajectory_reference.csv");
    //output
    const string filename_tF("/home/mikael/skole/rovi2/rovi2/data/kalman_test/trajectory_filtered.csv");

    cout << "Test: Kalman filter\n_______________________" << endl;
    cout << "Importing R..." << endl;
    Eigen::Matrix3d R;
    ifstream ifs(filename_R);
    if(!ifs.is_open())
        throw(runtime_error("Measurement noise covariance matrix file was not opened"));
    ifs >> R;
    ifs.close();
    cout << "R:\n" << R << endl;

    cout << "Importing reference and noisy trajectories..." << endl;
    Eigen::MatrixXd tR(num_points,3), tN(num_points,3);
    ifs.open(filename_tR);
    if(!ifs.is_open())
        throw(runtime_error("Reference trajectory file was not opened"));
    ifs >> tR;
    ifs.close();
    ifs.open(filename_tN);
    if(!ifs.is_open())
        throw(runtime_error("Noisy trajectory file was not opened"));
    ifs >> tN;
    ifs.close();

    cout << "Reference trajectory:\n" << tR << "\n\nNoisy trajectory:\n" << tN << endl;

    cout << "Creating Kalman filter..." << endl;
    Kalman myKalman;
    cout << "Kalman filter created." << endl;

    cout << "Setting sample rate to: " << dt << "." << endl;
    myKalman.setSamplingPeriod(dt);
    cout << "Sample rate was set." << endl;
    cout << "Setting measurement noise covariance matrix R..." << endl;
    myKalman.setMeasurementNoiseCovarianceMatrix(R);
    cout << "Measurement noise covariance matrix was set." << endl;

    cout << "Constructing process noise covariance matrix Q..." << endl;
    cout << " Using piecewise constant acceleration model with maximum acceleration delta = " << acceleration_delta << " m/s^2" << endl;
    cout << " and x-y-z acceleration noise std. deviation = " << acceleration_noise_std_deviation << '.' << endl;
    double sigsqx = std::pow(acceleration_noise_std_deviation,2);
    double sigsqy = sigsqx;
    double sigsqz = sigsqx;
    double dt2 = std::pow(dt,2);
    double dt3 = std::pow(dt,3);
    double dt4 = std::pow(dt,4);
    rovi::Matrix9d Q;
    Q << sigsqx*dt4/4.0, 0, 0, sigsqx*dt3/2.0, 0, 0, sigsqx*dt2/2.0, 0, 0,
            0, sigsqy*dt4/4.0, 0, 0, sigsqy*dt3/2.0, 0, 0, sigsqy*dt2/2.0, 0,
            0, 0, sigsqz*dt4/4.0, 0, 0, sigsqz*dt3/2.0, 0, 0, sigsqz*dt2/2.0,
            sigsqx*dt3/2.0, 0, 0, sigsqx*dt2, 0, 0, sigsqx*dt, 0, 0,
            0, sigsqy*dt3/2.0, 0, 0, sigsqy*dt2, 0, 0, sigsqy*dt, 0,
            0, 0, sigsqz*dt3/2.0, 0, 0, sigsqz*dt2, 0, 0, sigsqz*dt,
            sigsqx*dt2/2.0, 0, 0, sigsqx*dt, 0, 0, sigsqx, 0, 0,
            0, sigsqy*dt2/2.0, 0, 0, sigsqy*dt, 0, 0, sigsqy, 0,
            0, 0, sigsqz*dt2/2.0, 0, 0, sigsqz*dt, 0, 0, sigsqz;
    cout << "Process noise covariance matrix Q constructed.\nQ:\n" << Q << endl;

    cout << "Setting process noise covariance matrix Q in Kalman filter..." << endl;
    myKalman.setProcessNoiseCovarianceMatrix(Q);
    cout << "PRocess noise covariance matrix Q was set in Kalman filter." << endl;



    cout << "Creating filtered trajectory output file \"" << filename_tF << "\"..." << endl;
    ofstream ofs(filename_tF);
    if(!ofs.is_open())
        throw(runtime_error("Output file for filtered trajectory was not opened"));
    //ofs << "px py pz pdx pdy pdz pddx pddy pddz ux uy uz udx udy udz uddx uddy uddz\n";
    cout << "Applying Kalman filter to noisy trajectory..." << endl;
    for(size_t i=0; i<num_points; ++i) {
        cout << "Iteration " << setw(6) << setfill(' ') << (i+1) << " of " << num_points << ": Predict... " << flush;
        Vector9d prediction =  myKalman.predict();
        cout << "Update... " << flush;
        Vector9d update = myKalman.update(tN.row(i));
        cout << "Output... " << flush;
        ofs << prediction.transpose() << ' ' << update.transpose() << "\n";
        cout << "Done." << endl;
    }
    ofs.close();


    cout << "Test has ended. Press enter to get a free segmentation fault." << endl;
    cin.get();
    return 0;
}
