#include <iostream>

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

#include <ctime> // used for tmp file name generation

#include <boost/filesystem.hpp>

#include "../hwcomponents/stereocamera.hpp"


void show(cv::Mat &left, cv::Mat &right){
    cv::namedWindow("left", cv::WINDOW_NORMAL);
    cv::namedWindow("right", cv::WINDOW_NORMAL);
    cv::imshow("left", left);
    cv::imshow("right", right);
    cv::waitKey(1);
}

void saveStereoImg(cv::Mat &left, cv::Mat &right){
    // get time to add to file name
    std::time_t timenow;
    std::time(&timenow);
    struct tm zero = {0};
    double t = std::difftime(timenow, std::mktime(&zero));

    std::string temporaryFilename_left = "left/img_" + std::to_string(t);
    int l = temporaryFilename_left.size();
    temporaryFilename_left.erase(l-7, std::string::npos);
    temporaryFilename_left += ".png";

    while(boost::filesystem::exists(temporaryFilename_left)){
        l = temporaryFilename_left.size();
        temporaryFilename_left.erase(l-4, std::string::npos);
        temporaryFilename_left += "f.png";
    }

    std::string temporaryFilename_right = "right/img_" + std::to_string(t);
    l = temporaryFilename_right.size();
    temporaryFilename_right.erase(l-7, std::string::npos);
    temporaryFilename_right += ".png";

    while(boost::filesystem::exists(temporaryFilename_right)){
        l = temporaryFilename_right.size();
        temporaryFilename_right.erase(l-4, std::string::npos);
        temporaryFilename_right += "f.png";
    }


    // save left and right image
    cv::imwrite(temporaryFilename_left, left);
    cv::imwrite(temporaryFilename_right, right);
}


int main(){
    std::cout << "-- Testing HW-Component: StereoCamera --" << std::endl;

    // setup ros
    char** argv = NULL;
    int argc = 0;
    ros::init(argc, argv,"stereocamera_imgreader");
    ROS_INFO("Connected to roscore");

    rovi::hwcomponents::Stereocamera cam("/stereo_camera/left/image_rect_color", "/stereo_camera/right/image_rect_color");
//    rovi::stereocamera cam("/monocamera_camera/image", "/monocamera_camera/image");
    cam.start(); // start the QThread to recieve data from ROS

    cv::Mat l, r;
    char c;

    std::time_t time_now;
    std::time_t time_start;
    std::time(&time_start);


    do {
        c = cvWaitKey(1);
        if(cam.getStereoImage(l,r)){
            show(l,r);
        }
        if(c == 'r'){
            saveStereoImg(l, r);
        }
        // output time to see how far its been running
        std::time(&time_now);
        int t = std::difftime(time_now, time_start);
        std::cout << "\r" << std::floor(t / (3600)) << "h " << std::floor((t % 3600) / 60) << "m ";
        if(t % 60 < 10){
            std::cout << (int)0;
        }
        std::cout << t % 60 << "s";

    } while( c != 'q');

    cam.quitNow();


    return 0;
}
