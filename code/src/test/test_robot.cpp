#include <iostream>


#include "../hwcomponents/robot.h"

#include <rw/loaders/WorkCellLoader.hpp>
#include <rw/models/WorkCell.hpp>


/*
 * WARNING: Use this program at own risk!
 *          To large displacement may cause the robot to hit nearby obstacles.
 */

/**
 * @brief main Tests the robot connection by moving it back and forth.
 * @return
 */
int main(){
    std::cout << "-- Testing HW-Component: Robot --" << std::endl;

    // - settings -
    std::string acting_namespace = "caros_universalrobot";
    double q_change = 0.1;

    // setup ros
    char** argv = NULL;
    int argc = 0;
    ros::init(argc, argv,"test_robot");
    ROS_INFO("Connected to roscore");
    

    // connect to robot
    rovi::hwcomponents::Robot ur5(acting_namespace);

    if(!ur5.isOK()){
        std::cerr << "Could not connect to robot!\n";
        return false;
    }

    // move robot
    rw::math::Q current_config = ur5.getConfiguration();

    rw::math::Q next_config = current_config + rw::math::Q(current_config.size(), q_change);

    if(ur5.isOK() && ur5.movePtp(next_config)){
        while(ur5.isMoving());

        if(ur5.movePtp(current_config)){
            while(ur5.isMoving());
        } else {
            std::cerr << "Could not move robot!\n";
        }
    } else {
        std::cerr << "Could not move robot!\n";
    }

    std::cout << "Test move complete!\n";


    return 0;
}
