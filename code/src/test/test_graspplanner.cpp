#include <iostream>
#include <chrono>
#include <ratio>


#include "../graspplanner/graspplanner.hpp"
#include "../pathplanning/constraintgenerator.hpp"
#include "../utils/log.hpp"


#include <rw/math/Math.hpp>
#include <rw/loaders/WorkCellFactory.hpp>

//not a Cyndi Lauper reference!
#define TIME_AFTER(TIME) std::chrono::duration_cast<std::chrono::duration<double> >(std::chrono::high_resolution_clock::now() - TIME).count()


//std::chrono::high_resolution_clock::time_point t1;
//std::cout << TIME_AFTER(t1) << ",\t"; //Time in seconds it take to calculate path


std::vector< rw::math::Transform3D<> > loadObjPath(std::string filename){
    std::fstream file(filename, std::fstream::in);

    // test if still open
    if(! file.is_open() ){
        ROVI_THROW("ERROR: Could not open " + filename + "'.\n");
    }

    std::string data_s;
    double number;
    rw::math::Transform3D<double> pose;
    rw::math::RPY<double> rot;

    std::vector< rw::math::Transform3D<> > path;

    while (!file.eof()) {
        int i = 0;
        while(!file.eof() && i < 6){
            if(i == 5){
                std::getline (file, data_s);
            } else {
                std::getline (file, data_s, ',');
            }
            std::istringstream(data_s) >> number;
            if(i < 3){
                pose.P()[i] = number;
            } else {
                rot[i - 3] = number * rw::math::Deg2Rad;
            }
            i++;
        }
        if(i == 6){
            pose.R() = rot.toRotation3D();
            path.emplace_back(pose);
        }
    }

    file.close();

    return path;
}


int main(){
    std::string outfile = "graspplanner_data.csv";
    std::string wcFile = "../../workcell/WorkStation3/WS3_Scene.wc.xml";
    std::string objPath_file = "../../workcell/testpaths/wc3_path03.csv";
    std::string robot_name = "UR1";
    std::string tcp_name = "PG70.TCP";
    std::string constraint_name = "ball_rod";
    std::string ball_name = "Ball";
    std::string camera_name = "CameraSimLeft";

    rw::models::WorkCell::Ptr work_cell = rw::loaders::WorkCellLoader::Factory::load(wcFile);
    rw::models::WorkCell* wc = &*work_cell;

    rw::kinematics::State state = wc->getDefaultState();
    rw::models::Device::Ptr device( wc->findDevice(robot_name) );

    rw::kinematics::Frame* ball = wc->findFrame(ball_name);
    rw::kinematics::Frame* endeffector = wc->findFrame(tcp_name);
    rw::kinematics::Frame* camera = wc->findFrame(camera_name);
    rw::kinematics::MovableFrame* pointer = dynamic_cast<rw::kinematics::MovableFrame*>(wc->findFrame(constraint_name));
    rw::kinematics::MovableFrame* mball = dynamic_cast<rw::kinematics::MovableFrame*>(ball);

    // init grasp planner
    rovi::GraspPlanner graspplanner(device, endeffector, wc);
    graspplanner.setTarget(ball);

    // init constraint generator
    rovi::pathplanners::ConstraintGenerator constraint;
    constraint.setPointerDistance(3.0,0.1); //first: length of rod. second: distance from ball center to rod end.
    constraint.setOrigo(camera);
    constraint.setPointer(pointer);

    // load obj path
    std::vector< rw::math::Transform3D<> > path = loadObjPath(objPath_file);

    // file
    std::fstream file(outfile, std::fstream::out);

    // loop through path and test for number of
    int maxTries = 10000;
    std::chrono::high_resolution_clock::time_point t1;
    for(double deg = 10; deg <= 40; deg += 2.5){
        graspplanner.setRandomness(deg * rw::math::Deg2Rad, maxTries);
        for(size_t i = 0; i < path.size(); i++){
            std::cout << "Degrees: " << deg << ", it: " << i << std::endl;

            rw::kinematics::State refstate = state;
            refstate.upgrade();
            // set ball pos
            mball->setTransform(path.at(i), refstate);

            // set constraint
            rw::math::Transform3D<> camTball = rw::kinematics::Kinematics::frameTframe(camera, ball, refstate);
            constraint.pointTowards(camTball, refstate);

            // plan grasp
            t1 = std::chrono::high_resolution_clock::now();
            try{
            graspplanner.planGrasp(refstate);
            } catch (std::runtime_error &err){
                std::cout << err.what() << std::endl;
            }

            double time = TIME_AFTER(t1);

            file << deg << ", " << graspplanner.getTries() << ", " << graspplanner.getColChecks() << ", " << time << ", " << time / static_cast<double>(graspplanner.getTries()) << "\n";


        }
    }
    file.close();

    return 0;
}
