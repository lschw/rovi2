#include <iostream>
#include <iomanip>
#include <fstream>
#include <exception>

#include "stereovision/stereoalgorithm.hpp"
#include "utils/defines_matrices.hpp"

#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/StdVector>
#include <opencv2/core/core.hpp>




using namespace std;

int main(int argc, char *argv[]){
    const size_t num_input_arguments = static_cast<size_t>(argc-1);

    //Parse input
    if(!((num_input_arguments==2)||(num_input_arguments==4)||(num_input_arguments==6))) {
        throw runtime_error("Triangulate\n"
                "Wrong number of input arguments.\n"
                "Syntax:\n"
                "test_triangulate path/to/left_2d_pixels.txt path/to/right_2d_pixels.txt\n"
                "or:\n"
                "test_triangulate path/to/left_2d_pixels.txt path/to/right_2d_pixels.txt"
                " path/to/left_projection_matrix_rect.txt path/to/right_projection_matrix_rect.txt"
                "or:\n"
                "test_triangulate path/to/left_2d_pixels.txt path/to/right_2d_pixels.txt"
                " path/to/left_projection_matrix_rect.txt path/to/right_projection_matrix_rect.txt"
                " path/to/2d_pixel_covarance_left.txt path/to/2d_pixel_covariance_right.txt"
             );
    }
    const string filename_pixels_left(argv[1]);
    const string filename_pixels_right(argv[2]);
    string filename_P_left, filename_P_right;
    string filename_cov_left, filename_cov_right;
    if(num_input_arguments==2) {
        filename_P_left="../../data/cam_calib/left_projection_matrix_rectified.txt";
        filename_P_right="../../data/cam_calib/right_projection_matrix_rectified.txt";
    }
    else {
        filename_P_left=argv[3];
        filename_P_right=argv[4];
    }
    if(num_input_arguments<6) {
        filename_cov_left="../../data/kalman_test/2d_pixel_covariance.txt";
        filename_cov_right="../../data/kalman_test/2d_pixel_covariance.txt";
    }
    else {
        filename_cov_left=argv[5];
        filename_cov_right=argv[6];
    }

    //Load projection matrices
    rovi::Matrix3by4d P_left(3,4), P_right(3,4);
    std::ifstream ifs(filename_P_left);
    if(!ifs.is_open())
        throw runtime_error("Left projection matrix could not be opened.");
    ifs >> P_left;
    ifs.close();
    ifs.open(filename_P_right);
    if(!ifs.is_open())
        throw runtime_error("Right projection matrix could not be opened.");
    ifs >> P_right;
    ifs.close();

    //Load pixel matrices
    std::vector<Eigen::Vector2d,Eigen::aligned_allocator<Eigen::Vector2d> > pixels_left, pixels_right;
    ifs.open(filename_pixels_left);
    if(!ifs.is_open())
        throw runtime_error("Left pixels file could not be opened.");
    string line;
    while(getline(ifs,line)) {
        stringstream ss(line);
        double u, v;
        ss >> u >> v;
        Eigen::Vector2d pixel;
        pixel << u, v;
        pixels_left.push_back(pixel);
    }
    ifs.close();
    ifs.open(filename_pixels_right);
    if(!ifs.is_open())
        throw runtime_error("Right pixels file could not be opened.");
    while(getline(ifs,line)) {
        stringstream ss(line);
        double u, v;
        ss >> u >> v;
        Eigen::Vector2d pixel;
        pixel << u, v;
        pixels_right.push_back(pixel);
    }
    ifs.close();

    //Load pixel uncertainties
    ifs.open(filename_cov_left);
    if(!ifs.is_open())
        throw runtime_error("Left pixel covariance matrix could not be opened.");
    Eigen::Matrix2d cov_left;
    ifs >> cov_left;
    ifs.close();
    ifs.open(filename_cov_right);
    if(!ifs.is_open())
        throw runtime_error("Right pixel covariance matrix could not be opened.");
    Eigen::Matrix2d cov_right;
    ifs >> cov_right;
    ifs.close();


    const size_t num_pixels = pixels_left.size();
    if(num_pixels!=pixels_right.size())
        throw runtime_error("Number of pixels in left/right not the same.");


    rovi::Stereoalgorithm stereo(P_left,P_right,cov_left,cov_right);
    Eigen::MatrixX3d points3d(num_pixels,3);
    Eigen::Matrix<double,Eigen::Dynamic,9> covs3d(num_pixels,9);
    for(size_t i=0; i<num_pixels; ++i) {
        cv::Point2d pix_left(pixels_left[i](0),pixels_left[i](1));
        cv::Point2d pix_right(pixels_right[i](0),pixels_right[i](1));
        Eigen::Vector3d point3d;
        Eigen::Matrix3d cov3d;
        std::tie(point3d,cov3d)=stereo.rectifiedStereopsis(make_pair(pix_left,pix_right));
        points3d.block<1,3>(i,0)=point3d.transpose();
        Eigen::Matrix<double,1,9> cov3dv;
        cov3dv << cov3d(0,0), cov3d(0,1), cov3d(0,2),
                cov3d(1,0), cov3d(1,1), cov3d(1,2),
                cov3d(2,0), cov3d(2,1), cov3d(2,2);
        covs3d.block<1,9>(i,0)=cov3dv;
    }
    std::ofstream ofs("stereoPoints.txt");
    ofs << scientific << setprecision(16) << points3d;
    ofs.close();
    ofs.open("stereoCovariances.txt");
    ofs << scientific << setprecision(16) << covs3d;
    ofs.close();

    return 0;
}
