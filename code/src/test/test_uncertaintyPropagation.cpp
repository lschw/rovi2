#include <iostream>
#include <chrono>

#include "../stereovision/stereoalgorithm.hpp"


void compareStereopses() {
    std::cout << "-----------Stereopses comparison!-------------" << std::endl;
    // -- generate projection matrices --
    const double f = 1281; //1281
    const double u = 521.352; //521.352
    const double v = 387.487; //387.487
    const double b = 76.8; //76.8
    // generate the two projection matrices
    rovi::Matrix3by4d p_left;
    p_left <<   f, 0, u, 0,
            0, f, v, 0,
            0, 0, 1, 0;
    rovi::Matrix3by4d p_right;
    p_right <<  f, 0, u, -2*b,
            0, f, v, 0,
            0, 0, 1, 0;
    Eigen::Matrix2d unc_2d;
    unc_2d << 1, 0, 0, 1;

    rovi::Stereoalgorithm stereo(p_left, p_right, unc_2d, unc_2d);

    std::vector< cv::Point2d > points;

    points.emplace_back(cv::Point2d(598.211, 387.487));
    points.emplace_back(cv::Point2d(444.492, 387.487));

    Eigen::Vector3d threeDpoint;
    Eigen::Matrix3d threeDcov;
    auto t_start = std::chrono::system_clock::now();
    stereo.completeStereopsis(points, threeDpoint, threeDcov);
    auto t_total = (std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now()-t_start)).count();

    //3D point should be approximately:
    //   0.0599528        0 0.999226
    //
    //3D covariance should be approximately:
    //    3.04228e-07            0 -3.29855e-11
    //              0  3.04228e-07            0
    //   -3.29855e-11            0  8.45089e-05

    //
    //Complete stereopsis time should be lower than 50 microseconds IMO (In Mikael's Operating system).
    std::cout << "3D point: " << threeDpoint.transpose() << "\n";
    std::cout << "3D cov  :\n" << threeDcov << "\n";
    std::cout << "Complete stereopsis time: " << t_total << " microseconds." << std::endl;

    auto mpoints = std::make_pair(points[0],points[1]);
    t_start = std::chrono::system_clock::now();
    std::tie( threeDpoint, threeDcov ) = stereo.rectifiedStereopsis(mpoints);
    t_total = (std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now()-t_start)).count();
    std::cout << "3D point: " << threeDpoint.transpose() << "\n";
    std::cout << "3D cov  :\n" << threeDcov << "\n";
    std::cout << "Rectified stereopsis time: " << t_total << " microseconds." << std::endl;




}

int main(){
    std::cout << "-- Testing Uncertainity Propagation --" << std::endl;

    // -- generate projection matrices --
    // Do not modify this
    // It's from the TA.
    double f = 1281; //1281
    double u = 521.352; //521.352
    double v = 387.487; //387.487
    double b = 76.8; //76.8
    // generate the two projection matrices
    rovi::Matrix3by4d p_left;
    p_left <<   f, 0, u, b,
            0, f, v, 0,
            0, 0, 1, 0;
    rovi::Matrix3by4d p_right;
    p_right <<  f, 0, u, -b,
            0, f, v, 0,
            0, 0, 1, 0;
    Eigen::Matrix2d unc_2d;
    unc_2d << 1, 0, 0, 1;

    rovi::Stereoalgorithm stereo(p_left, p_right, unc_2d, unc_2d);

    std::vector< cv::Point2d > points;

    points.emplace_back(cv::Point2d(598.211, 387.487));
    points.emplace_back(cv::Point2d(444.492, 387.487));

    Eigen::Vector3d threeDpoint;
    Eigen::Matrix3d threeDcov;
    auto t_start = std::chrono::system_clock::now();
    stereo.completeStereopsis(points, threeDpoint, threeDcov);
    auto t_total = (std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::system_clock::now()-t_start)).count();

    //3D point should be approximately:
    //   -3.90018e-07        0 0.999226
    //
    //3D covariance should be approximately:
    //    3.04228e-07            0 -3.29855e-11
    //              0  3.04228e-07            0
    //   -3.29855e-11            0  8.45089e-05
    //
    //Complete stereopsis time should be lower than 50 microseconds IMO (In Mikael's Operating system).
    std::cout << "3D point: " << threeDpoint.transpose() << "\n";
    std::cout << "3D cov  :\n" << threeDcov << "\n";
    std::cout << "Complete stereopsis time: " << t_total << " microseconds." << std::endl;

    compareStereopses();

    return 0;
}
