#include <iostream>
#include <iomanip>
#include <fstream>
#include <chrono>
#include <exception>
#include <unordered_set>

#include <rw/loaders/WorkCellLoader.hpp>
#include <rw/models/WorkCell.hpp>
#include <rw/models/Device.hpp>
#include <rw/kinematics/State.hpp>
#include <rw/math/Q.hpp>

#include "registration/registration.hpp"


using namespace std;


void test_workcell() {
    std::string wcFile = "../../workcell/WorkStation3/WS3_Scene.wc.xml";
    std::string robot = "UR1";
    rw::models::WorkCell::Ptr workcell(rw::loaders::WorkCellLoader::Factory::load(wcFile));
    rw::kinematics::Frame* camera = workcell->findFrame("Camera");
    rw::kinematics::Frame* world = workcell->findFrame("WORLD");
    rw::kinematics::Frame* base = workcell->findFrame("UR1.Base");
    rw::math::Transform3D<double> camTworld = camera->fTf(world,workcell->getDefaultState());
    rw::math::Transform3D<double> worldTcam = world->fTf(camera,workcell->getDefaultState());
    rw::math::Transform3D<double> baseTcam = base->fTf(camera,workcell->getDefaultState());
    //cout << "camTworld: " << camTworld << "\nworldTcam: " << worldTcam << endl;
    Eigen::Matrix4d e_camTworld = rovi::Registration::rwTransform3dToEigenMatrix(camTworld);
    Eigen::Matrix4d e_worldTcam = rovi::Registration::rwTransform3dToEigenMatrix(worldTcam);
    Eigen::Matrix4d e_baseTcam = rovi::Registration::rwTransform3dToEigenMatrix(baseTcam);
    cout << "camTworld:\n" << e_camTworld << "\n\nworldTcam:\n" << e_worldTcam
         << "\n\nbaseTcam:\n" << e_baseTcam << endl;
}


void old() {
    std::cout << "-- Testing Registration --" << std::endl;

    constexpr auto num_configurations = 12;

    rovi::Registration myReg(true);


    //world points (first), stereo points(second)
    std::pair<Eigen::Matrix3Xd,Eigen::Matrix3Xd> markers;
    markers = rovi::Registration::loadMarkerData(20,num_configurations);

    std::cout << "Marker chessboard corners in marker frame:\n" << markers.first.transpose() << std::endl;
    std::cout << "Press ENTER to continue." << std::flush;
    std::cin.get();
    std::cout << "Marker chessboard corners in camera frame (several image pairs):\n" << markers.second.transpose() << std::endl;
    std::cout << "Press ENTER to continue." << std::flush;
    std::cin.get();


    std::deque<rw::math::Transform3D<double> > camTmarker_poses;
    Eigen::Matrix<double,4,4*num_configurations> camTmarker_poses_eig(4,4*num_configurations);
    Eigen::Matrix3Xd transformed_stereo_points(3,20*num_configurations);
    for(size_t i=0; i<12; ++i) {
        rw::math::Transform3D<double> camTmarker;
        camTmarker=rovi::Registration::estimateCamToMarkerRW(0.001*markers.first,markers.second.block<3,20>(0,i*20));
        camTmarker_poses.push_back(camTmarker);
        camTmarker_poses_eig.block<4,4>(0,4*i)=rovi::Registration::estimateCamToMarkerEig(0.001*markers.first,markers.second.block<3,20>(0,i*20));
        std::cout << "camTmarker:\n" << camTmarker << std::endl;

        //For each stereo point, transform it to marker frame
        //and append to full list of points
        for(size_t j=0; j<20; ++j) {
            rw::math::Vector3D<double> stereo_point(markers.second.block<3,1>(0,i*20+j));
            rw::math::Vector3D<double> stereo_point_transformed = inverse(camTmarker)*stereo_point;
            transformed_stereo_points.block<3,1>(0,i*20+j)=stereo_point_transformed.e();
        }
    }

    std::cout << "Transformed stereo points:\n" << transformed_stereo_points.transpose() << std::endl;
    std::ofstream ofs("test_registration_output.txt");
    ofs << std::scientific << std::setprecision(16) <<  transformed_stereo_points.transpose() << std::endl;
    ofs.close();


    std::cout << "Press ENTER to continue." << std::flush;
    std::cin.get();

    //We now have 12 camTmarker transforms, equivalent
    //to 12 marker poses in camera coordinates.
    //These (camTmarker_poses) are inputs to the registration algorithm.


    std::cout << "baseTend transforms:\n";

    //We now obtain the baseTend transforms, equivalent
    //to the end effector pose in world (base) coordinates.
    //These are also inputs to the registration algorithm.
    std::string wcFile = "../../workcell/WorkStation3/WS3_Scene.wc.xml";
    std::string robot = "UR1";
    rw::models::WorkCell::Ptr workcell(rw::loaders::WorkCellLoader::Factory::load(wcFile));
    rw::models::Device::Ptr device(workcell->findDevice(robot));
    rw::kinematics::State state(workcell->getDefaultState());
    std::deque<rw::math::Transform3D<double> > baseTend_poses;
    Eigen::Matrix<double,4,4*num_configurations> baseTend_poses_eig(4,4*num_configurations);
    std::ifstream ifs("ok_configurations.txt");
    for(size_t i=0; i<num_configurations; ++i) {
        double q0, q1, q2, q3, q4, q5;
        ifs >> q0 >> q1 >> q2 >> q3 >> q4 >> q5;
        rw::math::Q q(6,q0,q1,q2,q3,q4,q5);
        device->setQ(q,state);
        rw::math::Transform3D<double> baseTend = device->baseTend(state);
        std::cout << baseTend << std::endl;
        baseTend_poses.push_back(baseTend);
        Eigen::Matrix4d baseTend_eig;
        baseTend_eig << baseTend(0,0), baseTend(0,1), baseTend(0,2), baseTend(0,3),
                baseTend(1,0), baseTend(1,1), baseTend(1,2), baseTend(1,3),
                baseTend(2,0), baseTend(2,1), baseTend(2,2), baseTend(2,3),
                0.0,0.0,0.0,1.0;
        baseTend_poses_eig.block<4,4>(0,4*i)=baseTend_eig;
    }
    ifs.close();


    ofs.open("test_registration_output_baseTend.txt");
    ofs << std::scientific << std::setprecision(16) <<  baseTend_poses_eig;
    ofs.close();
    ofs.open("test_registration_output_camTmarker.txt");
    ofs << std::scientific << std::setprecision(16) << camTmarker_poses_eig;
    ofs.close();

    std::cout << "Calibrating with Shah method..." << std::endl;
    myReg.calibrate_Shah(baseTend_poses_eig,camTmarker_poses_eig);

    ofs.open("baseTcam.txt");
    ofs << std::scientific << std::setprecision(16) << myReg.getWorldTcamEig();
    ofs.close();
    ofs.open("endTmarker.txt");
    ofs << std::scientific << std::setprecision(16) << myReg.getEndTmarkerEig();
    ofs.close();




    //Export A and B for the AX=XB problem
    //with X = endTmarker
    std::pair<Eigen::Matrix4Xd,Eigen::Matrix4Xd> AB_endTmarker = rovi::Registration::getAB_endTmarker(baseTend_poses_eig,camTmarker_poses_eig);
    //with X = baseTcam
    std::pair<Eigen::Matrix4Xd,Eigen::Matrix4Xd> AB_baseTcam = rovi::Registration::getAB_worldTcam(baseTend_poses_eig,camTmarker_poses_eig);
    ofs.open("test_registration_XendTmarker_A.txt");
    ofs << std::scientific << std::setprecision(16) << AB_endTmarker.first.transpose();
    ofs.close();
    ofs.open("test_registration_XendTmarker_B.txt");
    ofs << std::scientific << std::setprecision(16) << AB_endTmarker.second.transpose();
    ofs.close();
    ofs.open("test_registration_XbaseTcam_A.txt");
    ofs << std::scientific << std::setprecision(16) << AB_baseTcam.first.transpose();
    ofs.close();
    ofs.open("test_registration_XbaseTcam_B.txt");
    ofs << std::scientific << std::setprecision(16) << AB_baseTcam.second.transpose();
    ofs.close();






    //Obtain marker points in camera frame
    //using a test q
    ifs.open("test_configuration.txt");
    double q0, q1, q2, q3, q4, q5;
    ifs >> q0 >> q1 >> q2 >> q3 >> q4 >> q5;
    ifs.close();
    rw::math::Q test_q(6,q0,q1,q2,q3,q4,q5);
    device->setQ(test_q,state);
    rw::math::Transform3D<double> test_baseTend = device->baseTend(state);
    Eigen::Matrix4d test_baseTend_eig;
    test_baseTend_eig << test_baseTend.R().e(), test_baseTend.P().e(),
            0,0,0,1;
    rw::math::Transform3D<double> endTmarker = myReg.getEndTmarker();
    rw::math::Transform3D<double> camTbase = inverse(myReg.getWorldTcam());

    Eigen::JacobiSVD<Eigen::Matrix3d,Eigen::FullPivHouseholderQRPreconditioner> mySVD(camTbase.R().e(),Eigen::ComputeFullU|Eigen::ComputeFullV);
    Eigen::Matrix3d NNN = (mySVD.matrixU()*(mySVD.matrixV().transpose()));
    rw::math::Transform3D<double> camTbaseO(camTbase.P());
    for(size_t i=0; i<3; ++i)
        for(size_t j=0; j<3; ++j)
            camTbaseO(i,j)=NNN(i,j);

    Eigen::Matrix3Xd test_markers(3,markers.first.cols());
    for(int i=0; i<markers.first.cols(); ++i) {
        Eigen::Vector3d m_m = 0.001*markers.first.block<3,1>(0,i);
        rw::math::Vector3D<double> m_end = endTmarker*rw::math::Vector3D<double>(m_m(0),m_m(1),m_m(2));
        rw::math::Vector3D<double> m_base = test_baseTend*m_end;
//        rw::math::Vector3D<double> m_cam = camTbase*m_base;
//        rw::math::Vector3D<double> m_cam = Registration::transformOrthogonalize(camTbase,rw::math::Transform3D<double>(m_base)).P();
        rw::math::Vector3D<double> m_cam = camTbaseO*m_base;
        test_markers.block<3,1>(0,i)=m_cam.e();
    }

    ofs.open("test_registration_marker.txt");
    ofs << test_markers.transpose();
    ofs.close();


    //rw::math::Q test_q


//    std::cout << "Calibrating robot-to-camera with QR24 method..." << std::endl;
//    myReg.calibrate_QR24(baseTend_poses,camTmarker_poses);
//    std::cout << "..." << std::endl;


    std::cout << "-- Returning from main --" << std::endl;
}

void old2(int argc, char *argv[]){
    constexpr bool multiply_baseTend_translation_by_minus_1 = true;

    //old();
    //num_checkerboard_corners stereoPoints.txt
    const size_t num_input_arguments = static_cast<size_t>(argc-1);

    //Parse input
    if(!(num_input_arguments==4)) {
        throw runtime_error("Registration:\n"
                            "Number of input arguments is wrong. Usage:\n"
                            "test_registration path/to/3DMarkerPointsInMarkerframe.txt path/to/3DMarkerPointsInCameraframe.txt path/to/robotConfigurations.txt path/to/workcell.xml");
    }

    const string filename_markerPointsMarkerframe(argv[1]);
    const string filename_markerPointsCameraframe(argv[2]);
    const string filename_robot_configurations(argv[3]);
    const string filename_workcell(argv[4]);
    ifstream ifs(filename_markerPointsMarkerframe);
    if(!ifs.is_open())
        throw runtime_error("Registration:\n"
                            "Could not open file with 3d parker points in marker frame.");
    Eigen::Matrix3Xd markerPointsMarkerframe;
    size_t num_checkerboard_corners;
    {
        std::vector<Eigen::Vector3d,Eigen::aligned_allocator<Eigen::Vector3d> > vectorM;
        string line;
        while(getline(ifs,line)) {
            stringstream ss(line);
            Eigen::Vector3d point3d(3);
            ss >> point3d(0) >> point3d(1) >> point3d(2);
            vectorM.push_back(point3d);
        }
        num_checkerboard_corners=vectorM.size();
        markerPointsMarkerframe.conservativeResize(3,vectorM.size());
        for(size_t i=0; i<num_checkerboard_corners; ++i)
            markerPointsMarkerframe.block<3,1>(0,i)=vectorM[i];
    }
    ifs.close();

    //Load stereo points
    ifs.open(filename_markerPointsCameraframe);
    if(!ifs.is_open())
        throw runtime_error("Registration:\n"
                            "Could not open file with 3d parker points in camera frame.");
    Eigen::Matrix3Xd markerPointsCameraframe;
    size_t num_configurations;
    {
        std::vector<Eigen::Vector3d,Eigen::aligned_allocator<Eigen::Vector3d> > vectorM;
        string line;
        while(getline(ifs,line)) {
            stringstream ss(line);
            Eigen::Vector3d point3d(3);
            ss >> point3d(0) >> point3d(1) >> point3d(2);
            vectorM.push_back(point3d);
        }
        if((vectorM.size()%num_checkerboard_corners)!=0)
            throw runtime_error("Number of 3d marker points in camera frame is not a multiple of checkerboard corners.");
        num_configurations=vectorM.size()/num_checkerboard_corners;
        markerPointsCameraframe.conservativeResize(3,vectorM.size());
        for(size_t i=0; i<vectorM.size(); ++i)
            markerPointsCameraframe.block<3,1>(0,i)=vectorM[i];
    }
    ifs.close();

    //Obtain camTmarker poses
    Eigen::Matrix4Xd camTmarker(4,4*num_configurations);
    for(size_t i=0; i<num_configurations; ++i)
        camTmarker.block<4,4>(0,4*i)=rovi::Registration::estimateCamToMarkerEig(markerPointsMarkerframe,markerPointsCameraframe.block(0,i*num_checkerboard_corners,3,num_checkerboard_corners));

// //This can be done in MATLAB!
//    //Transform 3d marker points in camera frame to marker frame
//    Eigen::Matrix3Xd markerPointsTransformedFromCamToMarker(3,num_checkerboard_corners*num_configurations);
//    for(size_t i=0; i<num_configurations; ++i) {
//        const Eigen::Matrix4d markerTcam = rovi::inverseHomogeneousTransform(camTmarker.block<4,4>(0,i*4));
//        for(size_t j=0; j<num_checkerboard_corners; ++j) {
//            Eigen::Vector4d pointProjective3d;
//            pointProjective3d << markerPointsCameraframe.block<3,1>(0,i*num_checkerboard_corners+j), 1.0;
//            markerPointsTransformedFromCamToMarker.block<3,1>(0,i*num_checkerboard_corners+j) =
//                    (markerTcam*pointProjective3d).segment<3>(0);
//        }
//    }

    //Obtain baseTend transforms
    rw::models::WorkCell::Ptr workcell(rw::loaders::WorkCellLoader::Factory::load(filename_workcell));
    rw::models::Device::Ptr device(workcell->findDevice("UR1"));
    rw::kinematics::State state(workcell->getDefaultState());
    Eigen::Matrix4Xd baseTend(4,4*num_configurations);
    const size_t num_joints = device->getDOF();
    //cout << "Joints: " << num_joints << endl;
    ifs.open(filename_robot_configurations);
    if(!ifs.is_open())
        throw runtime_error("Registration:\n"
                            "Could not open file with robot configurations.");
    for(size_t i=0; i<num_configurations; ++i) {
        rw::math::Q q(num_joints);
        for(size_t j=0; j<num_joints; ++j)
            ifs >> q(j);
        device->setQ(q,state);
        rw::math::Transform3D<double> ith_baseTend = device->baseTend(state);
        if(multiply_baseTend_translation_by_minus_1)
            ith_baseTend.P() *= -1.0;
        baseTend.block<3,3>(0,4*i)=ith_baseTend.R().e();
        baseTend.block<3,1>(0,4*i+3)=ith_baseTend.P().e();
        baseTend.block<1,3>(3,4*i).setZero();
        baseTend(3,4*i+3)=1.0;
    }
    ifs.close();

    //Export camTmarker and baseTend
    ofstream ofs("camTmarker.txt");
    ofs << scientific << setprecision(16) << camTmarker;
    ofs.close();
    ofs.open("baseTend.txt");
    ofs << scientific << setprecision(16) << baseTend;
    ofs.close();

    //Calibrate with AX=YB method
    //baseTend_i * endTmarker = baseTcam * camTmarker_i
    //    A_i    *     X      =    Y     *      B_i
    rovi::Registration myRegistration;
    myRegistration.calibrate_Shah(baseTend,camTmarker);
    //myRegistration.calibrate_QR24Eig(baseTend,camTmarker);
    Eigen::Matrix4d baseTcam = myRegistration.getWorldTcamEig();
    Eigen::Matrix4d endTmarker = myRegistration.getEndTmarkerEig();
    Eigen::Matrix4d baseTcamOrthogonal;
    baseTcamOrthogonal << rovi::Registration::nearestOrthonormalRotation(baseTcam.block<3,3>(0,0)), baseTcam.block<3,1>(0,3),
            0.0,0.0,0.0,1.0;
    Eigen::Matrix4d endTmarkerOrthogonal;
    endTmarkerOrthogonal << rovi::Registration::nearestOrthonormalRotation(endTmarker.block<3,3>(0,0)), endTmarker.block<3,1>(0,3),
            0.0,0.0,0.0,1.0;

    double error_translational_rmse_training, error_rotational_mae_training;
    std::tie(error_translational_rmse_training,error_rotational_mae_training) = myRegistration.computeErrorsEig(baseTend,camTmarker);
    cout << "Translational error: " << error_translational_rmse_training << "\nRotational error: " << error_rotational_mae_training << endl;

    //Export baseTcam, endTmarker, baseTcamOrthogonal and endTmarkerOrthogonal
    ofs.open("baseTcam.txt");
    ofs << scientific << setprecision(16) << baseTcam;
    ofs.close();
    ofs.open("endTmarker.txt");
    ofs << scientific << setprecision(16) << endTmarker;
    ofs.close();
    ofs.open("baseTcamOrthogonal.txt");
    ofs << scientific << setprecision(16) << baseTcamOrthogonal;
    ofs.close();
    ofs.open("endTmarkerOrthogonal.txt");
    ofs << scientific << setprecision(16) << endTmarkerOrthogonal;
    ofs.close();

}

int main(int argc, char *argv[]){
    //test_workcell();
    constexpr bool multiply_baseTend_translation_by_minus_1 = true;

    //num_checkerboard_corners stereoPoints.txt
    const size_t num_input_arguments = static_cast<size_t>(argc-1);

    //Parse input
    if(!(num_input_arguments==6)) {
        throw runtime_error("Registration:\n"
                            "Number of input arguments is wrong. Usage:\n"
                            "test_registration path/to/3DMarkerPointsInMarkerframe.txt "
                            "path/to/3DMarkerPointsInCameraframe.txt path/to/robotConfigurations.txt "
                            "path/to/workcell.xml path/to/trainIndices.txt path/to/testIndices.txt");
    }

    const string filename_markerPointsMarkerframe(argv[1]);
    const string filename_markerPointsCameraframe(argv[2]);
    const string filename_robot_configurations(argv[3]);
    const string filename_workcell(argv[4]);
    const string filename_trainIndices(argv[5]);
    const string filename_testIndices(argv[6]);
    ifstream ifs(filename_markerPointsMarkerframe);
    if(!ifs.is_open())
        throw runtime_error("Registration:\n"
                            "Could not open file with 3d parker points in marker frame.");
    Eigen::Matrix3Xd markerPointsMarkerframe;
    size_t num_checkerboard_corners;
    {
        std::vector<Eigen::Vector3d,Eigen::aligned_allocator<Eigen::Vector3d> > vectorM;
        string line;
        while(getline(ifs,line)) {
            stringstream ss(line);
            Eigen::Vector3d point3d(3);
            ss >> point3d(0) >> point3d(1) >> point3d(2);
            vectorM.push_back(point3d);
        }
        num_checkerboard_corners=vectorM.size();
        markerPointsMarkerframe.conservativeResize(3,vectorM.size());
        for(size_t i=0; i<num_checkerboard_corners; ++i)
            markerPointsMarkerframe.block<3,1>(0,i)=vectorM[i];
    }
    ifs.close();

    //Load training indices
    ifs.open(filename_trainIndices);
    if(!ifs.is_open())
        throw runtime_error("Registration:\n"
                            "Could not open file with training indices.");
    vector<size_t> trainIndices;
    {
        string line;
        while(getline(ifs,line)) {
            stringstream ss(line);
            size_t idx;
            ifs >> idx;
            trainIndices.push_back(idx-1);
        }
    }
    ifs.close();

    //Load testing indices
    ifs.open(filename_testIndices);
    if(!ifs.is_open())
        throw runtime_error("Registration:\n"
                            "Could not open file with testing indices.");
    vector<size_t> testIndices;
    {
        string line;
        while(getline(ifs,line)) {
            stringstream ss(line);
            size_t idx;
            ifs >> idx;
            testIndices.push_back(idx-1);
        }
    }
    ifs.close();
    const bool fullTrain=testIndices.size()==0;

    //Load stereo points
    ifs.open(filename_markerPointsCameraframe);
    if(!ifs.is_open())
        throw runtime_error("Registration:\n"
                            "Could not open file with 3d parker points in camera frame.");
    Eigen::Matrix3Xd markerPointsCameraframe;
    size_t num_configurations;
    {
        std::vector<Eigen::Vector3d,Eigen::aligned_allocator<Eigen::Vector3d> > vectorM;
        string line;
        while(getline(ifs,line)) {
            stringstream ss(line);
            Eigen::Vector3d point3d(3);
            ss >> point3d(0) >> point3d(1) >> point3d(2);
            vectorM.push_back(point3d);
        }
        if((vectorM.size()%num_checkerboard_corners)!=0)
            throw runtime_error("Number of 3d marker points in camera frame is not a multiple of checkerboard corners.");
        num_configurations=vectorM.size()/num_checkerboard_corners;
        markerPointsCameraframe.conservativeResize(3,vectorM.size());
        for(size_t i=0; i<vectorM.size(); ++i)
            markerPointsCameraframe.block<3,1>(0,i)=vectorM[i];
    }
    ifs.close();

    //Obtain camTmarker poses
    Eigen::Matrix4Xd camTmarker(4,4*num_configurations);
    for(size_t i=0; i<num_configurations; ++i)
        camTmarker.block<4,4>(0,4*i)=rovi::Registration::estimateCamToMarkerEig(markerPointsMarkerframe,markerPointsCameraframe.block(0,i*num_checkerboard_corners,3,num_checkerboard_corners));

// //This can be done in MATLAB!
//    //Transform 3d marker points in camera frame to marker frame
//    Eigen::Matrix3Xd markerPointsTransformedFromCamToMarker(3,num_checkerboard_corners*num_configurations);
//    for(size_t i=0; i<num_configurations; ++i) {
//        const Eigen::Matrix4d markerTcam = rovi::inverseHomogeneousTransform(camTmarker.block<4,4>(0,i*4));
//        for(size_t j=0; j<num_checkerboard_corners; ++j) {
//            Eigen::Vector4d pointProjective3d;
//            pointProjective3d << markerPointsCameraframe.block<3,1>(0,i*num_checkerboard_corners+j), 1.0;
//            markerPointsTransformedFromCamToMarker.block<3,1>(0,i*num_checkerboard_corners+j) =
//                    (markerTcam*pointProjective3d).segment<3>(0);
//        }
//    }

    //Obtain worldTend transforms
    rw::models::WorkCell::Ptr workcell(rw::loaders::WorkCellLoader::Factory::load(filename_workcell));
    rw::models::Device::Ptr device(workcell->findDevice("UR1"));
    rw::kinematics::State state(workcell->getDefaultState());
    rw::kinematics::Frame* worldFrame = workcell->findFrame("WORLD");
    rw::kinematics::Frame* endFrame = device->getEnd();
    Eigen::Matrix4Xd worldTend(4,4*num_configurations);
    const size_t num_joints = device->getDOF();
    //cout << "Joints: " << num_joints << endl;
    ifs.open(filename_robot_configurations);
    if(!ifs.is_open())
        throw runtime_error("Registration:\n"
                            "Could not open file with robot configurations.");
    for(size_t i=0; i<num_configurations; ++i) {
        rw::math::Q q(num_joints);
        for(size_t j=0; j<num_joints; ++j)
            ifs >> q(j);
        device->setQ(q,state);
        //rw::math::Transform3D<double> ith_baseTend = device->baseTend(state);
        rw::math::Transform3D<double> ith_worldTend = worldFrame->fTf(endFrame,state);
        if(multiply_baseTend_translation_by_minus_1)
            ith_worldTend.P() *= -1.0;
        worldTend.block<3,3>(0,4*i)=ith_worldTend.R().e();
        worldTend.block<3,1>(0,4*i+3)=ith_worldTend.P().e();
        worldTend.block<1,3>(3,4*i).setZero();
        worldTend(3,4*i+3)=1.0;
    }
    ifs.close();

    //Split into training and test set
    Eigen::Matrix4Xd camTmarker_test(4,4*(testIndices.size()));
    Eigen::Matrix4Xd worldTend_test(4,4*(testIndices.size()));
    if(!fullTrain) {
        Eigen::Matrix4Xd tmp_camTmarker=camTmarker;
        Eigen::Matrix4Xd tmp_baseTend=worldTend;
        camTmarker.conservativeResize(4,4*trainIndices.size());
        worldTend.conservativeResize(4,4*trainIndices.size());
        for(size_t i=0; i<trainIndices.size(); ++i) {
            camTmarker.block<4,4>(0,4*i)=tmp_camTmarker.block<4,4>(0,4*trainIndices[i]);
            worldTend.block<4,4>(0,4*i)=tmp_baseTend.block<4,4>(0,4*trainIndices[i]);
        }
        for(size_t i=0; i<testIndices.size(); ++i) {
            camTmarker_test.block<4,4>(0,4*i)=tmp_camTmarker.block<4,4>(0,4*testIndices[i]);
            worldTend_test.block<4,4>(0,4*i)=tmp_baseTend.block<4,4>(0,4*testIndices[i]);
        }
    }

    //Export camTmarker and worldTend
    ofstream ofs("camTmarker.txt");
    ofs << scientific << setprecision(16) << camTmarker;
    ofs.close();
    ofs.open("worldTend.txt");
    ofs << scientific << setprecision(16) << worldTend;
    ofs.close();

    //Calibrate with AX=YB method
    //worldTend_i * endTmarker = worldTcam * camTmarker_i
    //    A_i    *     X      =    Y     *      B_i
    rovi::Registration myRegistration;
    myRegistration.calibrate_Shah(worldTend,camTmarker);
    //myRegistration.calibrate_QR24Eig(baseTend,camTmarker);
    Eigen::Matrix4d worldTcam = myRegistration.getWorldTcamEig();
    Eigen::Matrix4d endTmarker = myRegistration.getEndTmarkerEig();
    Eigen::Matrix4d worldTcamOrthogonal;
    worldTcamOrthogonal << rovi::Registration::nearestOrthonormalRotation(worldTcam.block<3,3>(0,0)), worldTcam.block<3,1>(0,3),
            0.0,0.0,0.0,1.0;
    Eigen::Matrix4d endTmarkerOrthogonal;
    endTmarkerOrthogonal << rovi::Registration::nearestOrthonormalRotation(endTmarker.block<3,3>(0,0)), endTmarker.block<3,1>(0,3),
            0.0,0.0,0.0,1.0;

    double error_translational_rmse_training, error_rotational_mae_training;
    std::tie(error_translational_rmse_training,error_rotational_mae_training) = myRegistration.computeErrorsEig(worldTend,camTmarker);
    double error_translational_rmse_testing, error_rotational_mae_testing;
    if(!fullTrain)
        std::tie(error_translational_rmse_testing,error_rotational_mae_testing) = myRegistration.computeErrorsEig(worldTend_test,camTmarker_test);

    //cout << scientific <<setprecision(16) << "Translational error: " << error_translational_rmse_training << "\nRotational error: " << error_rotational_mae_training << endl;

    //Export worldTcam, endTmarker, worldTcamOrthogonal, endTmarkerOrthogonal, training error and testing error
    ofs.open("worldTcam.txt");
    ofs << scientific << setprecision(16) << worldTcam;
    ofs.close();
    ofs.open("endTmarker.txt");
    ofs << scientific << setprecision(16) << endTmarker;
    ofs.close();
    ofs.open("worldTcamOrthogonal.txt");
    ofs << scientific << setprecision(16) << worldTcamOrthogonal;
    ofs.close();
    ofs.open("endTmarkerOrthogonal.txt");
    ofs << scientific << setprecision(16) << endTmarkerOrthogonal;
    ofs.close();
    ofs.open("error_training.txt");
    ofs << scientific << setprecision(16) << error_translational_rmse_training << ' ' << error_rotational_mae_training;
    ofs.close();
    if(!fullTrain) {
        ofs.open("error_testing.txt");
        ofs << scientific << setprecision(16) << error_translational_rmse_testing << ' ' << error_rotational_mae_testing;
        ofs.close();
    }

    return 0;
}
