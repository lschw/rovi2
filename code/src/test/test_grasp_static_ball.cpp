#include <iostream>
#include <thread>

#include <ros/init.h>
#include <ros/macros.h>

#include <rw/models/WorkCell.hpp>
#include <rw/models/Device.hpp>
#include <rw/loaders/WorkCellLoader.hpp>
#include <rw/kinematics/State.hpp>

#include <rwlibs/pathoptimization/pathlength/PathLengthOptimizer.hpp>

#include "pathplanning/pathplanner.hpp" //Funny LogWriter vtable problem!!!! YEAH! // this pos in include shouldn't be a problem - Lukas
#include "tracker/tracker.hpp"
#include "pathplanning/constraintgenerator.hpp"
#include "hwcomponents/robot.h"
#include "hwcomponents/gripper.hpp"
#include "graspplanner/graspplanner.hpp"
#include "utils/log.hpp"



// used to either test the grasp from different angles or use the ball vel.
#define GRASP_VELOCITY 0
#define GRASP_PREDEFINED 1
#define GRASPING_DIRECTION GRASP_VELOCITY

// selfexplanatory - STUPITIDO!
#define NONE 0
#define PG70 1
#define USE_GRIPPER PG70


using namespace std;

std::atomic_bool stop_trackerLoop;
std::atomic_bool grasp_ball;

/// *********************** TODO *******************************
/// Use prm-wc for graspplanner, otherwise it can generate Q's in collision.
/// Check for collision with ball when planning path? it keeps hitting it from above when moving down.
/// Possibly an approach phase should be implemented!?!?
/// How to handle the moving of the obj when planning? might generate an initial or final state in collision because ball is there after update.
/// Can this be solved by only moving the ball when the destination does not result in a collision?
/// What if it would result in a collision? should the ball just disappear then?


void trackerLoop(const size_t num_nodes) {

    std::string wcFile = "../../workcell/WorkStation3/WS3_Scene_prm.wc.xml";
//    std::string robot = "UR1";
    rw::models::WorkCell::Ptr workcell=(rw::loaders::WorkCellLoader::Factory::load(wcFile));
    rw::models::Device::Ptr device=workcell->findDevice("UR1");
    rw::kinematics::Frame* camera = workcell->findFrame("CameraSimLeft");
//    rw::kinematics::Frame* world = workcell->findFrame("WORLD");
//    rw::kinematics::Frame* base = workcell->findFrame("UR1.Base");
//    //rw::math::Transform3D<double> camTworld = camera->fTf(world,workcell->getDefaultState());
    rw::kinematics::State state = workcell->getDefaultState();
    rw::math::Transform3D<double> worldTcam = rw::kinematics::Kinematics::worldTframe(camera,state);
//    //rw::math::Transform3D<double> baseTcam = base->fTf(camera,workcell->getDefaultState());
    rw::kinematics::Frame* tcp = workcell->findFrame("PG70.TCP");
    rw::kinematics::MovableFrame* ball = dynamic_cast<rw::kinematics::MovableFrame*>(workcell->findFrame("Ball"));

    enum GraspingState {
        Approach, Grasping, ReturningHome,
    };
    GraspingState robot_state = Approach;


    rovi::GraspPlanner grasper(device,tcp,workcell);

    grasper.setTarget(dynamic_cast<rw::kinematics::Frame*>(ball));
    grasper.setRandomness(20.0*rw::math::Deg2Rad, 200);

    rovi::pathplanners::ConstraintGenerator robot_move_const;
    robot_move_const.setPointerDistance(3.0,0.1); //first: length of rod. second: distance from ball center to rod end.
    rw::kinematics::MovableFrame* pointer = dynamic_cast<rw::kinematics::MovableFrame*>(workcell->findFrame("ball_rod"));
    robot_move_const.setOrigo(camera);
    robot_move_const.setPointer(pointer);

    cout << "Creating planner..." << endl;
    rovi::PathPlanner planner(workcell,"UR1","ball_rod",num_nodes);
    cout << "Planner created." << endl;



    cout << "Connecting to UR5 robot..." << endl;
    const std::string acting_namespace = "caros_universalrobot";
    rovi::hwcomponents::Robot ur5(acting_namespace);
    if(!ur5.isOK()){
        ROVI_THROW("Could not connect to robot!\n");
    }

#if USE_GRIPPER == PG70
    cout << "Connecting to PG70..." << endl;
    const std::string acting_namespace_gripper = "caros_schunkpg70";
    rovi::hwcomponents::Gripper schunk(acting_namespace_gripper);
    if(!schunk.isOK()){
        ROVI_THROW("Could not connect to robot!\n");
    } else {
        schunk.open();
    }
#endif

//    rwlibs::pathoptimization::PathLengthOptimizer myOptimizer(planner._constraint,planner._metric);

    rovi::Tracker ballTracker;
    cout << "Starting ball tracker in separate thread..." << endl;

    stop_trackerLoop=false;
    grasp_ball=false;

    ballTracker.startTracker();
    cout << "Ball tracker is running." << endl;

    rw::math::Vector3D<double> ballpos, ballvel;
    rw::math::Transform3D<double> ballpose;

    while(!stop_trackerLoop) {
        //Blocks until new ball state (up to 50 ms on live system, same rate as new images coming in).
        //There is also a non-blocking version which just gets the latest state.
//        const rovi::Vector9d ball_state = ballTracker.getBallBlocking();
//        cout << "Ball pos: " << ball_state.segment<3>(0).transpose()
//             << ", vel: " << ball_state.segment<3>(3).transpose() << endl;

        ballTracker.getBallPosVelBlocking(ballpos,ballvel);
        // apply magic number
        ballpos(0) *= -1;
//        std::cout << "T_cam^ball  : " << ballpos << "\n";
//        cout << "Ball velocity in camera frame: " << ballvel << endl;
        robot_move_const.pointTowards(rw::math::Transform3D<double>(ballpos), state);
        ballpose=(worldTcam*rw::math::Transform3D<double>(ballpos));
        ballpos = ballpose.P();
//        std::cout << "T_world^ball: " << ballpose.P() << "\n";
//        ballvel = (worldTcam*rw::math::Transform3D<double>(ballvel)).P();
        ballvel = worldTcam.R()*ballvel;
//        cout << "Ball velocity in world frame: " << ballvel << endl;

        ball->setTransform(ballpose,state);

//        rw::math::Transform3D<> constraint = rw::kinematics::Kinematics::worldTframe(pointer,state);

//        planner.move_constraint(constraint);
        planner.updateState(state);

        if(grasp_ball) {
            cout << "Grasping ball!" << endl;
            cout << "Ball pose: " << ballpose << endl;
            //ball->setTransform(ballpose,state);
            try {
                if(robot_state == Approach){
#if GRASPING_DIRECTION == GRASP_PREDEFINED
                    rw::math::Vector3D<> graspdir(0,0,1);
                    rw::math::Q graspQ = grasper.planGrasp(graspdir,state);
#elif GRASPING_DIRECTION == GRASP_VELOCITY
                    cout << "Plan grasp with ballvel: " << ballvel << endl;
                    rw::math::Q graspQ = grasper.planGrasp(ballvel,state);
#endif
                    cout << graspQ << endl;

                    //rw::math::Q start = device->getQ(state);
                    rw::math::Q start = ur5.getConfiguration();
                    cout << "Init q: " << start << endl;
                    rw::trajectory::QPath path = planner.plan(start,graspQ,true);
                    std::cout << "Planning done." << std::endl;

//                    cout << "Path length: " << path.size() << endl;
//                    path=myOptimizer.shortCut(path);
                    //                path = myOptimizer.pathPruning(path);
//                    cout << "Shortcut optimized to: " << path.size() << endl;

                    cout << "Moving robot..." << endl;
                    ur5.movePtp(path);
                    bool ur5isMoving=ur5.isMoving();
                    bool ur5isOK=ur5.isOK();
                    while(ur5isMoving && ur5isOK) {
                        ur5isMoving=ur5.isMoving();
                        ur5isOK=ur5.isOK();
                    }
                    if(!ur5.isNearGoal(path[path.size()-1])){
                        if(!ur5isMoving)
                            ROVI_WARN("UR5 is not moving.");
                        if(!ur5isOK)
                            ROVI_WARN("UR5 is not OK");
                        ROVI_THROW("Did not make it all the way to goal!");
                    }
                    cout << "Move complete." << endl;
                    robot_state = Grasping;
                }

                if(robot_state == Grasping){
                    cout << "Grasping..." << endl;
#if USE_GRIPPER == PG70
                    // grasp the ball
                    rw::math::Q force(1,30.0);
                    schunk.grip(force);
                    while (schunk.isMoving());
//                      if(schunk.getConfiguration().norm2() < 0.001){
//                        ROVI_THROW("Did not grasp the ball!");
//                    }
                    std::cout << "Grasped the ball at " << schunk.getConfiguration() << std::endl;
                    robot_state = ReturningHome;
                }
                if(robot_state == ReturningHome){
                    cout << "Moving to upright cup..." << endl;

                    // move home...
                    //rw::math::Q homeQ(6, 0.0, -rw::math::Pi/2, 0.0, -rw::math::Pi/2, 0.0, 0.0);

                    //ball pose above upright cup:
                    // -0.675  0.05  0.95
                    ball->setTransform(rw::math::Transform3D<double>(rw::math::Vector3D<double>(-0.675,0.05,0.95)),state);
                    planner.updateState(state);

                    cout << "planning the grasp..." << endl;
                    rw::math::Vector3D<> graspdir(0,0,1);
                    rw::math::Q dropCupQ = grasper.planGrasp(graspdir,state);

                    // upright cup location
                    //rw::math::Q dropCupQ(6,1.2829887229730952, -1.0572035099420358, 1.2820337815848513, -1.7562637239754944, -1.4019357214442802, 0.8056240716330295);

                    rw::math::Q start = ur5.getConfiguration();
                    cout << "Init q: " << start << endl;
                    rw::trajectory::QPath path = planner.plan(start,dropCupQ,true);
                    cout << "Path found. Moving!" << endl;

//                    cout << "Path length: " << path.size() << endl;
//                    path=myOptimizer.shortCut(path);
//                    cout << "Shortcut optimized to: " << path.size() << endl;

                    ur5.movePtp(path);
                    bool ur5isMoving=ur5.isMoving();
                    bool ur5isOK=ur5.isOK();
                    while(ur5isMoving && ur5isOK) {
                        ur5isMoving=ur5.isMoving();
                        ur5isOK=ur5.isOK();
                    }
                    if(!ur5.isNearGoal(path[path.size()-1])){
                        if(!ur5isMoving)
                            ROVI_WARN("UR5 is not moving.");
                        if(!ur5isOK)
                            ROVI_WARN("UR5 is not OK");
                        ROVI_THROW("Did not make it all the way to goal!");
                    }
                    //Release ball when in home position
                    schunk.open();

                    cout << "Moving home..." << endl;

                    // move home...
                    rw::math::Q homeQ(6, 0.0, -rw::math::Pi/2, 0.0, -rw::math::Pi/2, 0.0, 0.0);

                    start = ur5.getConfiguration();
                    cout << "Init q: " << start << endl;
                    path = planner.plan(start,homeQ,true);
                    cout << "Path found. Moving!" << endl;

//                    cout << "Path length: " << path.size() << endl;
//                    path=myOptimizer.shortCut(path);
//                    cout << "Shortcut optimized to: " << path.size() << endl;

                    ur5isMoving=ur5.isMoving();
                    ur5isOK=ur5.isOK();
                    while(ur5isMoving && ur5isOK) {
                        ur5isMoving=ur5.isMoving();
                        ur5isOK=ur5.isOK();
                    }
                    if(!ur5.isNearGoal(path[path.size()-1])){
                        if(!ur5isMoving)
                            ROVI_WARN("UR5 is not moving.");
                        if(!ur5isOK)
                            ROVI_WARN("UR5 is not OK");
                        ROVI_THROW("Did not make it all the way to goal!");
                    }
#endif
                }

//                for (size_t i = 0; i < path.size(); ++i) {
//                    //device->setQ(path.at(i),state);
//                    if(ur5.isOK() && ur5.movePtp(path.at(i))){
//                        std::cout << "I AM MOVING!!!" << std::endl;
//                        while(ur5.isMoving());
//                    } else {
//                        throw runtime_error("Could not move robot!\n");
//                    }
//                }

                planner.write();

                stop_trackerLoop=true;

            } catch(std::runtime_error &e) {
                cout << "NO GRASP AVAILABLE! RETRYING!" << endl;
                std::cout << "Failed with: " << e.what() << "\n";
            }
        }
    }
    cout << "Stopping ball tracker." << endl;
    ballTracker.stopTracker();
}


int main(int argc, char *argv[]){
    const size_t num_input_arguments = static_cast<size_t>(argc-1);

    cout << "Tracking system" << endl;
    int dummyInt=0;
    char **dummyPtr=nullptr;
    ros::init(dummyInt, dummyPtr,"stereocamera_imgreader");
    ROS_INFO("Connected to roscore");


    size_t num_nodes=2000;
    if(num_input_arguments==1)
        stringstream(argv[1]) >> num_nodes;

    thread ballTrackerLoop(trackerLoop,num_nodes);


    while(true) {
        cout << "Enter q to quit or g to grasp static ball." << endl;
        char c;
        cin >> c;
        if(c=='q'){
            break;
        } else if(c=='g'){
            grasp_ball=true;
        } else {
            grasp_ball=false;
        }
    }
    stop_trackerLoop=true;
    ballTrackerLoop.join();



    cout << "Ball tracker stopped." << endl;

    return 0;
}
