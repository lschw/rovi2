#include <iostream>
#include <fstream>
#include <ctime>
#include <ratio>
#include <chrono>


#include <eigen3/Eigen/Dense>


#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include "../vision/balldetection.hpp"

int main(){
    std::cout << " -- Finding uncertainty of Vision Algorithm --\n";

    // settings
    int repeats = 1000;
    int notfound = 0;
    int ball_radius_min = 20, ball_radius_max = 35;
    double noise_var = 5;

    // covariance
    Eigen::Matrix2d cov;
    double mean_x = 0,mean_y = 0;
    int number_images = 0;
    cov.setZero();

    // load the test image
    std::string filename = "../../data/cam_calib/Group4/test_images/left_rect.png";
    cv::Mat img = cv::imread(filename);
    std::string filename_ball = "../../data/cam_calib/Group4/test_images/ball.png";
    cv::Mat ball = cv::imread(filename_ball);

    // make rnd generator
    cv::RNG ran(0);

    rovi::vision::BallDetector detector;

    std::string savedata_filename = "balldetector_data.csv";
    std::fstream file(savedata_filename, std::fstream::out);

    // make tests
    for(int i = 0; i < repeats; i++){
        // clone image
        cv::Mat test_img = img.clone();

        // genreate random position on image within view
        cv::Point2i pos;
        pos.x = ran.uniform(0, test_img.cols - ball.cols * 2);
        pos.y = ran.uniform(0, test_img.rows - ball.rows * 2);

        // impose fake ball
        cv::Size axes;
        axes.height = ran.uniform(ball_radius_min, ball_radius_max);
        axes.width = ran.uniform(ball_radius_min, ball_radius_max);

        /// Compute a rotation matrix with respect to the center of the image
        double angle = ran.uniform(0.0, 360.0);
        double scale = ran.uniform(0.8, 2.0);
        int size = ball.rows * scale;

        cv::Mat ball_rot_final(size, size, ball.type());
        cv::Mat ball_rot(ball.rows, ball.rows, ball.type());
        cv::Point center = cv::Point( ball.cols/2, ball.rows/2);
        ball_rot.setTo(cv::Vec3b(255,255,255));
        ball_rot_final.setTo(cv::Vec3b(255,255,255));

        /// Get the rotation matrix with the specifications above
        // scale and rotation are done each alone since the refrence point for the transform is changing to keep it within img bounds
        cv::Mat rot_mat( 2, 3, CV_32FC1 );
        rot_mat = cv::getRotationMatrix2D( center, angle, 1 );

        /// Rotate the warped image
        cv::warpAffine( ball, ball_rot, rot_mat, ball_rot.size(), cv::INTER_LINEAR, cv::BORDER_TRANSPARENT);

        // scale
        rot_mat = cv::getRotationMatrix2D( cv::Point(0,0), 0, scale );
        cv::warpAffine( ball_rot, ball_rot_final, rot_mat, ball_rot_final.size(), cv::INTER_NEAREST, cv::BORDER_TRANSPARENT);

        double alpha = 0.8;
        for(int x = 0; x < ball_rot_final.cols; x++){
            for(int y = 0; y < ball_rot_final.rows; y++){
                double a = 0;
                if((y - ball_rot_final.rows/2)*(y - ball_rot_final.rows/2) + (x - ball_rot_final.cols/2)*(x - ball_rot_final.cols/2)
                        > (ball_rot_final.cols/2 - 1)*(ball_rot_final.cols/2 - 1 )){
                    a = alpha;
                }

                if((ball_rot_final.at< cv::Vec3b >(y,x) != cv::Vec3b(255,255,255))){
                    test_img.at< cv::Vec3b >(y + pos.y, x + pos.x) = ball_rot_final.at< cv::Vec3b >(y,x) * (1 - a) + test_img.at< cv::Vec3b >(y + pos.y, x + pos.x) * a;
                }
            }
        }

//        // add noise
        for(int x = 0; x < ball_rot_final.cols; x++){
            for(int y = 0; y < ball_rot_final.rows; y++){
                if((y - ball_rot_final.rows/2)*(y - ball_rot_final.rows/2) + (x - ball_rot_final.cols/2)*(x - ball_rot_final.cols/2)
                        <= (ball_rot_final.cols/2 - 1)*(ball_rot_final.cols/2 - 1 )){
                    for(int j = 0; j < ball_rot_final.channels(); j++){
                        double noise = ran.gaussian(noise_var);
                        int res = test_img.at< cv::Vec3b >(y + pos.y, x + pos.x)[j];
                        res = res + (int)noise;
                        if(res > 255){
                            res = 255;
                        } else if(res < 0) {
                            res = 0;
                        }
                        test_img.at< cv::Vec3b >(y + pos.y, x + pos.x)[j] = res;
                    }
                }
            }
        }

        // add blur to around the ball
        int filtersize = 5;
        int pixelblurouter = 0, pixelblurinner = 2;
        for(int x = -pixelblurouter; x < ball_rot_final.cols + pixelblurouter; x++){
            for(int y = -pixelblurouter; y < ball_rot_final.rows + pixelblurouter; y++){
                // if outside ball radius and not a transparent color then smooth
                double distance = (y - ball_rot_final.rows/2)*(y - ball_rot_final.rows/2) + (x - ball_rot_final.cols/2)*(x - ball_rot_final.cols/2);
                if( distance >= (ball_rot_final.cols/2 - pixelblurinner)*(ball_rot_final.cols/2 - pixelblurinner) &&
                        distance <= (ball_rot_final.cols/2 + pixelblurouter)*(ball_rot_final.cols/2 + pixelblurouter)
                        //                        && (ball_rot_final.at< cv::Vec3b >(y,x) != cv::Vec3b(255,255,255))
                        ){
                    // blur based on pixels around it
                    cv::Vec3i val = cv::Vec3i(0,0,0);
                    int n = 0;
                    for(int l = -filtersize/2; l < filtersize/2; l++){
                        for(int k = -filtersize/2; k < filtersize/2; k++){
                            // check if inside the img
                            if(x + pos.x + l >= 0 && x + pos.x + l < test_img.rows && y + pos.y + k >= 0 && y + pos.y + k < test_img.cols){
                                val += test_img.at< cv::Vec3b >(y + pos.y + k, x + pos.x + l);
                                n++;
                            }
                        }
                    }
                    val /= n;
                    test_img.at< cv::Vec3b >(y + pos.y, x + pos.x) = val;
                }
            }
        }

        // find ball pose
        std::vector< cv::Point2d > points;

        // time
        std::chrono::high_resolution_clock::time_point t1 = std::chrono::high_resolution_clock::now();
        detector.findBlob(test_img, points);
        std::chrono::high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();

        std::chrono::duration<double> time_span = std::chrono::duration_cast< std::chrono::duration<double> >(t2 - t1);

        if(points.size() == 1){
            double xerr = pos.x + (double)ball_rot_final.cols/2 - points[0].x;
            double yerr = pos.y + (double)ball_rot_final.rows/2 - points[0].y;
//            std::cout << "error: (" << xerr << ", " << yerr << ")\n";
            // use to calculate uncertainty
            mean_x += xerr;
            mean_y += yerr;
            number_images++;
            cov(0,0) += xerr * xerr;
            cov(1,1) += yerr * yerr;
            cov(0,1) = cov(1,0) += xerr * yerr;

            file << xerr << ", " << yerr << ", " << time_span.count() << "\n";
        } else {
            std::cout << "found: " << points.size() << " balls.\n";
            notfound++;
        }


        // show img
//        cv::imshow("Random ball", test_img);
//        cv::waitKey(0);

        // store values
    }

    mean_x /= number_images;
    mean_y /= number_images;
    cov(0,0) /= number_images;
    cov(0,1) /= number_images;
    cov(1,1) /= number_images;
    cov(1,0) /= number_images;

    cov(0,0) -= mean_x * mean_x;
    cov(1,1) -= mean_y * mean_y;
    cov(1,0) -= mean_x * mean_y;
    cov(0,1) -= mean_x * mean_y;

    std::cout << "The covariance found was:\n" << cov << "\n";
    std::cout << "The ball was not found: " << notfound << " / " << repeats << "\n";

    return 0;
}
