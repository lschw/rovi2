#include <iostream>
#include <iomanip>
#include <string>
#include <fstream>
#include <exception>
#include <cmath>
#include "kalman/kalman.hpp"
#include "utils/defines_matrices.hpp"
#include <eigen3/Eigen/Core>

using namespace rovi;
using namespace std;



int main(int argc, char *argv[]){
    const size_t num_input_arguments = static_cast<size_t>(argc-1);

    //Parse input
    if(!((num_input_arguments==4))) {
        throw runtime_error("Triangulate\n"
                "Wrong number of input arguments.\n"
                "Syntax:\n"
                "test_trackingsystem path/to/trajectory_reference.txt path/to/trajectory_noisy.txt path/to/measurement_noise_covariance_matrices.txt acceleration_delta"
             );
    }
    const string filename_tR(argv[1]);
    const string filename_tN(argv[2]);
    const string filename_R(argv[3]);
    double acceleration_delta; //0.05; //Maximum change in acceleration in one time step delta_t, in m/s^2
    stringstream(argv[4]) >> acceleration_delta;

    //input parameters
    const double dt = 1.0/20.0; //20Hz
    //input (CMake)

    //output
    const string filename_tF("trajectory_filtered.txt");

//    cout << "Test: Kalman filter\n_______________________" << endl;
//    cout << "Importing R..." << endl;
    Eigen::MatrixX3d R(0,3);
    ifstream ifs(filename_R);
    if(!ifs.is_open())
        throw(runtime_error("Measurement noise covariance matrix file was not opened"));
    {
        string line;
        while(getline(ifs,line)) {
            double a,b,c;
            stringstream(line) >> a >> b >> c;
            size_t r=R.rows();
            R.conservativeResize(r+1,Eigen::NoChange);
            R(r,0)=a;
            R(r,1)=b;
            R(r,2)=c;
        }
    }
    ifs.close();

    const size_t num_points = R.rows()/3;


//    cout << "Importing reference and noisy trajectories..." << endl;
    Eigen::MatrixXd tR(num_points,3), tN(num_points,3);
    ifs.open(filename_tR);
    if(!ifs.is_open())
        throw(runtime_error("Reference trajectory file was not opened"));
    ifs >> tR;
    ifs.close();
    ifs.open(filename_tN);
    if(!ifs.is_open())
        throw(runtime_error("Noisy trajectory file was not opened"));
    ifs >> tN;
    ifs.close();

    //cout << "Reference trajectory:\n" << tR << "\n\nNoisy trajectory:\n" << tN << endl;

//    cout << "Creating Kalman filter..." << endl;
    Vector9d initial_state;
    initial_state << tN(0,0),tN(0,1),tN(0,2),0.0,0.0,0.0,0.0,0.0,0.0;
    Kalman myKalman(acceleration_delta,dt);
    //myKalman.setMeasurementNoiseCovarianceMatrix(R.block<3,3>(0,0));
    myKalman.setInitialState(initial_state,R.block<3,3>(0,0),1);
    //cout << myKalman.predict().transpose() << endl << myKalman.update(tN.row(1),R.block<3,3>(1,0)).transpose() << endl;


//    cout << "Creating filtered trajectory output file \"" << filename_tF << "\"..." << endl;
    ofstream ofs(filename_tF);
    if(!ofs.is_open())
        throw(runtime_error("Output file for filtered trajectory was not opened"));
    //ofs << "px py pz pdx pdy pdz pddx pddy pddz ux uy uz udx udy udz uddx uddy uddz\n";
//    cout << "Applying Kalman filter to noisy trajectory..." << endl;
    for(size_t i=0; i<num_points; ++i) {
//        cout << "Iteration " << setw(6) << setfill(' ') << (i+1) << " of " << num_points << ": Predict... " << flush;
        Vector9d prediction =  myKalman.predict();
//        cout << "Update... " << flush;
        Vector9d update = myKalman.update(tN.row(i),R.block<3,3>(i*3,0));
//        Vector9d update = myKalman.update(tN.row(i));
//        cout << "R\n" << R.block<3,3>(i*3,0) << endl;
//        cout << "Output... " << flush;
        ofs << prediction.transpose() << ' ' << update.transpose() << "\n";
//        cout << "Done." << endl;
    }
    ofs.close();


//    cout << "Test has ended. Press enter to get a free segmentation fault." << endl;
//    cin.get();
    return 0;
}

