/*
 * ask predictor where the object will be in the future
 * ask predictor or kallman for the variance
 * if variance of prediction is small
 * * calculate grasp planning,
 * * get current position of robot
 * * calculate path from current pos to grasp position
 * * tell robot to move along that path
 *
 * get xyz from kalman filter
 * if xyz is equal to robot position
 * * close gripper
 * * get current position of robot
 * * calculate path from current pos to home position
 * * tell robot to move along that path
*/
    /// ---- Ideas for now... ----
    /// Run camera-kalman part in seperate thread and update it (the kalman) as often as possible
    /// Find a pose from which to grasp the ball...
    ///

/// *********************** TODO *******************************
/// Use prm-wc for graspplanner, otherwise it can generate Q's in collision.
/// Check for collision with ball when planning path? it keeps hitting it from above when moving down.
/// Possibly an approach phase should be implemented!?!?
/// How to handle the moving of the obj when planning? might generate an initial or final state in collision because ball is there after update.
/// Can this be solved by only moving the ball when the destination does not result in a collision?
/// What if it would result in a collision? should the ball just disappear then?


#include <iostream>
#include <thread>
#include <mutex>

#include <ros/init.h>
#include <ros/macros.h>

#include <rw/models/WorkCell.hpp>
#include <rw/models/Device.hpp>
#include <rw/loaders/WorkCellLoader.hpp>
#include <rw/kinematics/State.hpp>

#include <rwlibs/pathoptimization/pathlength/PathLengthOptimizer.hpp>

#include "pathplanning/pathplanner.hpp" //Funny LogWriter vtable problem!!!! YEAH! // this pos in include shouldn't be a problem - Lukas
#include "tracker/tracker.hpp"
#include "pathplanning/constraintgenerator.hpp"
#include "hwcomponents/robot.h"
#include "hwcomponents/gripper.hpp"
#include "graspplanner/graspplanner.hpp"
#include "utils/log.hpp"


/// ****************** NOTICE *************************
/// *** USE AT OWN RISK! ***
///  No liabilities will be taken for damages occured
///  when using or running this program.

enum ProgramState {
  Running, Exiting,
};
enum ActionState {
    Home, GraspBall, PositionBall, Wait
};

struct RunTimeVariables {
    std::mutex _lock;

    // overall program state
    ProgramState _programState;

    // actions of the robot
    ActionState _actions;
    RunTimeVariables() :
        _lock(),
        _programState(Running),
        _actions(Wait) {
    }

    void setActionState(ActionState act){
        _lock.lock();
        _actions = act;
        _lock.unlock();
    }

    void setProgramState(ProgramState prog){
        _lock.lock();
        _programState = prog;
        _lock.unlock();
    }

    ProgramState getProgramState(){
        _lock.lock();
        ProgramState ret = _programState;
        _lock.unlock();
        return ret;
    }

    ActionState getActionState(){
        _lock.lock();
        ActionState ret = _actions;
        _lock.unlock();
        return ret;
    }

};



void plannerLoop(RunTimeVariables &run_state, const size_t num_nodes) {
    // tracker
    std::string wcFile_real = "../../workcell/WorkStation3/WS3_Scene_RealCam.wc.xml";
    rw::models::WorkCell::Ptr wc_real = (rw::loaders::WorkCellLoader::Factory::load(wcFile_real));
    rw::kinematics::Frame* camera_real = wc_real->findFrame("CameraSimLeft");
    rw::kinematics::State state_real = wc_real->getDefaultState();
    rw::math::Transform3D<double> worldTcam_real = rw::kinematics::Kinematics::worldTframe(camera_real,state_real);


    rovi::Tracker ballTracker;
    std::cout << "Starting ball tracker in separate thread..." << std::endl;

    ballTracker.startTracker();
    std::cout << "Ball tracker is running." << std::endl;



    // set initial vars
    const std::string wc_name_prm = "../../workcell/WorkStation3/WS3_Scene_prm.wc.xml";
    const std::string acting_namespace = "caros_universalrobot";
    const std::string acting_namespace_gripper = "caros_schunkpg70";
    rw::models::WorkCell::Ptr wc_prm = (rw::loaders::WorkCellLoader::Factory::load(wc_name_prm));
    rw::models::Device::Ptr device=wc_prm->findDevice("UR1");
    rw::kinematics::Frame* camera = wc_prm->findFrame("CameraSimLeft");
    rw::kinematics::State state = wc_prm->getDefaultState();
    rw::kinematics::MovableFrame* ball = dynamic_cast<rw::kinematics::MovableFrame*>(wc_prm->findFrame("Ball"));
    rw::kinematics::Frame* tcp = wc_prm->findFrame("PG70.TCP");
    rw::kinematics::MovableFrame* pointer = dynamic_cast<rw::kinematics::MovableFrame*>(wc_prm->findFrame("ball_rod"));

    rw::math::Q robotHome(6, 0.0, -rw::math::Pi/2, 0.0, -rw::math::Pi, 0.0, 0.0);


    enum GraspState {
        Home, Grasping, Opening,
    };
    GraspState gripper_state;


    rovi::GraspPlanner grasper(device,tcp,wc_prm);
    grasper.setTarget(dynamic_cast<rw::kinematics::Frame*>(ball));
    grasper.setRandomness(20.0*rw::math::Deg2Rad, 200);

    rovi::pathplanners::ConstraintGenerator robot_move_const;
    robot_move_const.setPointerDistance(3.0,0.1); //first: length of rod. second: distance from ball center to rod end.
    robot_move_const.setOrigo(camera);
    robot_move_const.setPointer(pointer);

    std::cout << "Creating planner..." << std::endl;
    rovi::PathPlanner planner(wc_prm, "UR1", "ball_rod", num_nodes);
    std::cout << "Planner created." << std::endl;



    std::cout << "Connecting to UR5 robot..." << std::endl;
    rovi::hwcomponents::Robot robot(acting_namespace);
    if(!robot.isOK()){
        ROVI_THROW("Could not connect to robot!\n");
    }

    std::cout << "Connecting to PG70..." << std::endl;
    rovi::hwcomponents::Gripper gripper(acting_namespace_gripper);
    if(!gripper.isOK()){
        ROVI_THROW("Could not connect to robot!\n");
    } else {
        gripper.open();
        gripper_state = Opening;
        while(gripper.isMoving());
        gripper_state = Home;
    }


    rw::math::Vector3D<> ball_pose_cam, ball_vel_cam, ball_vel_world;
    rw::math::Transform3D<double> worldTball, camTball;


    // main loop
    while(run_state.getProgramState() == ProgramState::Running) {

        // get the current state
        //Blocks until new ball state (up to 50 ms on live system, same rate as new images coming in).
        //There is also a non-blocking version which just gets the latest state.
        ballTracker.getBallPosVelBlocking(ball_pose_cam, ball_vel_cam);
        // apply magic number
        ball_pose_cam(0) *= -1;

        camTball = rw::math::Transform3D<double>(ball_pose_cam);
        worldTball=(worldTcam_real * camTball);
        ball_pose_cam = worldTball.P();

        ball_vel_world = worldTcam_real.R() * ball_vel_cam;


        // check if it is set into collision here else it needs to go back
        // update constraints
        robot_move_const.pointTowards(camTball, state);
        ball->setTransform(worldTball, state);

        planner.updateState(state);

        // find what to do
        ActionState robot_action = run_state.getActionState();


        // do what told
        if(robot_action == ActionState::Wait){

        } else if(robot_action == ActionState::Home){

        } else if(robot_action == ActionState::GraspBall){

        } else if(robot_action == ActionState::PositionBall){

        }





//        if(grasp_ball) {
//            std::cout << "Grasping ball!" << std::endl;
//            ball->setTransform(ballpose,state);
//            try {
//                if(robot_state == Approach){
//                    rw::math::Q graspQ = grasper.planGrasp(ballvel,state);

//                    //rw::math::Q start = device->getQ(state);
//                    rw::math::Q start = ur5.getConfiguration();
//                    std::cout << "Init q: " << std::start << std::endl;
//                    rw::trajectory::QPath path = planner.plan(start,graspQ,true);


//                    robot.movePtp(path);
////                    while(robot.isMoving() && robot.isOK());
////                    if(!robot.isNearGoal(path[path.size()-1])){
////                        ROVI_THROW("Did not make it all the way to goal!");
////                    }
////                    robot_state = Grasping;
//                }

//                if(robot_state == Grasping){

//                    // grasp the ball
//                    rw::math::Q force(1,30.0);
//                    schunk.grip(force);
//                    while (schunk.isMoving());
////                      if(schunk.getConfiguration().norm2() < 0.001){
////                        ROVI_THROW("Did not grasp the ball!");
////                    }
//                    std::cout << "Grasped the ball at " << schunk.getConfiguration() << std::endl;
//                    robot_state = ReturningHome;
//                }
//                if(robot_state == ReturningHome){

//                    rw::math::Q start = ur5.getConfiguration();
//                    rw::trajectory::QPath path = planner.plan(start,homeQ,true);

//                    ur5.movePtp(path);
//                    while(ur5.isMoving() && ur5.isOK());
//                    if(!ur5.isNearGoal(path[path.size()-1])){
//                        ROVI_THROW("Did not make it all the way to goal!");
//                    }
//                }



    }

    std::cout << "Stopping ball tracker." << std::endl;
    ballTracker.stopTracker();
    std::cout << "Stopping robot." << std::endl;


}

int main(int argc, char *argv[]){
    const size_t num_input_arguments = static_cast<size_t>(argc-1);

    std::cout << " -- Running Master Controller --" << std::endl;
    int dummyInt=0;
    char **dummyPtr=nullptr;
    ros::init(dummyInt, dummyPtr,"stereocamera_imgreader");
    ROS_INFO("Connected to roscore");


    // get inputs for path planner
    size_t num_nodes=2000;
    if(num_input_arguments==1) {
        std::stringstream(argv[1]) >> num_nodes;
    }

    // make data containers
    // I'd liked to use atomic, but there is a bug in my current gcc and boost versions that prevent me from doing it :(
    // can't upgrade them becasue of dependencies to RW, ROS, etc.
    RunTimeVariables progam_state;

    std::thread pathPlannerLoop(plannerLoop, std::ref(progam_state), num_nodes);

    while(true) {
        std::cout << "Enter q to quit or g to grasp static ball." << std::endl;
        char c;
        std::cin >> c;
        if(c=='q'){
            progam_state.setProgramState(ProgramState::Exiting);
            break;
        } else if(c=='g'){
            progam_state.setActionState(ActionState::GraspBall);
        } else if(c=='p'){
            progam_state.setActionState(ActionState::PositionBall);
        } else if(c=='w'){
            progam_state.setActionState(ActionState::Wait);
        } else {
            progam_state.setActionState(ActionState::Home);
        }
    }

    pathPlannerLoop.join();



    std::cout << "Ball tracker stopped." << std::endl;

    return 0;
}
