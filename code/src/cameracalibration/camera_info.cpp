#include "camera_info.hpp"

void rovi::cameracalibration::StereoCameraCalibInfo::write()                       //Write serialization for this class
{
    _fs.open(_filename, cv::FileStorage::WRITE);
    _fs << "CM1" << _CM_left;
    _fs << "CM2" << _CM_right;
    _fs << "D1" << _D_left;
    _fs << "D2" << _D_right;
    _fs << "R" << _R;
    _fs << "T" << _T;
    _fs << "E" << _E;
    _fs << "F" << _F;
    _fs << "R1" << _R_left;
    _fs << "R2" << _R_right;
    _fs << "P1" << _P_left;
    _fs << "P2" << _P_right;
    _fs << "Q" << _Q;
    _fs.release();
}
void rovi::cameracalibration::StereoCameraCalibInfo::read()                          //Read serialization for this class
{
    _fs.open(_filename, cv::FileStorage::READ);
    _fs["CM1"] >> _CM_left;
    _fs["CM2"] >> _CM_right;
    _fs["D1"] >> _D_left;
    _fs["D2"] >> _D_right;
    _fs["R"] >> _R;
    _fs["T"] >> _T;
    _fs["E"] >> _E;
    _fs["F"] >> _F;
    _fs["R1"] >> _R_left;
    _fs["R2"] >> _R_right;
    _fs["P1"] >> _P_left;
    _fs["P2"] >> _P_right;
    _fs["Q"] >> _Q;
    _fs.release();
}
// getters and setters
cv::Mat rovi::cameracalibration::StereoCameraCalibInfo::get_CM1(){ return _CM_left; }
cv::Mat rovi::cameracalibration::StereoCameraCalibInfo::get_CM2(){ return _CM_right; }
cv::Mat rovi::cameracalibration::StereoCameraCalibInfo::get_D1(){ return _D_left; }
cv::Mat rovi::cameracalibration::StereoCameraCalibInfo::get_D2(){ return _D_right; }
cv::Mat rovi::cameracalibration::StereoCameraCalibInfo::get_R(){ return _R; }
cv::Mat rovi::cameracalibration::StereoCameraCalibInfo::get_T(){ return _T; }
cv::Mat rovi::cameracalibration::StereoCameraCalibInfo::get_E(){ return _E; }
cv::Mat rovi::cameracalibration::StereoCameraCalibInfo::get_F(){ return _F; }
cv::Mat rovi::cameracalibration::StereoCameraCalibInfo::get_R1(){ return _R_left; }
cv::Mat rovi::cameracalibration::StereoCameraCalibInfo::get_R2(){ return _R_right; }
cv::Mat rovi::cameracalibration::StereoCameraCalibInfo::get_P1(){ return _P_left; }
cv::Mat rovi::cameracalibration::StereoCameraCalibInfo::get_P2(){ return _P_right; }
cv::Mat rovi::cameracalibration::StereoCameraCalibInfo::get_Q(){ return _Q; }

void rovi::cameracalibration::StereoCameraCalibInfo::set_CM1(cv::Mat s_CM1){ _CM_left = s_CM1; }
void rovi::cameracalibration::StereoCameraCalibInfo::set_CM2(cv::Mat s_CM2){ _CM_right = s_CM2; }
void rovi::cameracalibration::StereoCameraCalibInfo::set_D1(cv::Mat s_D1){ _D_left = s_D1; }
void rovi::cameracalibration::StereoCameraCalibInfo::set_D2(cv::Mat s_D2){ _D_right = s_D2; }
void rovi::cameracalibration::StereoCameraCalibInfo::set_R(cv::Mat s_R){ _R = s_R; }
void rovi::cameracalibration::StereoCameraCalibInfo::set_T(cv::Mat s_T){ _T = s_T; }
void rovi::cameracalibration::StereoCameraCalibInfo::set_E(cv::Mat s_E){ _E = s_E; }
void rovi::cameracalibration::StereoCameraCalibInfo::set_F(cv::Mat s_F){ _F = s_F; }
void rovi::cameracalibration::StereoCameraCalibInfo::set_R1(cv::Mat s_R1){ _R_left = s_R1; }
void rovi::cameracalibration::StereoCameraCalibInfo::set_R2(cv::Mat s_R2){ _R_right = s_R2; }
void rovi::cameracalibration::StereoCameraCalibInfo::set_P1(cv::Mat s_P1){ _P_left = s_P1; }
void rovi::cameracalibration::StereoCameraCalibInfo::set_P2(cv::Mat s_P2){ _P_right = s_P2; }
void rovi::cameracalibration::StereoCameraCalibInfo::set_Q(cv::Mat s_Q){ _Q = s_Q; }
void rovi::cameracalibration::StereoCameraCalibInfo::set_file(std::string file){ _filename = file; }
