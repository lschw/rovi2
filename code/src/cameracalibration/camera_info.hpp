#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/opencv.hpp>

#include <string>
#include <iostream>
#include <vector>


namespace rovi {

namespace cameracalibration {



class StereoCameraCalibInfo
{
private:   // Data Members
    std::string _filename;
    cv::FileStorage _fs;
    cv::Mat _CM_left, _CM_right;
    cv::Mat _D_left, _D_right;
    cv::Mat _R;
    cv::Mat _T;
    cv::Mat _E;
    cv::Mat _F;
    cv::Mat _R_left, _R_right;
    cv::Mat _P_left, _P_right;
    cv::Mat _Q;

public:
    StereoCameraCalibInfo(){}
    StereoCameraCalibInfo(std::string file) : _filename(file) {}

    void write();
    void read();

    // getters and setters
    cv::Mat get_CM1();
    cv::Mat get_CM2();
    cv::Mat get_D1();
    cv::Mat get_D2();
    cv::Mat get_R();
    cv::Mat get_T();
    cv::Mat get_E();
    cv::Mat get_F();
    cv::Mat get_R1();
    cv::Mat get_R2();
    cv::Mat get_P1();
    cv::Mat get_P2();
    cv::Mat get_Q();

    void set_CM1(cv::Mat s_CM1);
    void set_CM2(cv::Mat s_CM2);
    void set_D1(cv::Mat s_D1);
    void set_D2(cv::Mat s_D2);
    void set_R(cv::Mat s_R);
    void set_T(cv::Mat s_T);
    void set_E(cv::Mat s_E);
    void set_F(cv::Mat s_F);
    void set_R1(cv::Mat s_R1);
    void set_R2(cv::Mat s_R2);
    void set_P1(cv::Mat s_P1);
    void set_P2(cv::Mat s_P2);
    void set_Q(cv::Mat s_Q);
    void set_file(std::string file);
};


}
}
