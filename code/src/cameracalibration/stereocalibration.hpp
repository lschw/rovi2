// modified version of: http://www.jayrambhia.com/blog/stereo-calibration/

#include <opencv2/core/core.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/contrib/contrib.hpp>

#include <stdio.h>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <exception>

#include <boost/filesystem.hpp>

#include "camera_info.hpp"

namespace rovi {

namespace cameracalibration {

class stereocalibration
{
public:
    stereocalibration():_calibrationDone(false) {}

    void setBoardSize(int w, int h);

    /**
     * @brief setImageFile
     * @param imagefile Filename of the file with the image list
     */
    void setImageFile(std::string imagefile_left, std::string imagefile_right);

    /**
     * @brief setSquareSize
     * @param size in whatever unit you want your output :D
     */
    void setSquareSize(double size);


    void saveCalibrationFile(std::string destination);

    void calibrate();


    void visualizeChessBoards();


protected:

    /**
     * @brief loadImageNames loads the image neames to load, but not the images itself!
     */
    void loadImageNames();

    void calibrateCamera();

    void findCorners();

    bool _calibrationDone;

    std::string _imagefilename_left, _imagefilename_right;
    cv::Size _board_size; // in number of squares
    double _squareSize; // assumes rectangular squares

    std::vector< std::vector< cv::Point3f > > _object_points;
    std::vector< std::vector< cv::Point2f > > _imagePoints_left, _imagePoints_right;

    std::vector< std::string > _images_left, _images_right;
    std::vector< bool > _images_board_found;

    cv::Size _imgSize; // used for calibration

    StereoCameraCalibInfo _calibration;

};


}

}
