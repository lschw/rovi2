#include "stereocalibration.hpp"


void rovi::cameracalibration::stereocalibration::setBoardSize(int w, int h){
    _board_size = cv::Size(w, h);
    _calibrationDone = false;
}

void rovi::cameracalibration::stereocalibration::setImageFile(std::string imagefile_left, std::string imagefile_right){
    _imagefilename_left = imagefile_left;
    _imagefilename_right = imagefile_right;
    _calibrationDone = false;
}

void rovi::cameracalibration::stereocalibration::setSquareSize(double size){
    _squareSize = size;
    _calibrationDone = false;
}

void rovi::cameracalibration::stereocalibration::loadImageNames(){
    std::fstream fs_left(_imagefilename_left, std::fstream::in), fs_right(_imagefilename_right, std::fstream::in);

    // check if files are valid
    if(!fs_left.is_open() || !fs_right.is_open()){
        throw(std::runtime_error("Stereocalibration: loadImageNames: Files given could not be opened!"));
    }

    // clear vectors
    _images_left.clear();
    _images_right.clear();

    // load the filenames
    std::string filename_l, filename_r;
    while(!fs_left.eof() && !fs_right.eof()){
        std::getline(fs_left, filename_l);
        std::getline(fs_right, filename_r);
        if(filename_l != "" && filename_l[0] != '#' && filename_r != "" && filename_r[0] != '#'){
            if(!boost::filesystem::exists(filename_l)){
                throw(std::runtime_error("Stereocalibration: loadImageNames: File '" + filename_l + " does not exist!"));
            }
            _images_left.emplace_back(filename_l);
            if(!boost::filesystem::exists(filename_r)){
                throw(std::runtime_error("Stereocalibration: loadImageNames: File '" + filename_r + "' does not exist!"));
            }
            _images_right.emplace_back(filename_r);
        }
    }

    // ensure that both file ends where reached, else throw error (might possibly be a problem if unequal images numbers given)
    if(fs_left.eof() != fs_right.eof()){
        throw(std::runtime_error("Stereocalibration: loadImageNames: End of file was not reached for both files! Make sure they have equal length."));
    }
    if(_images_left.size() != _images_right.size()){
        throw(std::runtime_error("Stereocalibration: loadImageNames: End of file was not reached for both files! Make sure they have equal length."));
    }

    fs_left.close();
    fs_right.close();
}


void rovi::cameracalibration::stereocalibration::calibrateCamera(){
    cv::Mat CM_left = cv::Mat(3, 3, CV_64FC1);
    cv::Mat CM_right = cv::Mat(3, 3, CV_64FC1);
    cv::Mat D_left, D_right;
    cv::Mat R, T, E, F;

    cv::stereoCalibrate(_object_points, _imagePoints_left, _imagePoints_right,
                        CM_left, D_left, CM_right, D_right, _imgSize, R, T, E, F,
                        cvTermCriteria(CV_TERMCRIT_ITER + CV_TERMCRIT_EPS, 100, 1e-5),
                        CV_CALIB_SAME_FOCAL_LENGTH | CV_CALIB_ZERO_TANGENT_DIST);

    _calibration.set_CM1(CM_left);
    _calibration.set_CM2(CM_right);
    _calibration.set_D1(D_left);
    _calibration.set_D2(D_right);
    _calibration.set_R(R);
    _calibration.set_T(T);
    _calibration.set_E(E);
    _calibration.set_F(F);

    cv::Mat R1, R2, P1, P2, Q;
    cv::stereoRectify(CM_left, D_left, CM_right, D_right, _imgSize, R, T, R1, R2, P1, P2, Q);
    _calibration.set_R1(R1);
    _calibration.set_R2(R2);
    _calibration.set_P1(P1);
    _calibration.set_P2(P2);
    _calibration.set_Q(Q);

    _calibrationDone = true;

}

void rovi::cameracalibration::stereocalibration::findCorners(){
    cv::Mat img_left, img_right, gray_left, gray_right;
    bool found_left = false, found_right = false;

    // make object
    std::vector< cv::Point3f > obj;
    for (int i = 0; i < _board_size.height; i++){
        for (int j = 0; j < _board_size.width; j++){
            obj.push_back(cv::Point3f(i * (float)_squareSize, j * (float)_squareSize, 0.0));
        }
    }

    _imagePoints_left.clear();
    _imagePoints_right.clear();
    _object_points.clear();

    _images_board_found.resize(_images_left.size());

    std::vector< cv::Point2f > corners_left, corners_right;
    // find corners
    for(int i = 0; i < _images_left.size(); i++){
        img_left = cv::imread(_images_left[i], 1);
        img_right = cv::imread(_images_right[i], 1);

        // convert to grayscale
        cv::cvtColor(img_left, gray_left, CV_BGR2GRAY);
        cv::cvtColor(img_right, gray_right, CV_BGR2GRAY);

        found_left = cv::findChessboardCorners(img_left, _board_size, corners_left, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS);
        found_right = cv::findChessboardCorners(img_right, _board_size, corners_right, CV_CALIB_CB_ADAPTIVE_THRESH | CV_CALIB_CB_FILTER_QUADS);

        if (found_left && found_right)
        {
            _imagePoints_left.push_back(corners_left);
            _imagePoints_right.push_back(corners_right);
            _object_points.push_back(obj);

            _images_board_found[i] = true;
        } else {
            if(!found_left){
                std::cout << " Stereocalibration: Did not find chessboard on '" << _images_left[i] << "'\n";
            }
            if(!found_right){
                std::cout << " Stereocalibration: Did not find chessboard on '" << _images_right[i] << "'\n";
            }
            _images_board_found[i] = false;
        }

    }

    _imgSize = img_left.size();

    std::cout << "Found the chessboard " << _imagePoints_left.size() << " / " << _images_left.size() << " times.\n";
}


void rovi::cameracalibration::stereocalibration::calibrate(){
    try{
        loadImageNames();
        findCorners();
        calibrateCamera();
    } catch(std::runtime_error &e) {
        std::cout << "Encountered error: " << e.what() << " - while calibrating camera.\n";
    }
}

void rovi::cameracalibration::stereocalibration::visualizeChessBoards(){

    if(_calibrationDone){
        cv::Mat img1, img2;
        int j = -1;
        for(int i = 0; i < _images_left.size(); i++){
            img1 = cv::imread(_images_left[i], 1);
            img2 = cv::imread(_images_right[i], 1);

            if (_images_board_found[i])
            {
                j++;
//                cv::cornerSubPix(img1, _imagePoints_left[j], cv::Size(11, 11), cv::Size(-1, -1), cv::TermCriteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 30, 0.1));
                cv::drawChessboardCorners(img1, _board_size, _imagePoints_left[j], true);
//                cv::cornerSubPix(img2, _imagePoints_right[j], cv::Size(11, 11), cv::Size(-1, -1), cv::TermCriteria(CV_TERMCRIT_EPS | CV_TERMCRIT_ITER, 30, 0.1));
                cv::drawChessboardCorners(img2, _board_size, _imagePoints_right[j], true);
            }

            cv::imshow("Left Image", img1);
            cv::imshow("Right Image", img2);

            char c;
            c = cv::waitKey(0);

            if(c == 'ESC'){
                break;
            }
        }
    }

}

void rovi::cameracalibration::stereocalibration::saveCalibrationFile(std::string destination){
    _calibration.set_file(destination);
    _calibration.write();
}
