#pragma once

#include <rws/RobWorkStudioPlugin.hpp>
#include <rws/RobWorkStudio.hpp>
#include <rw/common/Ptr.hpp>
#include <rw/common/Log.hpp>
#include <rw/kinematics/Frame.hpp>
#include <rw/kinematics/MovableFrame.hpp>
#include <rw/kinematics/Kinematics.hpp>
#include <rw/math/Transform3D.hpp>
#include <rw/math/Rotation3D.hpp>
#include <rwlibs/opengl/RenderImage.hpp>
#include <rw/loaders/ImageLoader.hpp>
#include <rw/graphics.hpp>

#include <boost/filesystem.hpp>

#include "ui_MovementSimulator.h"

#include <rwlibs/simulation/GLFrameGrabber.hpp>

#include "../../pathplanning/pathplanner.hpp" //this is bad, it should be able to be included anywhere

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/core/core.hpp>

#include <QObject>
#include <QtGui>
#include <QSlider>
#include <QVector>
#include <QFileDialog>

#include <vector>
#include <string>
#include <fstream>
#include <exception>

#include <Eigen/Core>

#include "../../vision/vision_algo.h"
#include "../../stereovision/stereoalgorithm.hpp"
#include "../../pathgenerator/linearpath.hpp"
#include "../../utils/defines_matrices.hpp"
#include "../../pathplanning/constraintgenerator.hpp"
#include "../../kalman/kalman.hpp"
#include "../../graspplanner/graspplanner.hpp"
//#include "../../pathplanning/pathplanner.hpp"


// test thing
#include <rw/invkin/JacobianIKSolver.hpp>
#include <rw/invkin.hpp>
#include <rw/invkin/ClosedFormIKSolverUR.hpp>

/**
 * @brief A plugin for testing grippers.
 */
class MovementSimulator: public rws::RobWorkStudioPlugin, private Ui::MovementSimulator {
	Q_OBJECT
	Q_INTERFACES(rws::RobWorkStudioPlugin)

public:
	//! @brief constructor
    MovementSimulator();

	//! @brief destructor
    virtual ~MovementSimulator();

	//! @copydoc rws::RobWorkStudioPlugin::open(rw::models::WorkCell* workcell)
    virtual void open(rw::models::WorkCell* workcell);

	//! @copydoc rws::RobWorkStudioPlugin::close()
	virtual void close();

	//! @copydoc rws::RobWorkStudioPlugin::initialize()
	virtual void initialize();

private slots:
    void stateChangedListener(const rw::kinematics::State& state);

    /**
     * @brief simBtnSimulator Runs the simulations of the system and stores the workcell states to be used when browsing through them.
     */
    void simBtnSimulator();

    /**
     * @brief simSliderTime Loops through the states stored by simBtnSimulator();
     */
    void simSliderTime();

    /**
     * @brief connectToDevices Connects to the devices and frames in the workcell to use. Sets the _devices_found to true is so.
     * @return True id it succeded to connect / find them
     */
    bool connectToDevices();

    /**
     * @brief plotData Plots the x and y vector on the graph on simulate tab
     * @param inx The X values.
     * @param iny The Y values.
     * @param xlab
     * @param ylab
     */
    void plotData(std::vector< double > &inx, std::vector< double > &iny, std::string &xlab, std::string &ylab);


    /**
     * @brief getStereoImages gets the two imges from the stereo camera
     * @param left the left image
     * @param right the right image
     * @return true if the process succeded
     */
    bool getStereoImages(cv::Mat &left, cv::Mat &right);


    /**
     * @brief loadObjectPath Loads the path to be traveled by the object [x,y,z,r,p,y].
     */
    void loadObjectPath();


    /**
     * @brief calculateObjectPath Calculates the path to be traveled by the object [x,y,z,r,p,y].
     */
    void calculateObjectPath();


    /**
     * @brief plotSimData Plots the simulation data requested
     */
    void plotSimData();

private:

    /**
     * @brief toOpenCVImage Image converter to opencv
     * @param img input image from wc
     * @return the opencv image
     */
    static cv::Mat toOpenCVImage(const rw::sensor::Image& img);

    /**
     * @brief setupGUI initiates all the GUI connections and framegrabber
     */
	void setupGUI();

    /**
     * @brief objectCought Checks if the object falls into the gripper or is in the gripper.
     * @param nextState The next state of the object. Used to interpolate the objects movement linearly.
     * @return true if object is cought.
     */
    bool objectCought(rw::math::Transform3D<double> nextState);

    // texture render for table
    rwlibs::opengl::RenderImage *_textureRender;


    // frame grabber for when collecting images from the two cameras
    rwlibs::simulation::GLFrameGrabber* _framegrabber_l, *_framegrabber_r;

    // states and workcell
    rw::models::WorkCell* _wc;
    rw::kinematics::State _state;

    // vector to store states of simulation
    std::vector< rw::kinematics::State > _state_vector;

    // vector to store poses of the ball
    std::vector< rw::math::Transform3D<double> > _object_pos;

    // vectors used to store information for plotting and string for naming
    std::vector< double > _time, _detection_error, _ball_tcp_dist, _kalman_error;
    std::string _detection_error_name, _ball_tcp_dist_name, _kalman_error_name;

    // devices and frames used for the program
    bool _devices_found;
    rw::models::Device::Ptr _device;
    rw::kinematics::MovableFrame* _object;
    rw::kinematics::Frame* _tcp_frame;

    //planner
    rovi::PathPlanner _planner;

};
