#include "MovementSimulator.hpp"
#include "kalman/kalman.hpp"



MovementSimulator::MovementSimulator() :
    RobWorkStudioPlugin("MovementSimulator", QIcon(":/design_icon.png")),
    _planner("@MY_CURRENT_CMAKE_PATH@/../workcell/WorkStation3/WS3_Scene_prm.wc.xml", "UR1", "ball_rod",10000)
    {
    setupGUI();

}

MovementSimulator::~MovementSimulator() {

}

void MovementSimulator::initialize() {
    getRobWorkStudio()->stateChangedEvent().add(boost::bind(&MovementSimulator::stateChangedListener, this, _1), this);

}

void MovementSimulator::open(rw::models::WorkCell * workcell) {
    _wc = workcell;
    _state = _wc->getDefaultState();

    // try to find the devices and frames
    connectToDevices();

}

void MovementSimulator::close() {
    // Delete the old framegrabber
    if (_framegrabber_l != NULL) {
        delete _framegrabber_l;
    }
    _framegrabber_l = NULL;
    if (_framegrabber_r != NULL) {
        delete _framegrabber_r;
    }
    _framegrabber_r = NULL;

    // Remove the texture render
    rw::kinematics::Frame* textureFrame = _wc->findFrame("TableTexture");
    if (textureFrame != NULL) {
        getRobWorkStudio()->getWorkCellScene()->removeDrawable("TextureImage",textureFrame);
    }


}


void MovementSimulator::setupGUI() {
    // add connections here
    setupUi(this);

    // simulation
    connect(_sim_btn_sim, SIGNAL(pressed()), this, SLOT(simBtnSimulator()));
    connect(_sim_slider_time, SIGNAL(sliderMoved(int)), this, SLOT(simSliderTime()));
    connect(_sim_comboBox_plot, SIGNAL(currentIndexChanged(int)), SLOT(plotSimData()));

    // path calculator
    connect(_path_btn_loadPath, SIGNAL(pressed()), SLOT(loadObjectPath()));
    connect(_path_calculate, SIGNAL(pressed()), SLOT(calculateObjectPath()));


    // for texture render
    rw::sensor::Image textureImage(350,700,rw::sensor::Image::GRAY,rw::sensor::Image::Depth8U);
    _textureRender = new rwlibs::opengl::RenderImage(textureImage, 1.3/700); // scale length of table / height of image

    // set names of Simulation Plot (Combo box)
    _detection_error_name = "Vision Detection Error";
    _ball_tcp_dist_name = "Ball->TCP Distance";
    _kalman_error_name = "Kalman Current Error";

    _sim_comboBox_plot->addItem(QString::fromStdString(_detection_error_name));
    _sim_comboBox_plot->addItem(QString::fromStdString(_kalman_error_name));
    _sim_comboBox_plot->addItem(QString::fromStdString(_ball_tcp_dist_name));

}

void MovementSimulator::simBtnSimulator(){
    bool stopSimWhenBallCought = false;

    if(!_devices_found && !connectToDevices()){
        rw::common::Log::log().info() << "Could not connected to the needed devices and frames.\n";
        return;
    }
    rw::common::Log::log().info() << "Simulation started.\n";

    // decide on size of state vector
    if(!_object_pos.size()){
        rw::common::Log::log().error() << "No object path to follow.\n";
        return;
    }
    _state_vector.resize(_object_pos.size());

    // resize the slider in time to suite this
    if(_object_pos.size() > 0){
        _sim_slider_time->setRange(0, _object_pos.size() - 1);
    } else {
        _sim_slider_time->setRange(0, 0);
    }

    // data vectors for plotting
    _time.resize(_object_pos.size());
    _detection_error.resize(_object_pos.size());
    _kalman_error.resize(_object_pos.size());
    _ball_tcp_dist.resize(_object_pos.size());

    bool objCought = false;
    // reset object position
    _object->setTransform(_object_pos[0], _state);

    // make constraint generator
    rovi::pathplanners::ConstraintGenerator robot_move_const;
    robot_move_const.setPointerDistance(3,_settings_obj_dist->value());
    rw::kinematics::Frame* cam = _wc->findFrame("CameraSimLeft");
    rw::kinematics::MovableFrame* pointer = dynamic_cast<rw::kinematics::MovableFrame*>(_wc->findFrame("ball_rod"));
    if(cam == NULL){
        ROVI_WARN("Could not find the frame '" << "CameraSimLeft" << "'.\n");
    }
    if(pointer == NULL){
        ROVI_WARN("Could not find the frame '" << "ball_rod" << "'.\n");
    }
    robot_move_const.setOrigo(cam);
    robot_move_const.setPointer(pointer);

    // init kalman
    rovi::Kalman myKalman;
    myKalman.setSamplingPeriod(1);

    // -- generate projection matrices --
    // focal length in pixels = (image width in pixels) * (focal length in mm) / (CCD width in mm) = 1067.6
    double f = 1335; // 1157.8 according to OpenCV calib, 1325 according to Matlab
    double u = 1024/2;
    double v = 768/2;
    double b = 156; // 127 / 139 opencv, 159 matlab
    // generate the two projection matrices
    rovi::Matrix3by4d p_left;
    p_left <<   f, 0, u, 0,
            0, f, v, 0,
            0, 0, 1, 0;
    rovi::Matrix3by4d p_right;
    p_right <<  f, 0, u, b,
            0, f, v, 0,
            0, 0, 1, 0;
    Eigen::Matrix2d unc_2d;
    unc_2d << 0.3, 0.15, 0.15, 0.3;

    rovi::Stereoalgorithm stereo(p_left, p_right, unc_2d, unc_2d);
    rovi::vision::vision_algo detector(rovi::vision::vision_algo::findRedBlob);

    // intiate graspplanner
    rovi::GraspPlanner graspplanner(_device, _tcp_frame, _wc);
    graspplanner.setTarget(dynamic_cast<rw::kinematics::Frame*>(_object));
    graspplanner.setRandomness(20.0 * rw::math::Deg2Rad, 30);

    // get camera position in world frame
    int ball_not_found = 0;
    // robot start at its current state
    // fill the state vector with states
    for(unsigned int i = 0; i < _object_pos.size(); i++){
        // get the next object pose
        rw::math::Transform3D<double> nextObjectPos;
        if(i == _object_pos.size() - 1){
            nextObjectPos = _object_pos[i];
        } else {
            nextObjectPos = _object_pos[i + 1];
        }

        // predict Kalman!!
        rovi::Vector9d ball_state_kalman = myKalman.predict();
        ball_state_kalman.setZero();
        // check if the object is in the gripper or travels into gripper, if not, continue
        rw::math::Transform3D<double> ball_pose(rw::math::Vector3D<double>(0,0,0),rw::math::Rotation3D<double>::identity());
        rw::math::Transform3D<double>  ball_pose_kalman(rw::math::Vector3D<double>(0,0,0),rw::math::Rotation3D<double>::identity());
        ball_pose_kalman.P() = rw::math::Vector3D<double>(ball_state_kalman.head<3>());
        if( (!objectCought(nextObjectPos) && !objCought ) || !stopSimWhenBallCought){
            // set the objects pose
            _object->setTransform(_object_pos[i], _state);

            // get stereo images
            std::vector< cv::Mat > img(2);
            if(!getStereoImages(img[0], img[1])){
                rw::common::Log::log().error() << "Could not get images.\n";
                return;
            }
            // show image and wait
            if(false){
                cv::imshow("Left", img[0]);
                cv::waitKey(0);
            }

            // -- detect the red ball in the images --
            std::vector< std::vector< cv::Point2d > > points;
            stereo.applyAlgorithm(img, detector, points);



            // break algorithm if no point was found in one of the images
            if(!points[0].size() || !points[1].size()){
                ball_not_found++;
                rw::common::Log::log().error() << "There was found no red balls by the vision algo.\n";
            } else {

                // -- Stereopsis --
                Eigen::Vector3d obj_pose;
                Eigen::Matrix3d uncertainty;

                // compute the placement the 'new' way, using projection matrix
                // find the two most probable points
                std::vector< std::vector< cv::Point2d > > paired_points;
                stereo.simpleFeaturePairing(points[0], points[1], paired_points);

                if(paired_points.size() > 0){
                    if(paired_points.size() != 1){
                        rw::common::Log::log().error() << "Ball points found too many places.\n";
                    }

                    std::vector< cv::Point2d > point2D = paired_points[0];

                    // find the point
                    try{
                        stereo.completeStereopsis(point2D, obj_pose, uncertainty);
                        obj_pose(0) *= -1;
//                        obj_pose(1) *= -1;
//                        obj_pose(2) *= -1;
                        ball_pose = rw::math::Transform3D<double>(rw::math::Vector3D<double>(obj_pose(0), obj_pose(1), obj_pose(2)),rw::math::Rotation3D<double>::identity());
                        rw::common::Log::log().info() << "Ball pose:\n" << ball_pose << "\n";
                    } catch(std::runtime_error &e) {
                        rw::common::Log::log().error() << e.what();
                        ball_not_found++;
                    }

                    // -- kalman filter --
                    ball_state_kalman = myKalman.update(obj_pose, uncertainty);
                    // update ball pose
                    ball_pose_kalman.P() = rw::math::Vector3D<double>(ball_state_kalman.head<3>());
                } else {
                    ball_not_found++;
                    rw::common::Log::log().error() << "No points where paried!\n";
                }

            }
            // -- Plan the grasp --
            bool graspfound = true;
            rw::math::Q plannedQ = _device->getQ(_state);
            rw::math::Vector3D<> dir(_settings_grasp_x->value(), _settings_grasp_y->value(), _settings_grasp_z->value());
            try{
                 plannedQ = graspplanner.planGrasp(dir, _state);
//                plannedQ = graspplanner.planGrasp(_state);
//                 _device->setQ(plannedQ, _state);
                // -- update the constraint --
            } catch( std::runtime_error &err) {
                rw::common::Log::log().error() << err.what();
                graspfound = false;
            }


            // -- update the constraint --
            robot_move_const.pointTowards(ball_pose, _state);
            rw::math::Transform3D<> constraint = rw::kinematics::Kinematics::worldTframe(pointer,_state);
            _planner.move_constraint(constraint);

            // -- stear robot --
            if(graspfound){
                // -- stear robot --
                rw::math::Q from = _device->getQ(_state);
                rw::trajectory::QPath path = _planner.plan(from,plannedQ);
                // -- move robot --
                //TODO:take velocity (maybe also acceleration) constraint into account
                if(_settings_moverobot->isChecked()){
                    for (size_t q = 0; q < path.size(); ++q) {
                        _device->setQ(path.at(q),_state);
                        getRobWorkStudio()->setState(_state);
                        usleep(1000);
                        // std::string fn_left, fn_right;
                        // fn_left  = "@MY_CURRENT_CMAKE_PATH@/../path_pictures/" + std::to_string(i) + "Left"  + std::to_string(q) + ".png";
                        // fn_right = "@MY_CURRENT_CMAKE_PATH@/../path_pictures/" + std::to_string(i) + "Right" + std::to_string(q) + ".png";
                        //  if(getStereoImages(img[0], img[1])){
                        //     cv::imwrite(fn_left.str(), img[0]);
                        //      cv::imwrite(fn_right.str(),img[1]);
                        //  }
                    }
                }
            }

        } else {
            objCought = true;
            // set the object to grasp point if grasped
            _object->setTransform(rw::kinematics::Kinematics::worldTframe(_tcp_frame, _state), _state);
        }
        // get object to tcp distance
        _time[i] = i;
        // from predicted to actual ball
        rw::kinematics::Frame* camera_l = _wc->findFrame(_settings_camera->text().toStdString() + "Left");
        rw::math::Transform3D<double> real_cam_T_ball = rw::kinematics::Kinematics::frameTframe(camera_l, dynamic_cast<rw::kinematics::Frame*>(_object), _state);
//        rw::common::Log::log().info() << "real:\n" << real_cam_T_ball.P() << "\n found:\n" << ball_pose.P() << "\n";
        rw::math::Transform3D<double> dist = real_cam_T_ball;
        dist.invMult(dist, ball_pose);
        _detection_error[i] = dist.P().norm2() * 1000; // in mm
        dist = real_cam_T_ball;
        dist.invMult(dist, ball_pose_kalman);
        _kalman_error[i] = dist.P().norm2() * 1000; // in mm
        // from ball to gripper
        _ball_tcp_dist[i] = rw::kinematics::Kinematics::frameTframe(_tcp_frame, dynamic_cast<rw::kinematics::Frame*>(_object), _state).P().norm2();
        // store the current state
        _state_vector[i] = _state;
    }

    rw::common::Log::log().info() << "The ball was not found " << ball_not_found << " / "<< _object_pos.size() << " times.\n";
    // plot the new data
    plotSimData();

}


void MovementSimulator::plotSimData(){
    // is there data in general
    if(!_time.size()){
        rw::common::Log::log().info() << "No data to plot.\n";
        return;
    }

    std::string data_selector = _sim_comboBox_plot->currentText().toStdString();

    std::string xlab = "Step";
    std::string ylab = "Error [m]";

    if(data_selector == _detection_error_name){
        ylab = "Error [mm]";
        plotData(_time, _detection_error, xlab, ylab);
    } else if(data_selector == _ball_tcp_dist_name){
        plotData(_time, _ball_tcp_dist, xlab, ylab);
    } else if(data_selector == _kalman_error_name){
        ylab = "Error [mm]";
        plotData(_time, _kalman_error, xlab, ylab);
    }

}


void MovementSimulator::simSliderTime(){
    int sliderPos = _sim_slider_time->sliderPosition();
    if(_state_vector.size() > sliderPos){
        getRobWorkStudio()->setState(_state_vector[sliderPos]);

        double lower = _sim_plot->yAxis->range().lower;
        double upper = _sim_plot->yAxis->range().upper;
        QVector<double> xdata = {sliderPos, sliderPos};
        QVector<double> ydata = {lower, upper};

        // plot pointer
        _sim_plot->graph(1)->clearData();
        _sim_plot->graph(1)->setData(xdata, ydata);
        _sim_plot->replot();
    }
}

void MovementSimulator::plotData(std::vector< double > &inx, std::vector< double > &iny, std::string &xlab, std::string &ylab){
    if(inx.size() != iny.size() || inx.size() < 2){
        rw::common::Log::log().error() << "Plot data not of equal size!\n";
        return;
    }
    // convert data
    QVector<double> xdata(inx.size()), ydata(iny.size());

    double xmax = inx[0];
    double xmin = inx[0];
    for(unsigned int i = 0; i < inx.size(); i++){
        xdata[i] = inx[i];
        if(xmax < inx[i]){
            xmax = inx[i];
        }
        if(xmin > inx[i]){
            xmin = inx[i];
        }
    }

    double ymax = iny[0];
    double ymin = iny[0];
    for(unsigned int i = 0; i < iny.size(); i++){
        ydata[i] = iny[i];
        if(ymax < iny[i]){
            ymax = iny[i];
        }
        if(ymin > iny[i]){
            ymin = iny[i];
        }
    }

    // scale axis to get some space
    double adjuster = (xmax - xmin) * rw::math::Pi / 100;
    xmax += adjuster;
    xmin -= adjuster;

    adjuster = (ymax - ymin) * rw::math::Pi / 100;
    ymax += adjuster;
    ymin -= adjuster;

    // plot axis
    _sim_plot->xAxis->setRange(xmin,xmax);
    _sim_plot->yAxis->setRange(ymin,ymax);

    //
    _sim_plot->clearGraphs();
    _sim_plot->addGraph();
    _sim_plot->graph(0)->setData(xdata, ydata);

    // plot label
    _sim_plot->xAxis->setLabel(QString::fromStdString(xlab));
    _sim_plot->yAxis->setLabel(QString::fromStdString(ylab));

    // add layer for marker
    _sim_plot->addGraph();
    _sim_plot->graph(1)->setPen(QPen(QColor(255, 0, 0)));
    _sim_plot->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, 3));

    // reset slider
    _sim_slider_time->setSliderPosition(0);
    simSliderTime();

    // graph updated in simSliderTIme()

}

bool MovementSimulator::connectToDevices(){

    if (_wc != NULL) {
        std::string deviceName = _settings_device->text().toStdString();
        std::string tcpFrame = _settings_tcp->text().toStdString();
        std::string objName = _settings_object->text().toStdString();
        std::string cameraName = _settings_camera->text().toStdString();

        _devices_found = true;

        // search for device
        _device = _wc->findDevice(deviceName);
        if(_device == NULL){
            _devices_found = false;
            rw::common::Log::log().info() << "Device '" << deviceName << "' was not found.\n";
        }

        // search for tcp frame
        _tcp_frame = _wc->findFrame(tcpFrame);
        if(_tcp_frame == NULL){
            _devices_found = false;
            rw::common::Log::log().info() << "Tcp Frame '" << tcpFrame << "' was not found.\n";
        }

        // search for ball
        _object = dynamic_cast<rw::kinematics::MovableFrame*>(_wc->findFrame(objName));
        if(_object == NULL){
            _devices_found = false;
            rw::common::Log::log().info() << "Movable Frame '" << objName << "' was not found.\n";
        }
        // modify color of object
        rw::models::Object::Ptr ob_ball = _wc->findObject(objName);
        if(ob_ball == NULL){
            _devices_found = false;
            rw::common::Log::log().info() << "Model3D '" << objName << "' was not found.\n";
        } else{
            rw::graphics::Model3D::Material material("Spongy",1.0, 0.0, 0.0, 1.0); // name, r, g, b, alpha
            std::vector< rw::graphics::Model3D::Ptr > model = ob_ball->getModels();
            for(int i = 0; i < model.size(); i++){
                (model[i]->getMaterials()).clear();
                model[i]->addMaterial(material);
            }
        }

        // modify color of camera itself
        std::string camera_name = "Camera";
        rw::models::Object::Ptr ob_camera = _wc->findObject(camera_name);
        if(ob_camera == NULL){
            rw::common::Log::log().info() << "Model3D '" << camera_name << "' was not found.\n";
        } else{
            rw::graphics::Model3D::Material material("Gold, boy!", 0.85, 0.64, 0.13, 1.0); // name, r, g, b, alpha
            std::vector< rw::graphics::Model3D::Ptr > model = ob_camera->getModels();
            for(int i = 0; i < model.size(); i++){
                (model[i]->getMaterials()).clear();
                model[i]->addMaterial(material);
            }
        }

        // render table
        rw::kinematics::Frame* textureFrame = _wc->findFrame("TableTexture");
        std::string texture = "@MY_CURRENT_CMAKE_PATH@/../texture/table_texture02.jpg";
        // if file not found, ask for it
        if(!boost::filesystem::exists(texture) && textureFrame != NULL){
            rw::common::Log::log().info() << "Could not find texture file '" << texture << "'\n";
            QString textureName = QFileDialog::getOpenFileName(this,
                                                               tr("Open Table Texture"), "~/", tr(""));
            texture = textureName.toStdString();
        }
        if (textureFrame != NULL && boost::filesystem::exists(texture)) {
            rw::common::Log::log().info() << "Rendering with texture file '" << texture << "'\n";
            getRobWorkStudio()->getWorkCellScene()->addRender("TextureImage",_textureRender,textureFrame);
            rw::sensor::Image::Ptr image;
            image = rw::loaders::ImageLoader::Factory::load(texture);
            _textureRender->setImage(*image);
            getRobWorkStudio()->updateAndRepaint();
        } else{
            rw::common::Log::log().info() << "Could not render table background.\n";
        }


        // Create a GLFrameGrabber if there is a camera frame with a Camera property set
        rw::kinematics::Frame* cameraFrame_l = _wc->findFrame(cameraName + "Left");
        rw::kinematics::Frame* cameraFrame_r = _wc->findFrame(cameraName + "Right");
        if (cameraFrame_l != NULL && cameraFrame_r != NULL) {
            if (cameraFrame_l->getPropertyMap().has("Camera")) {
                // Read the dimensions and field of view
                double fovy;
                int width,height;
                std::string camParam = cameraFrame_l->getPropertyMap().get<std::string>("Camera");
                std::istringstream iss (camParam, std::istringstream::in);
                iss >> fovy >> width >> height;
                // Create a frame grabber
                _framegrabber_l = new rwlibs::simulation::GLFrameGrabber(width,height,fovy);
                rw::graphics::SceneViewer::Ptr gldrawer = getRobWorkStudio()->getView()->getSceneViewer();
                _framegrabber_l->init(gldrawer);
            } else{
                rw::common::Log::log().error() << "Could not get property map.\n";
            }
            if (cameraFrame_r->getPropertyMap().has("Camera")) {
                // Read the dimensions and field of view
                double fovy;
                int width,height;
                std::string camParam = cameraFrame_r->getPropertyMap().get<std::string>("Camera");
                std::istringstream iss (camParam, std::istringstream::in);
                iss >> fovy >> width >> height;
                // Create a frame grabber
                _framegrabber_r = new rwlibs::simulation::GLFrameGrabber(width,height,fovy);
                rw::graphics::SceneViewer::Ptr gldrawer = getRobWorkStudio()->getView()->getSceneViewer();
                _framegrabber_r->init(gldrawer);
            } else{
                rw::common::Log::log().error() << "Could not get property map.\n";
            }

        }
    }



    return _devices_found;
}


bool MovementSimulator::getStereoImages(cv::Mat &left, cv::Mat &right) {
    bool ret = false;
    if (_framegrabber_l != NULL && _framegrabber_r != NULL) {
        ret = true;
        // initiate
        std::string camera = _settings_camera->text().toStdString();
        getRobWorkStudio()->setState(_state);

        // Get the image as a RW image
        rw::kinematics::Frame* cameraFrame_l = _wc->findFrame(camera + "Left");
        rw::kinematics::Frame* cameraFrame_r = _wc->findFrame(camera + "Right");
        if(cameraFrame_l == NULL || cameraFrame_r == NULL){
            rw::common::Log::log().error() << "Camera  frames were not found!\n";
            return false;
        }
        _framegrabber_l->grab(cameraFrame_l, _state);
        _framegrabber_r->grab(cameraFrame_r, _state);
        const rw::sensor::Image& image_l = _framegrabber_l->getImage();
        const rw::sensor::Image& image_r = _framegrabber_r->getImage();

        // Convert to OpenCV image
        left = toOpenCVImage(image_l);
        right = toOpenCVImage(image_r);

    }
    return ret;
}


cv::Mat MovementSimulator::toOpenCVImage(const rw::sensor::Image& img) {
    cv::Mat res(img.getHeight(),img.getWidth(), CV_8UC3);
    res.data = (uchar*)img.getImageData();
    cv::flip(res, res, 0);
    cv::cvtColor(res, res, CV_RGB2BGR);
    return res;
}


bool MovementSimulator::objectCought(rw::math::Transform3D<double> nextState){
    // test if the object is at all within grippable range
    double graspingrange = 0.03;
    double tesselationstep = 0.0025; // tesselate to 0.5cm
    // range between two obj pos should be greater than distance to current point from tcp + gripping range
    rw::math::Transform3D<double> objTtcp = rw::kinematics::Kinematics::frameTframe(dynamic_cast<rw::kinematics::Frame*>(_object), _tcp_frame, _state);

    if((nextState.P() - _object->getTransform(_state).P()).norm2() >= objTtcp.P().norm2() - graspingrange){
        // the object is within range that it could be grasped, tesselate its movement to see if it truly is grasped
        // for now this is done linearly
        rw::math::Vector3D<double> diff = (nextState.P() - _object->getTransform(_state).P());
        rw::math::Transform3D<double> transform;
        transform.R() = nextState.R();
        int steps = diff.norm2() / tesselationstep + 1;
        // run through steps
        for(int i = 0; i < steps; i++){
            transform.P() = _object->getTransform(_state).P() + diff * i / (steps - 1);
            // check if within range
            _object->setTransform(transform, _state);
            objTtcp = rw::kinematics::Kinematics::frameTframe(dynamic_cast<rw::kinematics::Frame*>(_object), _tcp_frame, _state);
            if(objTtcp.P().norm2() <= graspingrange){
                return true;
            }
        }
    }

    return false;
}

void MovementSimulator::loadObjectPath(){
    // open path browsing
    QString fileName = QFileDialog::getOpenFileName(this,
                                                    tr("Open .csv"), "@MY_CURRENT_CMAKE_PATH@/../workcell/testpaths/", tr("Text Files (*.csv *.txt)"));

    // reset path
    _object_pos.clear();

    // open file
    std::fstream file(fileName.toStdString(), std::fstream::in);

    // test if still open
    if(! file.is_open() ){
        rw::common::Log::log().info() << "ERROR: Could not open " << fileName.toStdString() << "'.\n";
        return;
    }

    rw::common::Log::log().info() << "Loading file '" << fileName.toStdString() << "'.\n";

    std::string data_s;
    double number;
    rw::math::Transform3D<double> pose;
    rw::math::RPY<double> rot;

    while (!file.eof()) {
        int i = 0;
        while(!file.eof() && i < 6){
            if(i == 5){
                std::getline (file, data_s);
            } else {
                std::getline (file, data_s, ',');
            }
            std::istringstream(data_s) >> number;
            if(i < 3){
                pose.P()[i] = number;
            } else {
                rot[i - 3] = number * rw::math::Deg2Rad;
            }
            i++;
        }
        if(i == 6){
            pose.R() = rot.toRotation3D();
            _object_pos.emplace_back(pose);
        }
    }

    file.close();

    rw::common::Log::log().info() << "Loaded " << _object_pos.size() << " object poses.\n";
}

void MovementSimulator::calculateObjectPath(){
    rw::math::Vector3D<double> p(_path_p1->value(), _path_p2->value(), _path_p3->value());
    rw::math::Vector3D<double> v(_path_v1->value(), _path_v2->value(), _path_v3->value());
    rw::math::Vector3D<double> a(_path_a1->value(), _path_a2->value(), _path_a3->value());
    double maxt = _path_t->value();
    int samples = _path_n->value();
    rovi::LinearPath pathgenerator(p, v, a);

    rw::common::Log::log().info() << " P: " << p << "\n V: " << v << "\n A: "  << a << "\n t: " << maxt << ", n: " << samples << "\n";

    _object_pos.resize(samples);

    for(int s = 0; s < samples; s++){
        double t = (double)s * maxt / (samples - 1);
        _object_pos[s].P() = pathgenerator.pos(t);
        _object_pos[s].R() = rw::math::Rotation3D<double>::identity();

        // check that they confine to RWS constraints
        for(int i = 0; i < _object_pos[s].P().size(); i++){
            if(_object_pos[s].P()[i] > 5.0){
                _object_pos[s].P()[i] = 5.0;
            } else if (_object_pos[s].P()[i] < -5.0){
                _object_pos[s].P()[i] = -5.0;
            }
        }
    }
    rw::common::Log::log().info() << "Calculated " << _object_pos.size() << " object poses.\n";

}



void MovementSimulator::stateChangedListener(const rw::kinematics::State& state) {
    _state = state;
}


Q_EXPORT_PLUGIN(MovementSimulator);
