#include "tracker.hpp"
#include "vision/vision_algo.h"
#include <fstream>
#include <eigen3/Eigen/Dense>

using namespace std;

rovi::Tracker::Tracker(
        const Eigen::Matrix2d &covariance_left,
        const Eigen::Matrix2d &covariance_right,
        const rovi::Matrix3by4d &perspective_left,
        const rovi::Matrix3by4d &perspective_right,
        const double acceleration_delta,
        const double dt)
    : Stereoalgorithm(perspective_left, perspective_right, covariance_left, covariance_right),
      hwcomponents::Stereocamera("/stereo_camera/left/image_rect_color", "/stereo_camera/right/image_rect_color"),
      _kalman(new Kalman(_acceleration_delta,_dt)),
      _acceleration_delta(acceleration_delta),
      _dt(dt),
      _tracker_thread(),
      _tracker_thread_should_stop(true),
      _reinitialize_tracker(true),
      _left_and_right(2),
      _detector(rovi::vision::vision_algo::findRedBlob),
      _measurement3d(),
      _measurement3d_uncertainty(),
      _display_images(true)
{

}

rovi::Tracker::Tracker(
        const std::string &covariance_left,
        const std::string &covariance_right,
        const std::string &perspective_left,
        const std::string &perspective_right,
        const double acceleration_delta,
        const double dt)
    :
      hwcomponents::Stereocamera("/stereo_camera/left/image_rect_color", "/stereo_camera/right/image_rect_color"),
      _acceleration_delta(acceleration_delta),
      _dt(dt),
      _kalman(new Kalman(_acceleration_delta,_dt)),
      _tracker_thread(),
      _tracker_thread_should_stop(true),
      _reinitialize_tracker(true),
      _left_and_right(2),
      _detector(rovi::vision::vision_algo::findRedBlob),
      _measurement3d(),
      _measurement3d_uncertainty(),
      _display_images(true)
{
    Eigen::Matrix2d cov2d_left(2,2), cov2d_right(2,2);
    rovi::Matrix3by4d p_left(3,4), p_right(3,4);
    ifstream ifs(covariance_left);
    ifs >> cov2d_left;
    ifs.close();
    ifs.open(covariance_right);
    ifs >> cov2d_right;
    ifs.close();
    ifs.open(perspective_left);
    ifs >> p_left;
    ifs.close();
    ifs.open(perspective_right);
    ifs >> p_right;
    ifs.close();
    set2DUncertainity(cov2d_left,cov2d_right);
    setPerspectiveTransform(p_left,p_right);
}

void rovi::Tracker::startTracker() {
    if(_tracker_thread_should_stop) {
        start();
        _tracker_thread_should_stop=false;
        _tracker_thread=std::thread(&Tracker::trackerLoop,this);
    }
}

void rovi::Tracker::stopTracker() {
    if(!_tracker_thread_should_stop) {
        quitNow();
        _tracker_thread_should_stop=true;
        _tracker_thread.join();
    }
}

void rovi::Tracker::setDisplayMode(const bool display_images) {
    _display_images=display_images;
}

const rovi::Vector9d &rovi::Tracker::getBallNonBlocking() const {
    return _kalman->getStateUpdated();
}

const rovi::Vector9d &rovi::Tracker::getBallBlocking() const {
    const size_t current_state_count = _state_counter;
    while(current_state_count==_state_counter) {
        //Busy wait
    }
    return getBallNonBlocking();
}

std::tuple<rw::math::Vector3D<double>, rw::math::Vector3D<double>, rw::math::Vector3D<double> > rovi::Tracker::stateToRW(const rovi::Vector9d &state) const {
    rw::math::Vector3D<double> pos(state.segment<3>(0));
    rw::math::Vector3D<double> vel(state.segment<3>(3));
    rw::math::Vector3D<double> acc(state.segment<3>(6));
    return std::make_tuple(pos,vel,acc);
}

void rovi::Tracker::getBallPosVelNonBlocking(rw::math::Vector3D<double> &position, rw::math::Vector3D<double> &velocity) const {
    std::tie(position,velocity,std::ignore) = stateToRW(getBallNonBlocking());
}

void rovi::Tracker::getBallPosVelBlocking(rw::math::Vector3D<double> &position, rw::math::Vector3D<double> &velocity) const {
    std::tie(position,velocity,std::ignore) = stateToRW(getBallBlocking());
}

std::pair<size_t, bool> rovi::Tracker::getMeasurement3d() {
    //Block until we get stereo image
    size_t n = getStereoImage2(left(),right());
    //std::cout << "N = " << n << std::endl;
    //Detect ball in images
    std::vector<std::vector<cv::Point2d> > ball_points;
    applyAlgorithm(_left_and_right,_detector,ball_points);
    if(ball_points[0].empty() || ball_points[1].empty()) {
        std::cout << "NO BALLS AT ALL" << std::endl;
        return std::make_pair(n,false);
    }
//    std::cout << "BALLS: " << ball_points[0].size() << "," << ball_points[1].size() << std::endl;
    //Filter so we only have one valid ball position
    std::vector< std::vector< cv::Point2d > > paired_points;
    simpleFeaturePairing(ball_points[0], ball_points[1], paired_points);
    if(paired_points.size()!=1) {
        std::cout << "MORE/LESS than 1 pairs points (" << paired_points.size() << ")" << std::endl;
        return std::make_pair(n,false);
    }
    //Get ball position in 3d
    cv::Point2d left_point=paired_points[0][0];
    cv::Point2d right_point=paired_points[0][1];
//    std::cout << "LEFT: " << left_point << ", RIGHT: " << right_point << std::endl;
    std::tie(_measurement3d,_measurement3d_uncertainty) = rectifiedStereopsis(std::make_pair(left_point,right_point));
    //TODO: Filter only valid 3d camera coordinates
    return std::make_pair(n,true);
}
