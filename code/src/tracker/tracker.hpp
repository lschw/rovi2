#pragma once
#include <string>
#include <thread>
#include <opencv2/core/core.hpp>
#include "stereovision/stereoalgorithm.hpp"
#include "hwcomponents/stereocamera.hpp"
#include "kalman/kalman.hpp"
#include "utils/defines_matrices.hpp"
#include <rw/math/Vector3D.hpp>
#include <memory>

namespace rovi {

class Tracker : protected Stereoalgorithm, protected hwcomponents::Stereocamera
{
public:
    EIGEN_MAKE_ALIGNED_OPERATOR_NEW

    virtual ~Tracker() { }

    Tracker(
            const Eigen::Matrix2d &covariance_left,
            const Eigen::Matrix2d &covariance_right,
            const rovi::Matrix3by4d &perspective_left,
            const rovi::Matrix3by4d &perspective_right,
            const double acceleration_delta=3.0,
            const double dt=0.05
            );

    Tracker(
            const std::string &covariance_left="test_covariance_2d.txt",
            const std::string &covariance_right="test_covariance_2d.txt",
            const std::string &perspective_left="left_projection_matrix_rectified.txt",
            const std::string &perspective_right="right_projection_matrix_rectified.txt",
            const double acceleration_delta=3.0,
            const double dt=0.05
            );


    void startTracker();

    void stopTracker();


    void setDisplayMode(const bool display_images=true);

    const Vector9d &getBallNonBlocking() const;

    const Vector9d &getBallBlocking() const;

    std::tuple<rw::math::Vector3D<double>,rw::math::Vector3D<double>,rw::math::Vector3D<double> > stateToRW(const Vector9d &state) const;

    void getBallPosVelNonBlocking(rw::math::Vector3D<double> &position, rw::math::Vector3D<double> &velocity) const;

    void getBallPosVelBlocking(rw::math::Vector3D<double> &position, rw::math::Vector3D<double> &velocity) const;


protected:
    std::unique_ptr<Kalman> _kalman;
    double _acceleration_delta;
    double _dt;
    std::thread _tracker_thread;
    std::atomic_bool _tracker_thread_should_stop;
    std::atomic_bool _reinitialize_tracker;
    std::vector<cv::Mat> _left_and_right;
    //cv::Mat _left, _right;
    rovi::vision::vision_algo _detector;

    Eigen::Vector3d _measurement3d;
    Eigen::Matrix3d _measurement3d_uncertainty;
    std::atomic_bool _display_images;
    std::atomic_size_t _state_counter;


    cv::Mat& left() { return _left_and_right[0]; }
    cv::Mat& right() { return _left_and_right[1]; }

    /**
     * @brief getMeasurement3d Gets a 3d ball position measurement
     * @return Number of missed measurements plus one and indication of whether we actually got a usable last measurement
     */
    std::pair<size_t,bool> getMeasurement3d();

    void displayStereoImages(cv::Mat &left_img,cv::Mat &right_img) {
        //std::thread([this,&left_img,&right_img](){
        putPointOnImages(left_img,right_img,_measurement3d,cv::Scalar(255,0,0),15,false);
        putPointOnImages(left_img,right_img,_kalman->getStateUpdated().head<3>(),cv::Scalar(0,255,0),10,true);
        putPointOnImages(left_img,right_img,_kalman->getStatePredicted().head<3>(),cv::Scalar(0,255,255),12,false);
        cv::namedWindow("left", cv::WINDOW_NORMAL);
        cv::namedWindow("right", cv::WINDOW_NORMAL);
        cv::imshow("left", left_img);
        cv::imshow("right", right_img);
        cv::waitKey(30);
        //});
    }

    cv::Point2d point3dToPixel(const Eigen::Vector3d &point, const bool left) {
        Eigen::Vector4d proj3;
        proj3 << point, 1.0;
        Eigen::Vector3d proj2 = (left?_p_left:_p_right)*proj3;
        Eigen::Vector2d pix = (1.0/(proj2(2)))*proj2.head<2>();
        return cv::Point2d(pix(0),pix(1));
    }

    bool pixelIsWithinBounds(const cv::Point2d &pixel, const bool left_image=true) {
        bool within_bounds = false;
        if(left_image) {
            if((pixel.x>=0)&&(pixel.x<left().cols)&&(pixel.y>=0)&&(pixel.y<left().rows))
                within_bounds=true;
        }
        else if((pixel.x>=0)&&(pixel.x<right().cols)&&(pixel.y>=0)&&(pixel.y<right().rows))
            within_bounds=true;
        return within_bounds;
    }

    void putPointOnImages(cv::Mat &left_img,cv::Mat &right_img, const Eigen::Vector3d &point, const cv::Scalar color=cv::Scalar(255,0,0), const size_t radius=10, const bool filled=true) {
        cv::Point2d pix_left = point3dToPixel(point,true);
        cv::Point2d pix_right = point3dToPixel(point,false);
        if(pixelIsWithinBounds(pix_left,true)&&pixelIsWithinBounds(pix_right,false)) {
            cv::circle(left_img,pix_left,radius,color,(filled?-1:3));
            cv::circle(right_img,pix_right,radius,color,(filled?-1:3));
        }
    }

    void trackerLoop() {
        while(!_tracker_thread_should_stop) {
            //(Re)Initialize Kalman filter
            if(_reinitialize_tracker) {
                std::cout << "Initializing tracker" << std::endl;
                if(!(getMeasurement3d().second))
                    continue;
                rovi::Vector9d state;
                state << _measurement3d,0,0,0,0,0,0;
                std::cout << "Resetting tracker" << std::endl;
                _kalman.reset();
                _kalman=std::unique_ptr<Kalman>(new Kalman(_acceleration_delta,_dt));
                _kalman->setInitialState(state,_measurement3d_uncertainty);
                _reinitialize_tracker=false;
                ++_state_counter;
                std::cout << "Tracker initialized." << std::endl;
            }
            //std::cout << "Track ball!" << std::endl;
            //Track ball
            size_t n_measurements;
            bool last_measurement_usable;
            std::tie(n_measurements,last_measurement_usable) = getMeasurement3d();
            //std::cout << (last_measurement_usable?":-)":":'(") << std::endl;
            for(size_t i=0; i<n_measurements; ++i)
                _kalman->predict(); //predict for all measurements, including missed ones.
            if(last_measurement_usable)
                _kalman->update(_measurement3d,_measurement3d_uncertainty); //update for last measurement if it exists
            //Check if tracked ball is within camera view. If not, reset kalman filter.
            if(!pixelIsWithinBounds(point3dToPixel(_kalman->getStateUpdated().head<3>(),true),true)) {
                _reinitialize_tracker=true;
            }
            else {
                ++_state_counter;
                //Display images
                if(_display_images) {
                    displayStereoImages(left(),right());
                }
            }
        }
    }
};

} //namespace rovi
