#pragma once
#include <string>
#include <iostream>
#include <iomanip>

namespace rovi {
/**
 * @brief The progressBar functor.
 * Usage:
 *  cout << progressBar(0.2);   // 20 %
 *  cout << progressBar(0.356); // 35.6 %
 */
struct progressBar {
    std::string operator()(float progress,std::ostream &out = std::cout,
                           unsigned int barWidth=70) {
        out << std::setw(5) << std::setprecision(3)
            <<((progress)>=1?100:(progress*100.0)) <<"% [";
        for(unsigned int i=0, pos=barWidth*progress;i<barWidth;++i) {
            if(i<pos) out << "=";
            else if(i==pos) out << ">";
            else out << " ";
        }
        out << "] "
            << "\r" << std::flush;
        return "";
    }
} progressBar; //Global instance.
} //namespace rovi
