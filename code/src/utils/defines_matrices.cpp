#include "defines_matrices.hpp"

rovi::Matrix4d rovi::inverseHomogeneousTransform(const rovi::Matrix4d &A) {
    Matrix4d B(4,4);
    B << A.block<3,3>(0,0).transpose(), -A.block<3,3>(0,0).transpose()*A.block<3,1>(0,3),
            0.0,0.0,0.0,1.0;
    return B;
}
