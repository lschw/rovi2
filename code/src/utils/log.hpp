#pragma once
#include <boost/filesystem.hpp>

#define ROVI_WARN(msg) std::cerr <<                       \
    boost::filesystem::path(__FILE__).filename().string() \
    << ":"                                                \
    << __LINE__                                           \
    << " "                                                \
    << msg <<  std::endl;

#define ROVI_THROW(msg) throw( std::runtime_error(         \
    boost::filesystem::path(__FILE__).filename().string()  \
    + ":"                                                  \
    + std::to_string(__LINE__)                             \
    + " "                                                  \
    + msg                                                  \
    + "\n"                                                 \
));
