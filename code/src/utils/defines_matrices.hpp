#pragma once
#include <Eigen/Core>

//How to load an Eigen matrix
//from a text file, where numbers
//are separated by whitespace.
namespace Eigen{
template<typename Derived>
std::istream & operator >> (std::istream & s, MatrixBase<Derived> & m) {
    for (int i = 0; i < m.rows(); ++i)
        for (int j = 0; j < m.cols(); j++)
            s >> m(i,j);
    return s;
}
}

//also works, but probably slower.
//namespace Eigen{
//template<typename Derived>
//std::istream & operator >> (std::istream & s, MatrixBase<Derived> & m) {
//    for (int i = 0; i < m.rows(); ++i) {
//        string line;
//        getline(s,line);
//        stringstream ssline(line);
//        for (int j = 0; j < m.cols(); j++)
//            ssline >> m(i,j);
//    }
//    return s;
//}
//}

namespace rovi {



using Vector9d = Eigen::Matrix<double,9,1>;

using Matrix2by3d = Eigen::Matrix<double,2,3>;
using Matrix3by9d = Eigen::Matrix<double,3,9>;
using Matrix3by4d = Eigen::Matrix<double,3,4>;
using Matrix4by3d = Eigen::Matrix<double,4,3>;
using Matrix9by3d = Eigen::Matrix<double,9,3>;
using Matrix9d = Eigen::Matrix<double,9,9>;

using Matrix3d = Eigen::Matrix3d;
using Vector3d = Eigen::Vector3d;
using Matrix4d = Eigen::Matrix4d;


Matrix4d inverseHomogeneousTransform(const Matrix4d &A);


}
