#pragma once

#include <ros/ros.h>
#include <ros/io.h>

#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

#include <cv_bridge/cv_bridge.h>

#include <image_transport/image_transport.h>

#include <sensor_msgs/Image.h>
#include <sensor_msgs/image_encodings.h>

#include <QThread>

#include <atomic>
#include <condition_variable>

namespace rovi {
namespace hwcomponents {

/**
 * @brief The stereocamera class Implements an image getter for a stereocamera.
 */
class Stereocamera : public QThread
{
public:
    ///Note: The constructor will block until connected with roscore
    ///Instead of ros::spin(), start this thread with the start() method
    ///to run the event loop of ros
    Stereocamera(std::string leftCamera, std::string rightCamera);

    ~Stereocamera(){}

    /**
     * @brief run Starter of the thread, called with this.start().
     */
    void run();

    /**
     * @brief getStereoImage Way to access images without the use of connect method.
     * @param left
     * @param right
     * @return
     */
    bool getStereoImage(cv::Mat &left, cv::Mat &right);

    /**
     * @brief getStereoImage2
     * @param left
     * @param right
     * @return Number of received image pairs (At least 1). If 2 or above, image pairs have been lost.
     */
    size_t getStereoImage2(cv::Mat &left, cv::Mat &right);

    /**
     * @brief quitNow Stops the thread.
     */
    void quitNow();



protected:
    /**
     * @brief setCameras Sets the name of the two cameras.
     * @param leftCamera
     * @param rightCamera
     */
    void setCameras(std::string leftCamera, std::string rightCamera);

    /**
     * @brief connectToCameras Connects to the two cameras.
     */
    void connectToCameras();

    // for the bumbelbee2 suscribe to "/stereo_camera/left/image_raw" and right..
    std::string _camera_left, _camera_right;

    /**
     * @brief imageCallbackLeft Algorithm run when new image arives from publisher.
     * @param msg
     */
    void imageCallbackLeft(const sensor_msgs::ImageConstPtr& msg);
    void imageCallbackRight(const sensor_msgs::ImageConstPtr& msg);

    // to see if the images have been updated since last pull
    std::atomic_bool _img_updated_left, _img_updated_right;

    // disables the ros::spin
    std::atomic_bool _quitProgram;

    // mutex
    std::atomic_bool _mutexImage;

    // node handles and stuff..
    sensor_msgs::ImageConstPtr _imageIn;
    cv_bridge::CvImagePtr _imageOut_left, _imageOut_right;
    ros::NodeHandle _nh;
    image_transport::ImageTransport _it;
    image_transport::Subscriber _image_sub_left, _image_sub_right;


    //sync stuff
    cv::Mat _left, _right;
    std::mutex _myMutex;
    size_t _img_count;
    void updateImages();

};

}
}
