#include "robot.h"

rovi::hwcomponents::Robot::Robot(std::string actingNamespace) : _nodehandle("~"), _sdsip(_nodehandle, actingNamespace)
  // sdsip needs the name of the node to connect to, in this case the caros_ur..
{
    _quitProgram = false;
    _pathUpdated = false;
    _connected = false;

    // check if connected
    ros::Time current_timestamp = ros::Time::now();
    ros::Time obtained_timestamp ;
    ros::Time timer = current_timestamp + ros::Duration(5); // wait up to 5 seconds to see if connecting
    bool timeExpired = false;
    do
    {
        ros::Duration(0.05).sleep();  // In seconds // original 0.1s
        obtained_timestamp = _sdsip.getTimeStamp();
        ros::spinOnce();
        timeExpired = (timer < ros::Time::now());
    } while (current_timestamp > obtained_timestamp && !timeExpired);

    // if connected, set true
    if(!timeExpired){
        _connected = true;
    } else {
        ROVI_THROW("Could not connect to Robot.");
    }
}

rovi::hwcomponents::Robot::~Robot(){
}

bool rovi::hwcomponents::Robot::isOK(){
    return _connected;
}

bool rovi::hwcomponents::Robot::movePtp(const rw::math::Q &pos)
{
    if(_connected){
        if (!doMovePtp(pos))
        {
            return false;
        }
        
        return true;
    } else {
       ROVI_THROW("ERROR: Not connected!");
        return false;
    }

}

bool rovi::hwcomponents::Robot::movePtp(const rw::trajectory::QPath &path)
{
    if(_connected){
        if (!doFollowPath(path))
        {
            return false;
        }
        
        return true;
    } else {
        ROVI_THROW("ERROR: Not connected!");
        return false;
    }

}



bool rovi::hwcomponents::Robot::moveLin(const rw::math::Transform3D<double> &to)
{
    if(_connected){
        // use speed to make sure no velocity constraints apply.
        // do this by going through the path and calc velocity / vel. constraint and scale (reduce) velocity

        if (!doMoveLin(to))
        {
            return false;
        }
        
        return true;
    } else {
        ROVI_THROW("ERROR: Not connected!");
        return false;
    }

}


rw::math::Q rovi::hwcomponents::Robot::getConfiguration(){
    if(_connected){
        _configuration = getCurrentJointConfiguration();
    }
    return _configuration;
}


rw::math::Q rovi::hwcomponents::Robot::getCurrentJointConfiguration()
{
    /* Make sure to get and operate on fresh data from the serial device
     * It's assumed that the serial device is not moving
     * ^- That could be asserted / verified using sdsip.isMoving()
     * However other sources could invoke services on the UR that causes it to move...
     */
    ros::Time current_timestamp = ros::Time::now();
    ros::Time obtained_timestamp = _sdsip.getTimeStamp();
    while (current_timestamp > obtained_timestamp)
    {
        ros::Duration(0.05).sleep();  // In seconds // original 0.1s
        ros::spinOnce();
        obtained_timestamp = _sdsip.getTimeStamp();
    }

    return _sdsip.getQ();
}


bool rovi::hwcomponents::Robot::isMoving(){
    bool ret = true;
    rw::math::Q first = getCurrentJointConfiguration();
    ros::Duration(0.05).sleep();  // In seconds // original 0.1s
    rw::math::Q second = getCurrentJointConfiguration();
    // make vibration fix
    rw::math::Q diff = first-second;
    double vibrationAccountance = 0.0001;
    if (diff.norm2() < vibrationAccountance){
        ret = false;
    }
    return ret;
}


bool rovi::hwcomponents::Robot::doFollowPath(const rw::trajectory::QPath &path){
    bool return_status = true;
    for (const rw::math::Q& p : path)
    {
        bool ret = false;
        ret = _sdsip.movePtp(p);
        if (!ret)
        {
            return_status = false;
            ROVI_THROW("The serial device didn't acknowledge the movePtp command.");
            return return_status;

        }
    }
    return return_status;
}


bool rovi::hwcomponents::Robot::servoPtp(const rw::math::Q &pos){
    bool return_status = true;
    return_status = _sdsip.moveServoQ(pos);
    if (!return_status) {
        ROVI_THROW("The serial device didn't acknowledge the movePtp command.");
    }
    return return_status;
}


bool rovi::hwcomponents::Robot::doMovePtp(const rw::math::Q &goToPos)
{
    bool return_status = false;
    rw::trajectory::QPath path;
    path.emplace_back(goToPos);
    return_status = doFollowPath(path);

    return return_status;
}


bool rovi::hwcomponents::Robot::doMoveLin(const rw::math::Transform3D<double> &to)
{
    bool return_status = true;
    bool ret = false;
    ret = _sdsip.moveLin(to);
    if (!ret)
    {
        return_status = false;
        ROVI_THROW("The serial device didn't acknowledge the movePtp command.");
    }

    return return_status;
}


bool rovi::hwcomponents::Robot::isNearGoal(rw::math::Q &goal){
    rw::math::Q diff = goal - getConfiguration();
    double dist = diff.norm2();
    return (dist<0.0005)?true:false;
}

void rovi::hwcomponents::Robot::quitNow(){
    _quitProgram = true;
}

void rovi::hwcomponents::Robot::run(){
    while(ros::ok() && !_quitProgram) {
        // make sure that a point is given
        if(_servoPath.size() < 1){
            continue;
        }
        // -- walk through the path gained using servo
        // check if path is updated... if so move along it
        if(_pathUpdated){
            _pathUpdated = false;
            if(_servoPath.size() > 0){
                servoPtp(_servoPath[0]);
            }
        }
        // if not near goal, wait
        if(isNearGoal(_servoPath[0])){
            // if near goal: scrab the Q and send new one
            _servoPath.erase(_servoPath.begin());
            if(_servoPath.size() > 0){
                servoPtp(_servoPath[0]);
            }
        }
    }
    _quitProgram=false;
}

void rovi::hwcomponents::Robot::servoPtp(const rw::trajectory::QPath &path){
    _servoPath = path;
    _pathUpdated = true;

}
