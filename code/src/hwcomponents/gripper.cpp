#include "gripper.hpp"

rovi::hwcomponents::Gripper::Gripper(std::string actingNamespace) : _nh("~"), _sdsip(_nh, actingNamespace)// sdsip needs the name of the node to connect to, in this case the caros_ur..
{
    _configuration = rw::math::Q(1, 0.0);
    _connected = false;

    // test if conenction in established
    ros::Time current_timestamp = ros::Time::now();
    ros::Time obtained_timestamp ;
    ros::Time timer = current_timestamp + ros::Duration(5); // wait up to 5 seconds to see if connected
    bool timeExpired = false;
    do
    {
        ros::Duration(0.05).sleep();  // In seconds // original 0.1s
        obtained_timestamp = _sdsip.getTimeStamp();
        ros::spinOnce();
        timeExpired = (timer < ros::Time::now());
    } while (current_timestamp > obtained_timestamp && !timeExpired);

    if(!timeExpired){
        _connected = true;
    } else {
        ROVI_THROW("Could not connect to gripper.");
    }

}

bool rovi::hwcomponents::Gripper::isOK(){
    return _connected;
}

rw::math::Q rovi::hwcomponents::Gripper::getConfiguration(){
    return _configuration;
}

void rovi::hwcomponents::Gripper::grip(rw::math::Q &force){
    _sdsip.setForceQ(force);
    // does not support gripping at a specific location, if we want this we need to implement it ourself
    _sdsip.gripQ(rw::math::Q(1, 0.0));
}

void rovi::hwcomponents::Gripper::open(rw::math::Q pos){
    const double MAXPOS = 0.034;
    _sdsip.stopMovement(); // releases the force, but is stuck at the same position

    if(pos[0] <= MAXPOS && pos[0] >= 0 && pos.size() == 1){
        _sdsip.moveQ(pos); // doesn't work currently, idk why.
    }
    else {
        _sdsip.moveQ(rw::math::Q(1,MAXPOS));
    }
}

bool rovi::hwcomponents::Gripper::isMoving(){
    bool ret = true;
    rw::math::Q first = getCurrentJointConfiguration();
    ros::Duration(0.05).sleep();  // In seconds // original 0.1s
    rw::math::Q second = getCurrentJointConfiguration();
    if (first == second){
        ret = false;
    }
    return ret;
}


rw::math::Q rovi::hwcomponents::Gripper::getCurrentJointConfiguration()
{
    /* Make sure to get and operate on fresh data from the serial device
 * It's assumed that the serial device is not moving
 * However other sources could invoke services on the UR that causes it to move...
 */

    ros::Time current_timestamp = ros::Time::now();
    ros::Time obtained_timestamp = _sdsip.getTimeStamp();
    while (current_timestamp > obtained_timestamp)
    {
        ros::Duration(0.05).sleep();  // In seconds // original 0.1s
        ros::spinOnce();
        obtained_timestamp = _sdsip.getTimeStamp();
    }

    return _sdsip.getQ();
}

