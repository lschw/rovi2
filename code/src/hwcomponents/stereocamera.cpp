#include "stereocamera.hpp"


rovi::hwcomponents::Stereocamera::Stereocamera(std::string leftCamera, std::string rightCamera):_nh("~"), _it(_nh),_img_count(0) {
    _quitProgram = false;
    setCameras(leftCamera, rightCamera);
    connectToCameras();
    _mutexImage = false;
}

void rovi::hwcomponents::Stereocamera::setCameras(std::string leftCamera, std::string rightCamera){
    _img_updated_left = false;
    _img_updated_right = false;
    _camera_left = leftCamera;
    _camera_right = rightCamera;
}

void rovi::hwcomponents::Stereocamera::connectToCameras(){
    _image_sub_left = _it.subscribe(_camera_left, 1, &rovi::hwcomponents::Stereocamera::imageCallbackLeft, this);
    _image_sub_right = _it.subscribe(_camera_right, 1, &rovi::hwcomponents::Stereocamera::imageCallbackRight, this);
    ROS_INFO("Subscribed to cameras.");

}


void rovi::hwcomponents::Stereocamera::quitNow(){
    _quitProgram = true;
}

void rovi::hwcomponents::Stereocamera::imageCallbackLeft(const sensor_msgs::ImageConstPtr& msg)
{
    _imageIn = msg;
    //while (_mutexImage);
    std::unique_lock<std::mutex> lck(_myMutex);
    try{
        _imageOut_left = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
        _img_updated_left = true;
        updateImages();
    }
    catch(cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }
}

void rovi::hwcomponents::Stereocamera::imageCallbackRight(const sensor_msgs::ImageConstPtr& msg)
{
    _imageIn = msg;
    //while (_mutexImage);
    std::unique_lock<std::mutex> lck(_myMutex);
    try{
        _imageOut_right = cv_bridge::toCvCopy(msg, sensor_msgs::image_encodings::BGR8);
        _img_updated_right = true;
        updateImages();
    }
    catch(cv_bridge::Exception& e)
    {
        ROS_ERROR("cv_bridge exception: %s", e.what());
        return;
    }
}

void rovi::hwcomponents::Stereocamera::updateImages() {
    //std::unique_lock<std::mutex> lck(_myMutex);
    if (_img_updated_left && _img_updated_right && _imageOut_left->header.frame_id == _imageOut_right->header.frame_id) {
        _left = _imageOut_left->image.clone();
        _right = _imageOut_right->image.clone();
        ++_img_count;
        _img_updated_left=false;
        _img_updated_right=false;
    }
}

bool rovi::hwcomponents::Stereocamera::getStereoImage(cv::Mat &left, cv::Mat &right){
    bool ret = (_img_updated_left & _img_updated_right);
    if(ret && !(_imageOut_left->image.cols > 0 && _imageOut_left->image.rows > 0 && _imageOut_right->image.cols > 0 && _imageOut_right->image.rows > 0)){
        ret = false;
    }
    if(ret && (_imageOut_left->header.frame_id == _imageOut_right->header.frame_id)){
        _mutexImage = true;
        left = _imageOut_left->image.clone();
        right = _imageOut_right->image.clone();
        _img_updated_left = false;
        _img_updated_right = false;
        _mutexImage = false;
    } else{
        ret = false;
    }
    return ret;
}

size_t rovi::hwcomponents::Stereocamera::getStereoImage2(cv::Mat &left, cv::Mat &right) {
    do {
        std::unique_lock<std::mutex> lck(_myMutex);
    } while(_img_count==0);
    std::unique_lock<std::mutex> lck(_myMutex);
    _left.copyTo(left);
    _right.copyTo(right);
    const size_t image_count = _img_count;
    _img_count=0;
    return image_count;
}

void rovi::hwcomponents::Stereocamera::run(){
    while(ros::ok() && !_quitProgram) {
        ros::spinOnce();

        ros::Duration(0.1).sleep();
    }
    _quitProgram=false;
}
//http://answers.ros.org/question/53234/processing-an-image-outside-the-callback-function/
