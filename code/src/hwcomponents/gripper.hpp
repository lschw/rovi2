#pragma once

#include <string>
#include <stdexcept>
#include <thread>

#include <caros/gripper_si_proxy.h>

#include <ros/ros.h>

#include <rw/math/Q.hpp>

#include "../utils/log.hpp"


namespace rovi {
namespace hwcomponents {

class Gripper
{
public:
    Gripper(std::string actingNamespace);
    virtual ~Gripper(){}

    bool isOK();

    /**
     * @brief getConfiguration Gets the grippers configurations.
     * @return
     */
    rw::math::Q getConfiguration();

    /**
     * @brief grip Grasps with the specified force in N. (SchunkPG70 takes values between 20 and 200)
     * @param force The force. Set to about ~20. Max is 200, don't use it!
     */
    void grip(rw::math::Q &force);

    /**
     * @brief open Makes the gripper go to the specified configuration.
     * @param pos
     */
    void open(rw::math::Q pos = rw::math::Q(1, 0.034));

     bool isMoving();


protected:

    rw::math::Q getCurrentJointConfiguration();

    ros::NodeHandle _nh;
    // tere is some error here that causes it to fail, IDK WHY!?!
    caros::GripperSIProxy _sdsip;

    // used to check if it succeded finding the robot in workspace
    bool _connected;

    // used to hold the robot configurations and to tell if the conf. update thread has to stop
    rw::math::Q _configuration;
};

}
}
