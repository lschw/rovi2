#pragma once

#include <caros/serial_device_si_proxy.h>
#include <caros/serial_device_service_interface.h>
#include <caros/common_robwork.h>

#include <ros/ros.h>

#include <string>
#include <stdexcept>
#include <thread>

#include <rw/math.hpp>

#include <QThread>

#include "../utils/log.hpp"




/* WARNING: USE AT YOUR OWN RISK!
 */

namespace rovi {
namespace hwcomponents {

class Robot  : public QThread
{
public:
    Robot(std::string actingNamespace);
    
    virtual ~Robot();
    
    bool isOK();

    /**
     * @brief movePtp Moves the robot linearly in joint space.
     * @param pos The end position of the robot
     * @return
     */
    bool movePtp(const rw::math::Q &pos);

    /**
     * @brief movePtp Moves the robot linearly along a path in joint space.
     * @param path The path to follow.
     * @return
     */
    bool movePtp(const rw::trajectory::QPath &path);
    
    /**
     * @brief servoPtp Moves the robot to a point, but can be interrupted on the way.
     * @param pos The end pose of the robot
     * @return
     */
    bool servoPtp(const rw::math::Q &pos);


    void servoPtp(const rw::trajectory::QPath &path);

    /**
     * @brief moveLin Moves the robot linear in cartesian space.
     * @param to The end position of the robot
     * @return
     */
    bool moveLin(const rw::math::Transform3D<double> &to);
        
    /**
     * @brief getConfiguration Get the configuration of the robot.
     * @return The robot configuration.
     */
    rw::math::Q getConfiguration();
    
        
    /**
     * @brief isMoving Checks if the robot is moving
     * @return true if moving
     */
    // Was changed to account for vibration of the robot
    bool isMoving();

    bool isNearGoal(rw::math::Q &goal);


    void run();


    void quitNow();

protected:

    rw::math::Q getCurrentJointConfiguration();



    rw::trajectory::QPath getQPath(const double q_change);

    
    bool doFollowPath(const rw::trajectory::QPath &path);


    bool doMovePtp(const rw::math::Q &goToPos);


    bool doMoveLin(const rw::math::Transform3D<double> &to);


protected:

    ros::NodeHandle _nodehandle;
    caros::SerialDeviceSIProxy _sdsip;

    // used to check if it succeded finding the robot in workspace
    bool _connected;

    //
    bool _quitProgram;

    // used to store the path used when servoing
    rw::trajectory::QPath _servoPath;
    bool _pathUpdated;

    // used to hold the robot configurations and to tell if the conf. update thread has to stop
    rw::math::Q _configuration;
    

};

}
}
