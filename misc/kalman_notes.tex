\documentclass[]{article}
\usepackage{amsmath} %This is math
\usepackage{amssymb}
\usepackage{siunitx} %SI Units
\usepackage{textcomp,gensymb}
\usepackage{todonotes} %Todo notes: \todo[inline,author=Auth]{Todo note}
\usepackage{fullpage}
\DeclareMathSymbol{*}{\mathbin}{symbols}{"01}
%opening
\title{Kalman filter notes}
\author{Mikael Westermann}

\begin{document}

\maketitle

\section{Notation}
Boldface capitals denote matrices, and normal boldface denotes vectors.
Dot denotes time derivative.

\(\hat{\mathbf{x}}_{k|k-1}\) is our current estimate at iteration \(k\) of \(\mathbf{x}\)
based on information up to iteration \(k-1\).
\(\hat{\mathbf{x}}_{k|k}\) is our current estimate at iteration \(k\) of \(\mathbf{x}\)
based on all the information up to and including iteration \(k\) (ie. the up-to-date estimate).

\section{Linear dynamic system}
This section describes the system model in a way that is compatible with the Kalman filter.
\(\mathbf{x}\): State vector (Cartesian position, velocity and acceleration).
\begin{equation}
\mathbf{x}
=\begin{pmatrix}
x\\y\\z\\
\dot{x}\\\dot{y}\\\dot{z}\\
\ddot{x}\\\ddot{y}\\\ddot{z}
\end{pmatrix}
=\begin{pmatrix}
\\\textup{Position}\\\\
\\\textup{Velocity}\\\\
\\\textup{Acceleration}\\
\textup{}
\end{pmatrix}
\end{equation}
The state transition model explains how we go from \(\mathbf{x}_{k-1}\) to \(\mathbf{x}_{k}\),
where \(k-1\) is the previous iteration, and \(k\) is the current iteration.
The iterations are separated by sampling period \(\Delta t\).
With the Kalman filter, we assume that the state transition model follows equation \ref{eq:stateTransitionModel}, where \(\mathbf{F}_{k}\) is called the \textit{state transition matrix} and \(\mathbf{w}_{k}\sim \mathcal{N}(\mathbf{0},\mathbf{Q}_{k})\) is the \textit{process noise} (the noise of our state transition, \textit{NOT} the noise of our xyz measurements!).
\begin{align}
\label{eq:stateTransitionModel}
\textup{State transition model: }
\mathbf{x}_{k}
&=\mathbf{F}_{k}*\mathbf{x}_{k-1}
+\mathbf{B}_{k}*\mathbf{u}_{k}
+\mathbf{w}_{k}
\\
\label{eq:stateTransitionModelSimple}
\textup{Simple version: }
\mathbf{x}_{k}
&=\mathbf{F}*\mathbf{x}_{k-1}
+\mathbf{w}
\\
\label{eq:processNoise}
\textup{Process noise: }
\mathbf{w}_{k}
&\sim \mathcal{N}(\mathbf{0},\mathbf{Q}_{k})
\\
\label{eq:processNoiseSimple}
\textup{Simple version: }
\mathbf{w}
&\sim \mathcal{N}(\mathbf{0},\mathbf{Q})
\end{align}
Please do your best to ignore \(\mathbf{B}_{k}\) and \(\mathbf{u}_{k}\), which are the control input matrix and the control vector respectively. We don't use control input.
Actually, ignore equation \ref{eq:stateTransitionModel} and focus on equation \ref{eq:stateTransitionModelSimple} instead.
Take note that we assume, in our simplified state transition model
(eq. \ref{eq:stateTransitionModelSimple} and \ref{eq:processNoiseSimple}),
that the state transition matrix \(\mathbf{F}\) is always the same (\(\Delta t\)) is fixed,
and that the process noise always has the same covariance.
\todo[inline,author=Mikael]{Check the process noise constant covariance assumption. Does it make sense?}
The state transition matrix \(\mathbf{F}\) is given in equation \ref{eq:stateTransitionMatrix}.
\begin{equation}
\label{eq:stateTransitionMatrix}
\mathbf{F}
=\begin{pmatrix}
1&0&0&\Delta t&0&0&\frac{1}{2}\left(\Delta t\right)^{2}&0&0\\
0&1&0&0&\Delta t&0&0&\frac{1}{2}\left(\Delta t\right)^{2}&0\\
0&0&1&0&0&\Delta t&0&0&\frac{1}{2}\left(\Delta t\right)^{2}\\
0&0&0&1&0&0&\Delta t&0&0\\
0&0&0&0&1&0&0&\Delta t&0\\
0&0&0&0&0&1&0&0&\Delta t\\
0&0&0&0&0&0&1&0&0\\
0&0&0&0&0&0&0&1&0\\
0&0&0&0&0&0&0&0&1
\end{pmatrix}
\end{equation}
\todo[inline,author=Mikael]{Make sure we all agree that this is how we go from previous state to the current state. Euler method.}
Thus, the state transition from state \(\mathbf{x}_{k-1}\) to state \(\mathbf{x}_{k}\) is modeled as
equation \ref{eq:stateTransition} (and \ref{eq:stateTransitionRepeated}).
\begin{align}
\label{eq:stateTransitionRepeated}
\mathbf{x}_{k}
&=\mathbf{F}*\mathbf{x}_{k-1}
+\mathbf{w}
\\
\label{eq:stateTransition}
\begin{pmatrix}
x_{k}\\y_{k}\\z_{k}\\
\dot{x}_{k}\\\dot{y}_{k}\\\dot{z}_{k}\\
\ddot{x}_{k}\\\ddot{y}_{k}\\\ddot{z}_{k}
\end{pmatrix}
&=\begin{pmatrix}
1&0&0&\Delta t&0&0&\frac{1}{2}\left(\Delta t\right)^{2}&0&0\\
0&1&0&0&\Delta t&0&0&\frac{1}{2}\left(\Delta t\right)^{2}&0\\
0&0&1&0&0&\Delta t&0&0&\frac{1}{2}\left(\Delta t\right)^{2}\\
0&0&0&1&0&0&\Delta t&0&0\\
0&0&0&0&1&0&0&\Delta t&0\\
0&0&0&0&0&1&0&0&\Delta t\\
0&0&0&0&0&0&1&0&0\\
0&0&0&0&0&0&0&1&0\\
0&0&0&0&0&0&0&0&1
\end{pmatrix}
*
\begin{pmatrix}
x_{k-1}\\y_{k-1}\\z_{k-1}\\
\dot{x}_{k-1}\\\dot{y}_{k-1}\\\dot{z}_{k-1}\\
\ddot{x}_{k-1}\\\ddot{y}_{k-1}\\\ddot{z}_{k-1}
\end{pmatrix}
+\mathbf{w}
\end{align}

To the Kalman filter, a measurement \(\mathbf{z}=\left(x,y,z\right)^T\) is the ball position in cartesian coordinates.
The matrix \(\mathbf{H}\) relates the measurement to the state.
There are no advanced relations between measured position and the position represented in the state. The relation is one to one. However, the measured position does not relate to the state velocity nor acceleration.
This measurement model is shown in equations \ref{eq:measurementModel} and \ref{eq:measurementModelFull}.
The measurement noise \(\mathbf{v}_{k}\) is modeled by covariance \(\mathbf{R}_{k}\) as in equation
\ref{eq:measurementNoise}.
\begin{align}
\label{eq:measurementModel}
\mathbf{z}_{k}
&=\mathbf{H}*\mathbf{x}_{k}+\mathbf{v}_{k}
\\
\label{eq:measurementModelFull}
\begin{pmatrix}
x_{k}\\y_{k}\\z_{k}
\end{pmatrix}
&=\begin{pmatrix}
1&0&0&0&0&0&0&0&0\\
0&1&0&0&0&0&0&0&0\\
0&0&1&0&0&0&0&0&0
\end{pmatrix}
*
\begin{pmatrix}
x_{k}\\y_{k}\\z_{k}\\
\dot{x}_{k}\\\dot{y}_{k}\\\dot{z}_{k}\\
\ddot{x}_{k}\\\ddot{y}_{k}\\\ddot{z}_{k}
\end{pmatrix}
+\mathbf{v}_{k}
\\
\label{eq:measurementNoise}
\mathbf{v}_{k}
&\sim \mathcal{N}(\mathbf{0},\mathbf{R}_{k})
\end{align}
Note that this model is already a simplified version saying \(\mathbf{H}\) is iteration independent,
since the measurement always has the same form.
However, unlike the covariance \(\mathbf{Q}\) of the state transition model,
the covariance \(\mathbf{R}_{k}\) is still modeled as depending on the current iteration.
This is because the uncertainty propagated through the stereo vision algorithm
to the measurement \(\mathbf{z}_{k}\) varies with depth and thereby with time (\(k\)), since the ball is moving.

\section{Kalman filter}
\begin{figure}[ht]
\begin{center}
\def\svgwidth{0.8\textwidth}
\input{Basic_concept_of_Kalman_filtering.pdf_tex}%\includegraphics[width=0.8\textwidth]{Basic_concept_of_Kalman_filtering.pdf}
\caption{Kalman filter overview.}
\end{center}
\end{figure}
The Kalman filter uses the model and measurements to estimate the current
state \(\hat{\mathbf{x}}_{k|k}\) and its covariance \(\mathbf{P}_{k|k}\).
These are the outputs of the Kalman filter, and they give us position, velocity,
acceleration and uncertainty. This will be optimal if our system is indeed linear
and all the noise (and unmodeled physics) in our system behave as white noise.
Remember that we may have unmodeled human motion and stereo vision
algorithm characteristics in our system.

Implementation-wise, the Kalman filter incorporates information into the estimates
in two steps. It starts by using the state transition model of equation \ref{eq:stateTransition}
to \textit{predict} \(\hat{\mathbf{x}}_{k|k-1}\) (the current state based on previous information)
and its covariance \(\mathbf{P}_{k|k-1}\) using the state transition matrix \(\mathbf{F}\) (which depends only on \(\Delta t\)) and the process noise covariance \(\mathbf{Q}\) (global uncertainty).
The prediction step is shown in equations \ref{eq:predictx} and \ref{eq:predictP}.
\begin{align}
\label{eq:predictx}
\hat{\mathbf{x}}_{k|k-1}
&=\mathbf{F}*\hat{\mathbf{x}}_{k-1|k-1}
\\
\label{eq:predictP}
\mathbf{P}_{k|k-1}
&=\mathbf{F}*\mathbf{P}_{k-1|k-1}*\mathbf{F}^T+\mathbf{Q}
\end{align}
After the prediction step, the Kalman filter must \textit{update} the estimate from
\(\hat{\mathbf{x}}_{k|k-1}\) to \(\hat{\mathbf{x}}_{k|k}\) by incorporating
some information from iteration \(k\). This information is the position measurement
\(\mathbf{z}_{k}\) and its covariance (uncertainty) \(\mathbf{R}_k\).
The Kalman filter calculates a matrix \(\mathbf{K}_{k}\) called the optimal Kalman gain
at each iteration, which is used to weigh the new measurement.
The new measurement \(\mathbf{z}_{k}\) differs from the current state by some
cartesian space position residual
called \(\mathbf{y}_{k}=\left(\varepsilon_x,\varepsilon_y,\varepsilon_z\right)^T\).
Ideally, this residual is small, which means our system model used for prediction
is really good and
we have no measurement noise at all. This is not the case,
and we get a time-varying residual \(\mathbf{y}_{k}\) with covariance \(\mathbf{S}_{k}\).
If we have modelled our system correctly, the residual is white noise,
in which case the Kalman filter is optimal.
This first part of the \textit{update} step is called \textit{innovation},
which basically means incorporating the new measurement into our filter
by comparing it to the predicted state and converting it into a residual.
The \textit{innovation} is shown in equations \ref{eq:innovationy} and \ref{eq:innovationS}.
\begin{align}
\label{eq:innovationy}
\mathbf{y}_{k}
&=\mathbf{z}_{k}-\mathbf{H}*\hat{\mathbf{x}}_{k|k-1}
\\
\label{eq:innovationS}
\mathbf{S}_{k}
&=\mathbf{H}*\mathbf{P}_{k|k-1}*\mathbf{H}^T+\mathbf{R}_{k}
\end{align}
Note that the residual \(\mathbf{y}_{k}\) is a 3-element vector measured in measurement units,
and \(\mathbf{S}_{k}\) is a 3-by-3 covariance matrix.
In summary, the \textit{innovation} describes how the measurement differs from our prediction.

Finally, the optimal Kalman gain \(\mathbf{K}_{k}\) is calculated,
and we obtain the a posteriori estimates of \(\hat{\mathbf{x}}_{k|k}\) and
its covariance \(\mathbf{P}_{k|k}\) through equations \ref{eq:kalmangain},
\ref{eq:updatex} and \ref{eq:updateP}.
\begin{align}
\label{eq:kalmangain}
\mathbf{K}_{k}
&=\mathbf{P}_{k|k-1}*\mathbf{H}^T*\mathbf{S}_{k}^{-1}
\\
\label{eq:updatex}
\hat{\mathbf{x}}_{k|k}
&=\hat{\mathbf{x}}_{k|k-1}+\mathbf{K}_{k}*\mathbf{y}_{k}
\\
\label{eq:updateP}
\mathbf{P}_{k|k}
&=\left(\mathbf{I}-\mathbf{K}_{k}*\mathbf{H}_{k}\right)*\mathbf{P}_{k|k-1}
\end{align}

\subsection{Covariance matrices}
The covariance matrices \(\mathbf{Q}\) and \(\mathbf{R}_{k}\)
are very important to the Kalman filter performance.
They are treated separately in this section.
\subsubsection{Process noise covariance \(\mathbf{Q}\)}
The uncertainty of equation \ref{eq:stateTransition} is what \(\mathbf{Q}\) models.
So the question is: How (un)certain are we that the Euler integration
is the true way to predict the next state?
The answer lies in the (non)linearity of the system.
If the ball is moving in a straight line, we're pretty certain on the prediction.
If the ball has been thrown and moves affected by gravity and drag force,
we're still pretty certain on the prediction.
However, if a person is moving the ball around, naturally,
we become less certain on the linear prediction.
Actually, simply moving the ball in a circular pattern
makes the system dynamics nonlinear, and the prediction
loses accuracy (of course depending on the speed).

Thus, if we find that we are apparantly moving the ball
in a way that is too advanced for the filter to track it,
we can either move it more slowly,
increase the uncertainty \(\mathbf{Q}\),
or implement an \textit{Extended Kalman Filter} (EKF).
In the EKF, we don't use the state transition matrix \(\mathbf{F}\)
to model the state transition, but instead "\textit{some function}" \(f\),
and \textit{linearize} it in each iteration (using Jacobians).
\todo[inline,author=Mikael]{Explain this if we actually end up needing it...}


Anyway, here we go...
Remember the state transition model:
\begin{equation}
\mathbf{x}_{k}=\mathbf{F}*\mathbf{x}_{k-1}+\mathbf{w}
\end{equation}
Now, let's make an assumption about our discrete time noise model, so that
we can estimate the covariance analytically.
Remember that, by making this assumption, we are stating something about the
unmodeled dynamics of our system. These unmodeled dynamics are basically human motion.
I don't know about you guys, but I tend to move discretely with piecewise constant acceleration.
What? My acceleration is constant in each iteration, but completely uncorrelated with previous
accelerations. That's all you can say about my random moves.
So, the human motion noise comes down to a 3D vector of accelerations, \(\mathbf{a}\).
\begin{equation}
\mathbf{w}=\mathbf{W}*\mathbf{a}^T
\end{equation}
\begin{equation}
\mathbf{a}=\begin{pmatrix}
\ddot{\mathbf{x}}\\
\ddot{\mathbf{y}}\\
\ddot{\mathbf{z}}
\end{pmatrix}
\sim \mathcal{N}(\mathbf{0},\mathbf{\Sigma}_a)
\end{equation}
\(\mathbf{a}\) has a covariance matrix \(\mathbf{\Sigma}_a\).
\todo[inline,author=Mikael]{I DON'T KNOW THE COVARIANCE BETWEEN THE X-Y-Z ACCELERATIONS!}
The x-y-z accelerations are of course uncorrelated, so \(\mathbf{\Sigma}_a\) is defined by three
independent variances. Anyway, they would be uncorrelated if human motion was \textit{white noise}.
\begin{equation}
\mathbf{\Sigma}_a=\begin{pmatrix}
\sigma_{\ddot{x}}^2 & 0 & 0\\
0 & \sigma_{\ddot{y}}^2 & 0\\
0 & 0 & \sigma_{\ddot{z}}^2
\end{pmatrix}
\end{equation}
So if we put x-axis acceleration noise into our discrete (\(\Delta t\)) process,
the x position of the state
will be affected by \(\frac{{\Delta t}^2}{2}\). This relation between
9-dimensional state and (now) 3-dimensional acceleration noise is what is encoded in
\(\mathbf{W}\).
\begin{equation}
\mathbf{W}=
\begin{pmatrix}
\frac{{\Delta t}^2}{2} & 0 & 0\\ 
0 & \frac{{\Delta t}^2}{2} & 0\\ 
0 & 0 & \frac{{\Delta t}^2}{2}\\ 
{\Delta t} & 0 & 0\\ 
0 & {\Delta t} & 0\\ 
0 & 0 & {\Delta t}\\ 
1 & 0 & 0\\ 
0 & 1 & 0\\ 
0 & 0 & 1
\end{pmatrix}
\end{equation}
\todo[inline,author=Mikael]{We could try modifying the characteristics of \(\mathbf{a}\),
specifically \(\mathbf{\Sigma}_a\), so that we set the noise (\(\mathbf{\Sigma}_a\)) to near-zero when
the tracked acceleration is low. This incorporates human motion into our filter:
When the tracked (measured) acceleration of the ball is low, it's probably because it's being held still.
In this case, we should trust our predictions more than our measurements,
and trust the human to hold the ball still for some time.
When the acceleration rises again, we increase \(\mathbf{\Sigma}_a\) again,
because now the human is doing white noise motion that we know nothing about.}

Now we can finally use this to find \(\mathbf{Q}\):
Since the process noise \(\mathbf{w}\) has covariance \(\mathbf{Q}\),
\(\mathbf{W}*a^T\) must also have covariance \(\mathbf{Q}\),
leading to the simple equation \ref{eq:processNoiseCovariancePiecewise},
which leads to the solution in equation \ref{eq:processNoiseCovariancePiecewiseFull}.
\begin{equation}
\mathbf{w}\sim \mathcal{N}(\mathbf{0},\mathbf{Q})
\Rightarrow
\mathbf{Q}=\mathrm{Cov}\left(\mathbf{W}*a^T\right)=
\mathbb{E}\left(\mathbf{W}*\mathbf{a}^T*\mathbf{a}*\mathbf{W}^T\right)=
\mathbf{W}*\mathbf{\Sigma}_a*\mathbf{W}^T
\label{eq:processNoiseCovariancePiecewise}
\end{equation}

\begin{equation}
\mathbf{Q}=\begin{pmatrix}
\frac{\sigma_{\ddot{x}}^2 {\Delta t}^4}{4} & 0 & 0 & \frac{\sigma_{\ddot{x}}^2 {\Delta t}^3}{2} & 0 & 0 & \frac{\sigma_{\ddot{x}}^2 {\Delta t}^2}{2} & 0 & 0\\ 
0 & \frac{\sigma_{\ddot{y}}^2 {\Delta t}^4}{4} & 0 & 0 & \frac{\sigma_{\ddot{y}}^2 {\Delta t}^3}{2} & 0 & 0 & \frac{\sigma_{\ddot{y}}^2 {\Delta t}^2}{2} & 0\\ 
0 & 0 & \frac{\sigma_{\ddot{z}}^2 {\Delta t}^4}{4} & 0 & 0 & \frac{\sigma_{\ddot{z}}^2 {\Delta t}^3}{2} & 0 & 0 & \frac{\sigma_{\ddot{z}}^2 {\Delta t}^2}{2}\\ 
\frac{\sigma_{\ddot{x}}^2 {\Delta t}^3}{2} & 0 & 0 & \sigma_{\ddot{x}}^2 {\Delta t}^2 & 0 & 0 & \sigma_{\ddot{x}}^2 {\Delta t} & 0 & 0\\ 
0 & \frac{\sigma_{\ddot{y}}^2 {\Delta t}^3}{2} & 0 & 0 & \sigma_{\ddot{y}}^2 {\Delta t}^2 & 0 & 0 & \sigma_{\ddot{y}}^2 {\Delta t} & 0\\ 
0 & 0 & \frac{\sigma_{\ddot{z}}^2 {\Delta t}^3}{2} & 0 & 0 & \sigma_{\ddot{z}}^2 {\Delta t}^2 & 0 & 0 & \sigma_{\ddot{z}}^2 {\Delta t}\\ 
\frac{\sigma_{\ddot{x}}^2 {\Delta t}^2}{2} & 0 & 0 & \sigma_{\ddot{x}}^2 {\Delta t} & 0 & 0 & \sigma_{\ddot{x}}^2 & 0 & 0\\ 
0 & \frac{\sigma_{\ddot{y}}^2 {\Delta t}^2}{2} & 0 & 0 & \sigma_{\ddot{y}}^2 {\Delta t} & 0 & 0 & \sigma_{\ddot{y}}^2 & 0\\ 
0 & 0 & \frac{\sigma_{\ddot{z}}^2 {\Delta t}^2}{2} & 0 & 0 & \sigma_{\ddot{z}}^2 {\Delta t} & 0 & 0 & \sigma_{\ddot{z}}^2
\end{pmatrix}
\label{eq:processNoiseCovariancePiecewiseFull}
\end{equation}
%\begin{equation}
%\mathbf{Q}=\begin{pmatrix}
%\frac{{\Delta t}^4}{4} & 0 & 0 & \frac{{\Delta t}^3}{2} & 0 & 0 & \frac{{\Delta t}^2}{2} & 0 & 0\\ 
%0 & \frac{{\Delta t}^4}{4} & 0 & 0 & \frac{{\Delta t}^3}{2} & 0 & 0 & \frac{{\Delta t}^2}{2} & 0\\ 
%0 & 0 & \frac{{\Delta t}^4}{4} & 0 & 0 & \frac{{\Delta t}^3}{2} & 0 & 0 & \frac{{\Delta t}^2}{2}\\ 
%\frac{{\Delta t}^3}{2} & 0 & 0 & {\Delta t}^2 & 0 & 0 & {\Delta t} & 0 & 0\\ 
%0 & \frac{{\Delta t}^3}{2} & 0 & 0 & {\Delta t}^2 & 0 & 0 & {\Delta t} & 0\\ 
%0 & 0 & \frac{{\Delta t}^3}{2} & 0 & 0 & {\Delta t}^2 & 0 & 0 & {\Delta t}\\ 
%\frac{{\Delta t}^2}{2} & 0 & 0 & {\Delta t} & 0 & 0 & 1 & 0 & 0\\ 
%0 & \frac{{\Delta t}^2}{2} & 0 & 0 & {\Delta t} & 0 & 0 & 1 & 0\\ 
%0 & 0 & \frac{{\Delta t}^2}{2} & 0 & 0 & {\Delta t} & 0 & 0 & 1
%\end{pmatrix}
%\begin{pmatrix}
%\sigma_{\ddot{x}}^2\\
%\sigma_{\ddot{y}}^2\\
%\sigma_{\ddot{z}}^2\\
%\sigma_{\ddot{x}}^2\\
%\sigma_{\ddot{y}}^2\\
%\sigma_{\ddot{z}}^2\\
%\sigma_{\ddot{x}}^2\\
%\sigma_{\ddot{y}}^2\\
%\sigma_{\ddot{z}}^2
%\end{pmatrix}
%\label{eq:processNoiseCovariancePiecewiseFull}
%\end{equation}

What's that? We don't have \(\sigma_{\ddot{x}}\), \(\sigma_{\ddot{y}}\) nor \(\sigma_{\ddot{z}}\)?
Well, they describe the standard deviation of the change in acceleration
between each time step. Now, if you have just one number \(\Delta a\), which is the maximum
change in acceleration between each time step (in \(\frac{\textup{m}}{\textup{s}^2}\)),
maybe you should choose the standard deviations somewhere between \(\frac{\Delta a}{2}\) and \(\Delta a\).
So now this has been reduced to \textit{one number} describing our unmodeled process dynamics.
That's probably not very accurate! Well, it's an \textit{approximation}, and it's
better than completely randomized diagonal matrix or (as some people actually do)
only setting the acceleration-to-acceleration covariance terms of \(\mathbf{Q}\) to some small number.

\subsubsection{Measurement noise covariance \(\mathbf{R}_{k}\)}
The measurement noise covariance is the uncertainty on the cartesian position
measurements \(\mathbf{z}_{k}\).
These measurements are the output of a stereo vision algorithm
which takes stereo images as input.
Luckily, we pretty much know how to model the covariance of \(\mathbf{z}_{k}\),
because we know how to propagate the image pixel uncertainty
\(\mathbf{\Sigma}_{u,v,k}\) through
the stereo vision algorithm (if it is the LSO version).
This means \(\mathbf{R}_{k}\) of each iteration
can be found based on uncertainty propagation.

Thus, since \(\mathbf{R}_{k}\) depends on the pixel uncertainty \(\mathbf{\Sigma}_{u,v,k}\),
and a standard method exists for propagation,
we just need the model for \(\mathbf{\Sigma}_{u,v,k}\).
This depends on the ball detection uncertainty,
ie. how precise (in pixels) are we in detecting exactly the ball center
in each (mono) image?
If we say we are always eg. 2 pixels certain, then \(\mathbf{\Sigma}_{u,v,k}\) becomes
a constant \(\mathbf{\Sigma}_{u,v}\).
This might lead one to think that \(\mathbf{R}_{k}\) is also constant,
and that all covariance matrices in the Kalman filter are constants.
However, the uncertainty propagation from \(\mathbf{\Sigma}_{u,v,k}\)
to \(\mathbf{R}_{k}\) depends on \textit{two} sets of \(\left(u,v\right)\)
and the stereo triangulation,
thereby rendering \(\mathbf{R}_{k}\) depth dependent (and time-varying, if the ball is moving).
\todo[inline,author=Mikael]{Right?}

\section{Implementation}
This section is Mikael's proposal for an implementation.
We are using C++, so we make a class named Kalman (Lukas has already made an attempt at a signature),
and use the wonderful matrix library named Eigen (Eigen3), which is also used
internally in RobWork.
This section is meant as an \textit{overview} of the math going on inside
the Kalman class, and only goes into detail with the "interesting" stuff.

For some reason, we were discouraged from using the OpenCV built-in Kalman filter,
so if we're gonna do this, we're gonna do this right!
I'm talking optimizations and Cholesky decompositions,
as well as utility methods allowing us to modify the
filter parameters in runtime and do serious debugging.

\subsection{Class attributes (stored data members)}
\begin{itemize}
\item[\(\hat{\mathbf{x}}_{k|k-1}\)]{Predicted state vector.}
\item[\(\hat{\mathbf{x}}_{k|k}\)]
{Updated state vector. Used as \(\hat{\mathbf{x}}_{k-1|k-1}\) in the next iteration.}
\item[\(\mathbf{F}\)]{State transition matrix. Will be constant unless \(\Delta t\) is changed.}
\item[\(\Delta t\)]{Sampling period. Probably \(\frac{1}{20 \textup{Hz}}=50 \textup{ms}\).}
\item[\(\mathbf{Q}\)]
{Process noise covariance matrix.
Perhaps this should change with velocity or something like that.}
\item[\(\mathbf{P}_{k|k-1}\)]
{Predicted state estimate covariance matrix (for \(\hat{\mathbf{x}}_{k|k-1}\)).}
\item[\(\mathbf{P}_{k|k}\)]
{Updated state estimate covariance matrix (for \(\hat{\mathbf{x}}_{k|k}\)).
Used as \(\mathbf{P}_{k-1|k-1}\) in the next iteration.}
\item[\(\mathbf{H}\)]
{Measurement matrix. Constant, because our measurement model does not change.}
\item[\(\mathbf{S_{k}}\)]
{Innovation (measurement residual) covariance matrix.
It is not super necessary to store this, as it is computed anew in each update step.
However, if our filter diverges, we should study \(\mathbf{S_{k}}\) to see if it
does not stay positive semidefinite as assumed (for some reason I can not think of).}
\item[\(\mathbf{K}_{k}\)]{Kalman gain. It is not really an output of our filter, but it
is nice to store this for debugging purposes, and optionally, if we want to
set it manually.}
\item[\(\mathbf{H}*\mathbf{P}_{k|k-1}\)]
{Temporary matrix used three times in the \textit{update} step,
for calculating \(\mathbf{S}_{k}\), \(\mathbf{K}_{k}\) and \(\mathbf{P}_{k|k}\) respectively.}
\end{itemize}

\subsection{Class methods (functions...)}
\subsubsection{predict()}
A method which simply performs the prediction step,
with an optimized order of computations,
see\\\texttt{http://eigen.tuxfamily.org/dox/\\TopicWritingEfficientProductExpression.html}.
\begin{align}
\hat{\mathbf{x}}_{k|k-1}&=\mathbf{F}*\hat{\mathbf{x}_{k-1|k-1}}\\
\mathbf{P}_{k|k-1}&=\mathbf{F}*\mathbf{P}_{k-1|k-1}*\mathbf{F}^{T}+\mathbf{Q}
\end{align}
Returns a reference to \(\hat{\mathbf{x}}_{k|k-1}\).

Additionally, this method "updates" \(\hat{\mathbf{x}}_{k|k}\) to \(\hat{\mathbf{x}}_{k|k-1}\)
and \(\mathbf{P}_{k|k}\) to \(\mathbf{P}_{k|k-1}\),
just in case someone forgets to call the update() method during
one iteration, or in case we lose a measurement due to inadequate
ball detection. This way, we still have the best possible
information in our stored updated estimates which
will be used in the next prediction.

\subsubsection{update()}
A method which performs the update step, but in a more clever
way than the equations of the previous sections imply.
I thought about the matrix inversion needed for obtaining \(\mathbf{S}_{k}^{-1}\),
in the equations, and it doesn't make sense to do it that way numerically.
You may correct me if I'm wrong, but \(\mathbf{S}_{k}\) is positive semidefinite
because it is a covariance matrix (of the measurement residual).

So we start by computing \(\mathbf{S}_{k}\) according to equation \ref{eq:innovationS},
in the correct order of computations.
Then comes the part where we do \textit{something else} than matrix inversion.
We need to calculate \(\mathbf{K}_{k}\) for computing
\(\hat{\mathbf{x}}_{k|k}\) and \(\mathbf{P}_{k|k}\).
The formula for \(\mathbf{K}_{k}\) is in equation \ref{eq:kalmangain2}.
\begin{align}
\label{eq:kalmangain2}
\mathbf{K}_{k}
&=\mathbf{P}_{k|k-1}*\mathbf{H}^T*\mathbf{S}_{k}^{-1}
\\
\label{eq:kalmangain3}
\mathbf{K}_{k}*\mathbf{S}&=\mathbf{P}_{k|k-1}*\mathbf{H}^T
\\
\label{eq:kalmangain4}
\mathbf{S}^T*\mathbf{K}_{k}^T&=\mathbf{H}*\mathbf{P}_{k|k-1}^T
\\
\label{eq:kalmangain5}
\mathbf{S}*\mathbf{K}_{k}^T&=\mathbf{H}*\mathbf{P}_{k|k-1}
\end{align}
Muliplying by \(\mathbf{S}_{k}\) on both sides,
we obtain equation \ref{eq:kalmangain3},
and taking the transpose yields equation \ref{eq:kalmangain4}.
Since \(\mathbf{S}_{k}=\mathbf{S}_{k}^T\) and \(\mathbf{P}_{k|k-1}=\mathbf{P}_{k|k-1}^T\)
(because positive semidefinite matrices are symmetric),
we obtain equation \ref{eq:kalmangain5}.

We can solve equation \ref{eq:kalmangain5} for \(\mathbf{K}_{k}^T\)
through Cholesky decomposition of positive semidefinite matrix \(\mathbf{S}_{k}\).
Then we just take the transpose to get the Kalman gain.

\todo[inline,author=Mikael]
{This is where my speculations end, and I start writing code to evaluate
if I'm doing this wrong.}

\end{document}
