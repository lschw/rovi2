\documentclass[12pt,a4paper]{IEEEtran}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}

\usepackage{mathtools}  
\mathtoolsset{showonlyrefs}

\usepackage{bm}

\begin{document}

\title{The Short Intro to Uncertainty Propagation}
\author{Lukas Chr. M. W. Schwartz}
\maketitle

\begin{abstract}
This document quickly sums up the math behind uncertainty propagation.
The propagation is from 2D image uncertainty to 3D position uncertainty.
This completely based on material from the ROVI2 course (Multiple-view vision-based robot
calibration, K. R. Petersen and M. B.). 
\end{abstract}

\section{The Basic Intro for 3D Point Reconstruction}
In a multi-camera system, the 3D position of a point can be found from \eqref{eq:stereopsis_PM_m}.

\begin{equation}
\bm{P}_i \cdot \tilde{\bm{M}} = \mu \cdot \tilde{\bm{m}}_i
\label{eq:stereopsis_PM_m}
\end{equation}

Where $i$ is the index of the respective data gathered from a camera.
Remember that $\bm{P}$ is a $3 \times 4$, $\tilde{\bm{M}}$ a $4 \times 1$ and $\tilde{\bm{m}}$ a $3 \times 1$ matrix.
To solve for $\bm{M}$ elements are juggled around to get the $2 \times 3$ matrix $\bm{A}_i$ and $2 \times 1$ matrix $\bm{b}$ in equation \eqref{eq:stereopsis_AM_b}.

\begin{equation}
\bm{A}_i \cdot \bm{M} = \bm{b}_i
\label{eq:stereopsis_AM_b}
\end{equation}

To solve equation \ref{eq:stereopsis_AM_b}, the equations ($\bm{A}$ and $\bm{b}$'s) are stacked and solved to get the 3D coordinate.
The equation is then best solved using SVD to obtain numerical stability.

\section{Uncertainty Propagation}
To further arrive at an equation to map the 2D uncertainty to a 3D uncertainty, then equation \eqref{eq:stereopsis_AM_b} is solved for $M$.

\begin{gather}
\bm{A} \bm{M} = \bm{b}\\
\bm{A}^T \bm{A} \bm{M} = \bm{A}^T \bm{b}\\
\left( \bm{A}^T \bm{A} \right)^{-1} \bm{A}^T \bm{A} \bm{M} = \left( \bm{A}^T \bm{A} \right)^{-1} \bm{A}^T \bm{b}\\
\bm{M} = \left( \bm{A}^T \bm{A} \right)^{-1} \bm{A}^T \bm{b} \label{eq:2D_to_3D_mapping}
\end{gather}

Giving the 2D-to-3D transfer function in equation \eqref{eq:2D_to_3D_mapping}.

It is known that the uncertainty propagation can be found using equation \eqref{eq:unc_propagation}.

\begin{equation}
\bm{\Sigma}_M = \bm{J}_M \bm{\Sigma}_m \bm{J}_M^T
\label{eq:unc_propagation}
\end{equation}

Where $\bm{\Sigma}_M$ is the 3D point variance-covariance matrix, $\bm{\Sigma}_m = diag(\bm{\Sigma}_1 , . . . , \bm{\Sigma}_m )$ are the 2D point variance-covariance matrices and $J_M$ is the derivative of the 2D-3D transfer function in equation \eqref{eq:2D_to_3D_mapping}. By definition given by \eqref{eq:errorpropagation}.

\begin{equation}
\bm{J}_M = \left[ \frac{\partial \bm{M}}{\partial \bm{m}_1} \cdots \frac{\partial \bm{M}}{\partial \bm{m}_m} \right]
\label{eq:errorpropagation}
\end{equation}

which is a $3 \times 2m$ matrix.

\subsection{Finding $\frac{\partial M}{\partial m_i}$}

To find the $3 \times 2$ matrix $\frac{\partial \bm{M}}{\partial \bm{m}_i}$ given by 

\begin{equation}
\frac{\partial \bm{M}}{\partial \bm{m}_i} = \left[ \frac{\partial \bm{M}}{\partial u_i} , \frac{\partial \bm{M}}{\partial v_i} \right]
\end{equation}

then each of the parts can be solved individually.

\begin{gather}
\frac{\partial \bm{M}}{\partial u_i} = \frac{\partial}{\partial u_i} \left( \left( \bm{A}^T \bm{A} \right)^{-1} \bm{A}^T \bm{b} \right)\\
\frac{\partial \bm{M}}{\partial u_i} = \frac{\partial \left( \bm{A}^T \bm{A} \right)^{-1}}{\partial u_i} \bm{A}^T \bm{b} \\
+ \left( \bm{A}^T \bm{A} \right)^{-1} \left[ \frac{\partial \bm{A}}{\partial u_i} \right]^T \bm{b} \\
+ \left( \left( \bm{A}^T \bm{A} \right)^{-1} \bm{A}^T \frac{\partial \bm{b}}{\partial u_i} \right) \label{eq:dMdu}
\end{gather}

And similarly for $\frac{\partial \bm{M}}{\partial v_i}$.

Using a few clever tricks, then equation \eqref{eq:dATAdu} for $\frac{\partial \left( \bm{A}^T \bm{A} \right)^{-1}}{\partial u_i} $ in \eqref{eq:dMdu} can be obtained.

\begin{gather}
\frac{\partial \left( \bm{A}^T \bm{A} \right)^{-1}}{\partial u_i} = -\left( \bm{A}^T \bm{A} \right)^{-1} \\
 \cdot  \left( \frac{\partial \bm{A}^T}{\partial u_i} \bm{A} + \bm{A}^T \frac{\partial \bm{A}}{\partial u_i}  \right) \left( \bm{A}^T \bm{A} \right)^{-1} \label{eq:dATAdu}
\end{gather}

Since $\bm{A}$ and $\bm{b}$ are given, then the partial derivatives of such can easily be computed for $u$ and $v$.
These are given as follows, now with the general index omit earlier for simplicity.

\begin{gather}
\frac{\partial \bm{A}_i}{\partial u_i} = \left[
\begin{tabular}{c}
$- \bm{Q}_{i3}^T $\\ 
$\bm{0}$
\end{tabular}
\right]
\\
\frac{\partial \bm{A}_i}{\partial v_i} = \left[
\begin{tabular}{c}
$\bm{0}$\\
$- \bm{Q}_{i3}^T $
\end{tabular}
\right]
\\
\frac{\partial \bm{b}_i}{\partial u_i} = \left[
\begin{tabular}{c}
$ q_{i34} $\\ 
0
\end{tabular}
\right]
\\
\frac{\partial \bm{b}_i}{\partial v_i} = \left[
\begin{tabular}{c}
0\\
$ q_{i34} $
\end{tabular}
\right]
\end{gather}


\section{Summing up for Practical Implementation}
To get the resulting 3D uncertainty $\bm{\Sigma}_M$ then $\bm{\Sigma}_m$ is given as the argument to the function together with prior found data, $\bm{P}_i$.

Remember that $\bm{P}_i$ is given as

\begin{equation}
\bm{P}_i = \left[
\begin{tabular}{c c}
$\bm{Q}_{i1}^T$ & $q_{i14}$\\
$\bm{Q}_{i2}^T$ & $q_{i24}$\\
$\bm{Q}_{i3}^T$ & $q_{i34}$
\end{tabular}
\right]
\end{equation}

And $\bm{A}_i M = \bm{b}_i$ is given by equation \eqref{eq:A_i}.

\begin{equation}
\left[
\begin{tabular}{c}
$\bm{Q}_{i1}^T - u_i \bm{Q}_{i3}^T$\\
$\bm{Q}_{i2}^T - v_i \bm{Q}_{i3}^T$
\end{tabular}
\right]
\bm{M} =
\left[
\begin{tabular}{c}
$u_i q_{i34} - q_{i14}$\\
$v_i q_{i34} - q_{i24}$
\end{tabular}
\right]
\label{eq:A_i}
\end{equation}

In the stereo case, the 2D uncertainty is given as

\begin{equation}
\bm{\Sigma}_m = \left[
\begin{tabular}{c c}
$\bm{\Sigma}_1$ & $\bm{0}$ \\
$\bm{0}$ & $\bm{\Sigma}_2$
\end{tabular}
\right]
\end{equation}

where $\bm{\Sigma}_m$ is a $ 4 \times 4$ matrix.
Likewise then $\bm{J}_M$ is a $3 \times 4$ matrix

\begin{equation}
\bm{J}_M =  \left[ 
\frac{\partial \bm{M}}{\partial u_1}, 
\frac{\partial \bm{M}}{\partial v_1},
\frac{\partial \bm{M}}{\partial u_2}, 
\frac{\partial \bm{M}}{\partial v_2}
 \right]
\end{equation}

where 1 and 2 are correspond to each their own camera data (left and right camera).

Combining all the formulas from earlier then gives

\begin{gather}
\frac{\partial \bm{M}}{\partial u_i} =
- \bm{A}^{T2I} \bm{A}^{DS} \bm{A}^{T2I} \bm{A}^T \bm{b}\\
 + \bm{A}^{T2I} 
\left[
\begin{tabular}{c}
$- \bm{Q}_{i3}^T $\\ 
$\bm{0}$
\end{tabular}
\right]^T \bm{b} \\
 + \bm{A}^{T2I} \bm{A}^T
 \left[
\begin{tabular}{c}
$ q_{i34} $\\
0
\end{tabular}
\right]
\label{eq:dm_du}
\end{gather}

where 
\begin{gather}
\bm{A}^{T2I} = \left( \bm{A}^T \bm{A} \right)^{-1}\\
\bm{A}^{DS} = 
\left[
\begin{tabular}{c}
$- \bm{Q}_{i3}^T $\\ 
$\bm{0}$
\end{tabular}
\right]^T \bm{A} 
+ \bm{A}^T 
\left[
\begin{tabular}{c}
$- \bm{Q}_{i3}^T $\\ 
$\bm{0}$
\end{tabular}
\right]
\end{gather}

and likewise can $\frac{\partial \bm{M}_i}{\partial \bm{v}_i}$ be computed.
Note that since $\bm{A}$ and $\bm{b}$ are dependent on $(u, v)$ from the image algorithm, then the whole computation has to be executed for every sample.
For each $\bm{J}_M$ it is, however, possible to reuse $\bm{A}^{T2I}$ for both the partial derivative with respect to $u$ and $v$.


\section{The Numeric Solution}

To solve \eqref{eq:dm_du}, then remember that \eqref{eq:ATAx_ATb} analytically equals to \eqref{eq:x_ATAinv_ATb}.

\begin{gather}
\bm{A}^{T2} \bm{x} = \bm{A}^T b \label{eq:ATAx_ATb} \\
\bm{x} = \bm{A}^{T2I} \bm{A}^T \bm{b} \label{eq:x_ATAinv_ATb}
\end{gather}


It is hence possible to solve for a part of first term of \eqref{eq:dm_du} to get \eqref{eq:num_dM_du}.
Here it is important to solve the linear system of equations using SVD for numerical stability.
It is theoretically possible to simplify \eqref{eq:ATAx_ATb} such that the $A^T$ are not part of it, but keeping them in, allows to use the same SV-Decomposition for the whole system of equations.

\begin{equation}
\bm{A}^{T2} \frac{\partial\bm{M}}{\partial u_i} =
- \bm{A}^{DS} \bm{x} 
 + 
\left[
\begin{tabular}{c}
$- \bm{Q}_{i3}^T $\\ 
$\bm{0}$
\end{tabular}
\right]^T \bm{b} 
 +\bm{A}^T
 \left[
\begin{tabular}{c}
$ q_{i34} $\\
0
\end{tabular}
\right]
\label{eq:num_dM_du}
\end{equation}

Where $ \bm{A}^{T2} = \bm{A}^T \bm{A}$.

Again the same SVD calculation is then used to solve \eqref{eq:num_dM_du} for the column vector $\frac{\partial\bm{M}}{\partial u_i}$.
The same then goes for the remaining components of the Jacobian.


\end{document}