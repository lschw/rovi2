\subsection{Results}
The Robot-Camera Calibration was evaluated using errors measured in the camera frame.
The grasp planner was tested in simulation.
It was investigated how fast the planner
can find a configuration that is not in collision with the scene.
The path planner was tested in how fast the offline part
is at building a roadmap, with and without loading from file.
The online part of the path planner is tested in how fast a viable path is found.
The smoothness of the generated path is then tested
by comparing the generated path with a optimized path using path pruning.
The entire system is tested by moving
the robot to a grasping position and detecting if the object gets picked up.

\subsubsection{Robot-Camera Calibration}
The errors are measured by comparing the \(\bm{T}_{\mathrm{Marker}_i}^{\mathrm{Camera}}\)
transformation found using PCL to the equivalent transformation
\(\bm{T}_{\mathrm{Marker}}^{\mathrm{Tool}}
*\bm{T}_{\mathrm{Tool}_i}^{\mathrm{Base}}
*\bm{T}_{\mathrm{Base}}^{\mathrm{Camera}}
\),
based on known marker dimensions embedded in \(\bm{T}_{\mathrm{Marker}}^{\mathrm{Tool}}\).
Translational Root Mean Square Error (RMSE)
is measured by taking the Euclidean norm of the differences
between the translational components of all the transformation pairs.
Rotational Mean Absolute Error (MAE) is measured by
taking the average of the lengths of the differences
between the EAA representations of the rotational components of the transformation pairs.

Figure \ref{fig:robot-camera-calibration-error-vs-train-size96}
shows the translational and rotational errors
vs. number of image pairs based on 10 repeats of 10-fold cross validation
using a total of 96 image pairs and a 5 \by 7 checkerboard.
It is observed that the test set error becomes equal to or lower than the
training set error when the number of image pairs is above around 40.

Figure \ref{fig:robot-camera-calibration-error-vs-train-size54}
shows the same measures for a different dataset consisting
of 54 image pairs and using a 7 \by 10 checkerboard.
A convergence of test set error to training set error is seen
near 40 image pairs, as in figure \ref{fig:robot-camera-calibration-error-vs-train-size96}.

\begin{figure}[h]
\centering
\includegraphics[width=\figscale]{graphics/robot-camera-calibration-error-vs-train-size96}
\caption[Robot-Camera calibration error on a dataset with 96 image pairs.]{Robot-Camera calibration error on a dataset with 96 image pairs using a 5 \by 7 checkerboard.}
\label{fig:robot-camera-calibration-error-vs-train-size96}
\end{figure}

\begin{figure}[h]
\centering
\includegraphics[width=\figscale]{graphics/robot-camera-calibration-error-vs-train-size54}
\caption[Robot-Camera calibration error on a dataset with 54 image pairs.]{Robot-Camera calibration error on a dataset with 54 image pairs using a 7 \by 10 checkerboard.}
\label{fig:robot-camera-calibration-error-vs-train-size54}
\end{figure}

The \(\bm{T}_{\mathrm{Marker}_i}^{\mathrm{Camera}}\) poses were
evaluated qualitatively
by visualizing the transformed marker points in the marker frame.
The dataset with 54 image pairs was used for the visualization
in figure \ref{fig:robot-camera-calibration-stereopoints-markerframe54}.
It is observed that the measured marker points are approximately aligned
with the theoretical marker points after transformation.

\begin{figure}[h]
\centering
\includegraphics[width=\figscale]{graphics/robot-camera-calibration-stereopoints-markerframe54}
\caption[Matched marker points transformed from the camera frame to the marker frame.]{Matched marker points transformed from the camera frame to the marker frame
using the dataset with 54 image pairs and a 7 \by 10 checkerboard.
Three marker corners are marked for each transformation to show that the
transformation does not have an invalid rotation.}
\label{fig:robot-camera-calibration-stereopoints-markerframe54}
\end{figure}
\clearpage
\subsubsection{Grasp Planner}
The grasp planner was tested to find the optimal settings in the current use case.
This was done by making a circular path tracing the top of the table.
This kind of path was chosen since this
is expected to be the most difficult grasp poses to calculate.
The circular path was sampled at twenty locations.
The velocity of the ball was set to zero,
such that it is forced to grasp approaching from the base of the robot.

The final results are seen in figure \ref{fig:grasp_planner_collcheck}.
At each entry, the number of adjustments (\textit{Tries}) and the total time to find the configuration were recorded.
The standard deviation is shown on the figure.

\begin{figure}[h]
\centering
\includegraphics[width=\figscale]{graphics/graspplanner_collcheck}
\caption[Timing and tries of the grasp planner for circular path on the table.]{Timing and tries of the grasp planner for circular path on the table where initial configuration is in collision, with standard deviations.}
\label{fig:grasp_planner_collcheck}
\end{figure}

The angles for less than 15 degrees in figure \ref{fig:grasp_planner_collcheck}
are not shown, to focus on the region of interest.
The excluded data points are (10 deg, 2.20 s / 9956 tries) and (12.5 deg, 0.23 s / 979 tries)
respectively.
The grasping is measured to find a viable configuration to grasp the object at $0.011$ seconds by letting the orientation of the wrist wary by 20 degrees.

\subsubsection{Time of Building the Roadmap}
\label{sec:result_path_planning_timing_build}
In order to use a path planner reliably, the timing of each stage is important.
To test the time it takes to build the roadmap,
a roadmap is built 5 times with the same number of nodes.
It is then loaded from file 5 times, to measure the loading time.
In figure \ref{fig:prm_offline_build_time}, the build time of the roadmap is shown.
The standard deviation is very high,
and it can be seen that the build time is approximately linearly dependent
on the number of nodes.
It can also be seen that it
becomes quite time consuming to build the roadmap,
with a mean of $134.286$ seconds to build a roadmap of $10 000$ nodes.

\begin{figure}[h]
    \centering
\begin{subfigure}[t]{0.45\linewidth}
    \includegraphics[width=\linewidth]{graphics/prm_offline_build_time}
    \caption{Generating PRM.}
    \label{fig:prm_offline_build_time}
\end{subfigure}
\qquad
\begin{subfigure}[t]{0.45\linewidth}
    \includegraphics[width=\linewidth]{graphics/prm_loading_build_time}
    \caption{Loading from disk.}
    \label{fig:prm_loading_build_time}
\end{subfigure}
\caption[Offline build time for the PRM.]{Offline build time for the PRM.}
\end{figure}

Since the roadmap needs to be built every time the application starts,
this can be quite taxing to calculate.
As the scene is static, there is no reason to do this,
so the roadmap is written to and then read from a file the next time the program starts.
In figure \ref{fig:prm_loading_build_time} the loading time when loading from disk is shown.
This reduces the build time to $0.262$ seconds for $10 000$ nodes.

\subsubsection{Time of Path Planning}
\label{sec:result_path_planning_timing_plan}
The time it takes to calculate a path between two points
is measured and shown in figure \ref{fig:prm_online_plan_time}.
$5$ separate positions were chosen and a path between each pair
was generated, resulting in $10$ different paths.
The running time decreases as more nodes are introduced to the roadmap.

\begin{figure}[h]
    \centering
%     \includegraphics[width=\figscale]{graphics/prm_online_plan_time}
    \includegraphics[width=\figscale]{graphics/prm_plan_time}
    \caption[Online path planning times for the PRM.]{Online path planning times for the PRM.}
    \label{fig:prm_online_plan_time}
\end{figure}

\subsubsection{Smoothness of Path Planning}
\label{sec:result_path_planning_smooth}
In order to use the PRIME path planning,
the rod must be moved to be in the position between the rod and the camera.
The camera is seen as static,
so the rod is positioned at the camera,
pointing to the object by calculating the orientation. 
The workcell does not have a lot of objects it
can collide with within the workable frame.
This means the biggest challenge in the workcell is when the robot is
moving to the other side of the ball to camera constraint.
An example of a path planned around the movable constraint object
is shown in figure \ref{fig:path_shown}.

\begin{figure}[h]
    \centering
    \includegraphics[width=\figscale]{graphics/show_the_path}
    \caption[A path planned around the movable object.]{A path planned around the movable object.}
    \label{fig:path_shown}
\end{figure}

The path is found using an A* search in the roadmap and thus the path
would be optimal according to the nodes in the roadmap.
Therefore further path optimization has little to no effect.
Testing this hypothesis, a series of paths were generated,
and the sum of distances to all points in the path generated by the PRIME
algorithm and the output from the path pruning optimization was compared.
The total distance was zero at all the tested paths.
