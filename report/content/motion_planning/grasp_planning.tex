\subsection{Grasp Planning}
In order to grasp the moving object,
a suitable pose from which to grasp the object must be found.
In this project, the object is a ball,
so every orientation has an equal chance of grasping the object.
Since the ball is moving, 
it is convenient to grasp the ball from the opposite direction of which it is approaching.
Given the velocity and the position of the ball at any instance in time,
a grasp can then be calculated for which the gripper is opposing the ball.

When calculating the grasp, however,
it is possible that the specific grasp calculated places the robot in collision.
This can be the case when grasping close to objects in the scene
such as when the ball lies on the table as shown in figure
\ref{fig:graspplanner_nonrandom}.
In such instances the direct grasp calculated may be in collision,
but with a small rotation it may become possible to grasp the ball
without collisions, as in figure \ref{fig:graspplanner_random}.
In the case of the ball on the table,
this would be by tilting the gripper to be grasping
from slightly above the table.
To accommodate for such cases,
it was decided to apply small random rotations to the gripper
when the initial grasp opposing the ball motion direction was in collision.

\begin{figure}[h]
    \centering
    \begin{subfigure}[t]{\figscale / 2}
        \includegraphics[width=\linewidth]{graphics/graspplanner_nonrandom}
        \caption{Grasp found directly from position.}
        \label{fig:graspplanner_nonrandom}
    \end{subfigure}
    \qquad
    \begin{subfigure}[t]{\figscale / 2}
        \includegraphics[width=\linewidth]{graphics/graspplanner_random}
        \caption{Grasp found when adding random rotations.}
        \label{fig:graspplanner_random}
    \end{subfigure}
    \caption[Example of proposed grasps of the ball on a table.]{Example of proposed grasps of the ball on a table. The red highlight on the tool indicates collision.}
    \label{fig:graspplanner}
\end{figure}

\subsubsection{Implementation}
The velocity of the object was taken in the world frame.
From here it is converted into pitch and yaw of the gripper
that would result in the gripper pointing towards the approaching object.
Here the pitch controls the angle around the horizontal axis
(how much it is tilted up- and down-wards) and yaw the rotation around the vertical axis.
This implementation does,
however, always keep the gripper vertical, since no roll is applied per default.
The only roll that it is exposed to is therefore that of the random rotation.

The generation of the random rotation was done using an EAA representation.
A random 3D vector was generated around which to rotate and a random angle
within a specified bound was generated.
The new grasping position was
then calculated by multiplying the initial rotation by the small random rotation.
The final transformation was then converted into a
configuration vector using the \textit{ClosedFormIKSolverUR} of RobWork \citep{robwork}.
This resulted in a set of configurations that were checked for collisions.
If this produced a set of valid configurations,
the resulting configurations were checked for
ambiguities due to the joints being able to rotate
more than a full rotation about themselves.
The nearest configuration in joint space,
using euclidean metric, was then found and returned.
The process was repeated a set number of times
before the grasp planner would generate an error if no grasp was found.
When trying to find a valid configuration, 
a set of different transformations are tried
in parallel using the OpenMP \cite{openmp} multithreading API.

When the ball is stationary or moving at slow speeds,
the generated pose might vary a lot due to the fluctuations
in the perceived velocity direction.
In those cases it was chosen to default to a base grasp.
The base grasp was calculated such that the robot
grasp approaches from the direction of the base of the robot
with the gripper placed horizontally.
This was chosen as the robot base frame,
as the robot then would avoid having to rotate its
arm in towards itself giving it a shorter reach.
