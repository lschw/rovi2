\subsection{Robot-Camera Calibration}
Robot-Camera calibration, also known as Registration or Eye-Hand calibration,
is a class of methods for obtaining a homogeneous transformation
\(\bm{T}_{Camera}^{Base}\)
between a robot and a camera.
In this project, a transformation is sought between
the camera frame and the robot base frame so that poses tracked by the camera
can be expressed in the robot frame.
This allows the robot to move eg. the end effector to positions
seen by the camera, which is useful when grasping objects and planning
trajectories.

Three Robot-Camera calibration methods were considered.
Pros and cons of these are discussed briefly in the following three sections.
The third method was chosen for its advantages,
and it is explained in greater detail than the two other methods.
All three methods have in common, that the robot joint configuration is known
(implying known tool pose),
a marker is mounted on the tool,
and the marker pose can be detected by the camera.
Following these three sections, a description of the detection of the marker pose
in the camera frame is given.
Lastly, a short description of the implementation
is given.

\subsubsection{Robot-Camera Calibration with known \(\mathbf{T}_{\mathrm{Tool}}^{\mathrm{Marker}}\)}
When \(\bm{T}_{\mathrm{Tool}}^{\mathrm{Marker}}\) is known,
that is, when the marker has been placed in a known orientation and position
in the end effector frame, the Robot-Camera calibration is the easiest.
Here, the marker pose is immediately known in end effector frame (Hand),
and can be detected in the camera frame (Eye).
The desired transform can be computed for any image pair with a detected
marker pose using equation \eqref{eq:robcam_known_endTmarker},
in which the transformations on the right-hand side would be known.
\begin{equation}
\bm{T}_{\mathrm{Camera}}^{\mathrm{Base}}
=
\bm{T}_{\mathrm{Camera}}^{\mathrm{Marker}}
*
\bm{T}_{\mathrm{Marker}}^{\mathrm{Tool}}
*
\bm{T}_{\mathrm{Tool}}^{\mathrm{Base}}
\label{eq:robcam_known_endTmarker}
\end{equation}
Measurements from multiple image pairs may be combined
eg. by averaging over multiple transformations.
This can be done by taking the element-wise average of the translational
components, and averaging (and normalizing) the rotational component in a quaternion
or Equivalent-Angle-Axis (EAA) representation.

The advantage of this method is its simplicity in the calculation
of \(\bm{T}_{\mathrm{Camera}}^{\mathrm{Base}}\).
The disadvantage lies in the requirement that \(\bm{T}_{\mathrm{Tool}}^{\mathrm{Marker}}\)
is known. This means the marker position and orientation has to be measured,
which takes time and may reduce flexibility.
The error of this measurement is not easily computed
after Robot-Camera calibration, and is simply assumed to be zero,
which is not feasible for evaluating this method.

\subsubsection{Robot-Camera Calibration with unknown \(\mathbf{T}_{\mathrm{Tool}}^{\mathrm{Marker}}\)}
When \(\bm{T}_{\mathrm{Tool}}^{\mathrm{Marker}}\) is unknown,
one class of methods for Robot-Camera Calibration computes
\(\bm{T}_{\mathrm{Camera}}^{\mathrm{Base}}\) by canceling
out \(\bm{T}_{\mathrm{Tool}}^{\mathrm{Marker}}\) through
\textit{relative transformations}.
The underlying algorithm solves the problem known as
\(\bm{A}*\bm{X}=\bm{X}*\bm{B}\).
As suggested in \citep{shiu},
\textit{relative transformations}
can be used to write the Robot-Camera Calibration problem
as a linear system of equations of the form \(\bm{A}*\bm{X}=\bm{X}*\bm{B}\).

To formulate the \(\bm{A}*\bm{X}=\bm{X}*\bm{B}\) problem
for the relative motion between frames \(i\) and \(j\),
equation \eqref{eq:robcam_axxb1} is rewritten to equation \eqref{eq:robcam_axxb2}.
\begin{align}
\label{eq:robcam_axxb1}
\bm{T}_{\mathrm{Tool}_{i}}^{\mathrm{Base}}
*
\bm{T}_{\mathrm{Base}}^{\mathrm{Camera}}
*
\bm{T}_{\mathrm{Camera}_{i}}^{\mathrm{Marker}}
&=
\bm{T}_{\mathrm{Tool}_{j}}^{\mathrm{Base}}
*
\bm{T}_{\mathrm{Base}}^{\mathrm{Camera}}
*
\bm{T}_{\mathrm{Camera}_{j}}^{\mathrm{Marker}}
\\
\label{eq:robcam_axxb2}
\bm{T}_{\mathrm{Base}_{j}}^{\mathrm{Tool}}
*
\bm{T}_{\mathrm{Tool}_{i}}^{\mathrm{Base}}
*
\bm{T}_{\mathrm{Base}}^{\mathrm{Camera}}
&=
\bm{T}_{\mathrm{Base}}^{\mathrm{Camera}}
*
\bm{T}_{\mathrm{Camera}_{j}}^{\mathrm{Marker}}
*
\bm{T}_{\mathrm{Marker}_{i}}^{\mathrm{Camera}}
\end{align}
Defining
\(\bm{A}_{ij}=\bm{T}_{\mathrm{Base}_{j}}^{\mathrm{Tool}}
*
\bm{T}_{\mathrm{Tool}_{i}}^{\mathrm{Base}}\),
\(\bm{B}_{ij}=\bm{T}_{\mathrm{Camera}_{j}}^{\mathrm{Marker}}
*
\bm{T}_{\mathrm{Marker}_{i}}^{\mathrm{Camera}}\)
and \(\bm{X}=\bm{T}_{\mathrm{Base}}^{\mathrm{Camera}}\),
equation \eqref{eq:robcam_axxb2}
can be written as equation \eqref{eq:robcam_axxb3},
which is a linear system of equations.
\begin{equation}
\bm{A}_{ij}*\bm{X}=\bm{X}*\bm{B}_{ij}
\label{eq:robcam_axxb3}
\end{equation}
This equation can be stacked for any transformation between the frames of
stereo image pair \(i\) and stereo image pair \(j\),
and then solved for a common \(\bm{X}\).
Closed-form solution methods may either solve for the rotational component first,
and then deriving the translational component afterwards (separable solutions),
or solving both components simulateneously (simultaneous solutions).
Iterative methods also exist, which require an accurate initial estimate,
such as that from a closed-form method.
All three kinds of methods are discussed in \citep{shah_overview},
where it is concluded that separable closed-form solutions
perform better than simultaneous closed-form solutions in general,
while iterative solutions generally perform better than closed-form solutions.

The advantage of the method is that \(\bm{T}_{\mathrm{Tool}}^{\mathrm{Marker}}\)
is not part of the equations, and hence does not need to be measured in any way.
However, this is also the disadvantage of the method:
In order to evaluate the method in every \(i\)'th image pair,
\(\bm{T}_{\mathrm{Tool}}^{\mathrm{Marker}}\)
needs to be found for every image pair using
the solved \(\bm{T}_{\mathrm{Base}}^{\mathrm{Camera}}\),
\(\bm{T}_{\mathrm{Tool}_{i}}^{\mathrm{Base}}\)
and
\(\bm{T}_{\mathrm{Camera}_{i}}^{\mathrm{Marker}}\),
so that the error of multiplying the marker frame
through the robot kinematics and the Robot-Camera Calibration result
can actually be measured.
This will result in several different \(\bm{T}_{\mathrm{Tool}_{i}}^{\mathrm{Marker}}\),
which is problematic, as this is not a variable.
Of course, the \(\bm{A}*\bm{X}=\bm{X}*\bm{B}\)
solution method could be applied to solve for
\(\bm{T}_{\mathrm{Tool}}^{\mathrm{Marker}}\)
separately, yielding a solution which accepts variable
\(\bm{T}_{\mathrm{Base}}^{\mathrm{Camera}}\),
which is also expected to end in suboptimal results,
as this is not variable either.

\subsubsection{Robot-Camera and Tool-Marker Calibration with unknown \(\mathbf{T}_{\mathrm{Tool}}^{\mathrm{Marker}}\)}
\label{sec:axyb}
Another class of methods for Robot-Camera calibration with
unknown \(\bm{T}_{\mathrm{Tool}}^{\mathrm{Marker}}\)
actually estimates \(\bm{T}_{\mathrm{Tool}}^{\mathrm{Marker}}\)
in addition to the Robot-Camera calibration,
as part of solving the problem known as
\(\bm{A}*\bm{X}=\bm{Y}*\bm{B}\).
Defining
\(\bm{A}=\bm{T}_{\mathrm{Tool}_{i}}^{Base}\),
\(\bm{X}=\bm{T}_{\mathrm{Base}}^{Camera}\),
\(\bm{Y}=\bm{T}_{\mathrm{Tool}}^{Marker}\)
and
\(\bm{B}=\bm{T}_{\mathrm{Marker}_{i}}^{Camera}\),
the Robot-Camera Calibration problem simultaneously defined
with the Tool-Marker Calibration problem in equation \eqref{eq:robcam_axyb1}
can be written as equation \eqref{eq:robcam_axyb2}.
\begin{align}
\label{eq:robcam_axyb1}
\bm{T}_{\mathrm{Tool}_{i}}^{Base}
*
\bm{T}_{\mathrm{Base}}^{Camera}
&=
\bm{T}_{\mathrm{Tool}}^{Marker}
*
\bm{T}_{\mathrm{Marker}_{i}}^{Camera}
\\
\label{eq:robcam_axyb2}
\bm{A}*\bm{X}
&=
\bm{Y}*\bm{B}
\end{align}
This problem is extended to include all image pairs by
stacking \(\bm{A}_{i}\)'s and \(\bm{B}_{i}\)'s.

Closed-form and iterative solutions to the \(\bm{A}*\bm{X}=\bm{Y}*\bm{B}\)
problem are discussed in \citep{shah_overview},
and the same conclusions are made as for the \(\bm{A}*\bm{X}=\bm{X}*\bm{B}\)
problem.

This method has several advantages.
First, the method allows for an arbitrarily mounted marker,
and involves finding the transformation between the marker
and the tool as part of the solution.
Second, the method contains both \(\bm{T}_{\mathrm{Base}}^{Camera}\)
\textit{and} \(\bm{T}_{\mathrm{Tool}}^{Marker}\)
in the solution. Although \(\bm{T}_{\mathrm{Tool}}^{Marker}\)
may not be sought, imposing it as a fixed constraint
means the resulting \(\bm{T}_{\mathrm{Base}}^{Camera}\)
is based on more correct information than the output of
the \(\bm{A}*\bm{X}=\bm{X}*\bm{B}\) method.
Furthermore, an error metric can easily be defined
in any of the frames along the kinematic chain from marker to camera,
as will be discussed below.

Follwing the publication of \citep{shah_overview},
a method was developed in \citep{shah_axyb},
which solves the \(\bm{A}*\bm{X}=\bm{Y}*\bm{B}\)
problem using the Kronecker product in a closed-form separable solution.
An overview of this method, different from that of \citep{shah_axyb},
is given below, focusing on the simple to understand concepts
and with an intuitive implementation in mind.

The rotational component of \(\bm{A}*\bm{X}=\bm{Y}*\bm{B}\),
written in equation \eqref{eq:robcam_axyb3} can be rewritten to
equation \eqref{eq:robcam_axyb4}.
\begin{equation}
\label{eq:robcam_axyb3}
\bm{R}_{\bm{A}_i}*\bm{R}_{\bm{X}}=\bm{R}_{\bm{Y}}*\bm{R}_{\bm{B}_i}
\end{equation}
\begin{equation}
\label{eq:robcam_axyb4}
\bm{R}_{\bm{A}_i}*\bm{R}_{\bm{X}}*\bm{R}_{\bm{B}_i}^{T}*\bm{R}_{\bm{Y}}^{T}=\bm{I}
\end{equation}
Through properties of the Kronecker product not explained here,
equation \eqref{eq:robcam_axyb4}
is equivalent to equation \eqref{eq:robcam_axyb5}.
\begin{equation}
\label{eq:robcam_axyb5}
\begin{pmatrix}
-\bm{I} & \bm{R}_{\bm{B}_i}\otimes\bm{R}_{\bm{A}_i}
\end{pmatrix}
*
\begin{pmatrix}
\vect\left(\bm{R}_{\bm{Y}}\right)\\
\vect\left(\bm{R}_{\bm{X}}\right)\\
\end{pmatrix}
=\bm{0}
\end{equation}
This linear system needs to be stacked for \(n\geq 3\) frame pairs
with nonparallel axes of rotation \citep{shah_axyb},
as in equation \eqref{eq:robcam_axyb6}.
\begin{align}
\label{eq:robcam_axyb6}
\begin{pmatrix}
-\bm{I} & \bm{R}_{\bm{B}_1}\otimes\bm{R}_{\bm{A}_1}\\
-\bm{I} & \bm{R}_{\bm{B}_2}\otimes\bm{R}_{\bm{A}_2}\\
\vdots & \vdots\\
-\bm{I} & \bm{R}_{\bm{B}_n}\otimes\bm{R}_{\bm{A}_n}\\
\end{pmatrix}
*
\begin{pmatrix}
\vect\left(\bm{R}_{\bm{Y}}\right)\\
\vect\left(\bm{R}_{\bm{X}}\right)\\
\end{pmatrix}
=\bm{0}, & & n\geq 3
\end{align}
The solution method of \citep{shah_axyb} utilizes the fact that
the solutions \(\vect\left(\bm{R}_{\bm{X}}\right)\)
and \(\vect\left(\bm{R}_{\bm{Y}}\right)\)
are proportional to the vectors \(\bm{u}_{\bm{Y}}\) and \(\bm{v}_{\bm{X}}\),
which are the left and right singular vectors corresponding to the largest
singular value of matrix \(\bm{K}\) defined in equation \eqref{eq:robcam_axyb7}.
\begin{equation}
\label{eq:robcam_axyb7}
\bm{K}=\sum_{i=1}^{n}\bm{R}_{\bm{B}_i}\otimes\bm{R}_{\bm{A}_i}
\end{equation}
Thus, in practice, if \(\bm{K}\) is decomposed through SVD to \(\bm{K}=\bm{U}*\bm{S}*\bm{V}^{T}\),
with conventionally ordered diagonal elements of \(\bm{S}\),
then \(\bm{u}_{\bm{Y}}\) is the first column of \(\bm{U}\)
and \(\bm{v}_{\bm{X}}\) is the first column of \(\bm{V}\).
Defining \(\bm{V}_{\bm{X}}=\vect{}^{-1}\left(\bm{v}_{\bm{X}}\right)\)
and \(\bm{V}_{\bm{Y}}=\vect{}^{-1}\left(\bm{u}_{\bm{Y}}\right)\),
\(\bm{R}_{\bm{X}}\) and \(\bm{R}_{\bm{Y}}\) can be found using equations
\eqref{eq:robcam_axyb8} and \eqref{eq:robcam_axyb9}.
\begin{align}
\label{eq:robcam_axyb8}
\bm{R}_{\bm{X}}&=\sign\left(\det\left(\bm{V}_{\bm{X}}\right)\right)^{-1/3}*\bm{V}_{\bm{X}}\\
\label{eq:robcam_axyb9}
\bm{R}_{\bm{Y}}&=\sign\left(\det\left(\bm{V}_{\bm{Y}}\right)\right)^{-1/3}*\bm{V}_{\bm{Y}}
\end{align}

Once the rotation matrix \(\bm{R}_{\bm{Y}}\) is known,
the translational components \(\bm{t_X}\) and \(\bm{t_Y}\)
of \(\bm{X}\) and \(\bm{Y}\) respectively, can be found by rewriting
equation \eqref{eq:robcam_axyb10} to equation \eqref{eq:robcam_axyb11}.
\begin{align}
\label{eq:robcam_axyb10}
\bm{R}_{\bm{A}_i}*\bm{t_X}+\bm{t}_{\bm{A}_i}&=\bm{R_Y}*\bm{t_Y}
\\
\label{eq:robcam_axyb11}
\begin{pmatrix}
\bm{I} & -\bm{R}_{\bm{A}_i}
\end{pmatrix}
*
\begin{pmatrix}
\bm{t_Y}\\
\bm{t_X}
\end{pmatrix}
 & =\bm{t}_{\bm{A}_i}-\bm{R_Y}*\bm{t}_{\bm{B}_i}
\end{align}
Equation \eqref{eq:robcam_axyb11} can be stacked and solved for directly using SVD,
using the formulation in equation \eqref{eq:robcam_axyb12}.
\begin{equation}
\label{eq:robcam_axyb12}
\begin{pmatrix}
\bm{I} & -\bm{R}_{\bm{A}_1}\\
\bm{I} & -\bm{R}_{\bm{A}_2}\\
\vdots & \vdots\\
\bm{I} & -\bm{R}_{\bm{A}_n}
\end{pmatrix}
*
\begin{pmatrix}
\bm{t_Y}\\
\bm{t_X}
\end{pmatrix}
=
\begin{pmatrix}
\bm{t}_{\bm{A}_1}-\bm{R_Y}*\bm{t}_{\bm{B}_1}\\
\bm{t}_{\bm{A}_2}-\bm{R_Y}*\bm{t}_{\bm{B}_2}\\
\vdots\\
\bm{t}_{\bm{A}_n}-\bm{R_Y}*\bm{t}_{\bm{B}_n}
\end{pmatrix}
\end{equation}

Due to measurement noise, \(\bm{R}_{\bm{X}}\) and \(\bm{R}_{\bm{Y}}\)
are not necessarily orthogonal, and hence do not represent pure rotations.
Instead, they may represent a shear,
which would violate the \(SE\left(3\right)\) constraints
when applied in a transformation matrix.
This problem can be solved by reorthogonalization (the Orthogonal Procrustes Problem),
a procedure which finds the nearest rotation matrix \(\bm{R}\)
to a non-orthogonal matrix \(\bm{Q}\) with SVD \(\bm{Q}=\bm{U}*\bm{S}*\bm{V}^{T}\)
as \(\bm{R}=\bm{U}*\bm{V}^{T}\) \citep{zhang}.
As suggested in \citep{shah_axyb}, the reorthogonalization can be applied
directly to \(\bm{R}_{\bm{X}}\) and \(\bm{R}_{\bm{Y}}\).
However, as discussed in \citep{ernst}, non-orthogonal rotation matrices
may actually improve accuracy in case of imperfect camera and/or robot intrinsic calibration.
This requires the reorthogonalization to take place after
each application of the non-orthogonal
rotation matrix. This reorthogonalization would orthogonalize
the rotation matrix component of a transformed pose.
In this project, only positions are actually transformed,
so the matrices \(\bm{R}_{\bm{X}}\) and \(\bm{R}_{\bm{Y}}\) output
by the above calibration procedure are simply reorthogonalized
once, before application.

\subsubsection{Finding the marker pose in the camera frame}
\label{sec:markerTcam}
For each image pair, \(\bm{T}_{\mathrm{Marker}_i}^{\mathrm{Camera}}\) needs to be found
as part of the Robot-Camera calibration.
The marker is a flat checkerboard pattern of known dimensions, similar to that of the
camera calibration.
This marker is detected by the MATLAB Stereo Camera Calibrator App,
which outputs all the checkerboard corners in camera coordinates in a specific order.
These coordinates are also known in the marker frame (and are all in the XY-plane of the marker frame).
Thus, transforming the points of the camera frame to the marker frame
can be done by the desired transformation \(\bm{T}_{\mathrm{Marker}_i}^{Camera}\).
This is done by minimizing the error of correspondences between the
checkerboard marker points in the two frames.
This minimization is found through the following procedure:
\begin{enumerate}
\item{
Compute centroids 
\(\bm{\mu}_{\mathrm{Marker}} = \frac{1}{N}\sum_{j=1}^{N}\bm{t}_{j}^{\mathrm{Marker}}\)
and
\(\bm{\mu}_{\mathrm{Camera}_i} = \frac{1}{N}\sum_{j=1}^{N}\bm{t}_{i j}^{\mathrm{Camera}}\)
of the \(N\) marker points in the marker frame and the \(i\)'th camera frame respectively.
}
\item{
Compute correlation matrix \(\bm{H}=\sum_{j=1}^{N}
\left(
\left(\bm{t}_{j}^{\mathrm{Marker}}-\bm{\mu}_{\mathrm{Marker}}\right)
*
\left(\bm{t}_{i j}^{\mathrm{Camera}}-\bm{\mu}_{\mathrm{Camera}_i}\right)^{T}
\right)
\).
}
\item{
Calculate \(\bm{R}_{\mathrm{Marker}_i}^{\mathrm{Camera}}\) through reorthogonalization
of \(\bm{H}\) (see section \ref{sec:axyb}).
}
\item{
Calculate \(\bm{t}_{\mathrm{Marker}_i}^{\mathrm{Camera}}
=-\bm{R}_{\mathrm{Marker}_i}^{\mathrm{Camera}}*\bm{\mu}_{\mathrm{Marker}}
+\bm{\mu}_{\mathrm{Camera}_i}\).
}
\end{enumerate}

\subsubsection{Implementation}
The calibration method of \ref{sec:axyb}
was implemented in C++ using the SVD module
and the unsupported Kronecker product module of the Eigen library \citep{eigen}.
The minimization of correspondences for finding all the \(\bm{T}_{\mathrm{Marker}_i}^{\mathrm{Camera}}\) transformations
(section \ref{sec:markerTcam})
was implemented using the Point Cloud Library \citep{pcl}.