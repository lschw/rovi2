\subsection{Uncertainty Propagation}
\label{sec:unc_prop}
This section is heavily based on work from \citep{stereo_vis_master}.
To further arrive at an equation to map the 2D uncertainty to a 3D uncertainty,
equation \eqref{eq:stereopsis_AM_b_stacked} is solved for $\bm{m}_3$ in equation
\eqref{eq:2D_to_3D_mapping} (note that \(\bm{A}_i\)'s and
\(\bm{b}_i\)'s are stacked vertically into \(\bm{A}\) and \(\bm{b}\) respectively).
\begin{align}
\label{eq:stereopsis_AM_b_stacked}
\bm{A} * \bm{m}_3 &= \bm{b}\\
\bm{m}_3 &= \left( \bm{A}^T * \bm{A} \right)^{-1} * \bm{A}^T * \bm{b} \label{eq:2D_to_3D_mapping}
\end{align}
The uncertainty propagation can be done using equation \eqref{eq:unc_propagation} \cite{modellingUnc},
\begin{equation}
\bm{\Sigma}_{m_3} = \bm{J}_{m_3} * \bm{\Sigma}_{m_2} * \bm{J}_{m_3} ^T
\label{eq:unc_propagation}
\end{equation}
where $\bm{\Sigma}_{m_3}$ is the 3D point variance-covariance matrix,
$\bm{\Sigma}_{m_2} = diag(\bm{\Sigma}_{m_2,1} ,\ldots , \bm{\Sigma}_{m_2,n} )$ are
the 2D point variance-covariance matrices
and $\bm{J}_{m_3}$ is the derivative of the 2D-3D transfer function in
equation \eqref{eq:2D_to_3D_mapping}, found by equation \eqref{eq:errorpropagation}
for \(n\) cameras (\(n=2\) in this project).
\begin{equation}
\bm{J}_{m_3} = \left[ \frac{\partial \bm{m}_3}{\partial \bm{m}_{2,1}} \cdots \frac{\partial \bm{m}_3}{\partial \bm{m}_{2,n}} \right]
\label{eq:errorpropagation}
\end{equation}

The matrix $\frac{\partial \bm{m}_3}{\partial \bm{m}_{2,i}}$ can be found
using equation \eqref{eq:stereo_par_dM_du},
\begin{equation}
\frac{\partial \bm{m}_3}{\partial \bm{m}_{2,i}} = \left[ \frac{\partial \bm{m}_3}{\partial u_i} , \frac{\partial \bm{m}_3}{\partial v_i} \right]
\label{eq:stereo_par_dM_du}
\end{equation}
from which each of the partial derivatives can be solved individually as
in equation \eqref{eq:dMdu}.
\begin{gather}
\frac{\partial \bm{m}_3}{\partial u_i} = \frac{\partial}{\partial u_i} \left( \left( \bm{A}^T * \bm{A} \right)^{-1} * \bm{A}^T * \bm{b} \right)\\
\frac{\partial \bm{m}_3}{\partial u_i} = \frac{\partial \left( \bm{A}^T * \bm{A} \right)^{-1}}{\partial u_i} * \bm{A}^T * \bm{b} 
+ \left( \bm{A}^T * \bm{A} \right)^{-1} * \left[ \frac{\partial \bm{A}}{\partial u_i} \right]^T * \bm{b} 
+ \left( \left( \bm{A}^T * \bm{A} \right)^{-1} * \bm{A}^T \frac{\partial \bm{b}}{\partial u_i} \right) \label{eq:dMdu}
\end{gather}
And similarly for $\frac{\partial \bm{m}_3}{\partial v_i}$ as equation \eqref{eq:dMdu}.

To solve the final system of equations,
it is desirable to simplify the system down to a set
of equations only dependent on the partial derivatives
of $\bm{A}$ and $\bm{b}$ with respect to $u$ and $v$.

Therefore $\frac{\partial \left( \bm{A}^T * \bm{A} \right)^{-1}}{\partial u_i} $ in equation \eqref{eq:dMdu} is simplified to get \eqref{eq:dATAdu}.

\begin{gather}
\frac{\partial \left( \bm{A}^T * \bm{A} \right)^{-1}}{\partial u_i} = -\left( \bm{A}^T * \bm{A} \right)^{-1} 
  \left( \frac{\partial \bm{A}^T}{\partial u_i}  \bm{A} + \bm{A}^T \frac{\partial \bm{A}}{\partial u_i}  \right) * \left( \bm{A}^T * \bm{A} \right)^{-1} \label{eq:dATAdu}
\end{gather}

Since $\bm{A}$ and $\bm{b}$ are given,
the partial derivatives of these can easily be computed wrt. $u$ and $v$.
These are given as follows.
\begin{gather}
\frac{\partial \bm{A}_i}{\partial u_i} = \left[
\begin{tabular}{c}
$- \bm{p}_{3,i}^T $\\ 
$\bm{0}$
\end{tabular}
\right]
,\quad
\frac{\partial \bm{b}_i}{\partial u_i} = \left[
\begin{tabular}{c}
$ p_{3,4,i} $\\ 
0
\end{tabular}
\right]
\\
\frac{\partial \bm{A}_i}{\partial v_i} = \left[
\begin{tabular}{c}
$\bm{0}$\\
$- \bm{p}_{3,i}^T $
\end{tabular}
\right]
,\quad
\frac{\partial \bm{b}_i}{\partial v_i} = \left[
\begin{tabular}{c}
0\\
$ p_{3,4,i} $
\end{tabular}
\right]
\end{gather}

\subsubsection{Implementation}
To calculate the resulting 3D uncertainty
$\bm{\Sigma}_{m_3}$, $\bm{\Sigma}_{m_2}$ is given as the argument to the
implemented C++ method together with known $\bm{P}_i$.

In the stereo case, the 2D uncertainty is given as
\begin{equation}
\bm{\Sigma}_{m_2} = \left[
\begin{tabular}{c c}
$\bm{\Sigma}_{2,l}$ & $\bm{0}$ \\
$\bm{0}$ & $\bm{\Sigma}_{2,r}$
\end{tabular}
\right]
\end{equation}
where $\bm{\Sigma}_{m_2}$ is a 4 \by 4 matrix.
Likewise then $\bm{J}_{m_3}$ is a 3 \by 4 matrix:
\begin{equation}
\bm{J}_{m_3} =  \left[ 
\frac{\partial \bm{m}_3}{\partial u_l}, 
\frac{\partial \bm{m}_3}{\partial v_l},
\frac{\partial \bm{m}_3}{\partial u_r}, 
\frac{\partial \bm{m}_3}{\partial v_r}
 \right]
\end{equation}
where subscripts $l$ and $r$ correspond to the left and right cameras respectively.

Combining the previous equations gives equation \eqref{eq:dm_du}.
Here the knowledge of the results from equation
\eqref{eq:2D_to_3D_mapping} was used since $\bm{m}_3$ was found
during the stereopsis process with a method that avoids using the normal equations.
\begin{gather}
\frac{\partial \bm{m}_3}{\partial u_i} =
- \left( \bm{A}^T * \bm{A} \right)^{-1} * \bm{A}^{DS} * \bm{m}_3
 + \left( \bm{A}^T * \bm{A} \right)^{-1}  *
\left[
\begin{tabular}{c}
$- \bm{p}_{3,i}^T $ \\
$\bm{0}$
\end{tabular}
\right]^T * \bm{b} 
 + \left( \bm{A}^T * \bm{A} \right)^{-1} * \bm{A}^T
 \left[
\begin{tabular}{c}
$ p_{3,4,i} $\\
0
\end{tabular}
\right]
\label{eq:dm_du}
\end{gather}
where 
\begin{gather}
\bm{A}^{DS} = 
\left[
\begin{tabular}{c}
$- \bm{p}_{3,i}^T $\\ 
$\bm{0}$
\end{tabular}
\right]^T * \bm{A} 
+ \bm{A}^T *
\left[
\begin{tabular}{c}
$- \bm{p}_{3,i}^T $\\ 
$\bm{0}$
\end{tabular}
\right]
\end{gather}

Equation \eqref{eq:dm_du} is then pre-multiplied
by $\bm{A}^T \bm{A}$ to get equation \eqref{eq:num_dM_du}.
\begin{equation}
\bm{A}^T * \bm{A} \frac{\partial\bm{m}_3}{\partial u_i} =
- \bm{A}^{DS} * \bm{m}_3 
 + 
\left[
\begin{tabular}{c}
$- \bm{p}_{3,i}^T $\\ 
$\bm{0}$
\end{tabular}
\right]^T * \bm{b} 
 +\bm{A}^T *
 \left[
\begin{tabular}{c}
$ p_{3,4,i} $\\
0
\end{tabular}
\right]
\label{eq:num_dM_du}
\end{equation}
Given \eqref{eq:num_dM_du}, SVD can be used to solve the set of equations.
The computations for $\frac{\partial \bm{m}_{3,i}}{\partial \bm{v}_i}$ can
be computed likewise.
Since $\bm{A}$ and $\bm{b}$ are dependent on $(u, v)$ from the
image algorithm, then the entire computation has to be executed for every sample.
For each $\bm{J}_{m_3}$ it is,
however, possible to reuse
the SVD of $\bm{A}^T * \bm{A}$ for all four
partial derivatives with respect to $u$ and $v$ of the left and right image respectively.
The Jacobian is then used to propagate the
2D uncertainty into camera coordinates by equation
\eqref{eq:unc_propagation}.

All the matrix operations and the SVD were calculated in a C++ implementation
using fixed-size matrices
and the Eigen library \cite{eigen},
utilizing vectorized floating-point arithmetic capabilities of the main PC.