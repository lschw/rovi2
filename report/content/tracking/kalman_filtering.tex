\subsection{Kalman Filtering}
Once the stereo images have been processed,
either a measurement of the object position and its uncertainty
in camera coordinates is obtained, or the object was not detected.
A Kalman filter is applied to these measurements
to integrate the measurement uncertainty into the object position estimate
together with a prediction model.
This allows for undetected objects,
eg. in situations with corrupted or missing images, occlusions and/or
severe motion blur.
The Kalman filter ensures that an estimate of the object state is always
available to the motion planning part of the system.

Simple yet common use cases track linear motion governed by Newtonian mechanics,
and in this case, the process is linear in the state variables,
and a standard linear Kalman filter can be applied.
As the primary purpose of the Kalman filter of this project
is to provide accurate position and velocity estimates
of the object for small durations of time, in which images are lost
or the object is undetected, it was chosen to employ
the linear Kalman filter with a linear process model.
However, tests were more easily carried out by moving the object by hand,
so unmodeled nonlinear human motion needs to be accounted for in the Kalman filter.

The following sections describe the notation, the linear model of the dynamic process
and the measurement,
the process noise model accounting for human motion,
and the adaptive measurement noise model employed by this Kalman filter.
Finally, it is described how the Kalman filter relates these models,
and how the implementation aims to provide numerically stable
and efficient state estimates.

The Kalman filter is iterative, and subscript \(k\) denotes the \(k\)'th iteration.
\(\hat{\mathbf{x}}_{k|k-1}\) is the current estimate at iteration \(k\) of \(\mathbf{x}\)
based on information up to iteration \(k-1\) (a \textit{prediction}).
\(\hat{\mathbf{x}}_{k|k}\) is the current estimate at iteration \(k\) of \(\mathbf{x}\)
based on all the information up to and including iteration \(k\) (ie. the most current estimate).

\subsubsection{Linear dynamic system}
This section describes the system model in a way that is compatible with the Kalman filter.
\(\mathbf{x}\) is the state vector.
In order to be able to accurately track curved motions,
it was chosen to include Cartestian position, velocity and acceleration into the tracked state.
\begin{equation}
\mathbf{x}
=\left(
x, y, z, 
\dot{x}, \dot{y}, \dot{z}, 
\ddot{x}, \ddot{y}, \ddot{z}
\right)^T
\end{equation}
The state transition model explains how to go from \(\mathbf{x}_{k-1}\) to \(\mathbf{x}_{k}\).
The iterations are separated by sampling period \(\Delta t\).
It is assumed that the state transition model follows equation
\ref{eq:stateTransitionModel},
where \(\mathbf{F}_{k}\) is the \textit{state transition matrix}
and \(\mathbf{w}_{k}\sim \mathcal{N}(\mathbf{0},\mathbf{Q}_{k})\)
is the \textit{process noise}.
\begin{align}
\label{eq:stateTransitionModel}
\textup{State transition model: }
\mathbf{x}_{k}
&=\mathbf{F}_{k}*\mathbf{x}_{k-1}
+\mathbf{B}_{k}*\mathbf{u}_{k}
+\mathbf{w}_{k}
\\
\label{eq:stateTransitionModelSimple}
\textup{Simplified: }
\mathbf{x}_{k}
&=\mathbf{F}*\mathbf{x}_{k-1}
+\mathbf{w}
\\
\label{eq:processNoise}
\textup{Process noise: }
\mathbf{w}_{k}
&\sim \mathcal{N}(\mathbf{0},\mathbf{Q}_{k})
\\
\label{eq:processNoiseSimple}
\textup{Simplified: }
\mathbf{w}
&\sim \mathcal{N}(\mathbf{0},\mathbf{Q})
\end{align}
The control input matrix \(\mathbf{B}_{k}\) and the control input vector \(\mathbf{u}_{k}\)
are used when the tracked motion is known, and could be used to incorporate
gravity and drag equations into the model.
However, as the object would often be moved by humans, including
these linear control inputs would not improve the model.
Therefore, \(\mathbf{B}_{k}\) and \(\mathbf{u}_{k}\) are removed from the model,
and it is assumed that equation \ref{eq:stateTransitionModelSimple} models the process accurately.
One further simplification is the assumption that the process noise covariance does not
change, leading to equation \eqref{eq:processNoiseSimple}.
This corresponds to assuming that the unmodeled physics, in most cases human motion,
can be described as a nine-dimensional gaussian with constant variance.
Generally, this is a strong assumption, and will render the filter suboptimal,
but as the human motion is \textit{unmodeled} anyway,
the cost of using a constant covariance matrix is considered negligible compared
to the cost of having a generally nonlinear process described by a linear model.

The state transition is modeled by simple Euler integration,
so \(\mathbf{F}\) is given by equation \eqref{eq:stateTransitionMatrix}.
\begin{equation}
\label{eq:stateTransitionMatrix}
\mathbf{F}
=\begin{pmatrix}
1&0&0&\Delta t&0&0&\frac{1}{2}\left(\Delta t\right)^{2}&0&0\\
0&1&0&0&\Delta t&0&0&\frac{1}{2}\left(\Delta t\right)^{2}&0\\
0&0&1&0&0&\Delta t&0&0&\frac{1}{2}\left(\Delta t\right)^{2}\\
0&0&0&1&0&0&\Delta t&0&0\\
0&0&0&0&1&0&0&\Delta t&0\\
0&0&0&0&0&1&0&0&\Delta t\\
0&0&0&0&0&0&1&0&0\\
0&0&0&0&0&0&0&1&0\\
0&0&0&0&0&0&0&0&1
\end{pmatrix}
\end{equation}

To the Kalman filter, a measurement \(\mathbf{z}=\left(x,y,z\right)^T\) is the object position in cartesian coordinates.
The matrix \(\mathbf{H}\) relates the measurement to the state.
The relation is always one to one, rendering \(\mathbf{H}\) constant.
However, the measured position does not relate to the state velocity nor to the acceleration.
This measurement model is shown in equations \eqref{eq:measurementModel} and \eqref{eq:measurementModelFull}.
The measurement noise \(\mathbf{v}_{k}\) is modeled as a gaussian with covariance \(\mathbf{R}_{k}\) as in equation
\eqref{eq:measurementNoise}.
\begin{align}
\label{eq:measurementModel}
\mathbf{z}_{k}
&=\mathbf{H}*\mathbf{x}_{k}+\mathbf{v}_{k}
\\
\label{eq:measurementModelFull}
\begin{pmatrix}
x_{k}\\y_{k}\\z_{k}
\end{pmatrix}
&=\begin{pmatrix}
1&0&0&0&0&0&0&0&0\\
0&1&0&0&0&0&0&0&0\\
0&0&1&0&0&0&0&0&0
\end{pmatrix}
*
\begin{pmatrix}
x_{k}\\y_{k}\\z_{k}\\
\dot{x}_{k}\\\dot{y}_{k}\\\dot{z}_{k}\\
\ddot{x}_{k}\\\ddot{y}_{k}\\\ddot{z}_{k}
\end{pmatrix}
+\mathbf{v}_{k}
\\
\label{eq:measurementNoise}
\mathbf{v}_{k}
&\sim \mathcal{N}(\mathbf{0},\mathbf{R}_{k})
\end{align}
Unlike the covariance \(\mathbf{Q}\) of the state transition model,
the covariance \(\mathbf{R}_{k}\) is still modeled as depending on the current iteration.
This is because the uncertainty propagated through the stereo vision algorithm
to the measurement \(\mathbf{z}_{k}\) varies with depth and thereby with time (\(k\)), since the object is moving.
Since this uncertainty contributes to covariance \(\mathbf{R}_{k}\) and is known,
it was chosen to propagate it through the Kalman filter as well,
rendering the filter \textit{adaptive}.

\subsubsection{Process noise model}
The noise \(\mathbf{w}\) of equation \eqref{eq:stateTransitionModelSimple}
is to partially describe how the unmodeled part of the process,
that is, the nonlinear human motion, affects the prediction accuracy.
This is a complex task, and an approximation is sought in this section.

When the object is moved by a human, it can be said to be a maneuvering target.
A simple model of maneuvering target motion is the
\textit{Wiener-sequence acceleration} model,
which assumes the acceleration is piecewise-constant
during each sampling iteration, and that the changes in acceleration
between each sampling iteration are independent \citep{maneuvering_target_tracking}.

With the Wiener-sequence acceleration model,
the human motion noise is modeled by a 3D vector of accelerations, \(\mathbf{a}\),
described by equations \eqref{eq:whitenoise_w1} and \eqref{eq:whitenoise_w2}.
\(\mathbf{a}\) has a covariance matrix \(\mathbf{\Sigma}_a\).
\begin{equation}
\mathbf{w}=\mathbf{W}*\mathbf{a}^T
\label{eq:whitenoise_w1}
\end{equation}
\begin{equation}
\mathbf{a}=\begin{pmatrix}
\ddot{\mathbf{x}}\\
\ddot{\mathbf{y}}\\
\ddot{\mathbf{z}}
\end{pmatrix}
\sim \mathcal{N}(\mathbf{0},\mathbf{\Sigma}_a)
\label{eq:whitenoise_w2}
\end{equation}
Simplifying further, the accelerations of the three dimensions are uncorrelated,
so that \(\mathbf{\Sigma}_a\) can be described by equation \eqref{eq:acceleration_covariance}.
\begin{equation}
\mathbf{\Sigma}_a=\begin{pmatrix}
\sigma_{\ddot{x}}^2 & 0 & 0\\
0 & \sigma_{\ddot{y}}^2 & 0\\
0 & 0 & \sigma_{\ddot{z}}^2
\end{pmatrix}
\label{eq:acceleration_covariance}
\end{equation}
Matrix \(\mathbf{W}\) describes how \(\mathbf{a}\) affects the nine-dimensional state.
It is found by Euler integration, as per equation \eqref{eq:acc_to_state}.
\begin{equation}
\mathbf{W}=
\begin{pmatrix}
\frac{{\Delta t}^2}{2} & 0 & 0\\ 
0 & \frac{{\Delta t}^2}{2} & 0\\ 
0 & 0 & \frac{{\Delta t}^2}{2}\\ 
{\Delta t} & 0 & 0\\ 
0 & {\Delta t} & 0\\ 
0 & 0 & {\Delta t}\\ 
1 & 0 & 0\\ 
0 & 1 & 0\\ 
0 & 0 & 1
\end{pmatrix}
\label{eq:acc_to_state}
\end{equation}

Since the process noise \(\mathbf{w}\) has covariance \(\mathbf{Q}\),
\(\mathbf{W}*\bm{a}^T\) must also have covariance \(\mathbf{Q}\),
leading to equation \eqref{eq:processNoiseCovariancePiecewise},
with the solution in equation \eqref{eq:processNoiseCovariancePiecewiseFull}.
\begin{equation}
\mathbf{w}\sim \mathcal{N}(\mathbf{0},\mathbf{Q})
\Rightarrow
\mathbf{Q}=\mathrm{Cov}\left(\mathbf{W}*a^T\right)=
\mathbb{E}\left(\mathbf{W}*\mathbf{a}^T*\mathbf{a}*\mathbf{W}^T\right)=
\mathbf{W}*\mathbf{\Sigma}_a*\mathbf{W}^T
\label{eq:processNoiseCovariancePiecewise}
\end{equation}
\begin{equation}
\mathbf{Q}=\begin{pmatrix}
\frac{\sigma_{\ddot{x}}^2 {\Delta t}^4}{4} & 0 & 0 & \frac{\sigma_{\ddot{x}}^2 {\Delta t}^3}{2} & 0 & 0 & \frac{\sigma_{\ddot{x}}^2 {\Delta t}^2}{2} & 0 & 0\\ 
0 & \frac{\sigma_{\ddot{y}}^2 {\Delta t}^4}{4} & 0 & 0 & \frac{\sigma_{\ddot{y}}^2 {\Delta t}^3}{2} & 0 & 0 & \frac{\sigma_{\ddot{y}}^2 {\Delta t}^2}{2} & 0\\ 
0 & 0 & \frac{\sigma_{\ddot{z}}^2 {\Delta t}^4}{4} & 0 & 0 & \frac{\sigma_{\ddot{z}}^2 {\Delta t}^3}{2} & 0 & 0 & \frac{\sigma_{\ddot{z}}^2 {\Delta t}^2}{2}\\ 
\frac{\sigma_{\ddot{x}}^2 {\Delta t}^3}{2} & 0 & 0 & \sigma_{\ddot{x}}^2 {\Delta t}^2 & 0 & 0 & \sigma_{\ddot{x}}^2 {\Delta t} & 0 & 0\\ 
0 & \frac{\sigma_{\ddot{y}}^2 {\Delta t}^3}{2} & 0 & 0 & \sigma_{\ddot{y}}^2 {\Delta t}^2 & 0 & 0 & \sigma_{\ddot{y}}^2 {\Delta t} & 0\\ 
0 & 0 & \frac{\sigma_{\ddot{z}}^2 {\Delta t}^3}{2} & 0 & 0 & \sigma_{\ddot{z}}^2 {\Delta t}^2 & 0 & 0 & \sigma_{\ddot{z}}^2 {\Delta t}\\ 
\frac{\sigma_{\ddot{x}}^2 {\Delta t}^2}{2} & 0 & 0 & \sigma_{\ddot{x}}^2 {\Delta t} & 0 & 0 & \sigma_{\ddot{x}}^2 & 0 & 0\\ 
0 & \frac{\sigma_{\ddot{y}}^2 {\Delta t}^2}{2} & 0 & 0 & \sigma_{\ddot{y}}^2 {\Delta t} & 0 & 0 & \sigma_{\ddot{y}}^2 & 0\\ 
0 & 0 & \frac{\sigma_{\ddot{z}}^2 {\Delta t}^2}{2} & 0 & 0 & \sigma_{\ddot{z}}^2 {\Delta t} & 0 & 0 & \sigma_{\ddot{z}}^2
\end{pmatrix}
\label{eq:processNoiseCovariancePiecewiseFull}
\end{equation}
\(\sigma_{\ddot{x}}\), \(\sigma_{\ddot{y}}\) and \(\sigma_{\ddot{z}}\)
describe the standard deviation of the change in acceleration
between each time step.
Assuming the human motion is similar in all directions,
and is characterized by one scalar \(\Delta a\), which is the maximum
change in acceleration between each time step (in \(\frac{\textup{m}}{\textup{s}^2}\)),
a crude estimate of the standard deviations is
%somewhere between \(\frac{\Delta a}{2}\) and \(\Delta a\).
simply \(\Delta a\).
With fixed \(\Delta t\), \(\mathbf{Q}\) is now parametrized by a single number \(\Delta a\),
facilitating quick optimization tuning to various motion patterns.

\subsubsection{Measurement noise model}
Since \(\mathbf{R}_{k}\) depends on the uncertainty of the measured 3D input state \(\mathbf{\Sigma}_{x,y,z,k}\),
it will vary with depth in a similar way to \(\mathbf{\Sigma}_{x,y,z,k}\).
\(\mathbf{R}_{k}\) is thus set equal to the propagated uncertainty from the vision algorithm,
or scaled by an optional tuning scalar, if found necessary.

\subsubsection{Kalman filter}
The Kalman filter uses the model and measurements to estimate the current
state \(\hat{\mathbf{x}}_{k|k}\) and its covariance \(\mathbf{P}_{k|k}\).
These are the outputs of the Kalman filter, and they provide estimates of position, velocity,
acceleration and uncertainty.

Implementation-wise, the Kalman filter incorporates information into the estimates
in two steps. It starts by using the state transition model of equation \eqref{eq:stateTransitionModelSimple}
to \textit{predict} \(\hat{\mathbf{x}}_{k|k-1}\) (the current state based on previous information)
and its covariance \(\mathbf{P}_{k|k-1}\) using the state transition matrix \(\mathbf{F}\) (which depends only on \(\Delta t\)) and the process noise covariance \(\mathbf{Q}\) (global uncertainty).
The prediction step is shown in equations \eqref{eq:predictx} and \eqref{eq:predictP}.
\begin{align}
\label{eq:predictx}
\hat{\mathbf{x}}_{k|k-1}
&=\mathbf{F}*\hat{\mathbf{x}}_{k-1|k-1}
\\
\label{eq:predictP}
\mathbf{P}_{k|k-1}
&=\mathbf{F}*\mathbf{P}_{k-1|k-1}*\mathbf{F}^T+\mathbf{Q}
\end{align}
After the prediction step, the Kalman filter may \textit{update} the estimate from
\(\hat{\mathbf{x}}_{k|k-1}\) to \(\hat{\mathbf{x}}_{k|k}\) by incorporating
some information from iteration \(k\). This information is the position measurement
\(\mathbf{z}_{k}\) and its covariance (uncertainty) \(\mathbf{R}_k\).
The Kalman filter calculates a matrix \(\mathbf{K}_{k}\) called the optimal Kalman gain
at each iteration, which is used to weigh the new measurement.
The new measurement \(\mathbf{z}_{k}\) differs from the current state by some
cartesian space position residual
called \(\mathbf{y}_{k}=\left(\varepsilon_x,\varepsilon_y,\varepsilon_z\right)^T\).
Ideally, this residual is zero, which means the system model used for prediction
is perfect. This is not the case,
and the residual \(\mathbf{y}_{k}\) is time-varying with covariance \(\mathbf{S}_{k}\).
If the system is modeled correctly, the residual is white noise,
in which case the Kalman gain is optimal.
This first part of the \textit{update} step is called \textit{innovation}.
The \textit{innovation} is shown in equations \eqref{eq:innovationy} and \eqref{eq:innovationS}.
\begin{align}
\label{eq:innovationy}
\mathbf{y}_{k}
&=\mathbf{z}_{k}-\mathbf{H}*\hat{\mathbf{x}}_{k|k-1}
\\
\label{eq:innovationS}
\mathbf{S}_{k}
&=\mathbf{H}*\mathbf{P}_{k|k-1}*\mathbf{H}^T+\mathbf{R}_{k}
\end{align}
Note that the residual \(\mathbf{y}_{k}\) is a 3-element vector measured in measurement units,
and \(\mathbf{S}_{k}\) is a 3 \by 3 covariance matrix.
In short, the \textit{innovation} describes how the measurement differs from the prediction.

Finally, the optimal Kalman gain \(\mathbf{K}_{k}\) is calculated,
and posteriori estimates of \(\hat{\mathbf{x}}_{k|k}\) and
its covariance \(\mathbf{P}_{k|k}\) are obtained through equations \eqref{eq:kalmangain},
\eqref{eq:updatex} and \eqref{eq:updateP}.
\begin{align}
\label{eq:kalmangain}
\mathbf{K}_{k}
&=\mathbf{P}_{k|k-1}*\mathbf{H}^T*\mathbf{S}_{k}^{-1}
\\
\label{eq:updatex}
\hat{\mathbf{x}}_{k|k}
&=\hat{\mathbf{x}}_{k|k-1}+\mathbf{K}_{k}*\mathbf{y}_{k}
\\
\label{eq:updateP}
\mathbf{P}_{k|k}
&=\left(\mathbf{I}-\mathbf{K}_{k}*\mathbf{H}_{k}\right)*\mathbf{P}_{k|k-1}
\end{align}

\subsubsection{Implementation}
A C++ class named Kalman was developed using the Eigen library \citep{eigen}.
This section is meant as an \textit{overview} of the calculations inside
the main Kalman class methods.
The class was developed with numerical stability and easy debugging in mind.

% \paragraph{The predict() method}
The predict method performs the prediction step,
with computations arranged in a optimized order.
Returns a reference to \(\hat{\mathbf{x}}_{k|k-1}\).
Additionally, this method "updates" \(\hat{\mathbf{x}}_{k|k}\) to \(\hat{\mathbf{x}}_{k|k-1}\)
and \(\mathbf{P}_{k|k}\) to \(\mathbf{P}_{k|k-1}\),
so that missing measurements (undetected object) are handled simply by
not calling the update() method, and only calling the predict() method.

% \paragraph{The update() method}
The update method performs the update step, in a different
way than the update equations imply:
Equation \eqref{eq:kalmangain} is rewritten to equation \eqref{eq:kalmangain5},
using the fact that positive semidefinite matrices, such as \(\mathbf{S}_{k}\),
are also symmetric.
\begin{equation}
\label{eq:kalmangain5}
\mathbf{S}_k*\mathbf{K}_{k}^T=\mathbf{H}*\mathbf{P}_{k|k-1}
\end{equation}
This gives a system of linear equations with positive semidefinite matrices,
which can be solved using the Cholesky decomposition of \(\mathbf{S}_{k}\).
The result is the transpose of the desired Kalman gain.
