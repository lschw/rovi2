tR = dlmread('trajectory_reference.csv');
tN = dlmread('trajectory_noisy.csv');
%tF = dlmread('../../code/bin/trajectory_filtered.csv');
tF = dlmread('trajectory_filtered.csv');

figure(2);
plot3(tR(:,1),tR(:,2),tR(:,3),'b-'...
    ,tN(:,1),tN(:,2),tN(:,3),'ro-'...
    ...%,tF(:,1),tF(:,2),tF(:,3),'g.-'...
    ,tF(:,10),tF(:,11),tF(:,12),'g.-'...
    );
grid on;
grid minor;
legend('Reference','Measurement','Update');