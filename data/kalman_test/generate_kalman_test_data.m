%% Something resembling ball movement controlled by human motion
X=[linspace(0,1,100),linspace(1,0.5,50)];
Y=[linspace(0,1,75),linspace(1,0.5,75)];
Z=[linspace(0,1.2,100),linspace(1.2,0.8,20),linspace(0.8,1,30)];
%Measurement noise covariance matrix R, randomly chosen
R =[0.000443300757192,-0.000023118580189,0.000420182177003;
  -0.000023118580189,0.000975003666092,0.000951885085903;
   0.000420182177003,0.000951885085903,0.001372067262906];
Rchol=chol(R);
%Generate noise vector from R
v=randn(length(X),3)*Rchol;
clear Rchol;
a=[X',Y',Z']+v;
Xr=a(:,1);
Yr=a(:,2);
Zr=a(:,3);
clear a;
plot3(X,Y,Z,'b-',Xr,Yr,Zr,'ro-');

dlmwrite('measurement_noise_covariance_matrix.csv',R,'delimiter',' ',...
    'precision',15);
dlmwrite('trajectory_reference.csv',[X',Y',Z'],'delimiter',' ',...
    'precision',15);
dlmwrite('trajectory_noisy.csv',[Xr,Yr,Zr],'delimiter',' ',...
    'precision',15);