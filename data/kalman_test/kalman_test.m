clear variables;
close all;
inner_loop_ite=0;
n_repeats=10;
stepSize_da=0.2;
accel_range=0.4:stepSize_da:15;
chosen_acceleration_delta=3;
h=waitbar(0,'Kalman test');
clear errorp erroru
%%
for bigIterator=1:n_repeats
    inner_loop_ite=0;
    %clear errorp erroru
for acceleration_delta=accel_range
%%
    inner_loop_ite=inner_loop_ite+1;
    waitbar((bigIterator-1+inner_loop_ite*stepSize_da/(accel_range(end)-accel_range(1)))/n_repeats,h);
    
mult2dNoise=1;
mult3dcov=1;

mult2dNoise=1;
mult3dcov=[1 0 0;0 1 0; 0 0 1];

%acceleration_delta=1.05;
%acceleration_delta=15.85




%pathpath='../../workcell/testpaths/';
binpath='../../code/bin/';
%pathpath='';
P_path='../cam_calib/';

system([binpath,'test_pathgeneration ',round(rand*32000)]);

%3D path in camera frame
tR=csvread('test_pathgeneration_path.csv');
tR=tR(:,1:3);
n=size(tR,1);

%Rectified projection matrices
P_left_rect = dlmread([P_path,'left_projection_matrix_rectified.txt']);
P_right_rect = dlmread([P_path,'right_projection_matrix_rectified.txt']);

%Turn 3D path into pairs of pixels
tRproj=[transpose(tR);ones(1,n)];
left = transpose(P_left_rect*tRproj);
right = transpose(P_right_rect*tRproj);
left=left(:,1:2)./repmat(left(:,3),1,2);
right=right(:,1:2)./repmat(right(:,3),1,2);

%a=find(right(:,1)<0 | right(:,2)<0);
%b=find(left(:,1)<0 | left(:,2)<0);
%a/n*5
%b/n*5

cov2d=mult2dNoise*[...
    0.189158,0.0929934;
    0.0929934,0.160796...
    ]*1.4;
chol2d=chol(cov2d);
noise_left=randn(n,2)*chol2d;
noise_right=randn(n,2)*chol2d;
left_noisy=left+noise_left;
right_noisy=right+noise_right;

%%
dlmwrite('left_2d_pixels.txt',left,'delimiter',' ','precision','%.16g');
dlmwrite('right_2d_pixels.txt',right,'delimiter',' ','precision','%.16g');
dlmwrite('left_noisy_2d_pixels.txt',left_noisy,'delimiter',' ','precision','%.16g');
dlmwrite('right_noisy_2d_pixels.txt',right_noisy,'delimiter',' ','precision','%.16g');
dlmwrite('2d_pixel_covariance.txt',cov2d,'delimiter',' ','precision','%.16g');

system([binpath,'test_triangulate left_2d_pixels.txt right_2d_pixels.txt'...
    ,' ',P_path,'left_projection_matrix_rectified.txt'...
    ,' ',P_path,'right_projection_matrix_rectified.txt'...
    ,' 2d_pixel_covariance.txt 2d_pixel_covariance.txt']);
tR=dlmread('stereoPoints.txt');

system([binpath,'test_triangulate'...
    ,' left_noisy_2d_pixels.txt right_noisy_2d_pixels.txt'...
    ,' ',P_path,'left_projection_matrix_rectified.txt'...
    ,' ',P_path,'right_projection_matrix_rectified.txt'...
    ,' 2d_pixel_covariance.txt 2d_pixel_covariance.txt']);
tN=dlmread('stereoPoints.txt');
tcov=dlmread('stereoCovariances.txt');
for it=1:n
    cov3d(3*(it-1)+1:3*it,1:3)=mult3dcov*reshape(tcov(it,:),3,3);
end

%%
dlmwrite('trajectory_reference.txt',tR,'delimiter',' ','precision','%.16g');
dlmwrite('trajectory_noisy.txt',tN,'delimiter',' ','precision','%.16g');
dlmwrite('measurement_noise_covariance_matrices.txt',cov3d,'delimiter',' ','precision','%.16g');
system([binpath,'test_trackingsystem trajectory_reference.txt '...
    ,'trajectory_noisy.txt measurement_noise_covariance_matrices.txt '...
    ,num2str(acceleration_delta)]);
%test_trackingsystem path/to/trajectory_reference.txt path/to/trajectory_noisy.txt path/to/measurement_noise_covariance_matrices.txt acceleration_delta
%px py pz pdx pdy pdz pddx pddy pddz ux uy uz udx udy udz uddx uddy uddz
tF=dlmread('trajectory_filtered.txt');
tP=tF(:,1:9);
tU=tF(:,10:18);
clear tF;

%%

errorp(bigIterator,inner_loop_ite)=norm(tR-tP(:,1:3));
erroru(bigIterator,inner_loop_ite)=norm(tR-tU(:,1:3));

pr=1:n; %plot range
%pr=1:30; %plot range

if (acceleration_delta==chosen_acceleration_delta)&&(bigIterator==1)
    figure(1);
    plot3(tR(pr,1),tR(pr,2),tR(pr,3),'b.-'...
        ,tN(pr,1),tN(pr,2),tN(pr,3),'rx-'...
        ,tP(pr,1),tP(pr,2),tP(pr,3),'m.-'...
        ,tU(pr,1),tU(pr,2),tU(pr,3),'go-'...
        );
    axis equal;
    axis vis3d;
    rotate3d on;
    grid on;
    grid minor;
    xlabel('X [m]');
    ylabel('Y [m]');
    zlabel('Z [m]');
    legend(...
        'Reference'...
        ,'Measurement'...
        ,'Prediction'...
        ,'Update'...
        ,'Location','NorthWest');
    title({['Kalman filter output for test trajectory with \Delta a=',...
        num2str(chosen_acceleration_delta),' m/s^2,'],'3D-view'});
    %%
    print(['plots/kalman-sim-3d-da-',num2str(chosen_acceleration_delta),'.eps']...
        ,'-depsc');
    
    %%
    
    
    figure(2);
    plot(tR(pr,1),tR(pr,2),'b.-'...
        ,tN(pr,1),tN(pr,2),'r.-'...
        ,tP(pr,1),tP(pr,2),'m.-'...
        ,tU(pr,1),tU(pr,2),'go-'...
        );
    axis equal;
    grid on;
    grid minor;
    xlabel('X [m]');
    ylabel('Y [m]');
    legend(...
        'Reference'...
        ,'Measurement'...
        ,'Prediction'...
        ,'Update'...
        );
            title({['Kalman filter output for test trajectory with \Delta','a=',...
        num2str(chosen_acceleration_delta),' m/s^2,'],'XY-view'});
       print(['plots/kalman-sim-xy-da-',num2str(chosen_acceleration_delta),'.eps']...
        ,'-depsc');
    figure(3);
    plot(tR(pr,2),tR(pr,3),'b.-'...
        ,tN(pr,2),tN(pr,3),'r.-'...
        ,tP(pr,2),tP(pr,3),'m.-'...
        ,tU(pr,2),tU(pr,3),'go-'...
        );
    axis equal;
    grid on;
    grid minor;
    xlabel('Y [m]');
    ylabel('Z [m]');
    legend(...
        'Reference'...
        ,'Measurement'...
        ,'Prediction'...
        ,'Update'...
        );
            title({['Kalman filter output for test trajectory with \Delta','a=',...
        num2str(chosen_acceleration_delta),' m/s^2,'],'YZ-view'});
    print(['plots/kalman-sim-yz-da-',num2str(chosen_acceleration_delta),'.eps']...
        ,'-depsc');

end
%%
end
%bigErrp{bigIterator}=errorp;
%bigErru{bigIterator}=erroru;
end
%save('bigErr.mat','accel_range','bigErrp','bigErru');
close(h);

save('errors.mat','accel_range','stepSize_da','n_repeats','errorp','erroru');
%%
load('errors.mat');
meanp=mean(errorp);
meanu=mean(erroru);
stdp=std(errorp)/(n_repeats);
stdu=std(erroru)/(n_repeats);

%margErr=1.645*stdErrs; cipercentage='90';
%margErr=1.96*stdErrs; cipercentage='95';
margErrp=2.575*stdp; cipercentage='99';
margErru=2.575*stdu; cipercentage='99';

maxErrp=margErrp;
minErrp=-margErrp;
maxErru=margErru;
minErru=-margErru;

figure(4);
errorbar(accel_range,meanp,minErrp,maxErrp,'m.'); hold on;
errorbar(accel_range,meanu,minErru,maxErru,'g.'); hold off;
legend('Prediction error','Update error');
xlabel('\Delta a [m/s^2]');
ylabel('Error [m]');
grid on;
grid minor;
title({'Kalman filter output error on test trajectories'...
    ,'with 99 % confidence intervals'});
print('plots/kalman-error-vs-delta-a.eps','-depsc');
%%
% figure(3);
% plot(accel_range,errorp,'m.-'...
%     ,accel_range,erroru,'g.-'...
%     );
% legend('Prediction error','Update error');
% xlabel('\Delta a');
% ylabel('Error [m]');
% grid on;
% grid minor;