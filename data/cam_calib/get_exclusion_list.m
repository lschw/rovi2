function [exclusion_list] = get_exclusion_list(filename)
% get_exclusion_list returns cell array of excluded
%  image file names.
% Input file lines starting with # will be ignored.
fileID = fopen(filename);
exclusion_list = textscan(fileID,'%s',...
'TreatAsEmpty',{'NA','na'},'CommentStyle','#');
exclusion_list=exclusion_list{1};
fclose(fileID);
end