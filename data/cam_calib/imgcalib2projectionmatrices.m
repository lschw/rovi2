%% ImgCalib2ProjectionMatrices
%  Loads stereoParams and creates projection matrices,
%  assuming orego in left camera.
%% Load stereo parameters
load('stereoParams.mat')
%% Intrinsics
A_left = stereoParams.CameraParameters1.IntrinsicMatrix;
A_right = stereoParams.CameraParameters2.IntrinsicMatrix;
%% Rotation
R_left = eye(3);
R_right = stereoParams.RotationOfCamera2;
%% Translation
t_left = zeros(3,1)';
t_right = stereoParams.TranslationOfCamera2;
%% Projection matrices
P_left = cameraMatrix(stereoParams.CameraParameters1,R_left,t_left)';
P_right = cameraMatrix(stereoParams.CameraParameters2,R_right,t_right)';
dlmwrite('P_left.csv',P_left,'Precision', '%.15f');
dlmwrite('P_right.csv',P_right,'Precision', '%.15f');
