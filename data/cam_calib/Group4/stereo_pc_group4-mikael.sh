#Substitute <laptop> with laptop IP or laptop hostname
#if that is defined in /etc/hosts of the workcell PC.
#export ROS_MASTER_URI=http://<laptop>:11311
export ROS_MASTER_URI=http://group4-mikael:11311
export ROS_HOSTNAME=`hostname -I`
roslaunch /home/student/Desktop/Group4/stereo_camera.launch

