function mfprintf(fid, varargin)
    arrayfun(@(fid) fprintf(fid, varargin{:}), fid);
end
