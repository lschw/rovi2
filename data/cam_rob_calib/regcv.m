datapaths={'april19th/','april27th/','april28th/','may13th/'};
squareSides=[0.0283,0.040,0.0518,0.040];

for dataset_iterator=4
    datapath=datapaths{dataset_iterator};
    squareSide=squareSides(dataset_iterator);

%% Robot camera calibration (registration)
% Load images and parameters
rovi2_path='../../'; %contains rovi2 repository
negate_translation_in_output_transformation = true;

%datapath='april28th/'; %contains left/, right/ and configurations.csv
%squareSide=0.0518; %%meters
%datapath='april19th/'; %contains left/, right/ and configurations.csv
%squareSide=0.0283; %%meters
%datapath='april27th/'; %contains left/, right/ and configurations.csv
%squareSide=0.040; %%meters

configurations = dlmread([datapath,'configurations.csv'],',');
timestamps = configurations(:,1);
timestampsc = num2str(timestamps);
configurations = configurations(:,2:end);
clear left_images right_images
for it=1:size(timestamps,1)
    left_images(it,:)=[datapath,'left/img_',timestampsc(it,:),'.png'];
    right_images(it,:)=[datapath,'right/img_',timestampsc(it,:),'.png'];
end

left_images = cellstr(left_images);
right_images = cellstr(right_images);

[imagePoints,boardSize,pairsUsed] = detectCheckerboardPoints(...
    left_images,right_images);

save([datapath,'regcv.mat'],...
    'rovi2_path','datapath','squareSide','timestamps','timestampsc','configurations',...
    'left_images','right_images','imagePoints','boardSize','pairsUsed');
%% Load projection matrices and perform stereo matching
load([datapath,'regcv.mat']);
imageFileNames1 = left_images(pairsUsed);
imageFileNames2 = right_images(pairsUsed);
num_pairsUsed=sum(pairsUsed);
worldPoints = generateCheckerboardPoints(boardSize,squareSide);
worldPoints3d = [worldPoints, zeros(length(worldPoints),1)];

leftPoints=imagePoints(:,:,1,1);
rightPoints=imagePoints(:,:,1,2);
for it=2:sum(pairsUsed)
    leftPoints=[leftPoints;imagePoints(:,:,it,1)];
    rightPoints=[rightPoints;imagePoints(:,:,it,2)];
end

left_projection_matrix_rectified = dlmread(...
    [rovi2_path,'data/cam_calib/left_projection_matrix_rectified.txt'],' ');
right_projection_matrix_rectified = dlmread(...
    [rovi2_path,'data/cam_calib/right_projection_matrix_rectified.txt'],' ');
% Write triangulated 3D points (in meters, if loaded projection matrices are)
P_left = transpose(left_projection_matrix_rectified);
P_right= transpose(right_projection_matrix_rectified);

dlmwrite([datapath,'leftPoints.txt'],leftPoints,'delimiter',' ','precision','%.16g');
dlmwrite([datapath,'rightPoints.txt'],rightPoints,'delimiter',' ','precision','%.16g');

system(['./',rovi2_path,'code/bin/test_triangulate ',...
    datapath,'leftPoints.txt ',datapath,'rightPoints.txt ',...
    rovi2_path,'data/cam_calib/left_projection_matrix_rectified.txt ',...
    rovi2_path,'data/cam_calib/right_projection_matrix_rectified.txt']);
stereoPoints = dlmread('stereoPoints.txt',' ');
dlmwrite([datapath,'stereoPoints.txt'],stereoPoints,'delimiter',' ','precision','%.16g');

dlmwrite([datapath,'robot_configurations.txt'],configurations(pairsUsed,:)...
    ,'delimiter',' ','precision','%.16g');
dlmwrite([datapath,'markerPointsMarkerFrame.txt'],worldPoints3d...
    ,'delimiter',' ','precision','%.16g');

%% Repeated K-fold cross validation
%10-repeat 10-fold cross validation equals 100 runs
do_crossvalidation=false;
str=questdlg('Do you want to run a new cross validation?','Cross validation');
if strcmp(str,'Yes')
    str=questdlg('Are you sure?','Cross validation');
    if strcmp(str,'Yes')
        do_crossvalidation=true;
    end
end

if do_crossvalidation
    n_repeats = 10;
    n_folds=10;
    errs = cell(n_repeats);
    trainSizes=3:(num_pairsUsed-ceil(num_pairsUsed/n_folds));
    h=waitbar(0,{['Repeat 01 / ',num2str(n_repeats),...
        '. Fold 01 / ',num2str(n_folds),'.',...
        ' Train 01 / ',num2str(length(trainSizes)),'.'],...
        'Time left: ??? minutes.'});
    tic
    for kt=1:n_repeats
        c1=cvpartition(num_pairsUsed,'Kfold',n_folds);
        errs{kt}=0;
        for jt=1:n_folds
            testIdx=c1.test(jt);
            trainIdx=c1.training(jt);
            testSize=sum(testIdx);
            
            testSeq=1:num_pairsUsed;
            testSeq=testSeq(testIdx);
            dlmwrite('testIndices.txt',transpose(testSeq),' ');
            trainSeq=1:num_pairsUsed;
            trainSeq=trainSeq(trainIdx);
            
            clear error_training error_testing;
            for it=1:length(trainSizes)
                t_it=toc;
                p=(((kt-1)*n_folds+(jt-1))*length(trainSizes)+(it-1))/...
                    (n_repeats*n_folds*length(trainSizes));
                pmin=floor((t_it/p)*(1-p)/60);
                psec=floor(mod((t_it/p)*(1-p),60));
                waitbar(p,h,...
                    {['Repeat: ', num2str(kt), ' / ', num2str(n_repeats),...
                    '. Fold: ',num2str(jt),' / ',num2str(n_folds), '.',...
                    ' Train ',num2str(it),' / ',num2str(length(trainSizes)),'.'],...
                    ['Time left: ',num2str(pmin),' minutes ',num2str(psec),' seconds.']});
                
                trainSize=trainSizes(it);
                trainSeqLocal=trainSeq(randperm(num_pairsUsed-testSize));
                trainSeqLocal=trainSeqLocal(1:trainSize);
                dlmwrite('trainIndices.txt',transpose(trainSeqLocal),' ');
                system(['./',rovi2_path,'code/bin/test_registration ',...
                    datapath,'markerPointsMarkerFrame.txt ',...
                    datapath,'stereoPoints.txt ',...
                    datapath,'robot_configurations.txt ',...
                    rovi2_path,'workcell/WorkStation3/WS3_Scene.wc.xml ',...
                    'trainIndices.txt ',...
                    'testIndices.txt']);
                error_training(it,:) = dlmread('error_training.txt',' ');
                error_testing(it,:) = dlmread('error_testing.txt',' ');
            end
            errs{kt}((jt-1)*length(trainSizes)+1:jt*length(trainSizes),1:4)=...
                [error_training,error_testing];
        end
    end
    close(h)
    
    save([datapath,'errs.mat'],'errs','n_repeats','n_folds','trainSizes');
end
%%
load([datapath,'errs.mat']);
addpath('tight_subplot');

plotTrainSizes=9:(num_pairsUsed-ceil(num_pairsUsed/n_folds)); % or =trainSizes


nts=length(plotTrainSizes);

sumErrs=0;
for kt=1:n_repeats
    for jt=1:n_folds
        sumErrs=sumErrs+errs{kt}((jt-1)*length(trainSizes)+1:jt*length(trainSizes),1:4);
    end
end
avgErrs = sumErrs./(n_repeats*n_folds);
varErrs=0;
for kt=1:n_repeats
    for jt=1:n_folds
        varErrs=varErrs+...
            (avgErrs-errs{kt}((jt-1)*length(trainSizes)+1:jt*length(trainSizes),1:4)).^2;
    end
end
varErrs=varErrs./(n_repeats*n_folds-1);
stdDevs=sqrt(varErrs);
stdErrs=stdDevs./sqrt(n_repeats*n_folds);


%margErr=1.645*stdErrs; cipercentage='90';
%margErr=1.96*stdErrs; cipercentage='95';
margErr=2.575*stdErrs; cipercentage='99';

maxErr=margErr;
minErr=-margErr;
minErr(:,[2 4])=max(minErr(:,[2 4]),-pi());
maxErr(:,[2 4])=min(maxErr(:,[2 4]),pi());

figure(1);
pos=get(gcf,'Position');
pos(2)=1.1*pos(2);
pos(4)=1.5*pos(4);
set(gcf,'Position',pos);
[ha,~]=tight_subplot(2,1,0.04,0.1,[0.1 0.01]);
subplotaspect=[1 0.6];
%yyaxis left;
%subplot(2,1,1);
axes(ha(1));
errorbar(trainSizes,1000*avgErrs(:,1),1000*minErr(:,1),1000*maxErr(:,1),'b.-');
hold on;
errorbar(trainSizes,1000*avgErrs(:,3),1000*minErr(:,3),1000*maxErr(:,3),'r.-');
hold off;
ylabel('Translational error [mm]');
xlim([plotTrainSizes(1)-0.9 plotTrainSizes(end)+0.9]);
set(ha(1),'XTickLAbel','');
pbaspect([subplotaspect 1]);
grid on;
grid minor;
legend('Training RMSE, translation','Test RMSE, translation');
%yyaxis right;
%subplot(2,1,2);
axes(ha(2));
errorbar(trainSizes,avgErrs(:,2)*180/pi(),minErr(:,2)*180/pi(),maxErr(:,2)*180/pi(),'b.-');
hold on;
errorbar(trainSizes,avgErrs(:,4)*180/pi(),minErr(:,4)*180/pi(),maxErr(:,4)*180/pi(),'r.-');
hold off;
ylabel('Rotational error [°]');
xlim([plotTrainSizes(1)-0.9 plotTrainSizes(end)+0.9]);
pbaspect([subplotaspect 1]);
grid on;
grid minor;
xlabel('Number of calibration configurations');
legend('Train MAE, rotation','Test MAE, rotation');
%subplot(2,1,1);
axes(ha(1));
title({['Robot-camera calibration error with ',cipercentage,' % CI,']...
    ,[num2str(n_repeats),'-repeat ',num2str(n_folds),'-fold cross validation']});

print([datapath,'plots/robot-camera-calibration-error-vs-train-size.eps'],'-depsc');

figit1=1;
%% Calibrate with full training set
clear error_training error_testing stereoPointsRead;
trainSeq=1:num_pairsUsed;
dlmwrite('trainIndices.txt',transpose(trainSeq),' ');
dlmwrite('testIndices.txt',[''],' ');
system(['./',rovi2_path,'code/bin/test_registration ',...
    datapath,'markerPointsMarkerFrame.txt ',...
    datapath,'stereoPoints.txt ',...
    datapath,'robot_configurations.txt ',...
    rovi2_path,'workcell/WorkStation3/WS3_Scene.wc.xml ',...
    'trainIndices.txt ',...
    'testIndices.txt']);
error_training = dlmread('error_training.txt',' ');
%error_testing = dlmread('error_testing.txt',' ');

worldTcam=dlmread('worldTcam.txt',' ');
worldTcamOrthogonal=dlmread('worldTcamOrthogonal.txt',' ');
endTmarker=dlmread('endTmarker.txt',' ');
endTmarkerOrthogonal=dlmread('endTmarkerOrthogonal.txt',' ');
worldTend=dlmread('worldTend.txt');
camTmarker=dlmread('camTmarker.txt');
if negate_translation_in_output_transformation
    worldTcam(1:3,4)=-worldTcam(1:3,4);
    worldTcamOrthogonal(1:3,4)=-worldTcamOrthogonal(1:3,4);
    endTmarker(1:3,4)=-endTmarker(1:3,4);
    endTmarkerOrthogonal(1:3,4)=-endTmarkerOrthogonal(1:3,4);
end
dlmwrite([datapath,'worldTcam.txt'],worldTcam,'delimiter',' ','precision','%.16g');
dlmwrite([datapath,'worldTcamOrthogonal.txt'],worldTcamOrthogonal,'delimiter',' ','precision','%.16g');
dlmwrite([datapath,'endTmarker.txt'],endTmarker,'delimiter',' ','precision','%.16g');
dlmwrite([datapath,'endTmarkerOrthogonal.txt'],endTmarkerOrthogonal,'delimiter',' ','precision','%.16g');
if negate_translation_in_output_transformation
    worldTcam(1:3,4)=-worldTcam(1:3,4);
    worldTcamOrthogonal(1:3,4)=-worldTcamOrthogonal(1:3,4);
    endTmarker(1:3,4)=-endTmarker(1:3,4);
    endTmarkerOrthogonal(1:3,4)=-endTmarkerOrthogonal(1:3,4);
end

camTworld=invT(worldTcam);
camTworldOrthogonal=invT(worldTcamOrthogonal);
markerTend=invT(endTmarker);
markerTendOrthogonal=invT(endTmarkerOrthogonal);

for it=1:num_pairsUsed
    points=invT(camTmarker(:,4*it-3:4*it))*transpose([stereoPoints((it-1)*prod(boardSize-1)+1:it*prod(boardSize-1),:),ones(prod(boardSize-1),1)]);
    stereoPointsRead((it-1)*prod(boardSize-1)+1:it*prod(boardSize-1),1:3)=...
        transpose(points(1:3,:));
end

%%
numfigs=floor((num_pairsUsed-1)/12);
for figit2=figit1+1:numfigs+figit1+1
    figure(figit2);
    [ha,~]=tight_subplot(4,3,0.01,0.01);
    if figit2==(numfigs+figit1+1)
        endit=mod((num_pairsUsed-1),12)+1;
    else
        endit=12;
    end
    for it = 1:endit
        I = [imread(imageFileNames1{(figit2-figit1-1)*12+it}) imread(imageFileNames2{(figit2-figit1-1)*12+it})];
        axes(ha(it));%subplot(4, 3, it);
        imshow(I); hold on;
        plot(imagePoints(:,1,(figit2-figit1-1)*12+it,1), imagePoints(:,2,(figit2-figit1-1)*12+it,1), 'r.'...
            ,imagePoints(1,1,(figit2-figit1-1)*12+it,1), imagePoints(1,2,(figit2-figit1-1)*12+it,1), 'yo'...
            ,imagePoints(end,1,(figit2-figit1-1)*12+it,1),imagePoints(end,2,(figit2-figit1-1)*12+it,1), 'go'...
            ,imagePoints(:,1,(figit2-figit1-1)*12+it,2)+1024, imagePoints(:,2,(figit2-figit1-1)*12+it,2), 'r.'...
            ,imagePoints(1,1,(figit2-figit1-1)*12+it,2)+1024, imagePoints(1,2,(figit2-figit1-1)*12+it,2), 'yo'...
            ,imagePoints(end,1,(figit2-figit1-1)*12+it,2)+1024,imagePoints(end,2,(figit2-figit1-1)*12+it,2), 'go'...
            );
    end
end


%%
figit3=figit2+1;
figure(figit3);
plot3(...
    worldPoints3d(:,1),worldPoints3d(:,2),worldPoints3d(:,3),'r.',...
    'MarkerSize',20);
hold on;
plot3(...
    stereoPointsRead(:,1),stereoPointsRead(:,2),stereoPointsRead(:,3),'bx',...
    'MarkerSize',4);

for it=1:num_pairsUsed
    plot3(...
        stereoPointsRead((it-1)*prod(boardSize-1)+1,1),...
        stereoPointsRead((it-1)*prod(boardSize-1)+1,2),...
        stereoPointsRead((it-1)*prod(boardSize-1)+1,3),...
        'yo',...
        stereoPointsRead((it-1)*prod(boardSize-1)+boardSize(1)-1,1),...
        stereoPointsRead((it-1)*prod(boardSize-1)+boardSize(1)-1,2),...
        stereoPointsRead((it-1)*prod(boardSize-1)+boardSize(1)-1,3),...
        'mo',...
        stereoPointsRead((it)*prod(boardSize-1),1),...
        stereoPointsRead((it)*prod(boardSize-1),2),...
        stereoPointsRead((it)*prod(boardSize-1),3),...
        'go','MarkerSize',11);
end
hold off;

xlim([-squareSide/3,(boardSize(2)-2)*squareSide+squareSide/3]);
ylim([-squareSide/3,(boardSize(1)-2)*squareSide+squareSide/3]);
zlim([-squareSide/2 squareSide/2]);
set(gca,'xtick',[0:squareSide:squareSide*(boardSize(1)+1)]);
set(gca,'ytick',[0:squareSide:squareSide*(boardSize(2)+1)]);
rotate3d on;
set(gca,'CameraPosition',[-0.5853,-1.6570,0.8284]);
pbaspect([1,...
    ((boardSize(1)-2)*squareSide+2*squareSide/3)/((boardSize(2)-2)*squareSide+2*squareSide/3),...
    squareSide/((boardSize(2)-2)*squareSide+2*squareSide/3)]);
grid on;
grid minor;
xlabel('X_{marker} [m]');
ylabel('Y_{marker} [m]')
zlabel('Z_{marker} [m]');
title('Stereo matched marker points in marker frame');
legend('True points','Stereo points transformed'...
    ,'First corner','Second corner','Last corner','Location','SouthOutside');
print([datapath,'plots/robot-camera-calibration-stereopoints-markerframe.eps'],'-depsc');
%%
wTe=worldTend;
cTw=camTworldOrthogonal;
eTm=endTmarkerOrthogonal;

clear left right reversePoints;

%Find 3d marker points in camera frame, the reverse way
for it=1:num_pairsUsed
    point3d=transpose(cTw*wTe(:,4*it-3:4*it)*eTm*transpose([worldPoints3d,ones(prod(boardSize-1),1)]));
    reversePoints((it-1)*prod(boardSize-1)+1:(it)*prod(boardSize-1),1:3)=point3d(:,1:3);
end
%Turn 3d marker points into 2d pixels
for it=1:num_pairsUsed
    for jt=1:prod(boardSize-1)
        point3d=reversePoints((it-1)*prod(boardSize-1)+jt,:);
        point3d=point3d(:);
        point2dl=left_projection_matrix_rectified*[point3d;1];
        point2dl = point2dl(1:2) / point2dl(3);
        point2dr=right_projection_matrix_rectified*[point3d;1];
        point2dr = point2dr(1:2) / point2dr(3);
        left(jt,1:2,it)=point2dl;
        right(jt,1:2,it)=point2dr;
    end
end

numfigs=floor((num_pairsUsed-1)/12);
for figit4=figit3+1:(numfigs+figit3+1)
    figure(figit4);
    [ha,~]=tight_subplot(4,3,0.01,0.01);
    if figit4==(numfigs+figit3+1)
        endit=mod((num_pairsUsed-1),12)+1;
    else
        endit=12;
    end
    for it = 1:endit
        I = [imread(imageFileNames1{(figit4-figit3-1)*12+it}) imread(imageFileNames2{(figit4-figit3-1)*12+it})];
        axes(ha(it));%subplot(4, 3, it);
        imshow(I); hold on;
        plot(left(:,1,(figit4-figit3-1)*12+it), left(:,2,(figit4-figit3-1)*12+it), 'r.'...
            ,left(1,1,(figit4-figit3-1)*12+it), left(1,2,(figit4-figit3-1)*12+it), 'yo'...
            ,left(end,1,(figit4-figit3-1)*12+it),left(end,2,(figit4-figit3-1)*12+it), 'go'...
            ,right(:,1,(figit4-figit3-1)*12+it)+1024, right(:,2,(figit4-figit3-1)*12+it), 'r.'...
            ,right(1,1,(figit4-figit3-1)*12+it)+1024, right(1,2,(figit4-figit3-1)*12+it), 'yo'...
            ,right(end,1,(figit4-figit3-1)*12+it)+1024,right(end,2,(figit4-figit3-1)*12+it), 'go'...
            );
        hold off;
    end
end
%%
figit5=figit4+1;
figure(figit5);
plotRange=1:prod(boardSize-1)*(num_pairsUsed);
plot3(reversePoints(plotRange,1),reversePoints(plotRange,2),reversePoints(plotRange,3),'bx'...
    ,stereoPoints(plotRange,1),stereoPoints(plotRange,2),stereoPoints(plotRange,3),'r.');
%axis vis3d;
rotate3d on;
grid on;
grid minor;
xl=xlim;
yl=ylim;
zl=zlim;

pbaspect([1, (yl(2)-yl(1))/(xl(2)-xl(1)), (zl(2)-zl(1))/(xl(2)-xl(1))]);
xlabel('X_{marker} [m]');
ylabel('Y_{marker} [m]');
zlabel('Z_{marker} [m]');

%%
end