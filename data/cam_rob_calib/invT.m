function [T] = invT(A)
T=[transpose(A(1:3,1:3)),-transpose(A(1:3,1:3))*A(1:3,4);0,0,0,1];
end